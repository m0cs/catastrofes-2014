
sicaApp.controller('sicaController', ['$scope','$window','catastrofeService','planService','authService','auth','growl','leafletData','coordsFactory','desaparecidosService', function($scope,$window,catastrofeService,planService,authService,auth,growl,leafletData,coordsFactory,desaparecidosService)
{





//FUNCTIONS

//Ini del mapa 
// var grenMarker = L.AwesomeMarkers.icon({
//     icon: 'coffee',
//     markerColor: 'green'
//   });
// var map = L.map('mapcatastrofe', {
//     center: [-34.768913, -55.842145],
//     maxZoom: 16,
//     minZoom: 7,
//     zoomControl: true
// });

// $scope.defaults = {

//     // tileLayer: 'http://{s}.tile.openstreetmap.fr/hot/{z}/{x}/{y}.png',
//     // tileLayer: "http://{s}.tile.opencyclemap.org/cycle/{z}/{x}/{y}.png",     
//     maxZoom: 16,
//     minZoom: 7,
//     zoomControl: true,
//     lat:-34.768913, 
//     lon:-55.842145
// };


// var map = L.map('map');
// map.locate({setView: true, maxZoom: 16});
        // setView('map',[-34.768913, -55.842145]);
        // var map = L.map('mapcatastrofe').setView([-34.768913, -55.842145], 13);
        // map.panTo(new L.LatLng(-34.768913, -55.842145));
         // $scope.mapDefaults = {

         //        // tileLayer: 'http://{s}.tile.openstreetmap.fr/hot/{z}/{x}/{y}.png',
         //        // tileLayer: "http://{s}.tile.opencyclemap.org/cycle/{z}/{x}/{y}.png",     
         //        // maxZoom: 16,
         //        // minZoom: 7,
         //        // zoomControl: true,
         //        // focus: true,
         //        center: [-34.768913, -55.842145]
         //    };
        // $scope.mapDefaults = {

        //     // tileLayer: 'http://{s}.tile.openstreetmap.fr/hot/{z}/{x}/{y}.png',
        //     // tileLayer: "http://{s}.tile.opencyclemap.org/cycle/{z}/{x}/{y}.png",     
        //     maxZoom: 16,
        //     minZoom: 7,
        //     zoomControl: true,
        //     center: [-34.768913, -55.842145]
        // // };
        // leafletData.getMap('mapcatastrofe').then(function (map){

        //       // map.setView([-34.768913, -55.842145], 13);
        //        // L.map('map').setView([-34.768913, -55.842145], 13);
        //        // map.panTo(new L.LatLng(-34.768913, -55.842145));
        //        map.setView(new L.LatLng(-34.768913, -55.842145), 8);
        //        map.invalidateSize();
        //     });
         // angular.extend($scope, {
         //        center: {
         //            lat:-34.768913,//$scope.latitud,//,
         //            lng:-55.842145,
         //            zoom: 4

         //        }
         //    });



var ICON = L.icon({
    iconUrl: 'img/marker-icon-red.png',
    iconRetinaUrl: 'img/marker-icon-red.png',
    shadowUrl: 'img/marker-shadow.png',
    iconSize: [25, 41],
    iconAnchor: [12, 40],
    popupAnchor: [0, -40],
    shadowSize: [41, 41],
    shadowAnchor: [12, 40]
});
var ICONBLUE = L.icon({
    iconUrl: 'img/marker-icon.png',
    iconRetinaUrl: 'img/marker-icon.png',
    shadowUrl: 'img/marker-shadow.png',
    iconSize: [25, 41],
    iconAnchor: [12, 40],
    popupAnchor: [0, -40],
    shadowSize: [41, 41],
    shadowAnchor: [12, 40]
});

//Init markers
$scope.mapMarkers = new Array(); 


$scope.serverFailPlanes = function ()
{
    $scope.plnEmrR = "";
    $scope.plnEmer = "";
    $scope.plnComplete = "";
    $scope.shownoneE = true;
    $scope.shownoneR = true;
    $scope.shownoneR = true;
    $scope.shownoneC = true;
    $scope.plnemernone = "Servidor SICA caído, intentelo mas tarde.";
    $scope.plnriesgonone = "Servidor SICA caído, intentelo mas tarde.";
    $scope.plnCnone = "Servidor SICA caído, intentelo mas tarde.";
    //mostrar el boton completar planes
    $scope.btnCompletados=false;
}

//VISUALIZACION DEL MENU
$scope.initVisualizeMenu = function ()
{
    //MENU
    if(auth.isLogin())
    {   
        $scope.usrlogin = true;
        $scope.mostrarMenu = true;
        $scope.mostrarcruz = true;
        $scope.showplan = true;
        $scope.showinfo = true;
        // $scope.showperfil = true;
        $scope.showsalir = true;
        $scope.showdesaparecido = true;
    }
    else
    {   
        $scope.usrlogin = false;
        //Menu sin logear
        $scope.mostrarMenu = false;
        $scope.mostrarcruz = false;
        $scope.showplan = false;
        $scope.showinfo = false;
        // $scope.showperfil = false;
        $scope.showsalir = false;
        $scope.showdesaparecido = false;

    }
}

//complete plans 
$scope.myCompletePlans = function ()
{
    // carga en la grilla los planes que el rescatista ya completo
    planService.completePlans().then(function (planescomplete)
    {


 //TESTEO/////////////////////////////////////////////////////////////////////////////
//  var planescomplete = JSON.stringify(
// {
// "mensajes":"Solicitud ejecutada correctamente.",
// "cod":"200",
// "estado":"OK",
// "listaObjetos":
// [
//     {
//     "id":9,
//     "fecha":"07/12/2014",
//     "is_completado":false,
//     "idRescatista":2,
//     "nombreRescatista":"Fede",
//     "idEventoCatastrofe":30,
//     "nombreEventoCatastrofe":"Catastrofe Fede",
//     "planesEmergencia":
//     [
//         {"id":3,
//         "descripcion":"Rescate de persona",
//         "pasos":"Rescate",
//         "orden":3,
//         "tipoPlan":2},
//         {"id":4,
//         "descripcion":"Atención médica de persona",
//         "pasos":"Atención",
//         "orden":4,
//         "tipoPlan":2},
//         {"id":5,"descripcion":"Traslado de persona","pasos":"Traslado","orden":5,"tipoPlan":2},{"id":1,"descripcion":"Se recibe notificaciónes","pasos":"Alerta","orden":1,"tipoPlan":2},{"id":2,"descripcion":"Búsqueda de persona - resca","pasos":"Búsqueda","orden":2,"tipoPlan":2}
//     ],
//     "planesRiesgo":
//     [
//         {"id":1,
//         "descripcion":"Se recibe notificación",
//         "pasos":"Alerta","orden":1,"tipoPlan":1},
//         {"id":2,"descripcion":"Ánalisis de situación de la catástrofe",
//         "pasos":"Ánalis de situación",
//         "orden":2,
//         "tipoPlan":1},{"id":3,"descripcion":"Dar aviso a instituciones correspondientes","pasos":"Aviso","orden":3,"tipoPlan":1},{"id":4,"descripcion":"Preparación de materiales para rescate","pasos":"Preparación","orden":4,"tipoPlan":1},{"id":5,"descripcion":"Notificar reporte de notificación","pasos":"Reporte","orden":5,"tipoPlan":1}
//     ]
//     },
//     {
//     "id":99,
//     "fecha":"07/12/2014",
//     "is_completado":false,
//     "idRescatista":2,
//     "nombreRescatista":"Fede",
//     "idEventoCatastrofe":30,
//     "nombreEventoCatastrofe":"Catastrofe Fede",
//     "planesEmergencia":
//     [
//         {"id":3,
//         "descripcion":"Rescate de persona",
//         "pasos":"Rescate",
//         "orden":3,
//         "tipoPlan":2},
//         {"id":4,
//         "descripcion":"Atención médica de persona",
//         "pasos":"Atención",
//         "orden":4,
//         "tipoPlan":2},
//         {"id":5,"descripcion":"Traslado de persona","pasos":"Traslado","orden":5,"tipoPlan":2},{"id":1,"descripcion":"Se recibe notificaciónes","pasos":"Alerta","orden":1,"tipoPlan":2},{"id":2,"descripcion":"Búsqueda de persona - resca","pasos":"Búsqueda","orden":2,"tipoPlan":2}
//     ],
//     "planesRiesgo":
//     [
//         {"id":1,
//         "descripcion":"Se recibe notificación",
//         "pasos":"Alerta","orden":1,"tipoPlan":1},
//         {"id":2,"descripcion":"Ánalisis de situación de la catástrofe",
//         "pasos":"Ánalis de situación",
//         "orden":2,
//         "tipoPlan":1},{"id":3,"descripcion":"Dar aviso a instituciones correspondientes","pasos":"Aviso","orden":3,"tipoPlan":1},{"id":4,"descripcion":"Preparación de materiales para rescate","pasos":"Preparación","orden":4,"tipoPlan":1},{"id":5,"descripcion":"Notificar reporte de notificación","pasos":"Reporte","orden":5,"tipoPlan":1}
//     ]
//     },
//     {
//     "id":98,
//     "fecha":"07/12/2014",
//     "is_completado":false,
//     "idRescatista":2,
//     "nombreRescatista":"Fede",
//     "idEventoCatastrofe":30,
//     "nombreEventoCatastrofe":"Catastrofe Fede",
//     "planesEmergencia":
//     [
//         {"id":3,
//         "descripcion":"Rescate de persona",
//         "pasos":"Rescate",
//         "orden":3,
//         "tipoPlan":2},
//         {"id":4,
//         "descripcion":"Atención médica de persona",
//         "pasos":"Atención",
//         "orden":4,
//         "tipoPlan":2},
//         {"id":5,"descripcion":"Traslado de persona","pasos":"Traslado","orden":5,"tipoPlan":2},{"id":1,"descripcion":"Se recibe notificaciónes","pasos":"Alerta","orden":1,"tipoPlan":2},{"id":2,"descripcion":"Búsqueda de persona - resca","pasos":"Búsqueda","orden":2,"tipoPlan":2}
//     ],
//     "planesRiesgo":
//     [
//         {"id":1,
//         "descripcion":"Se recibe notificación",
//         "pasos":"Alerta","orden":1,"tipoPlan":1},
//         {"id":2,"descripcion":"Ánalisis de situación de la catástrofe",
//         "pasos":"Ánalis de situación",
//         "orden":2,
//         "tipoPlan":1},{"id":3,"descripcion":"Dar aviso a instituciones correspondientes","pasos":"Aviso","orden":3,"tipoPlan":1},{"id":4,"descripcion":"Preparación de materiales para rescate","pasos":"Preparación","orden":4,"tipoPlan":1},{"id":5,"descripcion":"Notificar reporte de notificación","pasos":"Reporte","orden":5,"tipoPlan":1}
//     ]
//     }

// ]
// }

// );


// window.localStorage.setItem("misplnCompletados",planescomplete);
 ///////////////////////////////////////////////////////////////////////////////////       
        var misplncomplete = window.localStorage.getItem("misplnCompletados");
        var misplanCompSesion = JSON.parse(misplncomplete);
        //si no esta vacio
        if(!jQuery.isEmptyObject(misplanCompSesion.listaObjetos))
        {
            //muestro
            $scope.plnComplete = misplanCompSesion.listaObjetos;
            // var arrayIdcat = [];
            // $.each(misplanCompSesion.listaObjetos,function (i, item){
                //guardo los id's de las castastrofes de planes completados
            // arrayIdcat.push(misplanCompSesion.listaObjetos);  
            // });
            //guardar el array de catastrofes id
            catastrofeService.saveCatPlanComplete(misplanCompSesion.listaObjetos);
            //vacio el mesaje de error y lo oculto
            $scope.shownoneC = false;
            $scope.plnCnone = "";
 

        }
        else
        {
            //vacio planes completos y muestro el mesaje de planes completos vacios
            $scope.plnComplete ="";
            $scope.shownoneC = true;
            $scope.plnCnone = "No existen planes completados por usted.";
        }

     });//FIN TRAER MIS  PLANES DEL SERVIDOR
}


//mostrar las catastrofes que se completaron en el mapa
$scope.getComCat = function ()
{
    var arrayIdCatSesion = JSON.parse(catastrofeService.getCatPlanComplete());

    if(arrayIdCatSesion === false)
    {
        growl.addWarnMessage("No existen Catátrofes completadas para mostrar.");
    }
    else
    {   
        //para cada id muestro la catastrofe en el mapa
        // $.each(arrayIdCatSesion, function (i, item) {
        $scope.mapMarkers=[];
        angular.forEach(arrayIdCatSesion, function(value, key) 
        {

            
            
            catastrofeService.getinfoCatCompl(value.idEventoCatastrofe).then(function (catastrofes)
            {
                    var misplncatcomplete = window.localStorage.getItem("serverCatastrofeCompl");
                    var misCatCompSesion = JSON.parse(misplncatcomplete);
                

                    var latCatastrofe = parseFloat(misCatCompSesion.lat);//parseFloat(arraycoords[0]);
                    var longCatastrofe = parseFloat(misCatCompSesion.lon); //parseFloat(arraycoords[1]);
                    // var radio = 20000;//parseInt();

                 

                //guardo las coords en sesion
                // coordsFactory.savecoords(misplanCompSesion.lat,misplanCompSesion.lon);

                    // $scope.nombreCatastrofe = "";

                    //OBTENGO EL MAPA
                    // leafletData.getMap('mapcatastrofe').then(function (map){
                    //     // map.panTo(new L.LatLng(latCatastrofe,longCatastrofe));
                    //    L.circle([latCatastrofe, longCatastrofe], radio).addTo(map);
                    //      map.invalidateSize();
                    // });
                    
                    $scope.mapMarkers.push(
                                    {
                                        lat:latCatastrofe,//-34.768913,//$scope.latitud,//,
                                        lng: longCatastrofe, //-55.842145, //$scope.longitud,// 
                                        message: "Castastrofe",
                                        focus: true,
                                        icon:ICON,
                                    }
                                );
                           
                    


            });//fin busqueda de catastrofes

        });//fin de  array id catastrofes
    }


}//fin action

$scope.plansControls = function ()
{
        //PLANS
    //tomo de sesion
    var pln = window.localStorage.getItem("pln");
    var plansesion = JSON.parse(pln);
    
    //PETICION EXITOSA
    if(plansesion.cod === "200")
    {
        //NO TIENE PLANES ASIGNADOS HAY MSJ ERROR PARA PLANES ASIGNADOS Y BUSCO SI TIENE COMPLETOS LOS MUESTRO SINO MENSAJE DE NO HAY PLANES COMPLETOS
        if(jQuery.isEmptyObject(plansesion.objeto))
        {   
            //muestro msj de planes asignado vacios
            $scope.plnEmrR = "";
            $scope.plnEmer = "";
            $scope.shownoneE = true;
            $scope.shownoneR = true;
            $scope.plnemernone = "No existe plan asignado";
            $scope.plnriesgonone = "No existe plan asignado";
            //mostrar el boton completar planes
            $scope.btnCompletados=false;

            
            //BUSCO SUS PLANES COMPLETOS O DEVULEVO MSJ DE PLANES COMPLETOS VACIO
            //mis planes completados
            $scope.myCompletePlans();
            

            
        }
        else
        {
            //EXISTEN PLANES ASIGNADOS Y PUEDE O NO HABER PLANES COMPLETOS
            //el objeto con los planes cargados
            //si hay planes los muestro
            $scope.plnEmrR = plansesion.objeto.planesEmergencia;
            $scope.plnEmer = plansesion.objeto.planesRiesgo;
            $scope.plnemernone = "";
            $scope.plnriesgonone = "";
            $scope.shownoneE = false;
            $scope.shownoneR = false;
            //mostrar el boton completar planes
            $scope.btnCompletados=true;


            //mis planes completados
            $scope.myCompletePlans();


            //CATASTROFE DEL PLAN ASIGNADO
            //obtengo el id de catastrofes por el plan del rescatista asignado

            var idCatas = plansesion.objeto.idEventoCatastrofe;
            catastrofeService.getinfo(idCatas).then(function (catastrofes)
            {
    // // /*TESTEO*/////////////////////////////////////////////////////////
    // var catassesion =  JSON.stringify({
    //     "id":30,
    //     "descripcion":"Esta es una catastrofe para el fede.",
    //     "estado":"ACTIVA",
    //     "magnitud":"ALTA",
    //     "nombre":"Catastrofe Fede",
    //     "ubicacion":"-32.268555446214776,-57.249755859375",
    //     "lat":"-32.268555446214776",
    //     "lon":"-57.249755859375",
    //     "fechaComienzo":"07/12/2014",
    //     "ongs":[],
    //     "planesRescatista":
    //     [
    //         {   "id":10,
    //             "fecha":"07/12/2014",
    //             "is_completado":false,
    //             "idRescatista":2,
    //             "nombreRescatista":"Fede",
    //             "idEventoCatastrofe":30,
    //             "nombreEventoCatastrofe":"Catastrofe Fede",
    //             "planesEmergencia":
    //             [
    //                 {"id":3,
    //                 "descripcion":"Rescate de persona",
    //                 "pasos":"Rescate",
    //                 "orden":3,
    //                 "tipoPlan":2
    //                 },
    //                 {"id":4,"descripcion":"AtenciÃ³n mÃ©dica de persona","pasos":"AtenciÃ³n","orden":4,"tipoPlan":2},{"id":5,"descripcion":"Traslado de persona","pasos":"Traslado","orden":5,"tipoPlan":2},{"id":1,"descripcion":"Se recibe notificaciÃ³nes","pasos":"Alerta","orden":1,"tipoPlan":2},{"id":2,"descripcion":"BÃºsqueda de persona - resca","pasos":"Busqueda","orden":2,"tipoPlan":2
    //                 }
    //             ],
    //             "planesRiesgo":
    //             [
    //                 {"id":1,"descripcion":"Se recibe notificaciÃ³n","pasos":"Alerta","orden":1,"tipoPlan":1},{"id":2,"descripcion":"Ãnalisis de situaciÃ³n de la catÃ¡strofe","pasos":"Ãnalis de situaciÃ³n","orden":2,"tipoPlan":1},{"id":3,"descripcion":"Dar aviso a instituciones correspondientes","pasos":"Aviso","orden":3,"tipoPlan":1},{"id":4,"descripcion":"PreparaciÃ³n de materiales para rescate","pasos":"PreparaciÃ³n","orden":4,"tipoPlan":1},{"id":5,"descripcion":"Notificar reporte de notificaciÃ³n","pasos":"Reporte","orden":5,"tipoPlan":1
    //                 }
    //             ]
    //         },
    //         {   "id":9,
    //             "fecha":"07/12/2014",
    //             "is_completado":true,
    //             "idRescatista":2,
    //             "nombreRescatista":"Fede",
    //             "idEventoCatastrofe":30,
    //             "nombreEventoCatastrofe":"Catastrofe Fede",
    //             "planesEmergencia":[{"id":3,"descripcion":"Rescate de persona","pasos":"Rescate","orden":3,"tipoPlan":2},{"id":4,"descripcion":"AtenciÃ³n mÃ©dica de persona","pasos":"AtenciÃ³n","orden":4,"tipoPlan":2},{"id":5,"descripcion":"Traslado de persona","pasos":"Traslado","orden":5,"tipoPlan":2},{"id":1,"descripcion":"Se recibe notificaciÃ³nes","pasos":"Alerta","orden":1,"tipoPlan":2},{"id":2,"descripcion":"BÃºsqueda de persona - resca","pasos":"BÃºsqueda","orden":2,"tipoPlan":2}],"planesRiesgo":[{"id":1,"descripcion":"Se recibe notificaciÃ³n","pasos":"Alerta","orden":1,"tipoPlan":1},{"id":2,"descripcion":"Ãnalisis de situaciÃ³n de la catÃ¡strofe","pasos":"Ãnalis de situaciÃ³n","orden":2,"tipoPlan":1},{"id":3,"descripcion":"Dar aviso a instituciones correspondientes","pasos":"Aviso","orden":3,"tipoPlan":1},{"id":4,"descripcion":"PreparaciÃ³n de materiales para rescate","pasos":"PreparaciÃ³n","orden":4,"tipoPlan":1},{"id":5,"descripcion":"Notificar reporte de notificaciÃ³n","pasos":"Reporte","orden":5,"tipoPlan":1}
    //             ]
    //         }
    //     ],
    //     "fuentes":[]
    //     });

    // window.localStorage.setItem("micatastrofe",catassesion);
    // ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


                    var catassesion = window.localStorage.getItem("micatastrofe");

                    var micatastrofe = JSON.parse(catassesion);

                //SI HAY CATASTROFES
                    var latCatastrofe = micatastrofe.lat;//parseFloat(arraycoords[0]);
                    var longCatastrofe = micatastrofe.lon; //parseFloat(arraycoords[1]);
                    var radio = 20000;//parseInt();


                    //guardo las coords en sesion
                // coordsFactory.savecoords(misplanCompSesion.lat,misplanCompSesion.lon);

                    $scope.nombreCatastrofe = micatastrofe.nombre;
                    // $scope.mapDefaults = {

                    //     // tileLayer: 'http://{s}.tile.openstreetmap.fr/hot/{z}/{x}/{y}.png',
                    //     // tileLayer: "http://{s}.tile.opencyclemap.org/cycle/{z}/{x}/{y}.png",     
                    //     maxZoom: 16,
                    //     minZoom: 7,
                    //     zoomControl: true,
                    // };

                    //oBTENGO EL MAPA
                    leafletData.getMap('mapcatastrofe').then(function (map){
                        // map.panTo(new L.LatLng(latCatastrofe,longCatastrofe));
                       L.circle([parseFloat(latCatastrofe), parseFloat(longCatastrofe)], radio).addTo(map);
                         map.invalidateSize();
                    });
                    


                    $scope.mapMarkers = { 
                        //COORDS DE LA CATASTROFE
                        m1: {
                                lat:parseFloat(latCatastrofe),//-34.768913,//$scope.latitud,//,
                                lng: parseFloat(longCatastrofe), //-55.842145, //$scope.longitud,// 
                                message: "Castastrofe",
                                focus: true,
                                icon:ICON,
                            }
                           
                    };

                    //guardo las coordenadas de la catastrofe para luego utilizarla 
                    coordsFactory.savecoords(latCatastrofe,longCatastrofe); 
            });//FIN CATASTROFE SERVICE
        }
    }
    else
    {
       //no tiene planes asignados pero puede tener o no completos 
        if(plansesion.cod === "500")
        {
            
            $scope.plnEmrR = "";
            $scope.plnEmer = "";
            $scope.shownoneE = true;
            $scope.shownoneR = true;
            $scope.plnemernone = plansesion.mensajes;
            $scope.plnriesgonone = plansesion.mensajes;
            //mostrar el boton completar planes
            $scope.btnCompletados=false;

            //mis planes completados
            $scope.myCompletePlans();

            //control para el id de catstrofe ya que no tengo el plan asignado
            //muestro las ctastrofes completas

        }
        else
        {
            //solicitud con el server FALLO, muestro mesaje de servidor caido 
            $scope.serverFailPlanes();  
        }
      
    }
}

//INICIALIZACION
if(auth.isLogin())
{
    //LOGIN
    $scope.loginview = false;
    $scope.usrlogin = true;

    //control de planes 
    $scope.plansControls();

    // //mis planes completados
    // $scope.myCompletePlans();

    //Generic data
    var email = window.localStorage.getItem("useremail");
    $scope.username = email;
    

    //show div
    $scope.infocatastrofeview = false;
    $scope.ejectplanview = true;
    $scope.infodesaparecido = false; 
}
else
{
    // NO ENTRA EN SESION
    //LOGIN
    $scope.usrlogin = false;
    $scope.loginview = true;
    //PLANS
    $scope.ejectplanview = false;
    //catastrofes
    $scope.infocatastrofeview = false;
    //desaparecidos
    $scope.infodesaparecido = false;

    //data
    $scope.username = "";
    $scope.plnEmrR = "";
    $scope.plnEmer = "";
    $scope.plnemernone = "";
    $scope.plnriesgonone = "";
} 


//Begin
//inicia la visualizacion

$scope.initVisualizeMenu();
    

//FIN INICIALIZACION

//ACTIONS
//LOGIN
$scope.login = function()
{ 

    

// var datos2 = $scope.getUserData();
//sino hya nadie en sesion 
    var email = window.localStorage.getItem("useremail");
    var datos2="";
    //SI ESTA VACIO QUE SE LOGUE

    if(email === null)
    {
        //retorna el usuario
        authService.login($scope.username, $scope.password).then(function (usuarioResp)
        { 


//TESTEO//////////////////////////////////////////////
            // var usuario = "fede@gmail.com";
            // window.localStorage.setItem("useremail",usuario);

            // var usuarioestado = [{"estado":"OK","mensajes":"Usuario No encontrado"}];
            // window.localStorage.setItem("userestado",usuarioestado);
//////////////////////////////////////////////////
            var usuario = window.localStorage.getItem("userdata");

            datos2 = angular.fromJson(usuario);
          
            if(datos2.estado !== "ERROR")
            {
                //SE LOGUEO

                //menu
                $scope.initVisualizeMenu();
                //Obtengo el email
                var email = window.localStorage.getItem("useremail");
                //visualizacion
                $scope.usrlogin=true;
                $scope.username = email;

                //PLANS
                $scope.ejectplanview = true;
                //LOGIN
                $scope.loginview = false;
                 //CATASTROFES
                $scope.infocatastrofeview = false;
                //desaparecidos
                $scope.infodesaparecido = false;

                //PLANES
                planService.getPlans().then(function (planes)
                {
/////TESTEO//////////////////////////
// var planes = JSON.stringify({
// "mensajes":"Solicitud ejecutada correctamente.",
// "cod":"200",
// "estado":"OK",
// "objeto":
// // [
//     {
//         "id":9,
//         "fecha":"07/12/2014",
//         "is_completado":false,
//         "idRescatista":2,
//         "nombreRescatista":"Fede",
//         "idEventoCatastrofe":30,
//         "nombreEventoCatastrofe":"Catastrofe Fede",
//         "planesEmergencia":
//         [
//             {"id":3,
//             "descripcion":"Rescate de persona",
//             "pasos":"Rescate",
//             "orden":3,
//             "tipoPlan":2},
//             {"id":4,
//             "descripcion":"Atención médica de persona",
//             "pasos":"Atención",
//             "orden":4,
//             "tipoPlan":2},
//             {"id":5,"descripcion":"Traslado de persona","pasos":"Traslado","orden":5,"tipoPlan":2},{"id":1,"descripcion":"Se recibe notificaciónes","pasos":"Alerta","orden":1,"tipoPlan":2},{"id":2,"descripcion":"Búsqueda de persona - resca","pasos":"Búsqueda","orden":2,"tipoPlan":2}
//         ],
//         "planesRiesgo":
//         [
//             {"id":1,
//             "descripcion":"Se recibe notificación",
//             "pasos":"Alerta","orden":1,"tipoPlan":1},
//             {"id":2,"descripcion":"Ánalisis de situación de la catástrofe",
//             "pasos":"Ánalis de situación",
//             "orden":2,
//             "tipoPlan":1},{"id":3,"descripcion":"Dar aviso a instituciones correspondientes","pasos":"Aviso","orden":3,"tipoPlan":1},{"id":4,"descripcion":"Preparación de materiales para rescate","pasos":"Preparación","orden":4,"tipoPlan":1},{"id":5,"descripcion":"Notificar reporte de notificación","pasos":"Reporte","orden":5,"tipoPlan":1}
//         ]
//     }
// // ]
// });
//                     window.localStorage.setItem("pln",planes);
// /////////////////////////////////////
                    //control de planes 
                    $scope.plansControls();
                    
                 });//FIN TRAER PLANES DEL SERVIDOR
                   
            }
            else
            {
                //NO LOGEADO
                //menu
                $scope.initVisualizeMenu();
                //visualizacion
                $scope.usrlogin=false;
                $scope.username = "";

                //PLANS
                $scope.ejectplanview = false;
                //CATASTROFES
                $scope.infocatastrofeview = false;
                //DESAPARECIDOS
                $scope.infodesaparecido = false;
                //LOGIN
                $scope.loginview = true;
                $scope.username = null;
                $scope.password = null;

                //MENSAJE DE ERROR
                growl.addErrorMessage(datos2.mensajes);
            }
            
        });//Fin login service THEN
    }//fin usuario cargado
    // else
    // {
    //     //el de sesion
    //     var usuario = window.localStorage.getItem("userdata");
    //     datos2 = angular.fromJson(usuario);
    // }
    
}//Fin login

//LOG OUT
$scope.logout = function()
{
    //inicia la visualizacion
 
    if(auth.logout())
    {
        //menu
        $scope.initVisualizeMenu();
        //views
        $scope.loginview = true;
        $scope.ejectplanview = false;
        $scope.infocatastrofeview = false;
        $scope.formNotasview = false;
        $scope.infodesaparecido = false;
        
        
        $scope.infousrview = false;
        $scope.userDataview = false;

        //clean data model
        $scope.usuarios = [];
        $scope.username = null;
        $scope.password = null;
        $scope.plnemernone = null;
        $scope.plnriesgonone = null;
        $scope.plnCnone = null;

        //clean coords
        $scope.distances = "";


        
        
    }
}

//SHOW MAP VIEW    
$scope.mostrarplans = function ()
{
    $scope.infocatastrofeview = false;
    $scope.infodesaparecido = false;
    $scope.ejectplanview = true; 
    //traer planes completados
    $scope.myCompletePlans();
}
//COMPLETAR PLANES 
$scope.completePlan = function ()
{
    //planes completados envio el id del plan que esta en sesion
    var pln = window.localStorage.getItem("pln");
    var plansesion = JSON.parse(pln);

    planService.sendCompletePlans(plansesion.objeto.id).then(function (plansOk)
    {
        //Planes send ok  growl success
        if(plansOk.estado === "OK")
        {
            var pln = window.localStorage.getItem("plnCompletados");
            var plancomplete = JSON.parse(pln);

            $scope.plnEmrR = "";
            $scope.plnEmer = "";
            $scope.shownoneR = true;
            $scope.shownoneE = true;
            $scope.plnemernone = plancomplete.mensajes;
            $scope.plnriesgonone = plancomplete.mensajes;
            //mostrar el boton completar planes
            $scope.btnCompletados=false;
            window.localStorage.removeItem("pln");
            //mis planes completados
            $scope.myCompletePlans();
            growl.addSuccessMessage('Exito planes Completados');
        }
        else
        {
            //sino 
            $scope.plnEmrR = plansesion.objeto.planesEmergencia;
            $scope.plnEmer = plansesion.objeto.planesRiesgo;
            $scope.plnemernone = "";
            $scope.plnriesgonone = "";
            $scope.shownoneE = false;
            $scope.shownoneR = false;

            //mostrar el boton completar planes
            $scope.btnCompletados=true;

            //guardo las coordenadas de las catastrofes de los planes completados para luego mostrarlas en un mapa
           // coordsFactory.saveCatComplete();


            //MENSAJE DE ERROR
            growl.addErrorMessage(plansOk.mensajes);
        }
       
    });
}

/////////////////fin plans/////////////////////////////////77777
//CATASTROFE
$scope.mostrarcatastrofe = function()
{

    $scope.infocatastrofeview = true;
    $scope.ejectplanview = false;
    $scope.infodesaparecido = false;
    //catastrofe
    //obtengo el id de catastrofes por el plan del rescatista asignado
    var pln = window.localStorage.getItem("pln");
    var plansesion = JSON.parse(pln);

    if (plansesion.objeto !== undefined) 
    {
         var idCatas = plansesion.objeto.idEventoCatastrofe;


//TESTEOOO////////////////////////////////////////////////////////////////////////
    catastrofeService.getinfo(idCatas).then(function (catastrofes)
    {
////////////////////////////////////////////////////////////////////////////////////



            var catassesion = window.localStorage.getItem("micatastrofe");
// /*TESTEO*/        var catassesion =  JSON.stringify({"id":30,"descripcion":"Esta es una catastrofe para el fede.","estado":"ACTIVA","magnitud":"ALTA","nombre":"Catastrofe Fede","ubicacion":"-32.268555446214776,-57.249755859375","lat":"-32.268555446214776","lon":"-57.249755859375","fechaComienzo":"07/12/2014","ongs":[],"planesRescatista":[{"id":10,"fecha":"07/12/2014","is_completado":false,"idRescatista":2,"nombreRescatista":"Fede","idEventoCatastrofe":30,"nombreEventoCatastrofe":"Catastrofe Fede","planesEmergencia":[{"id":3,"descripcion":"Rescate de persona","pasos":"Rescate","orden":3,"tipoPlan":2},{"id":4,"descripcion":"AtenciÃ³n mÃ©dica de persona","pasos":"AtenciÃ³n","orden":4,"tipoPlan":2},{"id":5,"descripcion":"Traslado de persona","pasos":"Traslado","orden":5,"tipoPlan":2},{"id":1,"descripcion":"Se recibe notificaciÃ³nes","pasos":"Alerta","orden":1,"tipoPlan":2},{"id":2,"descripcion":"BÃºsqueda de persona - resca","pasos":"BÃºsqueda","orden":2,"tipoPlan":2}],"planesRiesgo":[{"id":1,"descripcion":"Se recibe notificaciÃ³n","pasos":"Alerta","orden":1,"tipoPlan":1},{"id":2,"descripcion":"Ãnalisis de situaciÃ³n de la catÃ¡strofe","pasos":"Ãnalis de situaciÃ³n","orden":2,"tipoPlan":1},{"id":3,"descripcion":"Dar aviso a instituciones correspondientes","pasos":"Aviso","orden":3,"tipoPlan":1},{"id":4,"descripcion":"PreparaciÃ³n de materiales para rescate","pasos":"PreparaciÃ³n","orden":4,"tipoPlan":1},{"id":5,"descripcion":"Notificar reporte de notificaciÃ³n","pasos":"Reporte","orden":5,"tipoPlan":1}]},{"id":9,"fecha":"07/12/2014","is_completado":true,"idRescatista":2,"nombreRescatista":"Fede","idEventoCatastrofe":30,"nombreEventoCatastrofe":"Catastrofe Fede","planesEmergencia":[{"id":3,"descripcion":"Rescate de persona","pasos":"Rescate","orden":3,"tipoPlan":2},{"id":4,"descripcion":"AtenciÃ³n mÃ©dica de persona","pasos":"AtenciÃ³n","orden":4,"tipoPlan":2},{"id":5,"descripcion":"Traslado de persona","pasos":"Traslado","orden":5,"tipoPlan":2},{"id":1,"descripcion":"Se recibe notificaciÃ³nes","pasos":"Alerta","orden":1,"tipoPlan":2},{"id":2,"descripcion":"BÃºsqueda de persona - resca","pasos":"BÃºsqueda","orden":2,"tipoPlan":2}],"planesRiesgo":[{"id":1,"descripcion":"Se recibe notificaciÃ³n","pasos":"Alerta","orden":1,"tipoPlan":1},{"id":2,"descripcion":"Ãnalisis de situaciÃ³n de la catÃ¡strofe","pasos":"Ãnalis de situaciÃ³n","orden":2,"tipoPlan":1},{"id":3,"descripcion":"Dar aviso a instituciones correspondientes","pasos":"Aviso","orden":3,"tipoPlan":1},{"id":4,"descripcion":"PreparaciÃ³n de materiales para rescate","pasos":"PreparaciÃ³n","orden":4,"tipoPlan":1},{"id":5,"descripcion":"Notificar reporte de notificaciÃ³n","pasos":"Reporte","orden":5,"tipoPlan":1}]}],"fuentes":[]});
            var catastrofe = JSON.parse(catassesion);

        //SI HAY CATASTROFES
            var latCatastrofe = catastrofe.lat;//parseFloat(arraycoords[0]);
            var longCatastrofe = catastrofe.lon; //parseFloat(arraycoords[1]);
            var radio = 20000;//parseInt();


            //guardo las coords en sesion
        // coordsFactory.savecoords(catastrofe.lat,catastrofe.lon);

            $scope.nombreCatastrofe = catastrofe.nombre;
            // $scope.mapDefaults = {

            //     // tileLayer: 'http://{s}.tile.openstreetmap.fr/hot/{z}/{x}/{y}.png',
            //     // tileLayer: "http://{s}.tile.opencyclemap.org/cycle/{z}/{x}/{y}.png",     
            //     maxZoom: 16,
            //     minZoom: 7,
            //     zoomControl: true,
            // };

            //oBTENGO EL MAPA
            leafletData.getMap('mapcatastrofe').then(function (map){

               L.circle([latCatastrofe, longCatastrofe], radio).addTo(map);
               map.invalidateSize();

            });

              
            $scope.mapMarkers = { 
                //COORDS DE LA CATASTROFE
                m1: {
                        lat:parseFloat(latCatastrofe),//-34.768913,//$scope.latitud,//,
                        lng: parseFloat(longCatastrofe), //-55.842145, //$scope.longitud,// 
                        message: "Castastrofe",
                        focus: true,
                        icon:ICON,
                    }
                   
            }
            $scope.distancia = "";

            coordsFactory.savecoords(latCatastrofe,longCatastrofe); 
     });//FIN CATASTROFES POR PLAN 
      
    }


}//fin mostrar catastrofe 


$scope.setCoords = function()
{

        // CHECK conection
        // var conectado = conectionFactory.isConected();

        // if(conectado)
        // {
            window.localStorage.setItem("est", true);

            $window.navigator.geolocation.getCurrentPosition(function(position) {
                $scope.$apply(function() {
                       
                    //mi posicion
                    var lt=position.coords.latitude;
                    var lo = position.coords.longitude;


                    //guardo las coords en sesion
                    coordsFactory.saveMycoords(lt,lo);


                    // //tomo nuevamente la posicion de la catastrofe
                    // var info1 = catastrofeFactory.getinfo();
                    // // si  la info true ok , si llega en false capaz que le mando a juan los valores de un form que carga el rescatista.
                    

                    
                    // var intialcoords = info1["ubicacion"];
                    // var arraycoords = intialcoords.split(',');
                    // //cordenadas to float
                    // var latCatastrofe = parseFloat(arraycoords[0]);
                    // var longCatastrofe = parseFloat(arraycoords[1]);
                    // var radio = parseInt(info1["distancia"]);


                    //guardo las coords en sesion
                    // coordsFactory.savecoords(latCatastrofe,longCatastrofe);
                   



                    //obtengo de sesion las coords por si no hay conexion
                    var cordenadas = coordsFactory.getcoords();
                    var myPosition = coordsFactory.getMycoords();
                    

                    if(cordenadas !== false)
                    {
                        var ltAux = parseFloat(myPosition["lat"]);
                        var loAux = parseFloat(myPosition["lon"]);

                        //toma las coordenas de la ultima sesion
                        var catltAux = parseFloat(cordenadas["catlat"]);
                        var catloAux = parseFloat(cordenadas["catlon"]);

                        $scope.mapMarkers = {
                            m1: {
                                    lat:catltAux,//$scope.latitud,//,
                                    lng: catloAux, //$scope.longitud,// 
                                    message: "Catastrofe",
                                    icon:ICON,
                                },
                            m2: {
                                    lat: ltAux,
                                    lng: loAux,
                                    message: "Estoy Aqui!",
                                    focus: true,
                                    icon:ICONBLUE,
                                    draggable:'true',
                                }
                           
                        }

                        leafletData.getMap('mapcatastrofe').then(function (map){
                        // L.circle([catltAux, catloAux], radio).addTo(map);
                        //centro
                            // map.panTo(new L.LatLng(ltAux,loAux));
                            var distancia = L.latLng(ltAux,loAux).distanceTo(L.latLng(catltAux,catloAux));
                            map.invalidateSize();
                            //guardar en un scope apropiado en el index
                            $scope.distances = "Usted se encuentra a " + parseInt(distancia) + " metros de la catastrofe.";

                        });


                    }
                    else
                    {
                        if(myPosition !== false)
                        {
                            var ltAux = parseFloat(myPosition["lat"]);
                            var loAux = parseFloat(myPosition["lon"]);

                            $scope.mapMarkers = {
                            m2: {
                                    lat: ltAux,
                                    lng: loAux,
                                    message: "Estoy Aqui!",
                                    focus: true,
                                    icon:ICONBLUE,
                                    draggable:'true',
                                }
                           
                            }
                        }
                    }
                
                });
            }, function(error) {
                growl.addErrorMessage('No es posible acceder a su ubicación.');
            });
         
        // }
        // else
        // {
        //     window.localStorage.setItem("est", false);
        //     //ULTIMA POSICION GUARDADA con cenexión
        //     var cordenadas = coordsFactory.getcoords();
        //     var myPosition = coordsFactory.getMycoords();

        //     if(cordenadas !== false)
        //     {
        //         var ltAux = parseFloat(myPosition["lat"]);
        //         var loAux = parseFloat(myPosition["lon"]);
        //         var catltAux = parseFloat(cordenadas["catlat"]);
        //         var catloAux = parseFloat(cordenadas["catlon"]);
                         
        //         $scope.mapMarkers = {
        //             m1: {
        //                     lat:catltAux,//$scope.latitud,//,
        //                     lng:catloAux, //$scope.longitud,// 
        //                     message: "Catastrofe",
        //                     icon:ICON,
        //                 },
        //             m2: {
        //                     lat: ltAux,
        //                     lng: loAux,
        //                     message: "Estoy Aqui!",
        //                     focus: true,
        //                     icon:ICONBLUE,
        //                     draggable:'true',
        //                 }
                   
        //         }

        //         leafletData.getMap('mapcatastrofe').then(function (map){
        //         L.circle([catltAux, catloAux], radio).addTo(map);
        //             var distancia = L.latLng(ltAux,loAux).distanceTo(L.latLng(catltAux,catloAux));
                    
                
        //             //guardar en un scope apropiado en el index
        //             $scope.distances = "Usted se encuentra a " + parseInt(distancia) + " metros de la castrofe.";
        //         });
        //     }
        // }
}


//DESAPARECIDOS
$scope.mostrardesa = function () 
{
    $scope.infocatastrofeview = false;
    $scope.ejectplanview = false;
    $scope.infodesaparecido = true; 

    //traer desaparecidos de localstoage
    var desa = window.localStorage.getItem("desaparecidos");
    var desaLocaS = JSON.parse(desa);
      alert(desaLocaS);  
    if(desaLocaS === null)
    {
        //traer desaparecidos del servidor cargando el local storage desde el server
        desaparecidosService.getDesaparecidos().then(function (desaserve)
        {   
             var desa = window.localStorage.getItem("desaparecidos");
             var desaLocaS = JSON.parse(desa);
             alert(desaLocaS);
            $scope.todosDesaparecidos=desaLocaS;
        });  
    }
    else
    {
        //de sesion
        $scope.todosDesaparecidos=desaLocaS;
    }
              
}
















}]);//FIN CONTROLADOR
