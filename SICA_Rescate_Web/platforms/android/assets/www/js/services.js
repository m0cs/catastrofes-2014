//services
// Crear variables globalse para estos campos 
var IP = '192.168.0.110';
var PUERTO='8080';
var mainService ='SICA_Rest';
var resources ='resources';
var tipoRES='2';//rescatista


//SERVICE
//Get plans,send plans and complete plans
sicaApp.service('planService', function($q) {

    return{
        getPlans : function (){

                var email = window.localStorage.getItem("useremail");
                var deferred = $q.defer();
                    // GET FUNCIONAL
                $.ajax({
                  url : 'http://'+IP+':'+PUERTO+'/'+mainService+'/'+resources+'/planes/buscar/'+email,//'http://192.168.0.110:8080/SICA_Rest/resources/planes/buscar/'+email,
                  type : "GET",
                  dataType: "json"
                }).done(function(response) {
                    var dataJSON = JSON.stringify(response);
                    
                    //HAY PLANES ASIGNADO O MENSAJE DE ERROR 
                    window.localStorage.setItem("pln", dataJSON);
                    deferred.resolve(JSON.parse(dataJSON));
                    
                });
                // .fail(function(){
                //     alert("Servidor SICA caído, intentelo mas tarde.");
                // });
                return deferred.promise;


            // return  data = {
            //                 "id":4,"fecha":"04/12/2014","is_completado":false,"idRescatista":2,"nombreRescatista":"Fede",
            //                 "planesEmergencia":
            //                 [   {"id":3,"descripcion":"Rescate de persona","pasos":"Rescate","orden":3,"tipoPlan":2},
            //                     {"id":4,"descripcion":"Atención médica de persona","pasos":"Atención","orden":4,"tipoPlan":2},
            //                     {"id":5,"descripcion":"Traslado de persona","pasos":"Traslado","orden":5,"tipoPlan":2},
            //                     {"id":1,"descripcion":"Se recibe notificaciónes","pasos":"Alerta","orden":1,"tipoPlan":2},
            //                     {"id":2,"descripcion":"Búsqueda de persona - resca","pasos":"Búsqueda","orden":2,"tipoPlan":2}],
            //                 "planesRiesgo":
            //                 [   {"id":1,"descripcion":"Se recibe notificación","pasos":"Alerta","orden":1,"tipoPlan":1},
            //                     {"id":2,"descripcion":"Ánalisis de situación de la catástrofe","pasos":"Ánalis de situación","orden":2,"tipoPlan":1},
            //                     {"id":3,"descripcion":"Dar aviso a instituciones correspondientes","pasos":"Aviso","orden":3,"tipoPlan":1},
            //                     {"id":4,"descripcion":"Preparación de materiales para rescate","pasos":"Preparación","orden":4,"tipoPlan":1},
            //                     {"id":5,"descripcion":"Notificar reporte de notificación","pasos":"Reporte","orden":5,"tipoPlan":1}
            //                 ]
            //             };    
            
        },
        sendCompletePlans : function (idPlan)
        {   
            var deferred = $q.defer();
            $.ajax({
                  url : 'http://'+IP+':'+PUERTO+'/'+mainService+'/'+resources+'/planes/completar/'+idPlan,//'http://192.168.0.110:8080/SICA_Rest/resources/planes/buscar/'+email,
                  type : "GET",
                  dataType: "json"
                }).done(function(response) {

                    var dataJSON = JSON.stringify(response);
                    if(response.estado === "OK")
                    {
                        //SE COMPLETO
                        window.localStorage.setItem("plnCompletados", dataJSON);
                        
                    }
                    deferred.resolve(JSON.parse(dataJSON));
                    
                
                });
                // .fail(function(){
                //     alert("Servidor SICA caído, intentelo mas tarde.");
                // });
                return deferred.promise;
                
        },
        completePlans : function ()
        {   
            var email = window.localStorage.getItem("useremail");
            var deferred = $q.defer();
            $.ajax({
                  url : 'http://'+IP+':'+PUERTO+'/'+mainService+'/'+resources+'/planes/buscarCompletados/'+email,
                  type : "GET",
                  dataType: "json"
                }).done(function(response) {
                    
                    var dataJSON = JSON.stringify(response);
                    if(response.estado === "OK")
                    {
                        //SE COMPLETO
                        window.localStorage.setItem("misplnCompletados", dataJSON);
                        
                    }
                    deferred.resolve(JSON.parse(dataJSON));
                    
                
                });
                // .fail(function(){
                //     alert("Servidor SICA caído, intentelo mas tarde.");
                // });
                return deferred.promise;
                
        }
    }
});

//login
sicaApp.service('authService',function(growl, $http,$q){
  return {

    login: function(username, password) {
    
        var deferred = $q.defer();//DESCOMENTAR EN MONITOREO


        var encrypted_pwd = $.md5(password);
        var encrypted_user = $.md5(username);
         
        //PAR MONITOREO

        $.ajax({
              url : 'http://'+IP+':'+PUERTO+'/'+mainService+'/'+resources+'/users/login',
              type : "POST",
              contentType: "application/json; charset=utf-8",
              data: JSON.stringify({ email : username, password: password, tipo:tipoRES }),
            }).done(function(response) {
        
              var conect = window.localStorage.getItem("est");

                if(conect)
                {
                    window.localStorage.setItem("est", true);
                }
                else
                {
                    window.localStorage.setItem("est", false);
                }

                if(response.estado !== "ERROR")
                {
                    window.localStorage.setItem("useremail", response.email);
                }
                var datauser = JSON.stringify(response);
               
               window.localStorage.setItem("userdata", datauser);
               deferred.resolve(datauser);
            });
            // .fail(function(error){
            //        alert("Servidor SICA caído, intentelo mas tarde."); 
            //     });

            return deferred.promise;

        }

    }
});
sicaApp.service("catastrofeService", function($http,$q)
{
    return{

        getinfo : function(idCatastrofes)
        {

            var deferred = $q.defer();
            $.ajax({
                  url : 'http://'+IP+':'+PUERTO+'/'+mainService+'/'+resources+'/eventosCatastrofes/buscar/'+idCatastrofes,
                  type : "GET",
                  dataType: "json"
                }).done(function(response) {
                    
                    var dataJSON = JSON.stringify(response);

                    if(response.estado === "ACTIVA")
                    {
                        
                        window.localStorage.setItem("micatastrofe", dataJSON);
                        
                    }
                    deferred.resolve(JSON.parse(dataJSON));
                    
                
            });
                // .fail(function(){
                //     alert("Servidor SICA caído, intentelo mas tarde.");
                // });
            return deferred.promise;
        },
        getinfoCatCompl : function(idCatastrofes)
        {

            var deferred = $q.defer();
            $.ajax({
                  url : 'http://'+IP+':'+PUERTO+'/'+mainService+'/'+resources+'/eventosCatastrofes/buscar/'+idCatastrofes,
                  type : "GET",
                  dataType: "json"
                }).done(function(response) {
                    
                    var dataJSON = JSON.stringify(response);

                    
                        
                    window.localStorage.setItem("serverCatastrofeCompl", dataJSON);
                        
            
                    deferred.resolve(JSON.parse(dataJSON));
                    
                
            });
                // .fail(function(){
                //     alert("Servidor SICA caído, intentelo mas tarde.");
                // });
            return deferred.promise;
        },
        saveCatPlanComplete : function(arrayidcat)
        {
            var myplanscatis = JSON.stringify(arrayidcat);
            window.localStorage.setItem("micatastrofeplancomplete", myplanscatis);
        },
        getCatPlanComplete : function()
        {
            var arrayCatPln = window.localStorage.getItem("micatastrofeplancomplete");
            if(arrayCatPln === null)
            {
                return false;
            }
            else
            {
                return (arrayCatPln);
            }
        }
    }
});



////////////////////////////////////FABRICAS//////////////////////////////////
sicaApp.factory("auth", function($http)
{
    return{
       
        logout : function()
        {
            //al hacer logout eliminamos la cookie con $cookieStore.remove
        
            window.localStorage.removeItem("useremail");
            window.localStorage.removeItem("lat");
            window.localStorage.removeItem("catlon");
            window.localStorage.removeItem("catlat");
            window.localStorage.removeItem("lon");
            window.localStorage.removeItem("est");
            window.localStorage.removeItem("srcImg");
            window.localStorage.removeItem("sesionplans");
            window.localStorage.removeItem("notasesion");
            window.localStorage.removeItem("pln");
            window.localStorage.removeItem("plnCompletados");
            window.localStorage.removeItem("misplnCompletados");
            window.localStorage.removeItem("micatastrofe");
            window.localStorage.removeItem("completelat");
            window.localStorage.removeItem("completelon");
            window.localStorage.removeItem("micatastrofeplancomplete");
            window.localStorage.removeItem("serverCatastrofeCompl");
            window.localStorage.removeItem("desaparecidos");
            //test creo
            window.localStorage.removeItem("userestado");
            
            

            return true;
        },
        isLogin : function()
        {
            var email = window.localStorage.getItem("useremail");

            if(email == null)
            {            
                return false;
            }
            else
            {
                return true; 
            }            
        },
        in_array : function(needle, haystack)
        {
            var key = '';
            for(key in haystack)
            {
                if(haystack[key] == needle)
                {
                    return true;
                }
            }
            return false;
        }    
        
    }
});

sicaApp.factory("coordsFactory", function($http)
{ 
    return{
            savecoords :function(catlt,catlo)
            {
                window.localStorage.setItem("catlat",catlt);
                window.localStorage.setItem("catlon",catlo);
            },
            saveMycoords :function(lt,lo)
            {
                window.localStorage.setItem("lat", lt);
                window.localStorage.setItem("lon", lo);
            },
            saveCatComplete :function(lt,lo)
            {
                window.localStorage.setItem("completelat", lt);
                window.localStorage.setItem("completelon", lo);
            },
            getcoords : function()
            {
                var catlatitud = window.localStorage.getItem("catlat");
                var catlongitud = window.localStorage.getItem("catlon");
                // var rutasPrivadas = ["/","/login","/ejectplan", "/infocatastrofe"];
                if(catlongitud == null || catlongitud == null)
                {
                    return false;
                }
                else
                {
                    var datacoord ={"catlat" : catlatitud,"catlon" : catlongitud}
                    return (datacoord); 
                } 
            },
            getMycoords : function()
            {
                var latitud = window.localStorage.getItem("lat");
                var longitud = window.localStorage.getItem("lon");
                // var rutasPrivadas = ["/","/login","/ejectplan", "/infocatastrofe"];
                if(latitud == null || longitud == null)
                {
                    return false;
                }
                else
                {
                    var Mydatacoord ={"lat" : latitud,"lon" : longitud}
                    return (Mydatacoord); 
                } 
            },
            getCatComplete : function()
            {
                var latitud = window.localStorage.getItem("completelat");
                var longitud = window.localStorage.getItem("completelon");
                // var rutasPrivadas = ["/","/login","/ejectplan", "/infocatastrofe"];
                if(latitud == null || longitud == null)
                {
                    return false;
                }
                else
                {
                    var Mydatacoord ={"lat" : latitud,"lon" : longitud}
                    return (Mydatacoord); 
                } 
            }

    }
});

sicaApp.factory("conectionFactory", function($window)
{
    return{
        isConected : function()
        {
            
            var estatus = this.checkConnection();
            switch (estatus)
            {
               case "Ethernet connection":
               case "WiFi connection":
               case "Cell 2G connection": 
               case "Cell 3G connection": 
               case "Cell 4G connection":
               case "Cell generic connection":
                    
                    return true;
                    break;
              case "No network connection":
              case "Unknown connection":


                    return false;
                    break;

               default: 
                    return false;
                    break;
            }
        },
        checkConnection : function() {
            
        
            var states = {};
            var networkState = $window.navigator.network.connection.type;
            
            states[Connection.UNKNOWN]  = 'Unknown connection';
            states[Connection.ETHERNET] = 'Ethernet connection';
            states[Connection.WIFI]     = 'WiFi connection';
            states[Connection.CELL_2G]  = 'Cell 2G connection';
            states[Connection.CELL_3G]  = 'Cell 3G connection';
            states[Connection.CELL_4G]  = 'Cell 4G connection';
            states[Connection.CELL]     = 'Cell generic connection';
            states[Connection.NONE]     = 'No network connection';
            
            return (states[networkState]);

        }
    }
});

///////////////////////////unuse///////////////////////////////////////////////

sicaApp.factory("imgFactory", function($http,$q,$window)
{
 return{
        saveImg :function (src)
        {
            window.localStorage.setItem("srcImg", src);
            
        },
        getImg :function ()
        {
            var foto = window.localStorage.getItem("srcImg");
            return(foto);
        },
        takePicture: function(options) {
            var q = $q.defer();
            
            var pictureSource=$window.navigator.camera.PictureSourceType;
            var destinationType=$window.navigator.camera.DestinationType;
            $window.navigator.camera.getPicture(function(result) {

                q.resolve(result);
                }, function(err) {
                    q.reject(err);
                }, {quality : 100, 
                  destinationType : Camera.DestinationType.DATA_URL, 
                  sourceType : Camera.PictureSourceType.CAMERA, 
                  allowEdit : true,
                  encodingType: Camera.EncodingType.JPG,
                  targetWidth: 100,
                  targetHeight: 100}
            );

            return q.promise;
        }
        //POST PARA ENVIAR LA IMAGEN
    }
});



sicaApp.factory("planesFactory", function($http,$q)
{
    return{
            planes : function()
            {

                var email = window.localStorage.getItem("useremail");
                var deferred = $q.defer();
                // GET FUNCIONAL
                $.ajax({
                  url : 'http://'+IP+':'+PUERTO+'/'+mainService+'/'+resources+'/planes/buscar/'+email,//'http://192.168.0.110:8080/SICA_Rest/resources/planes/buscar/'+email,
                  type : "GET",
                  dataType: "json"
                }).done(function(response) {
           
                   var dataPlans = JSON.stringify(response);
               
                   window.localStorage.setItem("plans", dataPlans);
                   deferred.resolve(dataPlans);

                });
                // .fail(function(){
                //     alert("Servidor SICA caído, intentelo mas tarde.");
                // });
                return deferred.promise;
            }, 
            getPlans: function () {
              //return $cookieStore.get('logueado') || true;
              return  window.localStorage.getItem("plans") || false;//$cookieStore.get('plans') 
            },
            setNotas: function(notes) {
              // $cookies.plans = plans;
              
              window.localStorage.setItem("notasesion",notes); 
            },
            deletePlans: function() {
              window.localStorage.removeItem("plans");
            }    
    } 
});

sicaApp.service("desaparecidosService", function($q)
{
    return{
        getDesaparecidos : function ()
        {
            var deferred = $q.defer();
            $.ajax({
              url : 'http://'+IP+':'+PUERTO+'/'+mainService+'/'+resources+'/desaparecidos/buscarTodos',
              type : "GET",
              dataType: "json"
            }).done(function(response) {
       
               var dataDesa = JSON.stringify(response);
        
               window.localStorage.setItem("desaparecidos", dataDesa);
               deferred.resolve(dataDesa);

            });
            // .fail(function(){
            //     alert("Servidor SICA caído, intentelo mas tarde.");
            // });
            return deferred.promise;
        }

    }
});

// sicaApp.factory("prefilFactory", function($window)
// { return {
//     // getPlansCook: function () {
//     //   //return $cookieStore.get('logueado') || true;
//     //   return $cookieStore.get('plans') || false;
//     // },
//     // setPlans: function(plans) {
//     //   $cookies.plans = plans; 
//     // },
//     // sendPlans: function(plans) {
//     //   return $http.post(
//     //       param['ip']+"/registro",
//     //       { 
//     //         plans: plans        
//     //       })
//     //   .success(function(data, status, headers, config) {
//     //     //Ver como recibe el json y pasar los datos aca
        
//     //   })
//     // },
//     // deletePlans: function() {
//     //   $cookieStore.remove("plans");
//     //   // $location.hash('home');
//     //   // $cookieStore.remove("email");
//     //   // $cookieStore.remove("logo");
//     //   // $cookieStore.remove("id");
//     // }     
//       // return $http.get(
//       //       param['ip']+param['login']+mail+'/'+pass//md5.createHash(password))
//       //   ).success(function(data, status, headers, config) {
//       //     console.log(mail+"/"+pass);
//       //     console.info(data);
//       //   });

//       //Método que funciona mediante ajax!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!11
//       // $.ajax({
//       //     url : "http://192.168.1.110:8080/SICA_Rest/resources/users/login/jmg@gmail.com/jmg",
//       //     type : "GET"
//       //   }).done(function(response) {
//       //     //var data = $.parseJSON (response);
//       //     //alert(data);
//       //     alert(JSON.stringify(response));
//       //   }).fail(function() {
//       //     console.log("error");
//       //   }).always(function() {
//       //     console.log("complete");
//       //   });
//     },
// });