
  // WHEN READY
  $(function()
  {
    
    // $("html, body").animate({ scrollTop: 0 }, 0);
    
    // samall visulization
    $("#btnEnviarfoto").hide();
    // $("#infopass").find("small").hide();
    // $("#infopass").find("p").css("width","15px");
    $("#showplnRiesgo").hide();
    $("#showplnEmg").hide();
    $("#nuevoPlan").hide();
    $("#showMapa").show();
    $("#showplnComplete").hide();
    
  });

  $("#plnRiesgo").click(function (){
      $("#showplnRiesgo").toggle('slow');

      if($(this).find("i").hasClass("fa-chevron-up"))
      {
        $(this).find("i").removeClass("fa-chevron-up");
        $(this).find("i").addClass("fa-chevron-down");
      }
      else
      {
        $(this).find("i").removeClass("fa-chevron-down");
        $(this).find("i").addClass("fa-chevron-up");
      }

  });
  $("#plnEmg").click(function (){
      $("#showplnEmg").toggle('slow');

      if($(this).find("i").hasClass("fa-chevron-up"))
      {
        $(this).find("i").removeClass("fa-chevron-up");
        $(this).find("i").addClass("fa-chevron-down");
      }
      else
      {
        $(this).find("i").removeClass("fa-chevron-down");
        $(this).find("i").addClass("fa-chevron-up");
      }

  });
  $("#plnNew").click(function (){
      
      $("#nuevoPlan").toggle('slow');

      if($(this).find("i").hasClass("fa-chevron-up"))
      {
        $(this).find("i").removeClass("fa-chevron-up");
        $(this).find("i").addClass("fa-chevron-down");
      }
      else
      {
        $(this).find("i").removeClass("fa-chevron-down");
        $(this).find("i").addClass("fa-chevron-up");
      }

  });
 $("#planescompletados").click(function (){
    $("#showplnComplete").toggle('slow');

    if($(this).find("i").hasClass("fa-chevron-up"))
    {
      $(this).find("i").removeClass("fa-chevron-up");
      $(this).find("i").addClass("fa-chevron-down");
    }
    else
    {
      $(this).find("i").removeClass("fa-chevron-down");
      $(this).find("i").addClass("fa-chevron-up");
    }

  });

  
 $("#infoMapaColl").click(function (){
    $("#showMapa").toggle('slow');

    if($(this).find("i").hasClass("fa-chevron-up"))
    {
      $(this).find("i").removeClass("fa-chevron-up");
      $(this).find("i").addClass("fa-chevron-down");
    }
    else
    {
      $(this).find("i").removeClass("fa-chevron-down");
      $(this).find("i").addClass("fa-chevron-up");
    }

  });

 


  
  // $("#infopass").find("p").hover(function () {
  //   $("#infopass").find("small").toggle('slow');
  // });

  // MENU
  $(".sidebar-brand").click(function(e){

    $("#wrapper").toggleClass("toggled");


  });
  
  $("#menu-toggle").click(function(e) {
      
    e.preventDefault();
    $("#wrapper").toggleClass("toggled");
    
    
  });

  $("#menu-toggle-close").click(function(e){

    e.preventDefault();
    $("#wrapper").toggleClass("toggled");

  });