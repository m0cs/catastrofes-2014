//angular.module('app', ['ngRoute', 'appRoutes', 'controllers', 'services', 'resource']);
var sicaApp = angular.module("sicaApp", ['ngRoute', 'ngCookies','angular-growl','ngAnimate','leaflet-directive','ngTouch','ngCookies','blockUI']);//, 'services'

sicaApp.config(['growlProvider','blockUIConfig', function(growlProvider,blockUIConfig) {
	growlProvider.globalTimeToLive(10000);
	growlProvider.onlyUniqueMessages(false);
	blockUIConfig.autoBlock = true;
	blockUIConfig.delay = 0;
	blockUIConfig.template = "<div class='block-ui-overlay'></div><div class='block-ui-message-container' aria-live='assertive' aria-atomic='true'><div class='block-ui-message'><i class='fa fa-spinner fa-spin dark'></i></div></div>";
}]);
// app.run(function($window, $rootScope) {

// 	alert("en el run");
//       // $rootScope.online = navigator.onLine;
//       // $window.addEventListener("offline", function () {
//       //   $rootScope.$apply(function() {
//       //     $rootScope.online = false;
//       //   });
//       // }, false);
//       // $window.addEventListener("online", function () {
//       //   $rootScope.$apply(function() {
//       //     $rootScope.online = true;
//       //   });
//       // }, false);
//});