

//INICIALIZACION



//obentgo daots de servicios
var info1 = catastrofeFactory.getinfo();


// si  la info true ok , si llega en false capaz que le mando a juan los valores de un form que carga el rescatista.
if(info1 !== false)
{
    var intialcoords = info1["ubicacion"];
    var arraycoords = intialcoords.split(',');
    //cordenadas to float
    var latCatastrofe = parseFloat(arraycoords[0]);
    var longCatastrofe = parseFloat(arraycoords[1]);
    var radio = parseInt(info1["distancia"]);


        //guardo las coords en sesion
        coordsFactory.savecoords(latCatastrofe,longCatastrofe);


        $scope.mapDefaults = {

            // tileLayer: 'http://{s}.tile.openstreetmap.fr/hot/{z}/{x}/{y}.png',
            // tileLayer: "http://{s}.tile.opencyclemap.org/cycle/{z}/{x}/{y}.png",     
            maxZoom: 16,
            minZoom: 7,
            zoomControl: true,
        };

        leafletData.getMap('mapcatastrofe').then(function (map){
           L.circle([latCatastrofe, longCatastrofe], radio).addTo(map);

        });
        

        var ICON = L.icon({
            iconUrl: 'img/marker-icon-red.png',
            iconRetinaUrl: 'img/marker-icon-red.png',
            shadowUrl: 'img/marker-shadow.png',
            iconSize: [25, 41],
            iconAnchor: [12, 40],
            popupAnchor: [0, -40],
            shadowSize: [41, 41],
            shadowAnchor: [12, 40]
        });
        var ICONBLUE = L.icon({
            iconUrl: 'img/marker-icon.png',
            iconRetinaUrl: 'img/marker-icon.png',
            shadowUrl: 'img/marker-shadow.png',
            iconSize: [25, 41],
            iconAnchor: [12, 40],
            popupAnchor: [0, -40],
            shadowSize: [41, 41],
            shadowAnchor: [12, 40]
        });
        $scope.mapMarkers = { 
            //COORDS DE LA CATASTROFE
            m1: {
                    lat:latCatastrofe,//-34.768913,//$scope.latitud,//,
                    lng: longCatastrofe, //-55.842145, //$scope.longitud,// 
                    message: "Castastrofe",
                    focus: true,
                    icon:ICON,
                }
                  
        }

    //Img
    $scope.estadoImg = false;
}
else //sin conexión, muestra un form para que cargue el rescatista y lo envia al server
{
    growl.addErrorMessage('Intentelo mas tarde con conexion a internet.');
}
/////////////////////////////////

if(!auth.isLogin())
{
    // alert("entra vacio");
    $scope.showplan = false;
    $scope.showsalir = false;
    $scope.showinfo = false;
    $scope.mostrarFlecha = false;
    $scope.mostrarcruz = false;
    $scope.showperfil = false;

    $scope.loginview = true;
    $scope.ejectplanview = false;
    $scope.infocatastrofeview = false;
    //solo sin conexion
    $scope.formNotasview = false;
    
    

    //perfil
    $scope.infousrview = false;
    $scope.userDataview = false;
      
}
else
{
    // alert("entra lleno");
    $scope.showplan = true;
    $scope.showsalir = true;
    $scope.showinfo = true;
    $scope.mostrarFlecha = true;
    $scope.mostrarcruz = true;
    $scope.ejectplanview = true;
    $scope.showperfil = true;
    


    $scope.infousrview = false;
 
    $scope.loginview = false;
    
    $scope.infocatastrofeview = false;

    $scope.userDataview = false;

    // $scope.formNotasview = false;

    //DATA
    var email = window.localStorage.getItem("useremail");
    var pass = window.localStorage.getItem("password");
    $scope.username = email;
    $scope.password = pass;

   
    //cargo los scope de Informacion de catastrofe
    var info = catastrofeFactory.getinfo();
    $scope.descripcion = info["descripcion"];
    $scope.nombre = info["nombre"];
    
    //MI PERFIL
    //     //obtengo
    // var datauser = window.localStorage.getItem("userdata");
    // var datosUsrsesion = angular.fromJson(datauser);



    //PLANES

/////////////////hardcode//////////////

//        var myplnSesion = window.localStorage.getItem("sesionplans");

// //CUANOD ARRANCA Y ESTA VACIO ES undefined
        // if(myplnSesion !== undefined)
        // {
        //    // alert("al iiniciar"+myplnSesion.id);NOOO
        //     // alert("a ver "+myplnSesion);//TIRA OBJECT
        //     // var auxu=myplnSesion;
        //     // var auxu2 = jQuery.parseJSON(myplnSesion);
        //     // alert("papa" + myplnSesion);
        //     // alert("papa" + auxu2);
        //      $.each(auxu, function(i, item) {

        //         alert("valoresss111 "+ item);
        //         // alert("valoresss11DSADA1 "+ angular.fromJson(item));
        //         // alert("valoresss "+ item.planesEmergencia);
        //         // alert("valoresss222 "+ angular.fromJson(item.planesEmergencia));
            
            
        //         // $scope.plnEmer = item.planesEmergencia;
        //         // $scope.plnEmrR = item.planesRiesgo;

        //     });


            var planesresp = planesFactory.planes(); //NO me lo toma de sesion por eso lo vuelvo a llamar
            $scope.plnEmer = planesresp.planesEmergencia;
            $scope.plnEmrR = planesresp.planesRiesgo;

           
           
        }
///////////fin hard code/////////////



//////// OJOOOOO DESCOMENTAR EN PRODUCION///////////////////////////////////////////7
    // planesFactory.planes().then(function (resp){ 
            
    //         //con resp anda macho

           

    //          // if(datosPlans.id === undefined)
    //          // {
    //          //        window.localStorage.setItem("est", false);
    //          //        // var plnSesion = window.localStorage.getItem("plans");
    //                 // alert(resp);

                    
    //                 $scope.showplan = true;
    //                 $scope.showsalir = true;
    //                 $scope.showinfo = true;
    //                 $scope.mostrarFlecha = true;
    //                 $scope.mostrarcruz = true;
    //                 $scope.ejectplanview = true;
    //                 $scope.showperfil = true;
                    


    //                 $scope.infousrview = false;
                 
    //                 $scope.loginview = false;
                    
    //                 $scope.infocatastrofeview = false;

    //                 $scope.userDataview = false;

    //                 $scope.formNotasview = false;

                    
    //                 $scope.plnEmer = resp.planesEmergencia;
    //                 $scope.plnEmrR = resp.planesRiesgo;




    //                 // $scope.usuarios = plnSesion;//CAMBIAR ESTO A LO APROPIADO 
    //          // }
    //          // else
    //          // {
    //          //    //hay sesion
    //          //    window.localStorage.setItem("sesionplans", datosPlans);
    //          //    window.localStorage.setItem("est", true);
    //          // }

    // });



}//FIN DEL ELSE

    

    //ACTIONS
    $scope.login = function()
    { 

        var conectado = conectionFactory.isConected();

        if(conectado)
        {
            window.localStorage.setItem("est", true);
        }
        else
        {
            window.localStorage.setItem("est", false);

            growl.addErrorMessage('Intentelo mas tarde con conexion a internet.');
        }

///////////////////////////////SOLUCION HARDCODE//////////////////////////////////////


        var estamoslog = authService.login($scope.username, $scope.password);

         if(estamoslog)
         {
            var email = window.localStorage.getItem("useremail");

            $scope.username = email;
            //traigo planes
            var planesresp = planesFactory.planes(); 

        
            
            // alert("traigo planesEmergencia  : "+planesresp.planesEmergencia.id);//no funciono

            //visualizacion
                $scope.showplan = true;
                $scope.showsalir = true;
                $scope.showinfo = true;
                $scope.mostrarFlecha = true;
                $scope.mostrarcruz = true;
                
         
                $scope.loginview = false;
                $scope.ejectplanview = true;
                $scope.infocatastrofeview = false;
                

                $scope.showperfil = true;
                
                $scope.formNotasview = false;
                $scope.infousrview = false;
                $scope.userDataview = false;



            $scope.plnEmer = planesresp.planesEmergencia;
            $scope.plnEmrR = planesresp.planesRiesgo;
             window.localStorage.setItem("sesionplans",planesresp);
            FUNCIONA Y ME TREA LOS PASOS
            PARA PLANES DE EMERGENCIA 
            $.each(planesresp.planesEmergencia, function(i, item) {

                alert("valoresss "+ item.descripcion)
                alert("y esto :"+item);
                $scope.plnEmer = item;

            });
             //PARA PLANES DE RIESGO 
            $.each(planesresp.planesRiesgo, function(i, item) {

                alert("valoresss "+ item.descripcion)
                alert("y esto :"+item);
                $scope.plnEmrR = item;

            });


         }
    ///////////////////////////////////FIN SOLUCION HARDCORE///////////////////   

   /////////PARA MONIOTREO/////////////////////////////////////////////////////////
        //user logueado 
        authService.login($scope.username, $scope.password).then(function (resp){ 
            
            // //obtengo para mi perfil
            // var datauser = window.localStorage.getItem("userdata");
            // var datosUsrsesion = angular.fromJson(datauser);
            //deserilizo
            // alert(resp);


            var datos2 = angular.fromJson(resp);//ESTE SII
            
            // alert("nuu 2"+datos2.email);OK
            // alert(resp); OK

            if(datos2.estado !== "ERROR")
            {
                //controlar en otro if si trae un fail con mensaje de error y scope de show false menos logintrue


                $scope.showplan = true;
                $scope.showsalir = true;
                $scope.showinfo = true;
                $scope.mostrarFlecha = true;
                $scope.mostrarcruz = true;
                $scope.showperfil = true;
         
                $scope.loginview = false;
                $scope.ejectplanview = true;
                $scope.infocatastrofeview = false;
                
                $scope.formNotasview = false;
                $scope.infousrview = false;
                $scope.userDataview = false;

                //DATA
                var email = window.localStorage.getItem("useremail");
                var pass = window.localStorage.getItem("password");
                $scope.username = email;
                $scope.password = pass;
                //cargo los scope de Informacion de catastrofe
                var info = catastrofeFactory.getinfo();
                $scope.descripcion = info["descripcion"];
                $scope.nombre = info["nombre"];
                
                //ES LA PRIMERA VEZ QUE ENTRA Y TENGO QUE JUGAR CON LO QUE ME DEVUELVE
                //cargo los scope de Planes 

                //EN EL PRIMER IF ELSE DE ARRIBA DEL TODO TENGO LA SOLUCION DE AJAX
                // $scope.usuarios = planesFactory.planes();//NO VAAA
                //////// OJOOOOO DESCOMENTAR EN PRODUCION////////////////////////////////////////////
                // planesFactory.planes().then(function (resp2){ 
                        
                            
                //          var datosPlans = angular.fromJson(resp2);

                //          // if(datosPlans.id === undefined)
                //          // {
                //             window.localStorage.setItem("est", false);
                //                 // var plnSesion = window.localStorage.getItem("plans");//mal y no funciona seesion
        



                            var planesresp = planesFactory.planes(); 

                            $scope.plnEmer = resp2.planesEmergencia;
                            $scope.plnEmrR = resp2.planesRiesgo;





                         // }
                         // else
                         // {
                            // hay sesion






                         //    $scope.usuarios = datosPlans;//CAMBIAR ESTO A LO APROPIADO
                         //    window.localStorage.setItem("sesionplans", datosPlans);
                         //    window.localStorage.setItem("est", true);
                         // }

                // });


              
                


            }
            else
            {
            
                $scope.showplan = false;
                $scope.showsalir = false;
                $scope.showinfo = false;
                $scope.showperfil = false;

                $scope.loginview = true;
                $scope.ejectplanview = false;
                $scope.infocatastrofeview = false;
                $scope.formNotasview = false;
                
                
                $scope.infousrview = false;
                $scope.userDataview = false;

                growl.addErrorMessage(datos2.mensajes);
            }
        });

    //////////////////////FIN PARA MONITOREO////////////////////////////////////////

    }//fin login
    
    $scope.logout = function()
    {
        
        if(auth.logout())
        {

            $scope.showplan = false;
            $scope.showsalir = false;
            $scope.showinfo = false;
            $scope.mostrarFlecha = false;
            $scope.mostrarcruz = false;
        

            $scope.loginview = true;
            $scope.ejectplanview = false;
            $scope.infocatastrofeview = false;
            $scope.formNotasview = false;
            
            
            $scope.infousrview = false;
            $scope.userDataview = false;

            //clean data
            $scope.usuarios = [];
            $scope.username = null;
            $scope.password = null;

        }
        
    }
    $scope.mostrarplans = function()
    {
        //OJOOOOO
         planesFactory.planes();

        $scope.infocatastrofeview = false;
        $scope.infousrview = false;
        $scope.userDataview = false;
        $scope.formNotasview = false;
        $scope.ejectplanview = true;

                 // CHECK conection
        var conectado = conectionFactory.isConected();

        if(!conectado)//no conection
        {
            //no hay planes para mostrar.
            window.localStorage.setItem("est", false);
            //los tomo de sesion si no esta conectado
            var plnSesion = window.localStorage.getItem("sesionplans");
            $scope.usuarios = plnSesion;
            $scope.savePln = true;
        }
        else
        {
            //hay sesion y hay  planes
            $scope.usuarios = plnaux;
            window.localStorage.setItem("sesionplans", plnaux);
            window.localStorage.setItem("est", true);
            //ver si mostrarlo o no si no tiene planes
            $scope.savePln = false;
        }







    }
    $scope.mostrarcatastrofe = function()
    {


        $scope.infocatastrofeview = true;
        $scope.ejectplanview = false;
        $scope.formNotasview = false;
        
    
        $scope.infousrview = false;
        $scope.userDataview = false;



    }
    $scope.setCoords = function()
    {

        // CHECK conection
        var conectado = conectionFactory.isConected();

        if(conectado)
        {
            window.localStorage.setItem("est", true);

            $window.navigator.geolocation.getCurrentPosition(function(position) {
                $scope.$apply(function() {
                       
                    //mi posicion
                    var lt=position.coords.latitude;
                    var lo = position.coords.longitude;

                    //guardo las coords en sesion
                    coordsFactory.saveMycoords(lt,lo);


                    //tomo nuevamente la posicion de la catastrofe
                    var info1 = catastrofeFactory.getinfo();
                    // si  la info true ok , si llega en false capaz que le mando a juan los valores de un form que carga el rescatista.
                    

                    
                    var intialcoords = info1["ubicacion"];
                    var arraycoords = intialcoords.split(',');
                    //cordenadas to float
                    var latCatastrofe = parseFloat(arraycoords[0]);
                    var longCatastrofe = parseFloat(arraycoords[1]);
                    var radio = parseInt(info1["distancia"]);


                    //guardo las coords en sesion
                    coordsFactory.savecoords(latCatastrofe,longCatastrofe);
                   



                    //obtengo de sesion las coords por si no hay conexion
                    var cordenadas = coordsFactory.getcoords();
                    var myPosition = coordsFactory.getMycoords();

                    if(cordenadas !== false)
                    {
                        var ltAux = parseFloat(myPosition["lat"]);
                        var loAux = parseFloat(myPosition["lon"]);

                        //toma las coordenas de la ultima sesion
                        var catltAux = parseFloat(cordenadas["catlat"]);
                        var catloAux = parseFloat(cordenadas["catlon"]);

                        $scope.mapMarkers = {
                            m1: {
                                    lat:catltAux,//$scope.latitud,//,
                                    lng: catloAux, //$scope.longitud,// 
                                    message: "Catastrofe",
                                    icon:ICON,
                                },
                            m2: {
                                    lat: ltAux,
                                    lng: loAux,
                                    message: "Estoy Aqui!",
                                    focus: true,
                                    icon:ICONBLUE,
                                    draggable:'true',
                                }
                           
                        }

                        leafletData.getMap('mapcatastrofe').then(function (map){
                        L.circle([catltAux, catloAux], radio).addTo(map);
                            var distancia = L.latLng(ltAux,loAux).distanceTo(L.latLng(catltAux,catloAux));
                            
                        
                            //guardar en un scope apropiado en el index
                            $scope.distances = "Usted se encuentra a " + parseInt(distancia) + " metros de la catastrofe.";
                        });


                    }
                });
            }, function(error) {
                growl.addErrorMessage('No es posible acceder a su ubicación.');
            });
         
        }
        else
        {
            window.localStorage.setItem("est", false);
            //ULTIMA POSICION GUARDADA con cenexión
            var cordenadas = coordsFactory.getcoords();
            var myPosition = coordsFactory.getMycoords();

            if(cordenadas !== false)
            {
                var ltAux = parseFloat(myPosition["lat"]);
                var loAux = parseFloat(myPosition["lon"]);
                var catltAux = parseFloat(cordenadas["catlat"]);
                var catloAux = parseFloat(cordenadas["catlon"]);
                         
                $scope.mapMarkers = {
                    m1: {
                            lat:catltAux,//$scope.latitud,//,
                            lng:catloAux, //$scope.longitud,// 
                            message: "Catastrofe",
                            icon:ICON,
                        },
                    m2: {
                            lat: ltAux,
                            lng: loAux,
                            message: "Estoy Aqui!",
                            focus: true,
                            icon:ICONBLUE,
                            draggable:'true',
                        }
                   
                }

                leafletData.getMap('mapcatastrofe').then(function (map){
                L.circle([catltAux, catloAux], radio).addTo(map);
                    var distancia = L.latLng(ltAux,loAux).distanceTo(L.latLng(catltAux,catloAux));
                    
                
                    //guardar en un scope apropiado en el index
                    $scope.distances = "Usted se encuentra a " + parseInt(distancia) + " metros de la castrofe.";
                });
            }
        }
    }

    $scope.capturePhoto = function() {
    // Take picture using device camera and retrieve image as base64-encoded string

        //foto
        $scope.estadoImg = false;
        $scope.btnenvfoto = true;
        imgFactory.takePicture().then(function(imageData){
            console.log(imageData);
             // Do any magic you need
            var smallImage = document.getElementById('smallImage');

            // Unhide image elements
            //
            smallImage.style.display = 'block';

            // Show the captured photo
            // The inline CSS rules are used to resize the image
            //
            smallImage.src = "data:image/jpeg;base64," + imageData;
            $("#btnEnviarfoto").show();

            }, function(err) {
              console.err(err);
              growl.addErrorMessage('Error al sacar la foto, intentelo nuevamente.');
        });
          

    }

    $scope.enviaralServer = function()
    {
     //foto al server 
    
  
        var foto = $("#smallImage").attr("src");
        
            // CHECK conection
        var conectado = conectionFactory.isConected();

        if(conectado)
        {
            //guardo la img en sesion
            imgFactory.saveImg(foto); 

            var foto = imgFactory.getImg();
            $scope.estadoImg = true;
            $scope.btnenvfoto = false;
            //envio al server con ajax
            //llamado ajax asi mando al server la img JUAN
                // $.ajax({
                //       url : "http://192.168.1.110:8080/SICA_Rest/resources/users/login/jmg@gmail.com/jmg",
                //       type : "POST",
                //       data:{
                //         img:foto
                //       }

                //     }).done(function(response) {
                //       //var data = $.parseJSON (response);
                //       //alert(data);
                //       alert(JSON.stringify(response));

                    //contemplo estod de la imagen enviada sacttfactoriamente
            growl.addSuccessMessage('Exito al enviar la Imagen.');
                    //desplegar en la vista

                      
                //     }).fail(function() {
                //       console.log("error");
                      
                //     }).always(function() {
                //       console.log("complete");
                //     });

            //en el succes del envio limpio el attr("src") para aque elimine la imagen

        }
        else
        {

            //traigo la imagen de sesion

            growl.addErrorMessage('Intentelo mas tarde con conexion a internet.');

            // //cuando vuelva la conexion enviar al server
            // $rootScope.$broadcast('online');

            // $scope.$on('online', function(event, args) {

            //     var fotosesion = getImg();

            //     alert("opaa: "+fotosesion);
            // });

            


            //envio al server con ajax
            //llamado ajax asi mando al server la img JUAN
                // $.ajax({
                //       url : "http://192.168.1.110:8080/SICA_Rest/resources/users/login/jmg@gmail.com/jmg",
                //       type : "POST",
                //       data:{
                //         img:fotosesion
                //       }

                //     }).done(function(response) {
                //       //var data = $.parseJSON (response);
                //       //alert(data);
                //       alert(JSON.stringify(response));
                      
                //     }).fail(function() {
                //       console.log("error");
                      
                //     }).always(function() {
                //       console.log("complete");
                //     });

            //en el succes del envio limpio el attr("src") para aque elimine la imagen


        }

    }
    var i=0;
    //agrega mas planes al arreglo de planes del rescatista
    $scope.myNotes = {
        nota: []
    }
    $scope.def = {};

    $scope.addNotes = function() {
        
        //FALLA UNA COSITA FIJARSE EN EL BLOCK AMARRILLO

        $scope.myNotes.nota.push($scope.nota);
        $scope.nota = angular.copy($scope.def);
    }

    $scope.removeItemMyNota = function removeItem(row) {
        index = $scope.myNotes.nota.indexOf(row);
        if (index !== -1) {
           $scope.myNotes.nota.splice(index, 1);
        }
    }

     $scope.saveNotas = function ()
     {
        //guardar planes para luego ser envidos
        planesFactory.setNotas(angular.toJson($scope.def));
        growl.addSuccessMessage('Exito al guardar sus Notas.');
     }


     $scope.mostrarperfil  = function()
     {  

        var noteSesionjson = window.localStorage.getItem("notasesion");
        var notesesion = angular.fromJson(noteSesionjson);
        if(notesesion !== null)
        {
            $scope.myNotes.push(notesesion);
        }

        $scope.showplan = true;
        $scope.showsalir = true;
        $scope.showinfo = true;

        $scope.infocatastrofeview = false;
        $scope.ejectplanview = false;
        
        $scope.infousrview = true;
        $scope.userDataview = true;
        $scope.formNotasview = true;

        // CHECK conection
        var conectado = conectionFactory.isConected();

        if(!conectado)//no conection
        {
            //no hay planes para mostrar.
            window.localStorage.setItem("est", false);
            //los tomo de sesion si no esta conectado
            $scope.savePln = true;
        }
        else
        {
            window.localStorage.setItem("est", true);
            $scope.savePln = false;
        }
     }

$scope.completePlan = function()
{
    growl.addSuccessMessage('Exito los planes se han completado.');
}


    // magic
    $scope.prevSlide = function () {
        if(!auth.isLogin)
        {
            return false;
        }
        else
        {
            return true;
        }
    };
});