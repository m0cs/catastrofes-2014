
sicaApp.controller('sicaController', ['$scope','$window','catastrofeService','planService','authService','auth','growl','leafletData','coordsFactory','desaparecidosService','conectionFactory', function($scope,$window,catastrofeService,planService,authService,auth,growl,leafletData,coordsFactory,desaparecidosService,conectionFactory)
{





//FUNCTIONS

//Ini del mapa 
angular.extend($scope, {
    defaults: {
       zoomControl: true,
       scrollWheelZoom: true,
       maxZoom: 16,
       minZoom: 4,
    },
    uruguay: {
        lat: -32.414676,
        lng: -56.328054,
        zoom:6,
    },
});

var ICON = L.icon({
    iconUrl: 'img/marker-icon-red.png',
    iconRetinaUrl: 'img/marker-icon-red.png',
    shadowUrl: 'img/marker-shadow.png',
    iconSize: [25, 41],
    iconAnchor: [12, 40],
    popupAnchor: [0, -40],
    shadowSize: [41, 41],
    shadowAnchor: [12, 40]
});
var ICONBLUE = L.icon({
    iconUrl: 'img/marker-icon.png',
    iconRetinaUrl: 'img/marker-icon.png',
    shadowUrl: 'img/marker-shadow.png',
    iconSize: [25, 41],
    iconAnchor: [12, 40],
    popupAnchor: [0, -40],
    shadowSize: [41, 41],
    shadowAnchor: [12, 40]
});
var ICONGREEN = L.icon({
    iconUrl: 'img/marker-green-icon2.png',
    iconRetinaUrl: 'img/marker-green-icon2.png',
    shadowUrl: 'img/marker-shadow.png',
    iconSize: [25, 41],
    iconAnchor: [12, 40],
    popupAnchor: [0, -40],
    shadowSize: [41, 41],
    shadowAnchor: [12, 40]
});



//Init markers
$scope.mapMarkers = new Array(); 


$scope.serverFailPlanes = function ()
{
    $scope.plnEmrR = "";
    $scope.plnEmer = "";
    $scope.plnComplete = "";
    $scope.shownoneE = true;
    $scope.shownoneR = true;
    $scope.shownoneR = true;
    $scope.shownoneC = true;
    $scope.plnemernone = "Servidor SICA caído, intentelo mas tarde.";
    $scope.plnriesgonone = "Servidor SICA caído, intentelo mas tarde.";
    $scope.plnCnone = "Servidor SICA caído, intentelo mas tarde.";
    //mostrar el boton completar planes
    $scope.btnCompletados=false;
}

//VISUALIZACION DEL MENU
$scope.initVisualizeMenu = function ()
{
    //MENU
    if(auth.isLogin())
    {   
        $scope.usrlogin = true;
        $scope.mostrarMenu = true;
        $scope.mostrarcruz = true;
        $scope.showplan = true;
        $scope.showinfo = true;
        // $scope.showperfil = true;
        $scope.showsalir = true;
        $scope.showdesaparecido = true;
    }
    else
    {   
        $scope.usrlogin = false;
        //Menu sin logear
        $scope.mostrarMenu = false;
        $scope.mostrarcruz = false;
        $scope.showplan = false;
        $scope.showinfo = false;
        // $scope.showperfil = false;
        $scope.showsalir = false;
        $scope.showdesaparecido = false;

    }
}

//complete plans 
$scope.myCompletePlans = function ()
{
   //CONTROL ISCONECTED
// if(conectionFactory.isConected())
// {

    // carga en la grilla los planes que el rescatista ya completo
    planService.completePlans().then(function (planescomplete)
    {
      
        var misplncomplete = window.localStorage.getItem("misplnCompletados");
        var misplanCompSesion = JSON.parse(misplncomplete);
        //si no esta vacio
        if(!jQuery.isEmptyObject(misplanCompSesion.listaObjetos))
        {
            //muestro
            $scope.plnComplete = misplanCompSesion.listaObjetos;
            // var arrayIdcat = [];
            // $.each(misplanCompSesion.listaObjetos,function (i, item){
                //guardo los id's de las castastrofes de planes completados
            // arrayIdcat.push(misplanCompSesion.listaObjetos);  
            // });

            //guardar el array de catastrofes id
            catastrofeService.saveCatPlanComplete(misplanCompSesion.listaObjetos);
            //vacio el mesaje de error y lo oculto
            $scope.shownoneC = false;
            $scope.plnCnone = "";
            $scope.mapMarkers=[];

        }
        else
        {
            //vacio planes completos y muestro el mesaje de planes completos vacios
            $scope.plnComplete ="";
            $scope.shownoneC = true;
            $scope.plnCnone = "No existen planes completados por usted.";
        }
     });//FIN TRAER MIS  PLANES DEL SERVIDOR
//CONTROL SINO CONECTED
// }
// else
// { 
//  var misplncomplete = window.localStorage.getItem("misplnCompletados");
//         var misplanCompSesion = JSON.parse(misplncomplete);
//         //si no esta vacio
//         if(!jQuery.isEmptyObject(misplanCompSesion.listaObjetos))
//         {
//             //muestro
//             $scope.plnComplete = misplanCompSesion.listaObjetos;
//             // var arrayIdcat = [];
//             // $.each(misplanCompSesion.listaObjetos,function (i, item){
//                 //guardo los id's de las castastrofes de planes completados
//             // arrayIdcat.push(misplanCompSesion.listaObjetos);  
//             // });
//             //guardar el array de catastrofes id
//             catastrofeService.saveCatPlanComplete(misplanCompSesion.listaObjetos);
//             //vacio el mesaje de error y lo oculto
//             $scope.shownoneC = false;
//             $scope.plnCnone = "";
 

//         }
//         else
//         {
//             //vacio planes completos y muestro el mesaje de planes completos vacios
//             $scope.plnComplete ="";
//             $scope.shownoneC = true;
//             $scope.plnCnone = "No existen planes completados por usted.";
//         }
// }

}//end

//NO SE MUESTRA
//mostrar las catastrofes que se completaron en el mapa
$scope.getComCat = function ()
{
    var arrayIdCatSesion = JSON.parse(catastrofeService.getCatPlanComplete());

    if(arrayIdCatSesion === false)
    {
        growl.addWarnMessage("No existen Catátrofes completadas para mostrar.");
    }
    else
    {   
        //para cada id muestro la catastrofe en el mapa
        //vacio los markers guardados
        // $scope.mapMarkers=[];
        angular.forEach(arrayIdCatSesion, function(value, key) 
        {

            //CONTROL SI ES CONECTED
            // if(conectionFactory.isConected())
            // {    
                catastrofeService.getinfoCatCompl(value.idEventoCatastrofe).then(function (catastrofes)
                {
                    var misplncatcomplete = window.localStorage.getItem("serverCatastrofeCompl");
                    var misCatCompSesion = JSON.parse(misplncatcomplete);
                    var latCatastrofe = parseFloat(misCatCompSesion.lat);
                    var longCatastrofe = parseFloat(misCatCompSesion.lon);

                    $scope.mapMarkers.push({
                                lat:parseFloat(latCatastrofe),//-34.768913,//$scope.latitud,//,
                                lng: parseFloat(longCatastrofe), //-55.842145, //$scope.longitud,// 
                                message: "Completada",
                                icon:ICONGREEN,
                            });
                });//fin busqueda de catastrofes

            //CONTROL SINO CONECTED
            // }
            // else
            // {
            //     var misplncatcomplete = window.localStorage.getItem("serverCatastrofeCompl");
            //     var misCatCompSesion = JSON.parse(misplncatcomplete);
            //     var latCatastrofe = parseFloat(misCatCompSesion.lat);//parseFloat(arraycoords[0]);
            //     var longCatastrofe = parseFloat(misCatCompSesion.lon); //parseFloat(arraycoords[1]);

            // }
        });//fin de  array id catastrofes
    }
}//fin action

$scope.plansControls = function ()
{
    //PLANS
    //tomo de sesion
    var pln = window.localStorage.getItem("pln");
    var plansesion = JSON.parse(pln);

    
    //PETICION EXITOSA
    if(plansesion.cod === "200")
    {
        //NO TIENE PLANES ASIGNADOS HAY MSJ ERROR PARA PLANES ASIGNADOS Y BUSCO SI TIENE COMPLETOS LOS MUESTRO SINO MENSAJE DE NO HAY PLANES COMPLETOS
        if(jQuery.isEmptyObject(plansesion.objeto))
        {   
            //muestro msj de planes asignado vacios
            $scope.plnEmrR = "";
            $scope.plnEmer = "";
            $scope.shownoneE = true;
            $scope.shownoneR = true;
            $scope.plnemernone = "No existe plan asignado";
            $scope.plnriesgonone = "No existe plan asignado";
            //mostrar el boton completar planes
            $scope.btnCompletados=false;
            //BUSCO SUS PLANES COMPLETOS O DEVULEVO MSJ DE PLANES COMPLETOS VACIO
            //mis planes completados
            $scope.myCompletePlans();
            
        }
        else
        {
            //EXISTEN PLANES ASIGNADOS Y PUEDE O NO HABER PLANES COMPLETOS
            //el objeto con los planes cargados
            //si hay planes los muestro
            $scope.plnEmrR = plansesion.objeto.planesEmergencia;
            $scope.plnEmer = plansesion.objeto.planesRiesgo;
            $scope.plnemernone = "";
            $scope.plnriesgonone = "";
            $scope.shownoneE = false;
            $scope.shownoneR = false;
            //mostrar el boton completar planes
            $scope.btnCompletados=true;
            //mis planes completados
            $scope.myCompletePlans();
            //CATASTROFE DEL PLAN ASIGNADO
            //obtengo el id de catastrofes por el plan del rescatista asignado
            var idCatas = plansesion.objeto.idEventoCatastrofe;
            //CONTROL SI CONECTED
            // if(conectionFactory.isConected())
            // {
                catastrofeService.getinfo(idCatas).then(function (catastrofes)
                {
                    var catassesion = window.localStorage.getItem("micatastrofe");
                    var micatastrofe = JSON.parse(catassesion);
                    //SI HAY CATASTROFES
                    var latCatastrofe = micatastrofe.lat;
                    var longCatastrofe = micatastrofe.lon;
                    $scope.nombreCatastrofe = micatastrofe.nombre;
                     //OBTENGO EL MAPA
                leafletData.getMap('map').then(function (map){
               // L.circle([parseFloat(latCatastrofe), parseFloat(longCatastrofe)], radio).addTo(map);
                map.panTo(new L.LatLng(parseFloat(latCatastrofe), parseFloat(longCatastrofe)));
                });
                    $scope.mapMarkers = { 
                        //COORDS DE LA CATASTROFE
                        m1: {
                                lat:parseFloat(latCatastrofe),//-34.768913,//$scope.latitud,//,
                                lng: parseFloat(longCatastrofe), //-55.842145, //$scope.longitud,// 
                                message: "Castastrofe",
                                icon:ICON,
                            }
                           
                    };
                    //guardo las coordenadas de la catastrofe para luego utilizarla 
                    coordsFactory.savecoords(latCatastrofe,longCatastrofe); 
                });//FIN CATASTROFE SERVICE
            //CONTROL SINO CONECTED
            // }
            // else
            // {    
            //     var catassesion = window.localStorage.getItem("micatastrofe");
            //     var micatastrofe = JSON.parse(catassesion);
            //     //SI HAY CATASTROFES
            //     var latCatastrofe = micatastrofe.lat;//parseFloat(arraycoords[0]);
            //     var longCatastrofe = micatastrofe.lon; //parseFloat(arraycoords[1]);
            //     $scope.nombreCatastrofe = micatastrofe.nombre;
            //     $scope.mapMarkers = { 
            //         //COORDS DE LA CATASTROFE
            //         m1: {
            //                 lat:parseFloat(latCatastrofe),//-34.768913,//$scope.latitud,//,
            //                 lng: parseFloat(longCatastrofe), //-55.842145, //$scope.longitud,// 
            //                 message: "Castastrofe",
            //                 focus: true,
            //                 icon:ICON,
            //             }
            //     };
            //     //guardo las coordenadas de la catastrofe para luego utilizarla 
            //     coordsFactory.savecoords(latCatastrofe,longCatastrofe); 
            // }
        }
    }
    else
    {
        //no tiene planes asignados pero puede tener o no completos 
        if(plansesion.cod === "500")
        {
            $scope.plnEmrR = "";
            $scope.plnEmer = "";
            $scope.shownoneE = true;
            $scope.shownoneR = true;
            $scope.plnemernone = plansesion.mensajes;
            $scope.plnriesgonone = plansesion.mensajes;
            //mostrar el boton completar planes
            $scope.btnCompletados=false;
            //mis planes completados
            $scope.myCompletePlans();
        }
        else
        {
            //solicitud con el server FALLO, muestro mesaje de servidor caido 
            $scope.serverFailPlanes();  
        }
      
    }
}

//INICIALIZACION
if(auth.isLogin())
{
    //LOGIN
    $scope.loginview = false;
    $scope.usrlogin = true;

    //control de planes 
    $scope.plansControls();

    // //mis planes completados
    // $scope.myCompletePlans();

    //Generic data
    var email = window.localStorage.getItem("useremail");
    $scope.username = email;
    

    //show div
    $scope.infocatastrofeview = false;
    $scope.ejectplanview = true;
    $scope.infodesaparecido = false; 
}
else
{
    // NO ENTRA EN SESION
    //LOGIN
    $scope.usrlogin = false;
    $scope.loginview = true;
    //PLANS
    $scope.ejectplanview = false;
    //catastrofes
    $scope.infocatastrofeview = false;
    //desaparecidos
    $scope.infodesaparecido = false;

    //data
    $scope.username = "";
    $scope.plnEmrR = "";
    $scope.plnEmer = "";
    $scope.plnemernone = "";
    $scope.plnriesgonone = "";
    $scope.nombreCatastrofe ="";

    $scope.mapMarkers=[];
    $scope.distances = "";
} 


//Begin
//inicia la visualizacion
$scope.initVisualizeMenu();
//FIN INICIALIZACION

//ACTIONS
//LOGIN
$scope.login = function()
{ 
    var email = window.localStorage.getItem("useremail");
    var datos2="";
    //SI ESTA VACIO QUE SE LOGUE
    if(email === null)
    {
        //retorna el usuario
        authService.login($scope.username, $scope.password).then(function (usuarioResp)
        { 
            var usuario = window.localStorage.getItem("userdata");
            datos2 = angular.fromJson(usuario);
            if(datos2.estado !== "ERROR")
            {
                //SE LOGUEO
                //menu
                $scope.initVisualizeMenu();
                //Obtengo el email
                var email = window.localStorage.getItem("useremail");
                //visualizacion
                $scope.usrlogin=true;
                $scope.username = email;

                //PLANS
                $scope.ejectplanview = true;
                //LOGIN
                $scope.loginview = false;
                 //CATASTROFES
                $scope.infocatastrofeview = false;
                //desaparecidos
                $scope.infodesaparecido = false;

                //PLANES
                planService.getPlans().then(function (planes)
                {

                    $scope.plansControls();
                    
                 });//FIN TRAER PLANES DEL SERVIDOR
                   
            }
            else
            {
                //NO LOGEADO
                //menu
                $scope.initVisualizeMenu();
                //visualizacion
                $scope.usrlogin=false;
                $scope.username = "";

                //PLANS
                $scope.ejectplanview = false;
                //CATASTROFES
                $scope.infocatastrofeview = false;
                //DESAPARECIDOS
                $scope.infodesaparecido = false;
                //LOGIN
                $scope.loginview = true;
                $scope.username = null;
                $scope.password = null;

                //MENSAJE DE ERROR
                growl.addErrorMessage(datos2.mensajes);
            }
            
        });//Fin login service THEN
    }//fin usuario cargado
}//Fin login

//LOG OUT
$scope.logout = function()
{
    //inicia la visualizacion
 
    if(auth.logout())
    {
        //menu
        $scope.initVisualizeMenu();
        //views
        $scope.loginview = true;
        $scope.ejectplanview = false;
        $scope.infocatastrofeview = false;
        $scope.formNotasview = false;
        $scope.infodesaparecido = false;
        
        
        $scope.infousrview = false;
        $scope.userDataview = false;

        //clean data model
        $scope.usuarios = [];
        $scope.username = null;
        $scope.password = null;
        $scope.plnemernone = null;
        $scope.plnriesgonone = null;
        $scope.plnCnone = null;

        //clean coords
        $scope.distances = "";
    }
}

//SHOW MAP VIEW    
$scope.mostrarplans = function ()
{
    $scope.infocatastrofeview = false;
    $scope.infodesaparecido = false;
    $scope.ejectplanview = true; 
    //traer planes completados
    $scope.myCompletePlans();
}
//COMPLETAR PLANES 
$scope.completePlan = function ()
{
    //planes completados envio el id del plan que esta en sesion
    var pln = window.localStorage.getItem("pln");
    var plansesion = JSON.parse(pln);
    //CONTROL SI CONECTED
    // if(conectionFactory.isConected())
    // {
        planService.sendCompletePlans(plansesion.objeto.id).then(function (plansOk)
        {
            //Planes send ok  growl success
            if(plansOk.estado === "OK")
            {
                var pln = window.localStorage.getItem("plnCompletados");
                var plancomplete = JSON.parse(pln);

                $scope.plnEmrR = "";
                $scope.plnEmer = "";
                $scope.shownoneR = true;
                $scope.shownoneE = true;
                $scope.plnemernone = plancomplete.mensajes;
                $scope.plnriesgonone = plancomplete.mensajes;
                //mostrar el boton completar planes
                $scope.btnCompletados=false;
                window.localStorage.removeItem("pln");
                //mis planes completados
                $scope.myCompletePlans();
                //muestro las completadas en el mapa
                // $scope.getComCat();
                $scope.mapMarkers=[];
                growl.addSuccessMessage('Exito planes Completados');

                //clear 
                $scope.nombreCatastrofe ="";
                //limpio los las coord de la catsftro y la distancia
                $scope.distances="";
                window.localStorage.removeItem("catlon");
                window.localStorage.removeItem("catlat");
            }
            else
            {
                //sino 
                $scope.plnEmrR = plansesion.objeto.planesEmergencia;
                $scope.plnEmer = plansesion.objeto.planesRiesgo;
                $scope.plnemernone = "";
                $scope.plnriesgonone = "";
                $scope.shownoneE = false;
                $scope.shownoneR = false;

                //mostrar el boton completar planes
                $scope.btnCompletados=true;
                //MENSAJE DE ERROR
                growl.addErrorMessage(plansOk.mensajes);
            }
        });//FIN PLANS SERVICE

    //CONTROL SINO CONECTED 
    // }
    // else
    // {
    //     if(plansOk.estado === "OK")
    //     {
    //         var pln = window.localStorage.getItem("plnCompletados");
    //         var plancomplete = JSON.parse(pln);

    //         $scope.plnEmrR = "";
    //         $scope.plnEmer = "";
    //         $scope.shownoneR = true;
    //         $scope.shownoneE = true;
    //         $scope.plnemernone = plancomplete.mensajes;
    //         $scope.plnriesgonone = plancomplete.mensajes;
    //         //mostrar el boton completar planes
    //         $scope.btnCompletados=false;
    //         window.localStorage.removeItem("pln");
    //         //mis planes completados
    //         $scope.myCompletePlans();
    //         growl.addSuccessMessage('Exito planes Completados');
    //     }
    //     else
    //     {
    //         //sino 
    //         $scope.plnEmrR = plansesion.objeto.planesEmergencia;
    //         $scope.plnEmer = plansesion.objeto.planesRiesgo;
    //         $scope.plnemernone = "";
    //         $scope.plnriesgonone = "";
    //         $scope.shownoneE = false;
    //         $scope.shownoneR = false;
    //         //mostrar el boton completar planes
    //         $scope.btnCompletados=true;
    //         //MENSAJE DE ERROR
    //         growl.addErrorMessage(plansOk.mensajes);
    //     }
    // }//No conected
}

/////////////////fin plans/////////////////////////////////
//CATASTROFE
$scope.mostrarcatastrofe = function()
{

    $scope.infocatastrofeview = true;
    $scope.ejectplanview = false;
    $scope.infodesaparecido = false;
    //catastrofe
    //obtengo el id de catastrofes por el plan del rescatista asignado
    var pln = window.localStorage.getItem("pln");
    var plansesion = JSON.parse(pln);

    if (plansesion.objeto !== undefined) 
    {
         var idCatas = plansesion.objeto.idEventoCatastrofe;
        //CONTROL SI ES CONECTED
        // if(conectionFactory.isConected())
        // {
            catastrofeService.getinfo(idCatas).then(function (catastrofes)
            {
                var catassesion = window.localStorage.getItem("micatastrofe");
                var catastrofe = JSON.parse(catassesion);
                //SI HAY CATASTROFES
                var latCatastrofe = catastrofe.lat;//parseFloat(arraycoords[0]);
                var longCatastrofe = catastrofe.lon; //parseFloat(arraycoords[1]);
                var radio = 20000;//parseInt();
                //guardo las coords en sesion
                //coordsFactory.savecoords(catastrofe.lat,catastrofe.lon);
                $scope.nombreCatastrofe = catastrofe.nombre;
                    //OBTENGO EL MAPA
                    leafletData.getMap('map').then(function (map){
                   // L.circle([parseFloat(latCatastrofe), parseFloat(longCatastrofe)], radio).addTo(map);
                    map.panTo(new L.LatLng(parseFloat(latCatastrofe), parseFloat(longCatastrofe)));
                    });

                $scope.mapMarkers = { 
                    //COORDS DE LA CATASTROFE
                    m1: {
                            lat:parseFloat(latCatastrofe),//-34.768913,//$scope.latitud,//,
                            lng: parseFloat(longCatastrofe), //-55.842145, //$scope.longitud,// 
                            message: "Castastrofe",
                            icon:ICON,
                        }
                }
                $scope.distancia = "";
                coordsFactory.savecoords(latCatastrofe,longCatastrofe); 
             });//FIN CATASTROFES POR PLAN 
             
        //CONTROL SINO CONECTED
        // }
        // else
        // {
        //     var catassesion = window.localStorage.getItem("micatastrofe");
        //     var catastrofe = JSON.parse(catassesion);
        //     //SI HAY CATASTROFES
        //     var latCatastrofe = catastrofe.lat;
        //     var longCatastrofe = catastrofe.lon;
        //     $scope.nombreCatastrofe = catastrofe.nombre;
        //     //oBTENGO EL MAPA
        //     leafletData.getMap('map').then(function (map){

        //        L.circle([latCatastrofe, longCatastrofe], radio).addTo(map);
                    // map.panTo(new L.LatLng(parseFloat(latCatastrofe), parseFloat(longCatastrofe)));

        //     });                      
        //     $scope.mapMarkers = { 
        //         //COORDS DE LA CATASTROFE
        //         m1: {
        //                 lat:parseFloat(latCatastrofe),//-34.768913,//$scope.latitud,//,
        //                 lng: parseFloat(longCatastrofe), //-55.842145, //$scope.longitud,// 
        //                 message: "Castastrofe",
    
        //                 icon:ICON,
        //             }
                   
        //     }
        //     $scope.distancia = "";
        //     coordsFactory.savecoords(latCatastrofe,longCatastrofe); 
        // }//end else conecection
    }
}//fin mostrar catastrofe 


$scope.setCoords = function()
{
    window.localStorage.setItem("est", true);
    $window.navigator.geolocation.getCurrentPosition(function(position) {
        $scope.$apply(function() {
               
            //mi posicion
            var lt=position.coords.latitude;
            var lo = position.coords.longitude;
            //guardo las coords en sesion
            coordsFactory.saveMycoords(lt,lo);
            //obtengo de sesion las coords por si no hay conexion
            var cordenadas = coordsFactory.getcoords();
            var myPosition = coordsFactory.getMycoords();
            

            if(cordenadas !== false)
            {
                var ltAux = parseFloat(myPosition["lat"]);
                var loAux = parseFloat(myPosition["lon"]);

                //toma las coordenas de la ultima sesion
                var catltAux = parseFloat(cordenadas["catlat"]);
                var catloAux = parseFloat(cordenadas["catlon"]);

                $scope.mapMarkers = {
                    m1: {
                            lat:catltAux,//$scope.latitud,//,
                            lng: catloAux, //$scope.longitud,// 
                            message: "Catastrofe",
                            icon:ICON,
                        },
                    m2: {
                            lat: ltAux,
                            lng: loAux,
                            message: "Estoy Aqui!",
                            focus: true,
                            icon:ICONBLUE,
                            // draggable:'true',
                        }
                   
                }

                leafletData.getMap('map').then(function (map){
                    var distancia = L.latLng(ltAux,loAux).distanceTo(L.latLng(catltAux,catloAux));
                    map.panTo(new L.LatLng(ltAux, loAux));
                    //guardar en un scope apropiado en el index
                    $scope.distances = "Usted se encuentra a " + parseInt(distancia) + " metros de la catastrofe.";
                });
            }
            else
            {
                if(myPosition !== false)
                {
                    var ltAux = parseFloat(myPosition["lat"]);
                    var loAux = parseFloat(myPosition["lon"]);
                    $scope.mapMarkers = {
                    m2: {
                            lat: ltAux,
                            lng: loAux,
                            message: "Estoy Aqui!",
                            icon:ICONBLUE,
                            // draggable:'true',
                        }
                   
                    }
                }
            }
        
        });
    }, function(error) {
        growl.addErrorMessage('No es posible acceder a su ubicación.');
    });
}


//DESAPARECIDOS
$scope.mostrardesa = function () 
{
    $scope.infocatastrofeview = false;
    $scope.ejectplanview = false;
    $scope.infodesaparecido = true; 

    //traer desaparecidos de localstoage
    var desa = window.localStorage.getItem("desaparecidos");
    var desaLocaS = JSON.parse(desa);
        
    if(desaLocaS === null)
    {
        
        //CONTROL SI CONECTED
        // if(conectionFactory.isConected())
        // {
            //traer desaparecidos del servidor cargando el local storage desde el server
            desaparecidosService.getDesaparecidos().then(function (desaserve)
            {   
                 var desa = window.localStorage.getItem("desaparecidos");
                 var desaLocaS = JSON.parse(desa);
                 //FILTRAR POR DESAPARECIDOS CON ESTADO DESAPARECIDOS recorro el JSON con each y guardo en un array aux solo los que (if(desaLocaS.estado !== undefined || desaLocaS.estado !== "ENOCONTRADO"))
                $scope.todosDesaparecidos=desaLocaS;
            }); 
        //CONTROL SINO CONECTED
        // }
        // else
        // {
        //     var desa = window.localStorage.getItem("desaparecidos");
        //     var desaLocaS = JSON.parse(desa);
        //     //FILTRAR POR DESAPARECIDOS CON ESTADO DESAPARECIDOS recorro el JSON con each y guardo en un array aux solo los que (if(desaLocaS.estado !== undefined || desaLocaS.estado !== "ENOCONTRADO"))
        //     $scope.todosDesaparecidos=desaLocaS;

        // }//end conection

    }
    else
    {
        //de sesion
        $scope.todosDesaparecidos=desaLocaS;
    }
              
}


}]);//FIN CONTROLADOR
