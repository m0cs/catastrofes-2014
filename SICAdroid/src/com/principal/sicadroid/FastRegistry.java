package com.principal.sicadroid;

import org.json.JSONException;
import org.json.JSONObject;

import com.example.prototipo.R;

import util.AlertDialogManager;
import util.Validador;
import android.R.color;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class FastRegistry extends ActionBarActivity implements OnClickListener{
	
	private EditText nick;
	private EditText mail;
	private EditText password;
	private EditText repPassword;
	private Button registro;
	private Button abortar;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_fast_registry);
		
		nick = (EditText) findViewById(R.id.txtNick);
		mail = (EditText) findViewById(R.id.txtMail);
		password = (EditText) findViewById(R.id.txtPass);
		repPassword = (EditText) findViewById(R.id.txtRepPass);
		registro = (Button) findViewById(R.id.button_registry);
		abortar = (Button) findViewById(R.id.button_abort);
		
		nick.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				boolean validado = Validador.validateNick(nick.getText().toString());
				if (!validado){
					nick.setBackgroundResource(color.holo_red_light);
				}else {nick.setBackgroundResource(color.holo_green_light);} 
			}
		});
		
		mail.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				boolean validado = Validador.validateEmail(mail.getText().toString());
				if (!validado){
					mail.setBackgroundResource(color.holo_red_light);
				}else {mail.setBackgroundResource(color.holo_green_light);} 
			}
		});
		
		password.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				boolean validado = Validador.validatePassword(password.getText().toString());
				if (!validado){
					password.setBackgroundResource(color.holo_red_light);
				}else {password.setBackgroundResource(color.holo_green_light);} 
			}
		});
		
		repPassword.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				boolean validado = Validador.validatePasswordMatchRepeat(password.getText().toString(), repPassword.getText().toString());
				if (!validado){
					repPassword.setBackgroundResource(color.holo_red_light);
				}else {repPassword.setBackgroundResource(color.holo_green_light);} 
			}
		});
		
		registro.setOnClickListener(this);
		abortar.setOnClickListener(this);
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.fast_registry, menu);
		return true;
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.button_registry:
			
			boolean nickValido = Validador.validateNick(nick.getText().toString());
			boolean mailValido = Validador.validateEmail(mail.getText().toString());
			boolean repeatPassValido  = Validador.validatePasswordMatchRepeat(password.getText().toString(), repPassword.getText().toString());
			boolean passwordValido = Validador.validatePassword(password.getText().toString());
			boolean validacionesOk = false;
	
			String errorMessage = "";
			if (nickValido){
				if (mailValido){
					if (passwordValido){
						if (repeatPassValido){
							validacionesOk = true; 
						}else {errorMessage += "\n Las contraseñas no coinciden.";}
					}else {errorMessage += "\n Password Inválido.";} 
				}else {errorMessage += "\n E-mail Inválido.";}
			}else {errorMessage += "\n Nick Invalido, entre 4 y 15 carácteres.";}
			
			if (validacionesOk){
				//Armar Json y Mandar a Juan por WS
				JSONObject object = new JSONObject();
				  try {
				    object.put("nick", nick.getText().toString());
				    object.put("mail", mail.getText().toString());
				    object.put("password", password.getText().toString());
				  } catch (JSONException e) {
				    e.printStackTrace();
				  }
				//Armar Json y Mandar a Juan por WS
				  
				  Intent i = new Intent(getApplicationContext(), MainActivity.class);
				  i.putExtra("mail",mail.getText().toString());
				  i.putExtra("password",password.getText().toString());
				  startActivity(i);
				  
			}else {Toast.makeText(getApplicationContext(),"Campos incorrectos: \n" + errorMessage ,
					Toast.LENGTH_LONG).show();}
			
			break;

		case R.id.button_abort:
			finish();
			break;
					
				default:
					break;
		}
		
	}
	
	

}
