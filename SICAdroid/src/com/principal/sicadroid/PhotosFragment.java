package com.principal.sicadroid;

import util.PhotoUtils;
import android.support.v4.app.Fragment;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import com.example.prototipo.R;

import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;


public class PhotosFragment extends Fragment {
	
	private AlertDialog _photoDialog;
    private Uri mImageUri;
    private static final int ACTIVITY_SELECT_IMAGE = 1020,
            ACTIVITY_SELECT_FROM_CAMERA = 1040, ACTIVITY_SHARE = 1030;
    private PhotoUtils photoUtils;
    private Button photoButton;
    private ImageView photoViewer;
    private boolean fromShare;
	public PhotosFragment(){}
	
	@Override
 	public void onSaveInstanceState(Bundle outState){
		super.onSaveInstanceState(outState);
	    if (mImageUri != null)
	        outState.putString("Uri", mImageUri.toString());
 	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
	    super.onActivityCreated(savedInstanceState);
	    if (savedInstanceState != null) {
		    if (savedInstanceState.containsKey("Uri")) {
		        // recupera la uri
		    	mImageUri = Uri.parse(savedInstanceState.getString("Uri"));
		    }
	    }
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
	    super.onActivityResult(requestCode, resultCode, data);
//	    getActivity();
		if (requestCode == ACTIVITY_SELECT_IMAGE && resultCode == Activity.RESULT_OK) {
	        mImageUri = data.getData();
	        getImage(mImageUri);
	    } else if (requestCode == ACTIVITY_SELECT_FROM_CAMERA
	            && resultCode == Activity.RESULT_OK) {
	        getImage(mImageUri);
	    }
	}
	
	
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
//		setRetainInstance(true);
		
        View rootView = inflater.inflate(R.layout.fragment_photos, container, false);
         
        fromShare = false;
        photoUtils = new PhotoUtils(getActivity());
        // Get intent, action and MIME type
        Intent intent = getActivity().getIntent();
        String action = intent.getAction();
        String type = intent.getType();
        if (Intent.ACTION_SEND.equals(action) && type != null) {
            if ("text/plain".equals(type)) {
                fromShare = true;
            } else if (type.startsWith("image/")) {
                fromShare = true;
                mImageUri = (Uri) intent
                        .getParcelableExtra(Intent.EXTRA_STREAM);
                getImage(mImageUri);
            }
        }
        photoButton = (Button) rootView.findViewById(R.id.photoButton);
        photoViewer = (ImageView) rootView.findViewById(R.id.photoViewer);
        getPhotoDialog();
        setPhotoButton();
        
        return rootView;
    }
	public void getImage(Uri uri) {
        Bitmap bounds = photoUtils.getImage(uri);
        if (bounds != null) {
            setImageBitmap(bounds);
        } else {
            //showErrorToast();MOstrar el mensaje que cago fuego
 
        }
    }
	private void setImageBitmap(Bitmap bitmap){
	    photoViewer.setImageBitmap(bitmap);
	}
	
	private void setPhotoButton(){
	    photoButton.setOnClickListener(new OnClickListener() {	
			@Override
			public void onClick(View v) {
				
				 if(!getPhotoDialog().isShowing() && !((SegundaActivity)getActivity()).isFinishing())
					 getPhotoDialog().show();
				
			}
	    });
	}
	
	private AlertDialog getPhotoDialog() {
        if (_photoDialog == null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle("Ubicacion Foto");
            builder.setPositiveButton("Camara", new DialogInterface.OnClickListener() {
            	@Override
                public void onClick(DialogInterface dialog, int which) {
                    Intent intent = new Intent(
                            "android.media.action.IMAGE_CAPTURE");
                    File photo = null;
                    try {
                        // place where to store camera taken picture
                        photo = PhotoUtils.createTemporaryFile("picture", ".jpg", getActivity());
                        photo.delete();
                    } catch (Exception e) {
                        Log.v(getClass().getSimpleName(),
                                "Can't create file to take picture!");
                    }
                    mImageUri = Uri.fromFile(photo);
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageUri);
                    startActivityForResult(intent, ACTIVITY_SELECT_FROM_CAMERA);
 
                }

				
            });
            builder.setNegativeButton("Galeria", new DialogInterface.OnClickListener() {

            	@Override
                public void onClick(DialogInterface dialog, int which) {
                    Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
                    galleryIntent.setType("image/*");
                    startActivityForResult(galleryIntent, ACTIVITY_SELECT_IMAGE);
                }
 
            });
            _photoDialog = builder.create();
 
        }
        return _photoDialog;
 
    }
	
}
