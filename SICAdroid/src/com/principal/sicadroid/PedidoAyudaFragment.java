package com.principal.sicadroid;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import com.example.prototipo.R;

import util.AlertDialogManager;
import util.Validador;
import adapter.NavDrawerListAdapter;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.support.v4.app.Fragment;
import android.app.ActivityManager;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

@SuppressLint("NewApi")
public class PedidoAyudaFragment extends Fragment implements LocationListener{
	
	public PedidoAyudaFragment(){}
	
	LocationManager handle;//Gestor del servicio de geoloc.
	private boolean servicioActivo;
	private Button enviarPedido;
	private Button cancel;
	private String latitud;
	private String Longitud;
	private String proveedor;
	private String provider;
	private String mensaje;
	private TextView msg;
	private RadioGroup radioGrp;
	private RadioButton radioBien;
	private RadioButton radioLeve;
	private RadioButton radioMedio;
	private RadioButton radioGrave;
	private EditText textoDesc;
	private List<String> formulario;
	
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
 
        View rootView = inflater.inflate(R.layout.fragment_pedido_ayuda, container, false);
        
        //pedido
        enviarPedido = (Button) rootView.findViewById(R.id.button_enviar_pedido);
        cancel = (Button) rootView.findViewById(R.id.buttonCancel);
        msg = (TextView) rootView.findViewById(R.id.mensaje);
        radioGrp = (RadioGroup) rootView.findViewById(R.id.RadGroup);
        radioBien = (RadioButton) rootView.findViewById(R.id.RadioButtonB);
        radioLeve = (RadioButton) rootView.findViewById(R.id.RadioButtonL);
        radioMedio = (RadioButton) rootView.findViewById(R.id.radioButtonM);
        radioGrave = (RadioButton) rootView.findViewById(R.id.RadioButtonG);
        textoDesc = (EditText) rootView.findViewById(R.id.editTextDesc);
        
        formulario = new ArrayList<String>();
        
        servicioActivo = false;
        
        enviarPedido.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View rootView) {
				Location location = iniciarServicio();
				pararServicio();
				
				
				
				List<String> formulario = estadoUsr();
//				Toast.makeText(getActivity(), testMessage,
//						Toast.LENGTH_SHORT).show();
				boolean Valido = true;
				for (String dato : formulario){
					boolean valid1 = Validador.validateNotNullField(dato);
					boolean valid2 = Validador.validateNotNullField(dato);
					if (!valid1 || !valid2 || dato == "null"){
						//Muestro Mensaje en Pantalla
						Toast.makeText(getActivity().getApplicationContext(),"Todos los campos son requeridos, complete el formulario y vuelva a intentarlo" ,
								Toast.LENGTH_LONG).show();
						 
						Valido = false;
						break;
					}
					 
				}
				if (Valido = true){
					formulario.add(String.valueOf(location.getLatitude()));
					formulario.add(String.valueOf(location.getLongitude()));
					Handler_sqlite helper = new Handler_sqlite(getActivity());
					helper.abrir();
					String[] usuario = helper.leerRegistro();
					helper.cerrar();
					formulario.add(usuario[0].toString());
					try {
						writeJson(formulario);
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		});
        //pedido
        
        cancel.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View rootView) {
				Intent i = new Intent(getActivity(), SegundaActivity.class);
				startActivity(i);
//				((SegundaActivity)getActivity()).displayView(0);

			}
		});
        
        return rootView;
    }
	
	public void writeJson(List<String> jsonMessage) throws JSONException{
		JSONObject object = new JSONObject();
		  try {
		    object.put("estadoUsuario", jsonMessage.get(0)); //Etiqueta / Valor
		    object.put("descripcion", jsonMessage.get(1));
		    object.put("latitud", jsonMessage.get(2));
		    object.put("longitud", jsonMessage.get(3));
		    object.put("mail", jsonMessage.get(4));
		  } catch (JSONException e) {
		    e.printStackTrace();
		  }
//		  System.out.println(object);
//		  Toast.makeText(getActivity(), object.getString("estadoUsuario"),
//					Toast.LENGTH_SHORT).show();
		  Toast.makeText(getActivity(), object.toString(),
					Toast.LENGTH_SHORT).show();
	}
	
	public List<String> estadoUsr(){
		formulario.clear();
		String estado = null;
		
		int selectedId = radioGrp.getCheckedRadioButtonId();
		if (radioBien.getId() == selectedId){
			estado = radioBien.getText().toString();
		} else if (radioMedio.getId() == selectedId){
					estado = radioMedio.getText().toString();
				} else if (radioLeve.getId() == selectedId){
							estado = radioLeve.getText().toString();
						} else if (radioGrave.getId() == selectedId){
									estado = radioGrave.getText().toString();
								}
		String descripcion = textoDesc.getText().toString();
		if (estado!=null){
			formulario.add(estado);
		}else {
			String vacio = "null";
			formulario.add(vacio);
		}
		formulario.add(descripcion);
		return formulario;
	}
	
	 public void pararServicio(){
		    //Se para el servicio de localización
		    servicioActivo = false;
		    //Se desactivan las notificaciones
		    handle.removeUpdates(this);
	 }
	 public Location iniciarServicio(){
	    //Se activa el servicio de localización
	    servicioActivo = true;
	 
	    //Crea el objeto que gestiona las localizaciones
	    handle = (LocationManager)getActivity().getSystemService(Context.LOCATION_SERVICE);
	 
	    Criteria c = new Criteria();
	    c.setAccuracy(Criteria.ACCURACY_FINE);
	    //obtiene el mejor proveedor en función del criterio asignado
	    //(la mejor precisión posible)
	    provider = handle.getBestProvider(c, true);
	    proveedor = provider;
	    //Se activan las notificaciones de localización con los parámetros: proveedor, tiempo mínimo de actualización, distancia mínima, Locationlistener
	    handle.requestLocationUpdates(provider, 10000, 1, this);
	    //Obtenemos la última posición conocida dada por el proveedor
	    Location loc = handle.getLastKnownLocation(provider);
	    muestraPosicionActual(loc);
	    return loc;
	 }
	 public void muestraPosicionActual(Location loc){
	    if(loc == null){//Si no se encuentra localización, se mostrará "Desconocida"
	       mensaje = "longitud Desconocida, latitud Desconocida"; 
	    }else{//Si se encuentra, se mostrará la latitud y longitud
	       latitud = (String.valueOf(loc.getLatitude()));
	       Longitud = (String.valueOf(loc.getLongitude()));
	       mensaje = "latitud: " + latitud + ", longitud: " + Longitud + " Proveedor: " + proveedor;
	       msg.setText(mensaje);
	    }
	 }
	
	public void OnClickListener(View rooView){
		
	}
	
	@Override
	public void onLocationChanged(Location location) {
		// Se ha encontrado una nueva localización
		muestraPosicionActual(location);
		
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// Cambia el estado del proveedor
		
	}

	@Override
	public void onProviderEnabled(String provider) {
		// Proveedor habilitado
		
	}

	@Override
	public void onProviderDisabled(String provider) {
		// Proveedor deshabilitado
		
	}
}
