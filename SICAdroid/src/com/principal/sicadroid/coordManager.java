package com.principal.sicadroid;


import java.util.ArrayList;
import java.util.List;

import util.LocationInterface;
import android.app.AlertDialog;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;
import android.support.v4.app.FragmentManager;
import android.util.Log;

public class coordManager extends Service implements LocationListener{

	  private final Context mContext;
	  
	  	float  bestAccuracy;
	  	long bestTime;
	  	Location bestResult;
	    
	    boolean isGPSEnabled = false;
	    boolean isNetworkEnabled = false;
	    boolean canGetLocation = false;
	 
	    Location location;
	    double latitude; 
	    double longitude;
	 
	    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 100; // 100 metros	 
	    private static final long MIN_TIME_BW_UPDATES = 1000; // 1 segundo
	 
	    
	    protected LocationManager locationManager;
	 
	    public coordManager(Context context) {
	        this.mContext = context;
	        getLocation();
	    }
	    
	    public Location getLocation() {
	    	
	    	try {
	            locationManager = (LocationManager) mContext.getSystemService(LOCATION_SERVICE);
	 
	            isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
	 
	            
//	            ConnectivityManager connectivityMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
//	            
//	            		NetworkInfo[] nwInfos = connectivityMgr.getAllNetworkInfo();
//	            		for (NetworkInfo nwInfo : nwInfos) {
//	            		  Log.d("netw", "Network Type Name: " + nwInfo.getTypeName());
//	            		  Log.d("netw", "Network available: " + nwInfo.isAvailable());
//	            		  Log.d("netw", "Network c_or-c: " + nwInfo.isConnectedOrConnecting());
//	            		  Log.d("netw", "Network connected: " + nwInfo.isConnected());
//	            		} 
	            
	            isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
	 
	            if (!isGPSEnabled && !isNetworkEnabled) {
	                // ningun proveedor habilitado
	            } else {
	                this.canGetLocation = true;
	                
	                if (isGPSEnabled) {
	                    if (location == null) {
	                        locationManager.requestLocationUpdates(
	                                LocationManager.GPS_PROVIDER,
	                                MIN_TIME_BW_UPDATES,
	                                MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
	                        Log.d("GPS Enabled", "GPS Enabled");
	                        if (locationManager != null) {
	                            location = locationManager
	                                    .getLastKnownLocation(LocationManager.GPS_PROVIDER);
	                            if (location != null) {
	                                latitude = location.getLatitude();
	                                longitude = location.getLongitude();
	                            }
	                        }
	                    }
	                }
	                if (isNetworkEnabled) {
	                    locationManager.requestLocationUpdates(
	                            LocationManager.NETWORK_PROVIDER,
	                            MIN_TIME_BW_UPDATES,
	                            MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
	                    Log.d("Network", "Network");
	                    if (locationManager != null) {
	                        location = locationManager
	                                .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
	                        if (location != null) {
	                            latitude = location.getLatitude();
	                            longitude = location.getLongitude();
	                        }
	                    }
	                }
	            }
	 
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
	 
	        return location;
	    }
	     
	    /**
	     * Stop using GPS listener
	     * Calling this function will stop using GPS in your app
	     * */
	    public void stopUsingGPS(){
	        if(locationManager != null){
	            locationManager.removeUpdates(coordManager.this);
	        }      
	    }
	     
	    /**
	     * Function to get latitude
	     * */
	    public double getLatitude(){
	        if(location != null){
	            latitude = location.getLatitude();
	        }
	         
	        // return latitude
	        return latitude;
	    }
	     
	    /**
	     * Function to get longitude
	     * */
	    public double getLongitude(){
	        if(location != null){
	            longitude = location.getLongitude();
	        }
	         
	        // return longitude
	        return longitude;
	    }
	     
	    
	    public boolean canGetLocation() {
	        return this.canGetLocation;
	    }
	     
	    /**
	     * Function to show settings alert dialog
	     * On pressing Settings button will launch Settings Options
	     * */
	    public void showSettingsAlert(){
	        AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext);
	      
	        // Setting Dialog Title
	        alertDialog.setTitle("GPS y Network");
	  
	        // Setting Dialog Message
	        alertDialog.setMessage("GPS y Network deshabilitado. quiere ir al menu de configuración?");
	  
	        // On pressing Settings button
	        alertDialog.setPositiveButton("Configuración", new DialogInterface.OnClickListener() {
	            public void onClick(DialogInterface dialog,int which) {
	                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
	                mContext.startActivity(intent);
	            }
	        });
	  
	        // on pressing cancel button
	        alertDialog.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
	            public void onClick(DialogInterface dialog, int which) {
	            dialog.cancel();
	            }
	        });
	  
	        // Showing Alert Message
	        alertDialog.show();
	    }
	 
	    @Override
	    public void onLocationChanged(Location location) {
//	    	stopUsingGPS();
//	    	LocationInterface li = new UbicacionFragment();
//	    	li.changeInLocation(3);
	    }
	 
	    @Override
	    public void onProviderDisabled(String provider) {
//	    	stopUsingGPS();
//	    	LocationInterface li = new UbicacionFragment();
//	    	li.changeInLocation();
	    	    
	    }
	    
	 
	    @Override
	    public void onProviderEnabled(String provider) {
//	    	stopUsingGPS();
//	    	LocationInterface li = new UbicacionFragment();
//	    	li.changeInLocation();

	    	    
	    }
	 
	    @Override
	    public void onStatusChanged(String provider, int status, Bundle extras) {
//	    	stopUsingGPS();
//	    	LocationInterface li = new UbicacionFragment();
//	    	li.changeInLocation();
	    	    
	    }
	 
	    @Override
	    public IBinder onBind(Intent arg0) {
	        return null;
	    }
}
