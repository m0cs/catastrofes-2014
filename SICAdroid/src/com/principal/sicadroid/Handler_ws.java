package com.principal.sicadroid;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

public class Handler_ws {

	// variable to hold context
	private Context context;

	//save the context recievied via constructor in a local variable

	public Handler_ws(Context context){
	    this.context=context;
	}

	public void callWsGeoNames(String URL){
		 new ReadWeatherJSONFeedTask().execute(URL);
	 };
	
	 public void callWsPrototipo(String URL){
		 new ReadCicaPrototypeJSONFeedTask().execute(URL);
	 };
	 
	 private String readJSONFeed(String URL) {
	        StringBuilder stringBuilder = new StringBuilder();
	        HttpClient httpClient = new DefaultHttpClient();
	        HttpGet httpGet = new HttpGet(URL);
	        try {
	            HttpResponse response = httpClient.execute(httpGet);
	            StatusLine statusLine = response.getStatusLine(); // Represents a status line as returned from a HTTP server. See RFC2616, section 6.1. Implementations are expected to be thread safe.
	            int statusCode = statusLine.getStatusCode();
	            if (statusCode == 200) {
	                HttpEntity entity = response.getEntity();
	                InputStream inputStream = entity.getContent();
	                BufferedReader reader = new BufferedReader(
	                        new InputStreamReader(inputStream));
	                String line;
	                while ((line = reader.readLine()) != null) {
	                    stringBuilder.append(line);
	                }
	                inputStream.close();
	            } else {
	                Log.d("JSON", "Falló al leer el archivo");
	            }
	        } catch (Exception e) {
	            Log.d("readJSONFeed", e.getLocalizedMessage()); //API logging de androir, .d debbug, e el error
	        }        
	        return stringBuilder.toString();
	    }
	
	 
	 private class ReadWeatherJSONFeedTask extends AsyncTask
	    <String, Void, String> {
	        protected String doInBackground(String... urls) {
	            return readJSONFeed(urls[0]);
	        }
	 
	        protected void onPostExecute(String result) {
	            try {
	                JSONObject jsonObject = new JSONObject(result);
	                JSONObject weatherObservationItems = 
	                    new JSONObject(jsonObject.getString("weatherObservation"));
	 
	                Toast.makeText(context, 
	                    weatherObservationItems.getString("clouds") + 
	                 " - " + weatherObservationItems.getString("stationName"), 
	                 Toast.LENGTH_SHORT).show();
	            } catch (Exception e) {
	                Log.d("ReadWeatherJSONFeedTask", e.getLocalizedMessage());
	            }          
	        }
	    }
	 
	 private class ReadCicaPrototypeJSONFeedTask extends AsyncTask
	    <String, Void, String> {
	        protected String doInBackground(String... urls) {
	            return readJSONFeed(urls[0]);
	        }
	 
	        protected void onPostExecute(String result) {
	            try {
	                JSONObject jsonObject = new JSONObject(result);
//	                JSONObject infoPrototypeItems = 
//	                    new JSONObject(jsonObject.getString("info"));
	 
	                Toast.makeText(context, 
	                		jsonObject.getString("email") + 
	                 " - " + jsonObject.getString("password"), 
	                 Toast.LENGTH_SHORT).show();
	            } catch (Exception e) {
	                Log.d("ReadWeatherJSONFeedTask", e.getLocalizedMessage());
	            }          
	        }
	    }
	
}
