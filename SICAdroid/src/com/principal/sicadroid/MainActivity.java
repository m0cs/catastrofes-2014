package com.principal.sicadroid;

import com.example.prototipo.R;

import util.AlertDialogManager;
import util.Validador;
import android.support.v7.app.ActionBarActivity;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.app.Activity;
import android.support.v4.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
//import android.widget.Toast;
import android.widget.Toast;


public class MainActivity extends ActionBarActivity implements OnClickListener {
	EditText mail;
	EditText pass;
	Button login;
	Button cancel;
	Button sqliteBase;
	Button geoNames;
	CheckBox remember;
	EditText resultado;
	Handler_sqlite helper;
	Handler_ws handlerWS;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		mail = (EditText) findViewById(R.id.editText_mail);
		pass = (EditText) findViewById(R.id.editText_pass);
		login = (Button)findViewById(R.id.button_login);
		cancel = (Button)findViewById(R.id.button_cancel);
		sqliteBase = (Button)findViewById(R.id.btn_sqlitedb);
		geoNames = (Button)findViewById(R.id.btn_geonames);
		remember = (CheckBox) findViewById(R.id.checkBox_remember);
		//resultado = (EditText) findViewById(R.id.resultadoTxt);
		
		login.setOnClickListener(this);
		cancel.setOnClickListener(this);
		sqliteBase.setOnClickListener(this);
		geoNames.setOnClickListener(this);
		
		helper = new Handler_sqlite(this);
		helper.abrir();
		
		handlerWS = new Handler_ws(this);
		
		String[] datosUsrRecordados = helper.leerRegistro();
		if (datosUsrRecordados[0] != "" && datosUsrRecordados[1] != "")
		{
			mail.setText(datosUsrRecordados[0].toString());
			pass.setText(datosUsrRecordados[1].toString());
//			resultado.setText(datosUsrRecordados[0].toString() + "   " + datosUsrRecordados[1].toString());
		}
		else resultado.setText("NO hay usuario recordado");
		helper.cerrar();
		
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
		    String mailS = extras.getString("mail");
		    String passwordS = extras.getString("password");
		    
		    mail.setText(mailS);
		    pass.setText(passwordS);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		String em = mail.getText().toString();
		String ps = pass.getText().toString();
		
		
		
		switch (v.getId()) {
		case R.id.button_login:
			boolean validMail = Validador.validateEmail(em);
			boolean validPass = Validador.validatePassword(ps);

			//prueba del manejadorde alertas
			FragmentManager fm = getSupportFragmentManager();
	        AlertDialogManager dialogo = new AlertDialogManager("sdlkajdlsakjdlksajdsa", "titulo");
	        dialogo.show(fm, "tagAlerta");
	        //prueba del manejadorde alertas
	        
	        //Recuerda mail y password del usuario en SQLITE
			if (remember.isChecked())
			{
				Handler_sqlite helper = new Handler_sqlite(this);
				helper.abrir();
				helper.insertarRegistro(em, ps);
				helper.cerrar();
			}
			
			if (validMail && validPass){
				//Probando el Json del Prototipo Cica
				handlerWS.callWsPrototipo("http://172.16.103.145:8080/DemoRest/resources/users/login/jmg@gmail.com/jmg");
				
				boolean succesLogin = true;// Respuesta del server que el usuario existe
				//Probando el Json del Prototipo Cica
				if (succesLogin){
					Intent i = new Intent(this, SegundaActivity.class);
			        startActivity(i);
				} 
	        }
				
				
			
			break;

		case R.id.button_cancel:
//			Intent i = new Intent(this, SegundaActivity.class);
//	        startActivity(i);
			Intent i = new Intent(this, FastRegistry.class);
	        startActivity(i);
			
//			helper.abrir();
			
//			String[] usuEnDB = helper.leerRegistro();
//			helper.cerrar();
			
//			remember.setText(usuEnDB[0]);
			
			//****************************************
//			helper = new Handler_sqlite(this);
//			helper.abrir();
//			
//			String[] datosUsrRecordados = helper.leerRegistro();
//			if (datosUsrRecordados[0] != "" && datosUsrRecordados[1] != "")
//			{
//				mail.setText(datosUsrRecordados[0].toString());
//				pass.setText(datosUsrRecordados[1].toString());
////				resultado.setText(datosUsrRecordados[0].toString() + "   " + datosUsrRecordados[1].toString());
//			}
//			else resultado.setText("NO hay usuario recordado");
//			helper.cerrar();
			//****************************************			
			
			break;
			
		case R.id.btn_sqlitedb:
					
			helper = new Handler_sqlite(this);
			helper.abrir();
			
			String[] datosUsrRecordados = helper.leerRegistro();
			if (datosUsrRecordados[0] != "" && datosUsrRecordados[1] != "")
			{
				mail.setText(datosUsrRecordados[0].toString());
				pass.setText(datosUsrRecordados[1].toString());
//				resultado.setText(datosUsrRecordados[0].toString() + "   " + datosUsrRecordados[1].toString());
			}
			//else resultado.setText("NO hay usuario recordado");
			helper.cerrar();
			
			break;
					
			
		case R.id.btn_geonames:
			
			//Probando el WS de geoNames
			handlerWS.callWsGeoNames("http://ws.geonames.org/findNearByWeatherJSON?lat=" + 
		            "37.77493" + "&lng=" + 
		            "-122.419416" + "&username=" + "turco");  
			//Probando el WS
		
			break;
					
				default:
					break;
		}
		
		
	}
}
