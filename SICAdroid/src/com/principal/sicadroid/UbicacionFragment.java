package com.principal.sicadroid;

import org.osmdroid.api.IMapController;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapController;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.SimpleLocationOverlay;

import com.example.prototipo.R;

import util.AlertDialogManager;
import util.LocationInterface;
import adapter.NavDrawerListAdapter;
import android.R.layout;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.sax.RootElement;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.ListFragment;
import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.ListView;

@SuppressLint("NewApi")
public class UbicacionFragment extends Fragment{
	
	public UbicacionFragment(){}
	
	public coordManager gps;
	
	public LocationManager handle;
	public String provider;
	
	private MapView mapView;
	
	private Button actualizar;
	private Button ayuda;
	
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
 
        View rootView = inflater.inflate(R.layout.fragment_ubicacion, container, false);
        actualizar = (Button) rootView.findViewById(R.id.button1);
        ayuda = (Button) rootView.findViewById(R.id.button_ayuda);
        ayuda.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				((SegundaActivity)getActivity()).displayView(4);
				
			}
		});
        actualizar.setOnClickListener(new OnClickListener() {
        	
			@Override
			public void onClick(View v) {
				// DALE PUTO!
				
				mapView.invalidate();		
		        mapView.setTileSource(TileSourceFactory.MAPQUESTOSM);
		        mapView.setMultiTouchControls(true);
		        mapView.setBuiltInZoomControls(true);
		        																																																																																																																																																																																																		
		        MapController mapController =  mapView.getController(); 
		        mapController.setZoom(18);
				
				 gps = new coordManager(getActivity());
			       
				 
		            if(gps.canGetLocation()){
		                 
		                double lt = gps.getLatitude();
		                double lg = gps.getLongitude();
		               
		                GeoPoint myLocation = new GeoPoint(lt,lg); // obtiene las coordenadas brindado mejor proveedor, por el manejador
		    	        SimpleLocationOverlay myLocationOverlay = new SimpleLocationOverlay(getActivity());
		    	        
		    	        mapView.getOverlays().clear();
		    	        mapView.getOverlays().add(myLocationOverlay);
		    	        mapController.setCenter(myLocation);
		    	        myLocationOverlay.setLocation(myLocation); 
		    	        mapView.invalidate();
		            }else{
		                gps.showSettingsAlert();
		            }
			}
		});
//        coordManager cMgr = new coordManager(getActivity().getBaseContext());
//        
//        String[] datosCMgr = cMgr.iniciarServicio();
//        if (datosCMgr != null){
//	        Double lt = Double.parseDouble(datosCMgr[0]);
//	        Double lg = Double.parseDouble(datosCMgr[1]);
//	        String proveedor = datosCMgr[2];
//	        String mensaje = datosCMgr[3];
//	        if (proveedor.trim() != "gps"){
//	        	FragmentManager fm = getFragmentManager();
//		        AlertDialogManager dialogo = new AlertDialogManager("Encienda su GPS para una mejor localizacion", "informacion");
//		        dialogo.show(fm, "tagAlerta");
//	        }
	          
	        mapView = (MapView) rootView.findViewById(R.id.mapView);
	        mapView.setTileSource(TileSourceFactory.MAPQUESTOSM);
	        mapView.setMultiTouchControls(true);
	        mapView.setBuiltInZoomControls(true);
	       
	        MapController mapController =  mapView.getController(); 
	        mapController.setZoom(18);
	        
	        
	       // String directory = Environment.getExternalStorageDirectory().toString();
	        //devolvio Storage/sdcar0 ahi crea una carpeta osmdroid
//	        String[] datos = iniciarServicio();
//	        
//	        if (datos != null){
//		        Double lt = Double.parseDouble(datos[0]);
//		        Double lg = Double.parseDouble(datos[1]);
//		        String proveedor = datos[2];
//		        String mensaje = datos[3];
//		        if (proveedor.trim() != "gps"){
//		        	FragmentManager fm = getFragmentManager();
//			        AlertDialogManager dialogo = new AlertDialogManager("Encienda su GPS para una mejor localizacion", "informacion");
//			        dialogo.show(fm, "tagAlerta");
//		        }
	        
	        gps = new coordManager(getActivity());
	        
            // check if GPS enabled    
            if(gps.canGetLocation()){
                 
                double lt = gps.getLatitude();
                double lg = gps.getLongitude();
               
                GeoPoint myLocation = new GeoPoint(lt,lg); // obtiene las coordenadas brindado mejor proveedor, por el manejador
    	        SimpleLocationOverlay myLocationOverlay = new SimpleLocationOverlay(getActivity());
    	        mapView.getOverlays().add(myLocationOverlay);
    	        mapController.setCenter(myLocation);
    	        myLocationOverlay.setLocation(myLocation); 
                   
            }else{
                // can't get location
                // GPS or Network is not enabled
                // Ask user to enable GPS/network in settings
                gps.showSettingsAlert();
            }
            
            
//	        GeoPoint myLocation = new GeoPoint(lt,lg); // obtiene las coordenadas brindado mejor proveedor, por el manejador
//	        SimpleLocationOverlay myLocationOverlay = new SimpleLocationOverlay(getActivity());
//	        mapView.getOverlays().add(myLocationOverlay);
//	        mapController.setCenter(myLocation);
//	        myLocationOverlay.setLocation(myLocation);
//	        }
	        
//        }else{
//        	FragmentManager fm = getFragmentManager();
//	        AlertDialogManager dialogo = new AlertDialogManager("No se pudo encontrar su Ubicación, verifique que cuente con conexión de internet", "informacion");
//	        dialogo.show(fm, "tagAlerta");
//        }
           
        return rootView;
    }
	
	private Location getMyLocation(){
		getActivity().getApplicationContext();
		LocationManager locationManager = (LocationManager) getActivity().getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
		return locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
		}

	

	
	
	
//	 public String[] iniciarServicio(){
//	    //Se activa el servicio de localización
//	//    servicioActivo = true;
//	 
//	    //Crea el objeto que gestiona las localizaciones
//	    handle = (LocationManager)getActivity().getBaseContext().getSystemService(Context.LOCATION_SERVICE);
//	    
//	    Criteria c = new Criteria();
//	    c.setAccuracy(Criteria.ACCURACY_FINE);
//	    //obtiene el mejor proveedor en función del criterio asignado
//	    //(la mejor precisión posible)
//	    provider = handle.getBestProvider(c, true);
//	    Location loc;
//	    
//	 
//	    //Se activan las notificaciones de localización con los parámetros: proveedor, tiempo mínimo de actualización, distancia mínima, Locationlistener
////	    handle.requestLocationUpdates(provider, 10000, 1, this);
//	    //Obtenemos la última posición conocida dada por el proveedor
//	    loc = handle.getLastKnownLocation(provider);
//	    String[] datos = muestraPosicionActual(loc);
//	    
//	    return datos;
//	 }
//	 public String[] muestraPosicionActual(Location loc){
//	    if(loc == null){
//	        //localización "Desconocida"
//	    }else{//Si se encuentra, se mostrará la latitud y longitud
//	    	String mensaje = "encontrado";
//	    	String latitud = (String.valueOf(loc.getLatitude()));
//	       String longitud = (String.valueOf(loc.getLongitude()));
//	       
//	       String datosUbicacion[] = {latitud,longitud,provider,mensaje};
//	       pararServicio();
//	       return (datosUbicacion);
//	    }
//	    pararServicio();
//		return null;
//	 }
	
}
