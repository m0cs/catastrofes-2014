package com.principal.sicadroid;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase; //objeto base de datos
import android.database.sqlite.SQLiteOpenHelper; //permitir manipular la bd
import static android.provider.BaseColumns._ID; //identificador de los registros en la tabla

public class Handler_sqlite extends SQLiteOpenHelper{
	
	public Handler_sqlite(Context ctx) //Contructor
	{
		super(ctx, "MiBase.db", null, 1);
	}
	
	@Override 
	public void onCreate(SQLiteDatabase db) 
	{
		String query = "CREATE TABLE Usuarios ("+_ID+" INTEGER PRIMARY KEY AUTOINCREMENT, " + "email TEXT, password TEXT);";
		db.execSQL(query);
	}
	
	@Override
	public void onUpgrade(SQLiteDatabase db, int version_ant, int version_nueva) 
	{
		db.execSQL("DROP TABLE IF EXIST Usuarios;");
		onCreate(db);
	}
	
	public void insertarRegistro(String mail, String pass)
	{
		
		if (mail.length() > 0 && pass.length() > 0) //Aca Usar algun validador en vez de ver si esta vacio **PENDIENTE EL VALIDADOR DE MAIL Y PASS
		{
			
			ContentValues valores = new ContentValues();
			valores.put("email", mail);
			valores.put("password", pass);
			
			String columnas[] = {_ID,"email","password"};
			Cursor c = this.getReadableDatabase().query("Usuarios", columnas, null, null, null, null, null);
			if(c.getCount()>0)
			{
				this.vaciarTablaUsuario();
			}
			
			this.getWritableDatabase().insert("Usuarios", null, valores);
			
		}
		
	}
	
	public String[] leerRegistro()
	{
		String[] consultaResult = new String[2];
		
		String columnas[] = {_ID,"email","password"};
		Cursor c = this.getReadableDatabase().query("Usuarios", columnas, null, null, null, null, null);
		
		consultaResult[0] = "";
		consultaResult[1] = "";
		try 
		{
			c.moveToFirst();
			int id, ie, ip;
			id = c.getColumnIndex(_ID);
			ie = c.getColumnIndex("email");
			ip = c.getColumnIndex("password");
			if (c.getCount() != 0)
			{
				consultaResult[0] = c.getString(ie);
				consultaResult[1] = c.getString(ip);
			}
		} catch (Exception e) {
			// TODO: handle exception por ahora naranja
		}
		
		return consultaResult;
	}
	
	public void vaciarTablaUsuario()
	{
		
		this.getReadableDatabase().delete("Usuarios", null, null);

	}
	
	public void abrir()
	{
		this.getWritableDatabase();
	}
	
	public void cerrar()
	{
		this.close();
	}
	
}
