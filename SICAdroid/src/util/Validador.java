package util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Validador {

	private static final String PATTERN_PASSWORD = "(?=^.{8,}$)((?=.*\\d)(?=.*\\W+))(?![.\\n])(?=.*[A-Z])(?=.*[a-z]).*$";
//  Contraseñas que contengan al menos una letra mayúscula.
//  Contraseñas que contengan al menos una letra minúscula.
//  Contraseñas que contengan al menos un número y caracter especial.
//  Contraseñas cuya longitud sea como mínimo 8 caracteres.
//  Contraseñas cuya longitud máxima no debe ser arbitrariamente limitada.
	
	private static final String PATTERN_EMAIL = "^[_a-z0-9-]+(\\.[_a-z0-9-]+)*@[a-z0-9-]+(\\.[a-z0-9-]+)*(\\.[a-z]{2,3})$";
	
	private static final String PATTERN_NICK = "^[a-zA-Z\\d_]{4,15}";
//	Entre 4 y 15 caracteres el nick
	
	public static boolean validatePassword(String password) {
		 
        // COmpila la expresion regular dada dentro del pattern.
        Pattern pattern = Pattern.compile(PATTERN_PASSWORD);
 
        // Match el input contra el pattern
        Matcher matcher = pattern.matcher(password);
        return matcher.matches();
 
    }
	
	 public static boolean validateEmail(String email) {
		 
	        Pattern pattern = Pattern.compile(PATTERN_EMAIL);
	 
	        Matcher matcher = pattern.matcher(email);
	        return matcher.matches();
	 
	    }
	
	 public static boolean validatePasswordMatchRepeat(String password,String passwordRepeat){
		 return password.equals(passwordRepeat);
	 }
	 
	 public static boolean validateNick(String nick){
		 Pattern pattern = Pattern.compile(PATTERN_NICK);
		 
	        Matcher matcher = pattern.matcher(nick);
	        return matcher.matches();
	 }
	 
	 public static boolean validateNotNullField(String campo){
		 if (campo == null){
			 return false;
		 } else return true;
	 }
	 public static boolean validateNotEmptyField(String campo){
		 if (campo.length()==0){
			 return false;
		 } else return true;
	 }
}
