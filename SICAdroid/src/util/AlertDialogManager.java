package util;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.support.v4.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

@SuppressLint("NewApi")
public class AlertDialogManager extends DialogFragment{
	
	String mensaje;
	String info;
	
	public AlertDialogManager(String mensaje, String info){
		this.mensaje = mensaje;
		this.info = info;
	}
	
	@Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
 
        AlertDialog.Builder builder =
                new AlertDialog.Builder(getActivity());
 
        builder.setMessage(this.mensaje)
               .setTitle(this.info)
               .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                   public void onClick(DialogInterface dialog, int id) {
                       dialog.cancel();
                       
                   }
               });
 
        return builder.create();
	}
}
