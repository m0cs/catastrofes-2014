
//llamados fuente RSS ******************************************

var cargarFuenteModal = function(idFuente, callback ) {
	
	var url = requestParams['host']+ ":" + requestParams['puerto'] + "/" + requestParams['contexto'] + "/" + requestParams['path'];
	url += "/fuentes/buscar/" + idFuente;
	$.ajax({
		url : url,
		type : "GET"

	}).done(function(response) {
		callback( response );
		
	}).fail(function() {
		console.log("error");
		
	}).always(function() {
		console.log("complete");
	});
};


//llamados ONG ******************************************

var cargarOngModal = function(id, callback ) {
	
	var url = requestParams['host']+ ":" + requestParams['puerto'] + "/" + requestParams['contexto'] + "/" + requestParams['path'];
	url += "/ongs/buscar/" + id;
	$.ajax({
		url : url,
		type : "GET"

	}).done(function(response) {
		callback( response );
		
	}).fail(function() {
		console.log("error");
		
	}).always(function() {
		console.log("complete");
	});
};


//llamdos Usuarios ***********************************

var cargarUserModal = function(id, callback){

	var url = requestParams['host']+ ":" + requestParams['puerto'] + "/" + requestParams['contexto'] + "/" + requestParams['path'];
	url += "/users/buscar/" + id;
	$.ajax({
		url : url,
		type : "GET"

	}).done(function(response) {
		callback( response );
		
	}).fail(function() {
		console.log("error");
		
	}).always(function() {
		console.log("complete");
	});	
	
};

//llamados planes *************************************
var cargarPlanModal = function(id, tipo, callback){

	var url = requestParams['host']+ ":" + requestParams['puerto'] + "/" + requestParams['contexto'] + "/" + requestParams['path'];
	url += "/planes/buscar/" + id + "/" + tipo;
	$.ajax({
		url : url,
		type : "GET"

	}).done(function(response) {
		callback( response );
		
	}).fail(function() {
		console.log("error");
		
	}).always(function() {
		console.log("complete");
	});	
	
};


//llamados tipo catastrofe

var cargarTipoCatastrofeModal = function(id, callback){

	var url = requestParams['host']+ ":" + requestParams['puerto'] + "/" + requestParams['contexto'] + "/" + requestParams['path'];
	url += "/tiposCatastrofes/buscar/" + id;
	$.ajax({
		url : url,
		type : "GET"

	}).done(function(response) {
		callback( response );
		
	}).fail(function() {
		console.log("error");
		
	}).always(function() {
		console.log("complete");
	});	
	
};

