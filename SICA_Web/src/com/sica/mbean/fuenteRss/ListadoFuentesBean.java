package com.sica.mbean.fuenteRss;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import com.sica.dto.FuenteRssDto;
import com.sica.excepciones.NegocioException;
import com.sica.facade.IFacadeBackend;
import com.sica.mbean.BaseBean;

@ManagedBean(name="listadoFuentes")
@RequestScoped
public class ListadoFuentesBean extends BaseBean {

	@EJB
	private IFacadeBackend facadeBackend;
	
	private long idFuente;
	
	private List<FuenteRssDto> listadoFuentes = new ArrayList<FuenteRssDto>();
	
	@PostConstruct
    public void cargarListas()
    {       
		try {
			listadoFuentes = facadeBackend.buscarTodosFuenteRss();
		} 
		catch (Exception e) {
			handleServerException(e);
		}
    }	    
	
	@Override
	protected boolean validarCampos() {
		// TODO Auto-generated method stub
		return false;
	}

	public List<FuenteRssDto> getListadoFuentes() {
		return listadoFuentes;
	}

	public void setListadoFuentes(List<FuenteRssDto> listadoFuentes) {
		this.listadoFuentes = listadoFuentes;
	}

	public void eliminarFuenteRss(){
		try {
			facadeBackend.eliminarFuenteRss( idFuente );
			listadoFuentes = facadeBackend.buscarTodosFuenteRss();
		} 
		catch (NegocioException e) {
			handleServerException(e);
		}
	}

	public long getIdFuente() {
		return idFuente;
	}

	public void setIdFuente(long idFuente) {
		this.idFuente = idFuente;
	}
	
	
}
