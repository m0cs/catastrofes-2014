package com.sica.mbean.fuenteRss;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import com.sica.dto.FuenteRssDto;
import com.sica.excepciones.NegocioException;
import com.sica.facade.IFacadeBackend;
import com.sica.mbean.BaseBean;
import com.sica.utils.Utiles;

@ManagedBean(name="fuenteRssBean")
@RequestScoped
public class FuenteRssBean extends BaseBean {
	
	@EJB
	private IFacadeBackend facadeBackend;

	private long id;
	private String nombre;
	private String ruta;
	
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getRuta() {
		return ruta;
	}
	public void setRuta(String ruta) {
		this.ruta = ruta;
	}
	
	public void salvarFuenteRss(){
		
		try {
			FuenteRssDto dto = new FuenteRssDto();
			
			dto.setId( id );
			dto.setNombre(nombre);
			dto.setUrl(ruta);
			
			if (Utiles.isNuevo( id )){
				
				dto.setId(null);
				facadeBackend.insertarFuenteRss(dto);
			}
			else{
				facadeBackend.actualizarFuenteRss(dto);
			}
			
		} 
		catch (NegocioException e) {
			handleServerException(e);
		}
		
	}
	@Override
	protected boolean validarCampos() {
		// TODO Auto-generated method stub
		return false;
	}
	
	
	
}
