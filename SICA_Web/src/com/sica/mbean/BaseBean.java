package com.sica.mbean;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import com.sica.excepciones.DaoException;
import com.sica.excepciones.NegocioException;
import com.sica.utils.Utiles;

public abstract class BaseBean {

    // Utilidades de errores
    private static final String PREFIX_MSJ_ERROR_NEGOCIO = "Error de negocio: ";
    private static final String PREFIX_MSJ_ERROR_ACCESO_DATOS = "Error de acceso a datos: ";
	
    // Constantes de retornos
    protected static final String SUCCESS = "success";
    protected static final String ERROR = "error";
    protected static final String TO_INDEX = "to_index";
    
    private static final String ID_CATASTOFE_EDICION = "id_catastrofe_edicion";

    protected abstract boolean validarCampos();
    
    
    protected void handleServerException(Exception e){
    	
            if(e instanceof NegocioException){
                    addBeanError(PREFIX_MSJ_ERROR_NEGOCIO + ((NegocioException) e).getMessage());
            }
            else if(e instanceof DaoException){
                    addBeanError(PREFIX_MSJ_ERROR_ACCESO_DATOS + ((DaoException) e).getMessage());
            }
            else{
                    addBeanError(!Utiles.isNullOrEmpty(e.getMessage()) ? e.getMessage() : e.getLocalizedMessage());
            }
    } 
    
    protected void addBeanError(String message){
            addBeanError(null, message);
    }
    
    protected void addBeanError(String field, String message){
    	
            FacesContext.getCurrentInstance().addMessage(field, new FacesMessage(FacesMessage.SEVERITY_ERROR, message, null));
            FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
            FacesContext.getCurrentInstance().getExternalContext().getFlash().setRedirect(true);
    }  
    
    protected String getRequestParameter(String key){
            return FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get(key);
    }  
    
    // set id catastrofe edicion
    public static void saveCatastrofeEdicion(Long idCatastrofe){
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put(ID_CATASTOFE_EDICION, idCatastrofe);
    }
    
    //get id catastrofe edicion
    public static Long getCatastrofeEdicion(){
    	return (Long) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get(ID_CATASTOFE_EDICION);
    }
    
    //clear id catastrofe edicion
    public static void clearCatastrofeEdicion(){
    	FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put(ID_CATASTOFE_EDICION, null);
    }
    
    //devuelve true si la catastrofe se encuentra en modo edicion
    public static boolean isCatastrofeEdicion(){
    	Long idCatastrofe = (Long) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get(ID_CATASTOFE_EDICION);
    	return !Utiles.isNullOrCero(idCatastrofe);
    }
        
}

