package com.sica.mbean.catastrofe;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import com.sica.dto.TipoCatastrofeDto;
import com.sica.excepciones.NegocioException;
import com.sica.facade.IFacadeBackend;
import com.sica.mbean.BaseBean;

@ManagedBean(name="listadoTipoCatastrofe")
@RequestScoped
public class ListadoTipoCatastrofeBean extends BaseBean {

	private long idTipo;
	private List<TipoCatastrofeDto> listadoTipoCatastrofe = new ArrayList<TipoCatastrofeDto>();
	
	@EJB
	private IFacadeBackend facadeBackend;
	
	@PostConstruct
    public void cargarListas()
    {       
		try {
			listadoTipoCatastrofe = facadeBackend.buscarTodasTipoCatastrofe();
		} 
		catch (Exception e) {
			handleServerException(e);
		}
    }
	
	public void eliminarTipoCatastrofe(){
		try {
			facadeBackend.eliminarTipoCatastrofe(idTipo);
			listadoTipoCatastrofe = facadeBackend.buscarTodasTipoCatastrofe();
		} 
		catch (NegocioException e) {
			handleServerException(e);
		}
	}	
	
	@Override
	protected boolean validarCampos() {
		// TODO Auto-generated method stub
		return false;
	}


	public List<TipoCatastrofeDto> getListadoTipoCatastrofe() {
		return listadoTipoCatastrofe;
	}


	public void setListadoTipoCatastrofe(List<TipoCatastrofeDto> listadoTipoCatastrofe) {
		this.listadoTipoCatastrofe = listadoTipoCatastrofe;
	}


	public long getIdTipo() {
		return idTipo;
	}


	public void setIdTipo(long idTipo) {
		this.idTipo = idTipo;
	}
	
	
	

}
