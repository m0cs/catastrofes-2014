package com.sica.mbean.catastrofe;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import com.sica.dto.TipoCatastrofeDto;
import com.sica.excepciones.NegocioException;
import com.sica.facade.IFacadeBackend;
import com.sica.mbean.BaseBean;
import com.sica.utils.Utiles;

@ManagedBean(name="tipoCatastrofeBean")
@RequestScoped
public class TipoCatastrofeBean extends BaseBean {
	
	
	@EJB
	private IFacadeBackend facadeBackend;	
	
	private String id;
	private String nombre;
	private String estilo;
	
	public void salvarTipoCatastrofe(){
		try{
			TipoCatastrofeDto tipoDto = new TipoCatastrofeDto();
			if (!Utiles.isNullOrEmpty(id)){
				tipoDto.setId(Long.valueOf(id));
			}
			tipoDto.setNombre(nombre);
			tipoDto.setEstilo(estilo);
			
			if (Utiles.isNullOrEmpty( id )){
				tipoDto.setId(null);
				facadeBackend.insertarTipoCatastrofe(tipoDto);
			}
			else{
				facadeBackend.actualizarTipoCatastrofe(tipoDto);
			}
			
		} 
		catch (NegocioException e) {
			handleServerException(e);
		}		
	}	
	
	@Override
	protected boolean validarCampos() {
		// TODO Auto-generated method stub
		return false;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getEstilo() {
		return estilo;
	}

	public void setEstilo(String estilo) {
		this.estilo = estilo;
	}

	
}
