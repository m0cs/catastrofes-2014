package com.sica.mbean.catastrofe;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import com.sica.dto.EventoCatastrofeDto;
import com.sica.enums.EstadoCatastrofe;
import com.sica.excepciones.NegocioException;
import com.sica.facade.IFacadeBackend;
import com.sica.mbean.BaseBean;

@ManagedBean(name="listadoCatastrofeBean")
@RequestScoped
public class ListadoCatastrofeBean extends BaseBean {

	@EJB
	private IFacadeBackend facadeBackend;

	private long idCatastrofe;
	private List<EventoCatastrofeDto> listadoCatastrofes;
	
	private static final String TO_GESTION_CATASTROFE = "to_gestion_catastrofe";
	
	
	@PostConstruct
    public void cargarListas()
    {       
		try {
			listadoCatastrofes = facadeBackend.buscarTodasEventoCatastrofe();
		} 
		catch (Exception e) {
			handleServerException(e);
		}
    }	
	
	public String crearEventoCatastrofe(){
		clearCatastrofeEdicion();
		return TO_GESTION_CATASTROFE;
	}
	
    public String toGestionCatastrofe()
    {
    	saveCatastrofeEdicion(idCatastrofe);	
		return TO_GESTION_CATASTROFE;
    }	
	
	public void activarCatastrofe(){
		try {
			EventoCatastrofeDto dto = facadeBackend.buscarPorIdEventoCatastrofe(idCatastrofe);
			dto.setEstado(EstadoCatastrofe.ACTIVA.toString());
			facadeBackend.actualizarEventoCatastrofe( dto );
			listadoCatastrofes = facadeBackend.buscarTodasEventoCatastrofe();
		} 
		catch (NegocioException e) {
			handleServerException(e);
		}
	}
	
	public void finalizarCatastrofe(){
		try {
			EventoCatastrofeDto dto = facadeBackend.buscarPorIdEventoCatastrofe(idCatastrofe);
			dto.setEstado(EstadoCatastrofe.FINALIZADA.toString());
			facadeBackend.actualizarEventoCatastrofe( dto );
			listadoCatastrofes = facadeBackend.buscarTodasEventoCatastrofe();
		} 
		catch (NegocioException e) {
			handleServerException(e);
		}	
	}
	
	public void cancelarCatastrofe(){
		try {
			EventoCatastrofeDto dto = facadeBackend.buscarPorIdEventoCatastrofe(idCatastrofe);
			dto.setEstado(EstadoCatastrofe.CANCELADA.toString());
			facadeBackend.actualizarEventoCatastrofe( dto );
			listadoCatastrofes = facadeBackend.buscarTodasEventoCatastrofe();
		} 
		catch (NegocioException e) {
			handleServerException(e);
		}			
	}
	
	@Override
	protected boolean validarCampos() {
		// TODO Auto-generated method stub
		return false;
	}

	public long getIdCatastrofe() {
		return idCatastrofe;
	}

	public void setIdCatastrofe(long idCatastrofe) {
		this.idCatastrofe = idCatastrofe;
	}

	public List<EventoCatastrofeDto> getListadoCatastrofes() {
		return listadoCatastrofes;
	}

	public void setListadoCatastrofes(List<EventoCatastrofeDto> listadoCatastrofes) {
		this.listadoCatastrofes = listadoCatastrofes;
	}
	
	

}
