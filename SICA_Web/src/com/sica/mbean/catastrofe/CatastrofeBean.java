package com.sica.mbean.catastrofe;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import com.sica.dto.EventoCatastrofeDto;
import com.sica.dto.FuenteRssDto;
import com.sica.dto.ImagenDto;
import com.sica.dto.OngDto;
import com.sica.dto.PlanRescatistaDto;
import com.sica.dto.RescatistaDto;
import com.sica.dto.TipoCatastrofeDto;
import com.sica.excepciones.NegocioException;
import com.sica.facade.IFacadeBackend;
import com.sica.mbean.BaseBean;
import com.sica.utiles.Utiles_SICA;
import com.sica.utils.Utiles;

@ManagedBean(name="catastrofeBean")
@RequestScoped
public class CatastrofeBean extends BaseBean {

	@EJB
	private IFacadeBackend facadeBackend;
	
	private EventoCatastrofeDto cata;
	private List<TipoCatastrofeDto> tipos;
	private List<PlanRescatistaDto> planes;
	
	private List<String> rescatistaAsignados;
	private List<RescatistaDto> todosRescatistas;
	
	private List<String> ongsAsignadas;
	private List<OngDto> todasOng;
	
	private List<String> fuentesAsignadas;
	private List<FuenteRssDto> todasFuentes;
	
	private String idImages;
	
	@PostConstruct
	public void cargarDatos(){
		try{
			//else si se esta editando
			if (isCatastrofeEdicion()){
				Long idCatastrofe = getCatastrofeEdicion(); 
				cata = facadeBackend.buscarPorIdEventoCatastrofe((idCatastrofe));		
			}
			prePopulateDatos();			
		}
		catch (Exception e) {
			handleServerException(e);
		}
	}	
	
	public void prePopulateDatos(){
		try{
			//carga de datos
			tipos = new ArrayList<TipoCatastrofeDto>();
			tipos = facadeBackend.buscarTodasTipoCatastrofe();
			
			todosRescatistas = facadeBackend.buscarTodosRescatista();
			todasOng = facadeBackend.buscarTodasOng();
			todasFuentes = facadeBackend.buscarTodosFuenteRss();
			
			planes = new ArrayList<PlanRescatistaDto>();
			rescatistaAsignados = new ArrayList<String>();
			ongsAsignadas = new ArrayList<String>();
			fuentesAsignadas = new ArrayList<String>();
			
			//if es nuevo.
			if (!isCatastrofeEdicion()){
				cata = new EventoCatastrofeDto();
				cata.setPlanesRescatista(new ArrayList<PlanRescatistaDto>());
				cata.setOngs(new ArrayList<OngDto>());
				cata.setFuentes(new ArrayList<FuenteRssDto>());
				cata.setImagenes(new ArrayList<ImagenDto>());
			}
			//is modo edicion
			else{
				if (cata.getPlanesRescatista() != null ){
					for (PlanRescatistaDto plan : cata.getPlanesRescatista()){
						rescatistaAsignados.add(String.valueOf(plan.getIdRescatista()));
					}
				}
				
				if (cata.getOngs() != null ){
					for (OngDto ong : cata.getOngs()){
						ongsAsignadas.add(String.valueOf(ong.getId()));
					}
				}
				
				if (cata.getFuentes() != null){
					for (FuenteRssDto fuente : cata.getFuentes()){
						fuentesAsignadas.add(String.valueOf(fuente.getId()));
					}
				}
				
				if (cata.getImagenes() != null){
					if (idImages == null){
						idImages = "";
					}
					for (ImagenDto img : cata.getImagenes()){
						idImages += (idImages + String.valueOf(img.getId()) + ",");
					}
				}
				
			}
		}
		catch (Exception e) {
			handleServerException(e);
		}			
	}
	
	public void salvarCatastrofe(){
		
		try {
			procesarPlanesRescatistas();
			procesarOngs();
			procesarFuentes();
			procesarImagens();
			
			//Si es nueva.
			if (Utiles.isNullOrCero(cata.getId())){
				long idCatastrofe = facadeBackend.insertarEventoCatastrofe(cata);	
				cata.setId(idCatastrofe);
				saveCatastrofeEdicion(idCatastrofe);
			}
			else{
				facadeBackend.actualizarEventoCatastrofe(cata);	
			}
			cata = facadeBackend.buscarPorIdEventoCatastrofe((cata.getId()));	
		} 
		catch (NegocioException e) {
			handleServerException(e);
		}
	}
	
	public void	procesarImagens() throws NegocioException{
		cata.getImagenes().clear();
		if (!Utiles_SICA.isNullOrEmpty(idImages)){
			cata.getImagenes().clear();
			String[] arrayId = idImages.split(",");
			for (String id : arrayId){
				if (!Utiles_SICA.isNullOrEmpty(id)){
					ImagenDto imgDto = new ImagenDto();// facadeBackend.buscarImagenPorId(Long.valueOf(id));
					imgDto.setId(Long.valueOf(id));
					cata.getImagenes().add(imgDto);
				}
			}
		}
	}
	
	public void procesarPlanesRescatistas(){
		cata.getPlanesRescatista().clear();
		if (rescatistaAsignados != null && !rescatistaAsignados.isEmpty()) {
			for (String idresc : rescatistaAsignados){
				PlanRescatistaDto plan = new PlanRescatistaDto();
				plan.setIdRescatista(Long.valueOf(idresc));
				plan.setId( Utiles.getIdPlanPorRescatista(planes, Long.valueOf(idresc)));
				cata.getPlanesRescatista().add(plan);
			}
		} 	
	}
	
	public void procesarOngs() {

		if (ongsAsignadas != null && !ongsAsignadas.isEmpty()){
			for ( String idong : ongsAsignadas ){
				OngDto ongDto = new OngDto();
				ongDto.setId(Long.valueOf(idong));
				cata.getOngs().add(ongDto);
			}
		}
		else{
			cata.getOngs().clear();
		}		
	}
	
	public void procesarFuentes(){
		if (fuentesAsignadas != null && !fuentesAsignadas.isEmpty()){
			for ( String idfuente : fuentesAsignadas ){
				FuenteRssDto fuenteDto = new FuenteRssDto();
				fuenteDto.setId(Long.valueOf(idfuente));
				cata.getFuentes().add(fuenteDto);
			}
		}
		else{
			cata.getOngs().clear();
		}		
	}
	
	@Override
	protected boolean validarCampos() {
		// TODO Auto-generated method stub
		return false;
	}

	public EventoCatastrofeDto getCata() {
		return cata;
	}

	public void setCata(EventoCatastrofeDto cata) {
		this.cata = cata;
	}

	public List<TipoCatastrofeDto> getTipos() {
		return tipos;
	}

	public void setTipos(List<TipoCatastrofeDto> tipos) {
		this.tipos = tipos;
	}

	public List<RescatistaDto> getTodosRescatistas() {
		return todosRescatistas;
	}

	public void setTodosRescatistas(List<RescatistaDto> todosRescatistas) {
		this.todosRescatistas = todosRescatistas;
	}


	public List<OngDto> getTodasOng() {
		return todasOng;
	}

	public void setTodasOng(List<OngDto> todasOng) {
		this.todasOng = todasOng;
	}

	public List<PlanRescatistaDto> getPlanes() {
		return planes;
	}

	public void setPlanes(List<PlanRescatistaDto> planes) {
		this.planes = planes;
	}

	public List<String> getRescatistaAsignados() {
		return rescatistaAsignados;
	}

	public void setRescatistaAsignados(List<String> rescatistaAsignados) {
		this.rescatistaAsignados = rescatistaAsignados;
	}

	public List<String> getOngsAsignadas() {
		return ongsAsignadas;
	}

	public void setOngsAsignadas(List<String> ongsAsignadas) {
		this.ongsAsignadas = ongsAsignadas;
	}

	public List<String> getFuentesAsignadas() {
		return fuentesAsignadas;
	}

	public void setFuentesAsignadas(List<String> fuentesAsignadas) {
		this.fuentesAsignadas = fuentesAsignadas;
	}

	public List<FuenteRssDto> getTodasFuentes() {
		return todasFuentes;
	}

	public void setTodasFuentes(List<FuenteRssDto> todasFuentes) {
		this.todasFuentes = todasFuentes;
	}

	public String getIdImages() {
		return idImages;
	}

	public void setIdImages(String idImages) {
		this.idImages = idImages;
	}

	
	
	
}
