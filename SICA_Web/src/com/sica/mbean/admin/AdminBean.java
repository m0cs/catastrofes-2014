package com.sica.mbean.admin;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import com.sica.dto.AdministradorDto;
import com.sica.excepciones.NegocioException;
import com.sica.facade.IFacadeBackend;
import com.sica.mbean.BaseBean;
import com.sica.utils.Utiles;

@ManagedBean(name="adminBean")
@RequestScoped
public class AdminBean extends BaseBean {

	@EJB
	private IFacadeBackend facadeBackend;
	
	private String id;
	private String nombre;
	private String apellido;
	private String email;
	private String nickAdmin;
	private String passwordAdmin;
	private String repasswordAdmin;
	private String celular;
	private Boolean estado;
	private String fechaNacimiento;
	private String sexo;
	
	@Override
	protected boolean validarCampos() {
		// TODO Auto-generated method stub
		return false;
	}
	
	@PostConstruct
	public void preCargarValores(){
		if (id == null || id.equals("0")){
			id = "";
		}
	}
	
	public void salvarAdmin(){
		try{
			AdministradorDto adminDto = new AdministradorDto();
			if (!Utiles.isNullOrEmpty(id)){
				adminDto.setId(Long.valueOf(id));
			}
			adminDto.setNombre(nombre);
			adminDto.setApellido(apellido);
			adminDto.setEmail(email);
			adminDto.setPassword(passwordAdmin);
			adminDto.setNick(nickAdmin);
			adminDto.setFechaNacimiento(fechaNacimiento);
			adminDto.setCelular(celular);
			
			if (Utiles.isNullOrEmpty(id)){
				adminDto.setId(null);
				facadeBackend.insertarAdministrador(adminDto);
			}
			else{
				facadeBackend.actualizarAdministrador(adminDto);
			}
		} 
		catch (NegocioException e) {
			handleServerException(e);
		}				
	}



	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCelular() {
		return celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

	public Boolean getEstado() {
		return estado;
	}

	public void setEstado(Boolean estado) {
		this.estado = estado;
	}

	public String getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(String fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public String getNickAdmin() {
		return nickAdmin;
	}

	public void setNickAdmin(String nickAdmin) {
		this.nickAdmin = nickAdmin;
	}

	public String getPasswordAdmin() {
		return passwordAdmin;
	}

	public void setPasswordAdmin(String passwordAdmin) {
		this.passwordAdmin = passwordAdmin;
	}

	public String getRepasswordAdmin() {
		return repasswordAdmin;
	}

	public void setRepasswordAdmin(String repasswordAdmin) {
		this.repasswordAdmin = repasswordAdmin;
	}

}
