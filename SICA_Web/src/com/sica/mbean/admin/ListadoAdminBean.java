package com.sica.mbean.admin;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import com.sica.dto.AdministradorDto;
import com.sica.excepciones.NegocioException;
import com.sica.facade.IFacadeBackend;
import com.sica.mbean.BaseBean;

@ManagedBean(name="listadoAdmin")
@RequestScoped
public class ListadoAdminBean extends BaseBean {

	@EJB
	private IFacadeBackend facadeBackend;
	
	private long idAdmin;
	
	private List<AdministradorDto> listadoAdmin = new ArrayList<AdministradorDto>();
	
	
	@PostConstruct
    public void cargarListas()
    {       
		try {
			listadoAdmin = facadeBackend.buscarTodosAdministrador();
		} 
		catch (Exception e) {
			handleServerException(e);
		}
    }	  	
	
	@Override
	protected boolean validarCampos() {
		// TODO Auto-generated method stub
		return false;
	}

	public long getIdAdmin() {
		return idAdmin;
	}

	public void setIdAdmin(long idAdmin) {
		this.idAdmin = idAdmin;
	}

	public List<AdministradorDto> getListadoAdmin() {
		return listadoAdmin;
	}

	public void setListadoAdmin(List<AdministradorDto> listadoAdmin) {
		this.listadoAdmin = listadoAdmin;
	}
	
	public void eliminarAdmin(){
		try {
			facadeBackend.eliminarAdministrador( idAdmin );
			listadoAdmin = facadeBackend.buscarTodosAdministrador();
		} 
		catch (NegocioException e) {
			handleServerException(e);
		}
	}

}
