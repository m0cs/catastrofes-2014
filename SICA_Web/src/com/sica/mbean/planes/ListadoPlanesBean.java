package com.sica.mbean.planes;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import com.sica.dto.PlanDto;
import com.sica.excepciones.NegocioException;
import com.sica.facade.IFacadeBackend;
import com.sica.mbean.BaseBean;


@ManagedBean(name="listadoPlanesBean")
@RequestScoped
public class ListadoPlanesBean extends BaseBean {

	@EJB
	private IFacadeBackend facadeBackend;
	
	private long idPlan;
	private String nombrePlan;
	
	private List<PlanDto> listadoPlanesEmergencia = new ArrayList<PlanDto>();	
	private List<PlanDto> listadoPlanesRiesgo = new ArrayList<PlanDto>();	
	
	@PostConstruct
    public void cargarListas()
    {       
		try {
			listadoPlanesEmergencia = facadeBackend.buscarTodosPlanesEmergencia();
			listadoPlanesRiesgo = facadeBackend.buscarTodosPlanesRiesgo();
		} 
		catch (Exception e) {
			handleServerException(e);
		}
    }	
	
	public void eliminarPlanEmergencia(){
		try {
			facadeBackend.eliminarPlanEmergencia(idPlan);
			listadoPlanesEmergencia = facadeBackend.buscarTodosPlanesEmergencia();
			listadoPlanesRiesgo = facadeBackend.buscarTodosPlanesRiesgo();
		} 
		catch (NegocioException e) {

			e.printStackTrace();
			handleServerException(e);
		}
	}
	
	public void eliminarPlanRiesgo(){
		try {
			facadeBackend.eliminarPlanRiesgo(idPlan);
			listadoPlanesRiesgo = facadeBackend.buscarTodosPlanesRiesgo();
			listadoPlanesEmergencia = facadeBackend.buscarTodosPlanesEmergencia();
		}
		catch (NegocioException e) {

			e.printStackTrace();
			handleServerException(e);
		}
	}
	
	@Override
	protected boolean validarCampos() {
		// TODO Auto-generated method stub
		return false;
	}

	public long getIdPlan() {
		return idPlan;
	}

	public void setIdPlan(long idPlan) {
		this.idPlan = idPlan;
	}

	public String getNombrePlan() {
		return nombrePlan;
	}

	public void setNombrePlan(String nombrePlan) {
		this.nombrePlan = nombrePlan;
	}

	public List<PlanDto> getListadoPlanesEmergencia() {
		return listadoPlanesEmergencia;
	}

	public void setListadoPlanesEmergencia(List<PlanDto> listadoPlanesEmergencia) {
		this.listadoPlanesEmergencia = listadoPlanesEmergencia;
	}

	public List<PlanDto> getListadoPlanesRiesgo() {
		return listadoPlanesRiesgo;
	}

	public void setListadoPlanesRiesgo(List<PlanDto> listadoPlanesRiesgo) {
		this.listadoPlanesRiesgo = listadoPlanesRiesgo;
	}




}
