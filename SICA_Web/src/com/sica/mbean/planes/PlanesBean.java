package com.sica.mbean.planes;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import com.sica.dto.PlanDto;
import com.sica.excepciones.NegocioException;
import com.sica.facade.IFacadeBackend;
import com.sica.mbean.BaseBean;
import com.sica.utiles.Utiles_JPA;
import com.sica.utils.Utiles;



@ManagedBean(name="planesBean")
@RequestScoped
public class PlanesBean extends BaseBean {
	
	private long id;
	private int tipo;
	private String paso;
	private String orden;
	private String descripcion;

	@EJB
	private IFacadeBackend facadeBackend;
	
	@Override
	protected boolean validarCampos() {
		// TODO Auto-generated method stub
		return false;
	}
	
	public void salvarPaso(){
		try{
			PlanDto planDto = new PlanDto();
			planDto.setId(id);
			planDto.setPasos(paso);
			planDto.setOrden(Long.valueOf(orden));
			planDto.setDescripcion(descripcion);
			
			if (Utiles.isNuevo( id )){
				planDto.setId(null);
				if (tipo == Utiles_JPA.PLAN_EMERGENCIA){
					facadeBackend.insertarPlanEmergencia(planDto);
				}
				if (tipo == Utiles_JPA.PLAN_RIESGO){
					facadeBackend.insertarPlanRiesgo(planDto);
				}
			}
			else{
				if (tipo == Utiles_JPA.PLAN_EMERGENCIA){
					facadeBackend.actualizarPlanEmergencia(planDto);
				}
				if (tipo == Utiles_JPA.PLAN_RIESGO){
					facadeBackend.actualizarPlanRiesgo(planDto);
				}				
			}
			
		} 
		catch (NegocioException e) {
			handleServerException(e);
		}
	}


	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getPaso() {
		return paso;
	}

	public void setPaso(String paso) {
		this.paso = paso;
	}

	public int getTipo() {
		return tipo;
	}

	public void setTipo(int tipo) {
		this.tipo = tipo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public void setOrden(String orden) {
		this.orden = orden;
	}

	public String getOrden() {
		return orden;
	}

	
	

}
