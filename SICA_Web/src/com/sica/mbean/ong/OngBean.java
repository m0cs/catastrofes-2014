package com.sica.mbean.ong;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import com.sica.dto.OngDto;
import com.sica.excepciones.NegocioException;
import com.sica.facade.IFacadeBackend;
import com.sica.mbean.BaseBean;
import com.sica.utils.Utiles;

@ManagedBean(name="ongBean")
@RequestScoped
public class OngBean extends BaseBean {

	@EJB
	private IFacadeBackend facadeBackend;
	
	private long id;
	private String nombre;
	private String email;
	private String direccion;
	private String telefono;
	private String sitioWeb;
	
	
	@Override
	protected boolean validarCampos() {
		// TODO Auto-generated method stub
		return false;
	}

	public void salvarOng(){
		try{
			OngDto ongDto = new OngDto();
			ongDto.setId(id);
			ongDto.setNombre(nombre);
			ongDto.setEmail(email);
			ongDto.setDireccion(direccion);
			ongDto.setTelefono(telefono);
			ongDto.setSitioWeb(sitioWeb);
			
			if (Utiles.isNuevo( id )){
				ongDto.setId(null);
				facadeBackend.insertarOng(ongDto);
			}
			else{
				facadeBackend.actualizarOng(ongDto);
			}
			
		} 
		catch (NegocioException e) {
			handleServerException(e);
		}		
	}

	public long getId() {
		return id;
	}


	public void setId(long id) {
		this.id = id;
	}


	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getDireccion() {
		return direccion;
	}


	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}


	public String getTelefono() {
		return telefono;
	}


	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getSitioWeb() {
		return sitioWeb;
	}

	public void setSitioWeb(String sitioWeb) {
		this.sitioWeb = sitioWeb;
	}
	
	

}
