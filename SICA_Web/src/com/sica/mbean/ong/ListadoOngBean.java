package com.sica.mbean.ong;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import com.sica.dto.OngDto;
import com.sica.excepciones.NegocioException;
import com.sica.facade.IFacadeBackend;
import com.sica.mbean.BaseBean;

@ManagedBean(name="listadoOng")
@RequestScoped
public class ListadoOngBean extends BaseBean {

	@EJB
	private IFacadeBackend facadeBackend;
	
	private long idOng;
	
	private List<OngDto> listadoOng = new ArrayList<OngDto>();
	
	@PostConstruct
    public void cargarListas()
    {       
		try {
			listadoOng = facadeBackend.buscarTodasOng();
		} 
		catch (Exception e) {
			handleServerException(e);
		}
    }	
	
	public void eliminarOng(){
		try {
			facadeBackend.eliminarOng(idOng);
			listadoOng = facadeBackend.buscarTodasOng();
		} 
		catch (NegocioException e) {
			handleServerException(e);
		}
	}
	
	@Override
	protected boolean validarCampos() {
		// TODO Auto-generated method stub
		return false;
	}

	public long getIdOng() {
		return idOng;
	}

	public void setIdOng(long idOng) {
		this.idOng = idOng;
	}

	public List<OngDto> getListadoOng() {
		return listadoOng;
	}

	public void setListadoOng(List<OngDto> listadoOng) {
		this.listadoOng = listadoOng;
	}
}
