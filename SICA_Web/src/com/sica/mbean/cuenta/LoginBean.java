package com.sica.mbean.cuenta;



import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import com.sica.dto.LoginDto;
import com.sica.dto.UsuarioDto;
import com.sica.excepciones.NegocioException;
import com.sica.facade.IFacadeBackend;
import com.sica.mbean.BaseBean;
import com.sica.utils.UtilesSeguridad;

@ManagedBean(name="loginBean")
@RequestScoped
public class LoginBean extends BaseBean{	
	
	@EJB
	private IFacadeBackend facadeBackend;
	
	private String email;
	private String password;
	private boolean remember;
	
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}	

	public boolean isRemember() {
		return remember;
	}

	public void setRemember(boolean remember) {
		this.remember = remember;
	}
	
	@PostConstruct
	public void inicializar(){
		LoginDto loginDto = UtilesSeguridad.getLoginCookie();
		if (loginDto.existeLogin()){
			email = loginDto.getEmail();
			password = loginDto.getPassword();
			remember = true;
		}
		else{
			remember = false;
		}
	}

	public String iniciarSesion(){
		try{
			
//			LoginDto loginDto = new LoginDto();
			
			UsuarioDto user = facadeBackend.buscarPorEmailAndPassword(email, password);
			UtilesSeguridad.saveAuthUser(user);

			if (isRemember()){
				UtilesSeguridad.setRememberMe(getEmail(), getPassword());
			}
			else{
				UtilesSeguridad.limpiarCookie();
			}
			
			return SUCCESS;
		}
		catch(NegocioException e){
			addBeanError("loginForm:email", "");
			addBeanError("loginForm:password", e.getMessage());
			return ERROR;
		}
		catch( Exception e ){
            handleServerException( e );
            return ERROR;			
		}
	}
	
	public String cerrarSesion(){
		UtilesSeguridad.closeSession();
		return TO_INDEX;
	}
	
	
	public boolean validarCampos(){
		return true;
	}
}
