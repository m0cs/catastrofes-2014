package com.sica.mbean.rescatista;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import com.sica.dto.RescatistaDto;
import com.sica.excepciones.NegocioException;
import com.sica.facade.IFacadeBackend;
import com.sica.mbean.BaseBean;
import com.sica.utils.Utiles;

@ManagedBean(name="rescatistaBean")
@RequestScoped
public class RescatistaBean extends BaseBean {

	@EJB
	private IFacadeBackend facadeBackend;
	
	private String id;
	private String nombre;
	private String apellido;
	private String email;
	private String nick;
	private String password;
	private String repassword;
	private String fechaNacimiento;
	private String celular;
	private String sexo;
	private boolean isdisponible;
	private String descripcion;
	
	@Override
	protected boolean validarCampos() {
		// TODO Auto-generated method stub
		return false;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNick() {
		return nick;
	}

	public void setNick(String nick) {
		this.nick = nick;
	}
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRepassword() {
		return repassword;
	}

	public void setRepassword(String repassword) {
		this.repassword = repassword;
	}

	public String getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(String fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public String getCelular() {
		return celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public boolean isIsdisponible() {
		return isdisponible;
	}

	public void setIsdisponible(boolean isdisponible) {
		this.isdisponible = isdisponible;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public void salvarRescatista(){
		try{
			RescatistaDto resDto = new RescatistaDto();
			if (!Utiles.isNullOrEmpty(id)){
				resDto.setId(Long.valueOf(id));
			}
			resDto.setNombre(nombre);
			resDto.setApellido(apellido);
			resDto.setEmail(email);
			resDto.setNick(nick);
			resDto.setPassword(password);
			resDto.setFechaNacimiento(fechaNacimiento);
			resDto.setCelular(celular);
			resDto.setSexo(sexo);
			
			if (Utiles.isNullOrEmpty( id )){
				resDto.setId( null );
				facadeBackend.insertarRescatista(resDto);
			}
			else{
				facadeBackend.actualizarRescatista(resDto);
			}
			
		} 
		catch (NegocioException e) {
			handleServerException(e);
		}	
	}

}
