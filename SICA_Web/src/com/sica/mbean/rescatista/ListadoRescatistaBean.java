package com.sica.mbean.rescatista;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import com.sica.dto.RescatistaDto;
import com.sica.excepciones.NegocioException;
import com.sica.facade.IFacadeBackend;
import com.sica.mbean.BaseBean;

@ManagedBean(name="listadoRescatista")
@RequestScoped
public class ListadoRescatistaBean extends BaseBean {

	@EJB
	private IFacadeBackend facadeBackend;
	
	private long idRescatista;
	
	private List<RescatistaDto> listadoRescatista = new ArrayList<RescatistaDto>();
	
	
	@PostConstruct
    public void cargarListas()
    {       
		try {
			listadoRescatista = facadeBackend.buscarTodosRescatista();
		} 
		catch (Exception e) {
			handleServerException(e);
		}
    }	  
	
	@Override
	protected boolean validarCampos() {
		// TODO Auto-generated method stub
		return false;
	}

	public long getIdRescatista() {
		return idRescatista;
	}

	public void setIdRescatista(long idRescatista) {
		this.idRescatista = idRescatista;
	}

	public List<RescatistaDto> getListadoRescatista() {
		return listadoRescatista;
	}

	public void setListadoRescatista(List<RescatistaDto> listadoRescatista) {
		this.listadoRescatista = listadoRescatista;
	}
	
	public void eliminarRescatista(){
		try {
			facadeBackend.eliminarRescatista( idRescatista );
			listadoRescatista = facadeBackend.buscarTodosRescatista();
		} 
		catch (NegocioException e) {
			handleServerException(e);
		}
	}
}
