package com.sica.utils;

import java.util.List;

import com.sica.dto.PlanRescatistaDto;

public class Utiles {

    public static boolean isNullOrEmpty(String str){
    	return (str == null || str.isEmpty());
    }

    public static boolean isNuevo(long id){
    	return ( id == 0 );
    }
    
    public static boolean isNullOrCero(Long lng){
    	return (lng == null || lng.equals(0L));
    }
    
    public static Long getIdPlanPorRescatista(List<PlanRescatistaDto> planes, long idRescatista){
    	for (PlanRescatistaDto plan : planes){
    		if (plan.getIdRescatista() != null && plan.getIdRescatista().longValue() == idRescatista ){
    			return plan.getId();
    		}
    	}
    	return null;
    }
}
