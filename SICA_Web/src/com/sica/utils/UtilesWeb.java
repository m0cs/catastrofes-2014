package com.sica.utils;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import com.sica.dto.UsuarioDto;

@ManagedBean(name="sessionBean")
@SessionScoped
public class UtilesWeb {

	/**
	 * Verifica si existe un usuario autenticados.
	 * */
    public static boolean isAuthUser(){
    	return UtilesSeguridad.isAuthUser();
    }	
    
    /**
     * Obtiene usuario autenticado
     * */
    public static UsuarioDto getAuthUser(){
    	return UtilesSeguridad.getAuthUser();
    }

}
