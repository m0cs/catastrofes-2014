package com.sica.utils;

import javax.faces.context.FacesContext;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sica.dto.LoginDto;
import com.sica.dto.UsuarioDto;

public class UtilesSeguridad {
	
	
	//Constantes
	public static final String KEY_ADMIN_AUTENTICADO = "usuario";
	public static final String EMAIL_COOKIE = "email_cookie";
	public static final String PASS_COOKIE = "pass_cookie";
	public static final String REM_COOKIE = "rem_cookie";
	public static final int MAX_AGE_COOKIE = 3600;
	
	
    // Guarda usuario autenticado
    public static void saveAuthUser(UsuarioDto usuario){
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put(KEY_ADMIN_AUTENTICADO, usuario);
    }
    
    //verifica que el usuario este autenticado.
    public static boolean isAuthUser(){
    	
    	  return FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get(KEY_ADMIN_AUTENTICADO) != null;
    }
    
    //buscar usuario autenticado
    public static UsuarioDto getAuthUser(){
    	
    	return (UsuarioDto) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get(KEY_ADMIN_AUTENTICADO);
    }
    
    //borra usuario autenticado
    public static void closeSession(){
    	
    	FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
    }
    
    

    public static void setRememberMe( String email, String password ){

	    // Save the userid and password in a cookie
	    Cookie btuser = new Cookie(EMAIL_COOKIE, email);
	    Cookie btpasswd = new Cookie(PASS_COOKIE,password);
	    Cookie btremember = new Cookie(REM_COOKIE, "true");
	    btuser.setMaxAge(MAX_AGE_COOKIE);
	    btpasswd.setMaxAge(MAX_AGE_COOKIE);
	    btremember.setMaxAge(MAX_AGE_COOKIE);
	    
	    ((HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse()).addCookie(btuser);
	    ((HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse()).addCookie(btpasswd);
	    ((HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse()).addCookie(btremember);
    }
    
	public static LoginDto getLoginCookie() {

		FacesContext facesContext = FacesContext.getCurrentInstance();
		String cookieName = null;
		Cookie cookie[] = ((HttpServletRequest) facesContext.getExternalContext().getRequest()).getCookies();
		LoginDto loginDto = new LoginDto();
		
		if (cookie != null && cookie.length > 0) {

			for (int i = 0; i < cookie.length; i++) {
				cookieName = cookie[i].getName();

				if (cookieName.equals(EMAIL_COOKIE)) {
					loginDto.setEmail(cookie[i].getValue());
				} 
				else if (cookieName.equals(PASS_COOKIE)) {
					loginDto.setPassword(cookie[i].getValue());
				} 
			}
		}
		return loginDto;
	}
	
	public static void limpiarCookie(){
		
	    // Save the userid and password in a cookie
	    Cookie btuser = new Cookie(EMAIL_COOKIE, null);
	    Cookie btpasswd = new Cookie(PASS_COOKIE,null);
	    Cookie btremember = new Cookie(REM_COOKIE, "false");
	    btuser.setMaxAge(0);
	    btpasswd.setMaxAge(0);
	    btremember.setMaxAge(0);
	    
	    ((HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse()).addCookie(btuser);
	    ((HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse()).addCookie(btpasswd);
	    ((HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse()).addCookie(btremember);
	}
}
