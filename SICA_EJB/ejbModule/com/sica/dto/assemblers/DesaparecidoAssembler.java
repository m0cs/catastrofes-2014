package com.sica.dto.assemblers;

import java.text.ParseException;
import java.util.ArrayList;

import com.sica.dto.DesaparecidoDto;
import com.sica.entities.Desaparecido;
import com.sica.entities.Imagen;
import com.sica.utiles.Utiles_SICA;

public class DesaparecidoAssembler extends AbstractAssembler<Desaparecido, DesaparecidoDto> {

	private static DesaparecidoAssembler instance = null;
	
	public static DesaparecidoAssembler getInstance(){
		if (instance == null){
			instance = new DesaparecidoAssembler();
		}
		return instance;
	}	
	
	@Override
	public void toEntity(DesaparecidoDto source, Desaparecido target) {
		try {
			target.setId(source.getId());
			target.setNombre(source.getNombre());
			target.setApellido(source.getApellido());
			target.setEdad(source.getEdad());
			target.setEtnia(source.getEtnia());
			target.setColorOjos(source.getColorOjos());
			target.setColorPiel(source.getColorPiel());
			target.setEstatura( Double.valueOf(source.getEstatura()) );
			target.setEstadoDesaparecido(source.getEstado());
			target.setDetalles(source.getDetalles());
		
			if ( !Utiles_SICA.isNullOrEmpty(source.getFechaDesaparecido())){
				target.setFechaDesaparecido(Utiles_SICA.DATE_FORMAT_MIN.parse(source.getFechaDesaparecido()));
			}
		} 
		catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
		}
	}

	@Override
	public void toDto(Desaparecido source, DesaparecidoDto target) {
		
		target.setId(source.getId());
		target.setNombre(source.getNombre());
		target.setApellido(source.getApellido());
		target.setEdad(source.getEdad());
		target.setEtnia(source.getEtnia());
		target.setColorOjos(source.getColorOjos());
		target.setColorPiel(source.getColorPiel());
		target.setEstatura(String.valueOf(source.getEstatura()));
		target.setEstado(source.getEstadoDesaparecido());
		target.setDetalles(source.getDetalles());
		
		if (source.getFechaDesaparecido() != null){
			target.setFechaDesaparecido(Utiles_SICA.DATE_FORMAT_MIN.format(source.getFechaDesaparecido()));
		}			
		
		if (source.getUsuario() != null){
			target.setEmailUsuario(source.getUsuario().getEmail());
		}
		
		//lista de imagenes
		if (source.getImagens() != null){
			target.setIdImagenes(new ArrayList<Long>());
			for (Imagen imagen : source.getImagens()){
				target.getIdImagenes().add( imagen.getId() );
			}
		}
	}



}
