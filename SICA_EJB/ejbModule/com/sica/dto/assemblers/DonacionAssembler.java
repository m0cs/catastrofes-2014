package com.sica.dto.assemblers;

import com.sica.dto.DonacionDto;
import com.sica.entities.Donacion;

public class DonacionAssembler extends AbstractAssembler<Donacion, DonacionDto> {

	private static DonacionAssembler instance = null;
	
	public static DonacionAssembler getInstance(){
		if (instance == null){
			instance = new DonacionAssembler();
		}
		return instance;
	}
	
	private DonacionAssembler() {}	
	
	@Override
	public void toEntity(DonacionDto source, Donacion target) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void toDto(Donacion source, DonacionDto target) {
		// TODO Auto-generated method stub			
		
	}

}
