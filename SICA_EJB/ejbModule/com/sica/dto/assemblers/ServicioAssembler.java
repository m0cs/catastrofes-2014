package com.sica.dto.assemblers;

import java.text.ParseException;

import com.sica.dto.ServicioDto;
import com.sica.entities.Servicio;
import com.sica.utiles.Utiles_SICA;

public class ServicioAssembler extends AbstractAssembler<Servicio, ServicioDto > {

	private static ServicioAssembler instance = null;
	
	public static ServicioAssembler getInstance(){
		if (instance == null){
			instance = new ServicioAssembler();
		}
		return instance;
	}
	
	private ServicioAssembler() {}
	
	@Override
	public void toEntity(ServicioDto source, Servicio target) {
		try{
			target.setId(source.getId());
			target.setEmail(source.getEmail());
			
			if (!Utiles_SICA.isNullOrEmpty(source.getFecha())){
				target.setFecha(Utiles_SICA.DATE_FORMAT_MIN.parse(source.getFecha() ) );
			}	
			target.setNombre(source.getNombre());
			target.setTipo(source.getTipo());	
			target.setTelefono(source.getTelefono());
			target.setCantidad(source.getCantidad());
			target.setProfesion(source.getProfesion());
		}
		catch (ParseException e) {
			e.printStackTrace();
		}				
	}

	@Override
	public void toDto(Servicio source, ServicioDto target) {
		
		target.setId(source.getId());
		target.setEmail(source.getEmail());

		if (source.getFecha() != null){
			target.setFecha(Utiles_SICA.DATE_FORMAT_MIN.format(source.getFecha()));
		}	
		target.setNombre(source.getNombre());
		target.setTipo(source.getTipo());	
		target.setTelefono(source.getTelefono());
		target.setCantidad(source.getCantidad());		
		target.setProfesion(source.getProfesion());
	}
}
