package com.sica.dto.assemblers;

import java.text.ParseException;
import java.util.ArrayList;

import com.sica.dto.BienDto;
import com.sica.entities.Articulo;
import com.sica.entities.Bien;
import com.sica.utiles.Utiles_SICA;

public class BienAssembler extends AbstractAssembler<Bien, BienDto> {

	
	private static BienAssembler instance = null;
	
	public static BienAssembler getInstance(){
		if (instance == null){
			instance = new BienAssembler();
		}
		return instance;
	}
	
	private BienAssembler() {}
	
	
	@Override
	public void toEntity(BienDto source, Bien target) {
		try{
			target.setId(source.getId());
			target.setEmail(source.getEmail());
			
			if (!Utiles_SICA.isNullOrEmpty(source.getFecha())){
				target.setFecha(Utiles_SICA.DATE_FORMAT_MIN.parse(source.getFecha() ) );
			}	
			target.setNombre(source.getNombre());
			target.setTipo(source.getTipo());	
			target.setTelefono(source.getTelefono());
			target.setDirOrigen(source.getDirOrigen());
			target.setRetirar(source.getRetirar());
			
			if (target.getArticulos() == null){
				target.setArticulos(new ArrayList<Articulo>());
			}
			if (source.getArticulos() != null){
				target.setArticulos( ArticuloAssembler.getInstance().toEntityList(source.getArticulos() ));
			}			
			
		}
		catch (ParseException e) {
			e.printStackTrace();
		}		
	}

	@Override
	public void toDto(Bien source, BienDto target) { 
		
		target.setId(source.getId());
		target.setEmail(source.getEmail());
		
		if (source.getFecha() != null){
			target.setFecha(Utiles_SICA.DATE_FORMAT_MIN.format(source.getFecha()));
		}			
		
		target.setNombre(source.getNombre());
		target.setTipo(source.getTipo());	
		target.setTelefono(source.getTelefono());
		if (source.getOng() != null){
			target.setId(source.getOng().getId());
			target.setNombreOng(source.getOng().getNombre());
		}
		target.setDirOrigen(source.getDirOrigen());
		target.setRetirar(source.getRetirar());
		
		if (source.getArticulos() != null){ 
			target.setArticulos(ArticuloAssembler.getInstance().toDtoList( source.getArticulos() ));
		}		
	}

}
