package com.sica.dto.assemblers;

import com.sica.dto.ImagenDto;
import com.sica.entities.Imagen;

public class ImagenAssembler extends AbstractAssembler<Imagen, ImagenDto> {

	private static ImagenAssembler instance = null;
	
	public static ImagenAssembler getInstance(){
		if (instance == null){
			instance = new ImagenAssembler();
		}
		return instance;
	}
	
	private ImagenAssembler() {}	
	
	@Override
	public void toEntity(ImagenDto source, Imagen target) {
		target.setId(source.getId());
		target.setRuta(source.getRuta());
		
	}

	@Override
	public void toDto(Imagen source, ImagenDto target) {
		target.setId(source.getId());
		target.setRuta(source.getRuta());
		
	}



}
