package com.sica.dto.assemblers;

import java.text.ParseException;
import java.util.ArrayList;

import com.sica.dto.PlanRescatistaDto;
import com.sica.dto.RescatistaDto;
import com.sica.entities.PlanRescatista;
import com.sica.entities.Rescatista;
import com.sica.utiles.Utiles_JPA;
import com.sica.utiles.Utiles_SICA;

public class RescatistaAssembler extends AbstractAssembler<Rescatista, RescatistaDto> {

	private static RescatistaAssembler instance = null;
	
	public static RescatistaAssembler getInstance(){
		if (instance == null){
			instance = new RescatistaAssembler();
		}
		return instance;
	}
	
	@Override
	public void toEntity(RescatistaDto source, Rescatista target) {
		try{
			target.setId(source.getId());
			target.setNombre(source.getNombre());
			target.setApellido(source.getApellido());
			target.setEmail(source.getEmail());
			target.setNick(source.getNick());
			target.setPassword(source.getPassword());
			target.setCelular(source.getCelular());
			target.setTipo( Utiles_JPA.RESCATISTA );
			target.setIsDisponible(source.getIsDisponible());
			target.setDescripcion(source.getDescripcion());
			
			if (!Utiles_SICA.isNullOrEmpty(source.getFechaNacimiento())){
				target.setFechaNacimiento(Utiles_SICA.DATE_FORMAT_MIN.parse(source.getFechaNacimiento() ) );
			}		
			
			if (target.getPlanesRescatistas() == null){
				target.setPlanesRescatistas(new ArrayList<PlanRescatista>());
			}
			if (source.getPlanesRescatistasDto() != null){
				target.setPlanesRescatistas( PlanRescatistaAssembler.getInstance().toEntityList(source.getPlanesRescatistasDto()));
			}
		}
		catch (ParseException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void toDto(Rescatista source, RescatistaDto target) {
		target.setId(source.getId());
		target.setNombre(source.getNombre());
		target.setApellido(source.getApellido());
		target.setEmail(source.getEmail());
		target.setNick(source.getNick());
		target.setPassword(source.getPassword());
		target.setTipo(source.getTipo());
		target.setCelular(source.getCelular());
		target.setDescripcion(source.getDescripcion());
		
		if (source.getFechaNacimiento() != null){
			target.setFechaNacimiento(Utiles_SICA.DATE_FORMAT_MIN.format(source.getFechaNacimiento()));
		}		
		
		target.setIsDisponible(source.getIsDisponible());
		if (target.getPlanesRescatistasDto() == null){
			target.setPlanesRescatistasDto(new ArrayList<PlanRescatistaDto>());
		}
		if (source.getPlanesRescatistas() != null){
			target.setPlanesRescatistasDto( PlanRescatistaAssembler.getInstance().toDtoList(source.getPlanesRescatistas()));
		}
	}



}
