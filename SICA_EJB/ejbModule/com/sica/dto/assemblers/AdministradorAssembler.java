package com.sica.dto.assemblers;

import java.text.ParseException;

import com.sica.dto.AdministradorDto;
import com.sica.entities.Administrador;
import com.sica.utiles.Utiles_JPA;
import com.sica.utiles.Utiles_SICA;

public class AdministradorAssembler extends AbstractAssembler<Administrador, AdministradorDto> {

	private static AdministradorAssembler instance = null;
	
	public static AdministradorAssembler getInstance(){
		if (instance == null){
			instance = new AdministradorAssembler();
		}
		return instance;
	}	

	@Override
	public void toEntity(AdministradorDto source, Administrador target) {
		try {
			target.setId(source.getId());
			target.setNombre(source.getNombre());
			target.setApellido(source.getApellido());
			target.setEmail(source.getEmail());
			target.setNick(source.getNick());
			target.setPassword(source.getPassword());
			target.setTipo(Utiles_JPA.ADMINISTRADOR);
			target.setSexo(source.getSexo());
			target.setCelular(source.getCelular());
			
			if (!Utiles_SICA.isNullOrEmpty(source.getFechaNacimiento())){
				target.setFechaNacimiento(Utiles_SICA.DATE_FORMAT_MIN.parse(source.getFechaNacimiento() ) );
			}			
		} 
		catch (ParseException e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public void toDto(Administrador source, AdministradorDto target) {
		
		target.setId(source.getId());
		target.setNombre(source.getNombre());
		target.setApellido(source.getApellido());
		target.setEmail(source.getEmail());
		target.setNick(source.getNick());
		target.setPassword(source.getPassword());
		target.setTipo(Utiles_JPA.ADMINISTRADOR);	
		target.setSexo(source.getSexo());
		target.setCelular(source.getCelular());
		
		if (source.getFechaNacimiento() != null){
			target.setFechaNacimiento(Utiles_SICA.DATE_FORMAT_MIN.format(source.getFechaNacimiento()));
		}
	}

}
