package com.sica.dto.assemblers;

import java.text.ParseException;

import com.sica.dto.UsuarioDto;
import com.sica.entities.Usuario;
import com.sica.utiles.Utiles_SICA;



public class UsuarioAssembler extends AbstractAssembler<Usuario, UsuarioDto> {

	private static UsuarioAssembler instance = null;
	
	public static UsuarioAssembler getInstance(){
		if (instance == null){
			instance = new UsuarioAssembler();
		}
		return instance;
	}
	
	private UsuarioAssembler() {}
	
	
	@Override
	public void toEntity(UsuarioDto source, Usuario target) {
		try{
			target.setId(source.getId());
			target.setNombre(source.getNombre());
			target.setApellido(source.getApellido());
			target.setEmail(source.getEmail());
			if (!Utiles_SICA.isNullOrEmpty(source.getFechaNacimiento())){
				target.setFechaNacimiento(Utiles_SICA.DATE_FORMAT_MIN.parse(source.getFechaNacimiento() ) );
			}	
			target.setNick(source.getNick());
			target.setPassword(source.getPassword());
			target.setTipo(source.getTipo());
			target.setIsActivo(source.getIsActivo());
			target.setSexo(source.getSexo());
			target.setCelular(source.getCelular());
		}
		catch (ParseException e) {
			e.printStackTrace();
		}		
	}

	@Override
	public void toDto(Usuario source, UsuarioDto target) {
		target.setId(source.getId());
		target.setNombre(source.getNombre());
		target.setApellido(source.getApellido());
		target.setEmail(source.getEmail());
		if (source.getFechaNacimiento() != null){
			target.setFechaNacimiento(Utiles_SICA.DATE_FORMAT_MIN.format(source.getFechaNacimiento() ) );
		}	
		target.setNick(source.getNick());
		target.setPassword(source.getPassword());
		target.setTipo(source.getTipo());		
		target.setIsActivo(source.getIsActivo());
		target.setSexo(source.getSexo());
		target.setCelular(source.getCelular());
	}	

}
