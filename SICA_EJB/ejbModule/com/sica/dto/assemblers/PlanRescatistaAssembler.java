package com.sica.dto.assemblers;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import com.sica.dto.PlanDto;
import com.sica.dto.PlanRescatistaDto;
import com.sica.entities.PlanRescatista;
import com.sica.utiles.Utiles_SICA;

public class PlanRescatistaAssembler  extends AbstractAssembler<PlanRescatista, PlanRescatistaDto> {

	private static PlanRescatistaAssembler instance = null;
	
	public static PlanRescatistaAssembler getInstance(){
		if (instance == null){
			instance = new PlanRescatistaAssembler();
		}
		return instance;
	}	
	
	@Override
	public void toEntity(PlanRescatistaDto source, PlanRescatista target) {
		try{
			target.setId( source.getId() );
			target.setFecha( Utiles_SICA.DATE_FORMAT_MIN.parse(source.getFecha()) );
			target.setIsCompletado(source.getIs_completado());
			target.setRadio(source.getRadio());
		}
		catch (ParseException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void toDto(PlanRescatista source, PlanRescatistaDto target) {
			target.setId(source.getId());
			target.setFecha(Utiles_SICA.DATE_FORMAT_MIN.format(source.getFecha()));
			target.setIs_completado(source.getIsCompletado());
			target.setRadio(source.getRadio());
			
			if (source.getEventoCatastrofe() != null){
				target.setIdEventoCatastrofe(source.getEventoCatastrofe().getId());
				target.setNombreEventoCatastrofe(source.getEventoCatastrofe().getNombre());
			}
			
			if (source.getRescatista() != null){
				target.setIdRescatista(source.getRescatista().getId());
				target.setNombreRescatista(source.getRescatista().getNombre());
			}
			if (source.getPlanesEmergencia() != null){
				List<PlanDto> planDto = new ArrayList<PlanDto>();
				planDto =  PlanEmergenciaAssembler.getInstance().toDtoList(source.getPlanesEmergencia());
				target.setPlanesEmergencia(planDto);
					
			}
			
			if (source.getPlanesRiesgo() != null	){
				List<PlanDto> fDto = new ArrayList<PlanDto>();
				fDto =  PlanRiesgoAssembler.getInstance().toDtoList(source.getPlanesRiesgo());
				target.setPlanesRiesgo(fDto);
			}			
	}

}
