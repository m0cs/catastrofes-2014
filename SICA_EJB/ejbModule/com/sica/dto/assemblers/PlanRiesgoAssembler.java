package com.sica.dto.assemblers;

import com.sica.dto.PlanDto;
import com.sica.entities.PlanRiesgo;
import com.sica.utiles.Utiles_JPA;

public class PlanRiesgoAssembler extends AbstractAssembler<PlanRiesgo, PlanDto> {

	private static PlanRiesgoAssembler instance = null;
	
	public static PlanRiesgoAssembler getInstance(){
		if (instance == null){
			instance = new PlanRiesgoAssembler();
		}
		return instance;
	}
	
	@Override
	public void toEntity(PlanDto source, PlanRiesgo target) {
		target.setId(source.getId());
		target.setOrden(source.getOrden());
		target.setDescripcion(source.getDescripcion());
		target.setPasos(source.getPasos());
	}

	@Override
	public void toDto(PlanRiesgo source, PlanDto target) {
		target.setId(source.getId());
		target.setOrden(source.getOrden());
		target.setDescripcion(source.getDescripcion());
		target.setPasos(source.getPasos());
		target.setTipoPlan(Utiles_JPA.PLAN_RIESGO);
	}

}
