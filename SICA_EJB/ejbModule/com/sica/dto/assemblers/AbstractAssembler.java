package com.sica.dto.assemblers;

import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.List;

import com.sica.utiles.IUtilTransformer;

public abstract class AbstractAssembler<E, Dto> implements IUtilTransformer<E, Dto> {

	protected Class<E> entityClass = getEntityClass();
	protected Class<Dto> dtoClass = getDtoClass();
	
	public E toEntity(Dto source) {
		try {
			E target = entityClass.newInstance();
			toEntity(source, target);
			return target;
		} 
		catch (InstantiationException | IllegalAccessException e) {
			//logger;
			return null;
		}
	}


	public Dto toDto(E source) {
		
		try {
			Dto target = dtoClass.newInstance();
			toDto(source, target);
			return target;
		} 
		catch (InstantiationException | IllegalAccessException e) {
			return null;
		}
	}	
	

	public List<E> toEntityList(List<Dto> listaDtos) {
		
		List<E> listaEntities = new ArrayList<E>();

		for (Dto dto : listaDtos) {
			listaEntities.add(toEntity(dto));
		}

		return listaEntities;
	}

	public List<Dto> toDtoList(List<E> listaEntities) {
		
		List<Dto> listaDtos = new ArrayList<Dto>();

		for (E entity : listaEntities) {
			listaDtos.add(toDto(entity));
		}
		return listaDtos;
	}	
	
    @SuppressWarnings("unchecked")
	private Class<E> getEntityClass() {
            if (entityClass == null) {
                    ParameterizedType thisType = (ParameterizedType) getClass().getGenericSuperclass();
                    entityClass = (Class<E>) thisType.getActualTypeArguments()[0];
            }
            
            return entityClass;
    }
    
    /**
     * Recupera la clase del DtoType
     * @return
     */
    @SuppressWarnings("unchecked")
	private Class<Dto> getDtoClass() {
            if (dtoClass == null)            {
                    ParameterizedType thisType = (ParameterizedType) getClass().getGenericSuperclass();
                    dtoClass = (Class<Dto>) thisType.getActualTypeArguments()[1];
            }
            return dtoClass;
    }	
	
}
