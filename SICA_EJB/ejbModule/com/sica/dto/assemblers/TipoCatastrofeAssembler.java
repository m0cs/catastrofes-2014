package com.sica.dto.assemblers;

import com.sica.dto.TipoCatastrofeDto;
import com.sica.entities.TipoCatastrofe;
import com.sica.excepciones.NegocioException;
import com.sica.utiles.Utiles_SICA;

public class TipoCatastrofeAssembler extends AbstractAssembler<TipoCatastrofe, TipoCatastrofeDto> {

	private static TipoCatastrofeAssembler instance = null;
	
	public static TipoCatastrofeAssembler getInstance(){
		if (instance == null){
			instance = new TipoCatastrofeAssembler();
		}
		return instance;
	}	
	
	@Override
	public void toEntity(TipoCatastrofeDto source, TipoCatastrofe target) {
		try {
			target.setId(source.getId());
			target.setNombre(source.getNombre());
			target.setEstilo( Utiles_SICA.stringToBytea(source.getEstilo()));
		} 
		catch (NegocioException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void toDto(TipoCatastrofe source, TipoCatastrofeDto target) {
		target.setId(source.getId());
		target.setNombre(source.getNombre());
		target.setEstilo(Utiles_SICA.byteaToString( source.getEstilo() ));	
	}

}
