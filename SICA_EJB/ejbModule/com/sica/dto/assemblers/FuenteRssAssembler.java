package com.sica.dto.assemblers;

import com.sica.dto.FuenteRssDto;
import com.sica.entities.FuenteRss;

public class FuenteRssAssembler extends AbstractAssembler<FuenteRss, FuenteRssDto> {


	private static FuenteRssAssembler instance = null;
	
	public static FuenteRssAssembler getInstance(){
		if (instance == null){
			instance = new FuenteRssAssembler();
		}
		return instance;
	}
	
	private FuenteRssAssembler() {}
	
	@Override
	public void toEntity(FuenteRssDto source, FuenteRss target) {
		target.setId(source.getId());
		target.setNombre(source.getNombre());
		target.setUrl(source.getUrl());
	}

	@Override
	public void toDto(FuenteRss source, FuenteRssDto target) {
		target.setId(source.getId());
		target.setNombre(source.getNombre());
		target.setUrl(source.getUrl());		
	}

}
