package com.sica.dto.assemblers;

import com.sica.dto.ArticuloDto;
import com.sica.entities.Articulo;

public class ArticuloAssembler extends AbstractAssembler<Articulo, ArticuloDto> {

	
	private static ArticuloAssembler instance = null;
	
	public static ArticuloAssembler getInstance(){
		if (instance == null){
			instance = new ArticuloAssembler();
		}
		return instance;
	}
	 
	private ArticuloAssembler() {}	
	
	@Override
	public void toEntity(ArticuloDto source, Articulo target) {
		target.setId(source.getId());
		target.setCantidad(source.getCantidad());
		target.setNombre(source.getNombre());		
	}

	@Override
	public void toDto(Articulo source, ArticuloDto target) {
		target.setId(source.getId());
		target.setCantidad(source.getCantidad());
		target.setNombre(source.getNombre());	
	}

}
