package com.sica.dto.assemblers;

import com.sica.dto.OngDto;
import com.sica.entities.Ong;

public class OngAssembler extends AbstractAssembler<Ong, OngDto> {

	
	private static OngAssembler instance = null;
	
	public static OngAssembler getInstance(){
		if (instance == null){
			instance = new OngAssembler();
		}
		return instance;
	}	
	
	@Override
	public void toEntity(OngDto source, Ong target) {
		target.setId(source.getId());
		target.setNombre(source.getNombre());
		target.setEmail(source.getEmail());
		target.setDireccion(source.getDireccion());
		target.setSitioWeb(source.getSitioWeb());
		target.setTelefono(source.getTelefono());	
	}

	@Override
	public void toDto(Ong source, OngDto target) {
		target.setId(source.getId());
		target.setNombre(source.getNombre());
		target.setEmail(source.getEmail());
		target.setDireccion(source.getDireccion());
		target.setSitioWeb(source.getSitioWeb());
		target.setTelefono(source.getTelefono());
		
	}

}
