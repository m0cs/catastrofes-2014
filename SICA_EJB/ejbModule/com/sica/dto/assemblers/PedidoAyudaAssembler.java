package com.sica.dto.assemblers;

import java.text.ParseException;
import java.util.ArrayList;

import com.sica.dto.PedidoAyudaDto;
import com.sica.entities.PedidoAyuda;
import com.sica.entities.Recurso;
import com.sica.utiles.Utiles_SICA;

public class PedidoAyudaAssembler extends AbstractAssembler<PedidoAyuda, PedidoAyudaDto> {

	private static PedidoAyudaAssembler instance = null; 
	
	public static PedidoAyudaAssembler getInstance(){
		if (instance == null){
			instance = new PedidoAyudaAssembler();
		}
		return instance;
	}

	@Override
	public void toEntity(PedidoAyudaDto source, PedidoAyuda target) {
		try{
			target.setId(source.getId());
			target.setDescripcion(source.getDescripcion());
			target.setIsHabilitado(source.getIsHabilitado());
			target.setLatLong(source.getLatLong());
			
			if (!Utiles_SICA.isNullOrEmpty(source.getFecha())){
				target.setFecha(Utiles_SICA.DATE_FORMAT_MIN.parse(source.getFecha() ) );
			}		
			
		}
		catch (ParseException e) {
			e.printStackTrace();
		}			
		
	}

	@Override
	public void toDto(PedidoAyuda source, PedidoAyudaDto target) {
		
		target.setId(source.getId());
		target.setDescripcion(source.getDescripcion());
		target.setIsHabilitado(source.getIsHabilitado());
		target.setLatLong(source.getLatLong());
		
		if (source.getEstadoUsuario() != null){
			target.setIdEstadoUsuario(String.valueOf(source.getEstadoUsuario().getId()));
			target.setNombreEstadoUsuario(source.getEstadoUsuario().getCondicion());
		}
		
		if (source.getUsuario() != null){
			target.setIdUsuario(String.valueOf(source.getUsuario().getId()));
			target.setEmailUsuario(source.getUsuario().getEmail());
		}
		
		if ( source.getRecursos() != null ){ 
			target.setIdRecursos(new ArrayList<Long>());
			for ( Recurso recurso : source.getRecursos() ){
				target.getIdRecursos().add(recurso.getId());
			}
		}
	}
}
