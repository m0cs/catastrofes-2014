package com.sica.dto.assemblers;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import com.sica.dto.EventoCatastrofeDto;
import com.sica.dto.FuenteRssDto;
import com.sica.dto.ImagenDto;
import com.sica.dto.OngDto;
import com.sica.dto.PlanRescatistaDto;
import com.sica.entities.EventoCatastrofe;
import com.sica.utiles.Utiles_SICA;

public class EventoCatastrofeAssembler  extends AbstractAssembler<EventoCatastrofe, EventoCatastrofeDto> {


	private static EventoCatastrofeAssembler instance = null;
	
	public static EventoCatastrofeAssembler getInstance(){
		if (instance == null){
			instance = new EventoCatastrofeAssembler();
		}
		return instance;
	}	
	
	@Override
	public void toEntity(EventoCatastrofeDto source, EventoCatastrofe target) {

		try {
			target.setId(source.getId());
			target.setDescripcion(source.getDescripcion());
			target.setDistancia(source.getDistancia());
			target.setEstado(source.getEstado());
			target.setMagnitud(source.getMagnitud());
			target.setNombre(source.getNombre());
			target.setUbicacion(source.getLat() + "," +  source.getLon());
			
			if (!Utiles_SICA.isNullOrEmpty(source.getFechaComienzo())){
				target.setFechaComienzo(Utiles_SICA.DATE_FORMAT_MIN.parse(source.getFechaComienzo() ) );
			}		
		
			if (!Utiles_SICA.isNullOrEmpty(source.getFechaFin())){
					target.setFechaFin(Utiles_SICA.DATE_FORMAT_MIN.parse(source.getFechaFin() ) );
			}
		}
		catch (ParseException e) {
			//no se hace ya que se controla previamente
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		

		
	}

	@Override
	public void toDto(EventoCatastrofe source, EventoCatastrofeDto target) {	
		
		target.setId(source.getId());
		target.setDescripcion(source.getDescripcion());
		target.setDistancia(source.getDistancia());
		target.setEstado(source.getEstado());
		target.setMagnitud(source.getMagnitud());
		target.setNombre(source.getNombre());
		target.setUbicacion(source.getUbicacion());
		
		if (source.getUbicacion() != null){
			target.setLat(source.getUbicacion().split(",")[0]);
			target.setLon(source.getUbicacion().split(",")[1]);
		}
		
		if (source.getTipoCatastrofe() != null){
			target.setIdTipo(source.getTipoCatastrofe().getId());
			target.setNombreTipo(source.getTipoCatastrofe().getNombre());
		}
		
		if (source.getFechaComienzo() != null){
			target.setFechaComienzo(Utiles_SICA.DATE_FORMAT_MIN.format(source.getFechaComienzo() ) );
		}		

		if (source.getFechaFin() != null){
				target.setFechaFin(Utiles_SICA.DATE_FORMAT_MIN.format(source.getFechaFin() ) );
		}
		
		if (source.getPlanesRescatistas() != null){
			List<PlanRescatistaDto> resDto = new ArrayList<PlanRescatistaDto>();
			resDto =  PlanRescatistaAssembler.getInstance().toDtoList(source.getPlanesRescatistas());
			target.setPlanesRescatista(resDto);			
		}
		
		if (source.getOngs() != null){
			List<OngDto> ongDto = new ArrayList<OngDto>();
			ongDto = new ArrayList<OngDto>();
			ongDto =  OngAssembler.getInstance().toDtoList(source.getOngs());
			target.setOngs(ongDto);
				
		}
		
		if (source.getFuenteRsses() != null	){
			List<FuenteRssDto> fDto = new ArrayList<FuenteRssDto>();
			fDto = new ArrayList<FuenteRssDto>();
			fDto =  FuenteRssAssembler.getInstance().toDtoList(source.getFuenteRsses());
			target.setFuentes(fDto);
		}
		if (source.getImagenes() != null){
			List<ImagenDto> fDto = new ArrayList<ImagenDto>();
			fDto = new ArrayList<ImagenDto>();
			fDto =  ImagenAssembler.getInstance().toDtoList(source.getImagenes());
			target.setImagenes(fDto);
		}	
		
	}

}
