package com.sica.dto.assemblers;

import com.sica.dto.PlanDto;
import com.sica.entities.PlanEmergencia;
import com.sica.utiles.Utiles_JPA;

public class PlanEmergenciaAssembler extends AbstractAssembler<PlanEmergencia, PlanDto> {

	private static PlanEmergenciaAssembler instance = null;
	
	public static PlanEmergenciaAssembler getInstance(){
		if (instance == null){
			instance = new PlanEmergenciaAssembler();
		}
		return instance;
	}

	@Override
	public void toEntity(PlanDto source, PlanEmergencia target) {
		target.setId(source.getId());
		target.setOrden(source.getOrden());
		target.setDescripcion(source.getDescripcion());
		target.setPasos(source.getPasos());
	}

	@Override
	public void toDto(PlanEmergencia source, PlanDto target) {
		target.setId(source.getId());
		target.setOrden(source.getOrden());
		target.setDescripcion(source.getDescripcion());
		target.setPasos(source.getPasos());
		target.setTipoPlan(Utiles_JPA.PLAN_EMERGENCIA);
	}	
	
	
	
}
