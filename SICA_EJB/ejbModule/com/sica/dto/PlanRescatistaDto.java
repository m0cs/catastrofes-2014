package com.sica.dto;

import java.util.List;




public class PlanRescatistaDto {
	private Long id;
	private String fecha;
	private Boolean is_completado;
	private Integer radio;
	
	private Long idRescatista;
	private String nombreRescatista;
	 
	private Long idEventoCatastrofe;
	private String nombreEventoCatastrofe;
	
	private List<PlanDto> planesEmergencia;
	
	private List<PlanDto> planesRiesgo;	

	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public Boolean getIs_completado() {
		return is_completado;
	}
	public void setIs_completado(Boolean is_completado) {
		this.is_completado = is_completado;
	}
	public Integer getRadio() {
		return radio;
	}
	public void setRadio(Integer radio) {
		this.radio = radio;
	}
	public Long getIdRescatista() {
		return idRescatista;
	}
	public void setIdRescatista(Long idRescatista) {
		this.idRescatista = idRescatista;
	}
	public String getNombreRescatista() {
		return nombreRescatista;
	}
	public void setNombreRescatista(String nombreRescatista) {
		this.nombreRescatista = nombreRescatista;
	}
	public Long getIdEventoCatastrofe() {
		return idEventoCatastrofe;
	}
	public void setIdEventoCatastrofe(Long idEventoCatastrofe) {
		this.idEventoCatastrofe = idEventoCatastrofe;
	}
	public String getNombreEventoCatastrofe() {
		return nombreEventoCatastrofe;
	}
	public void setNombreEventoCatastrofe(String nombreEventoCatastrofe) {
		this.nombreEventoCatastrofe = nombreEventoCatastrofe;
	}
	public List<PlanDto> getPlanesEmergencia() {
		return planesEmergencia;
	}
	public void setPlanesEmergencia(List<PlanDto> planesEmergencia) {
		this.planesEmergencia = planesEmergencia;
	}
	public List<PlanDto> getPlanesRiesgo() {
		return planesRiesgo;
	}
	public void setPlanesRiesgo(List<PlanDto> planesRiesgo) {
		this.planesRiesgo = planesRiesgo;
	}
	
	
	
}
