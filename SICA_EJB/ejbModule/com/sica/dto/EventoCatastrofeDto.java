package com.sica.dto;

import java.util.List;

public class EventoCatastrofeDto {

	private Long id;
	private String descripcion;
	private Integer distancia;// radio
	private String estado;
	private String magnitud;
	private String nombre;
	private String ubicacion;
	private String lat;
	private String lon;
	private String fechaComienzo;
	private String fechaFin;
	private Long idTipo;
	private String nombreTipo;
	
	private List<OngDto> ongs;
	private List<PlanRescatistaDto> planesRescatista;
	private List<FuenteRssDto> fuentes;
	private List<ImagenDto> imagenes;
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public Integer getDistancia() {
		return distancia;
	}
	public void setDistancia(Integer distancia) {
		this.distancia = distancia;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getMagnitud() {
		return magnitud;
	}
	public void setMagnitud(String magnitud) {
		this.magnitud = magnitud;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getUbicacion() {
		return ubicacion;
	}
	public void setUbicacion(String ubicacion) {
		this.ubicacion = ubicacion;
	}
	public String getFechaComienzo() {
		return fechaComienzo;
	}
	public void setFechaComienzo(String fechaComienzo) {
		this.fechaComienzo = fechaComienzo;
	}
	public String getFechaFin() {
		return fechaFin;
	}
	public void setFechaFin(String fechaFin) {
		this.fechaFin = fechaFin;
	}
	public Long getIdTipo() {
		return idTipo;
	}
	public void setIdTipo(Long idTipo) {
		this.idTipo = idTipo;
	}
	public String getNombreTipo() {
		return nombreTipo;
	}
	public void setNombreTipo(String nombreTipo) {
		this.nombreTipo = nombreTipo;
	}
	public String getLat() {
		return lat;
	}
	public void setLat(String lat) {
		this.lat = lat;
	}
	public String getLon() {
		return lon;
	}
	public void setLon(String lon) {
		this.lon = lon;
	}
	public List<OngDto> getOngs() {
		return ongs;
	}
	public void setOngs(List<OngDto> ongs) {
		this.ongs = ongs;
	}
	public List<PlanRescatistaDto> getPlanesRescatista() {
		return planesRescatista;
	}
	public void setPlanesRescatista(List<PlanRescatistaDto> planesRescatista) {
		this.planesRescatista = planesRescatista;
	}
	public List<FuenteRssDto> getFuentes() {
		return fuentes;
	}
	public void setFuentes(List<FuenteRssDto> fuentes) {
		this.fuentes = fuentes;
	}
	public List<ImagenDto> getImagenes() {
		return imagenes;
	}
	public void setImagenes(List<ImagenDto> imagenes) {
		this.imagenes = imagenes;
	}
	
	
		
}
