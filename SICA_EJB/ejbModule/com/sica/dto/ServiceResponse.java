package com.sica.dto;

import java.util.ArrayList;
import java.util.List;

public class ServiceResponse {
	
	public static final String COD_ERROR = "500";
	public static final String COD_OK = "200";
	
	public static final String STATUS_OK = "OK";
	public static final String STATUS_ERROR = "ERROR";
	
	public static final String MENSAJE = "Solicitud realizada correctamente.";
	
	private static ServiceResponse instance = null;
	private String mensajes;
	private String cod;
	private String estado;
	private String elemento;
	private Object objeto;
	private List<Object> listaObjetos; 
	
	private ServiceResponse(){};
	
	public static ServiceResponse getInstance(){
		if (instance == null){
			instance = new ServiceResponse();
		}
		return instance;
	}
	
	public String getMensajes() {
		return mensajes;
	}
	public void setMensajes(String mensajes) {
		this.mensajes = mensajes;
	}
	public String getCod() {
		return cod;
	}
	public void setCod(String cod) {
		this.cod = cod;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getElemento() {
		return elemento;
	}

	public void setElemento(String elemento) {
		this.elemento = elemento;
	}

	public Object getObjeto() {
		return objeto;
	}

	public void setObjeto(Object objeto) {
		this.objeto = objeto;
	}

	public List<Object> getListaObjetos() {
		return listaObjetos;
	}

	public void setListaObjetos(List<Object> listaObjetos) {
		this.listaObjetos = listaObjetos;
	}
	
	public void reset(){
		mensajes = "";
		cod = "";
		estado = "";
		elemento = "";
		objeto = null;
		listaObjetos = new ArrayList<Object>(); 		
	}
	
}
