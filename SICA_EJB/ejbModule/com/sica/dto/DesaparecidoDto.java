package com.sica.dto;

import java.util.List;


public class DesaparecidoDto {
	
	private Long id;
	private String apellido;
	private Integer edad;
	private String estado;
	private String etnia;
	private String nombre;
	private String colorPiel;
	private String colorOjos;
	private String estatura;
	private String fechaDesaparecido;
	private String detalles;
	private String emailUsuario;
	private List<Long> idImagenes;
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	public Integer getEdad() {
		return edad;
	}
	public void setEdad(Integer edad) {
		this.edad = edad;
	}

	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getEtnia() {
		return etnia;
	}
	public void setEtnia(String etnia) {
		this.etnia = etnia;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getColorPiel() {
		return colorPiel;
	}
	public void setColorPiel(String colorPiel) {
		this.colorPiel = colorPiel;
	}
	public String getColorOjos() {
		return colorOjos;
	}
	public void setColorOjos(String colorOjos) {
		this.colorOjos = colorOjos;
	}
	public String getEstatura() {
		return estatura;
	}
	public void setEstatura(String estatura) {
		this.estatura = estatura;
	}
	public String getFechaDesaparecido() {
		return fechaDesaparecido;
	}
	public void setFechaDesaparecido(String fechaDesaparecido) {
		this.fechaDesaparecido = fechaDesaparecido;
	}

	public String getEmailUsuario() {
		return emailUsuario;
	}
	public void setEmailUsuario(String emailUsuario) {
		this.emailUsuario = emailUsuario;
	}
	public List<Long> getIdImagenes() {
		return idImagenes;
	}
	public void setIdImagenes(List<Long> idImagenes) {
		this.idImagenes = idImagenes;
	}
	public String getDetalles() {
		return detalles;
	}
	public void setDetalles(String detalles) {
		this.detalles = detalles;
	}
	
	
	
}
