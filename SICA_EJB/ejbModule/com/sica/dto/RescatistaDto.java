package com.sica.dto;

import java.util.List;

public class RescatistaDto {
	
	private Long id;
	private String apellido;
	private String email;
	private String fechaNacimiento;
	private String nick;
	private String nombre;
	private String password;
	private Integer tipo;	
	private String celular;
	private String descripcion;
	private String sexo;
	
	//Datos particulares del rescatista
	private Boolean isDisponible;
	private List<PlanRescatistaDto> planesRescatistasDto;
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}	
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getFechaNacimiento() {
		return fechaNacimiento;
	}
	public void setFechaNacimiento(String fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}
	public String getNick() {
		return nick;
	}
	public void setNick(String nick) {
		this.nick = nick;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Integer getTipo() {
		return tipo;
	}
	public void setTipo(Integer tipo) {
		this.tipo = tipo;
	}
	public Boolean getIsDisponible() {
		return isDisponible;
	}
	public void setIsDisponible(Boolean isDisponible) {
		this.isDisponible = isDisponible;
	}	
	public String getSexo() {
		return sexo;
	}
	public void setSexo(String sexo) {
		this.sexo = sexo;
	}
	public String getCelular() {
		return celular;
	}
	public void setCelular(String celular) {
		this.celular = celular;
	}
	public List<PlanRescatistaDto> getPlanesRescatistasDto() {
		return planesRescatistasDto;
	}
	public void setPlanesRescatistasDto(List<PlanRescatistaDto> planesRescatistasDto) {
		this.planesRescatistasDto = planesRescatistasDto;
	}
	
	
}
