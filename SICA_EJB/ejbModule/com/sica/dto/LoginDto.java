package com.sica.dto;

import com.sica.utiles.Utiles_SICA;

public class LoginDto {

	private String email;
	private String password;
	private String tipo;
	
	

	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	public boolean existeLogin(){
		return (!Utiles_SICA.isNullOrEmpty(email) && !Utiles_SICA.isNullOrEmpty(password));
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
	
	
}
