package com.sica.dto;

import java.util.List;


public class BienDto {

	private Long id;
	private String email;
	private String fecha;
	private String nombre;
	private Integer tipo;	
	private String telefono;
	private Long idOng;	
	private String nombreOng;
	private String dirOrigen;
	private Boolean retirar;
	private List<ArticuloDto> articulos;
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public Integer getTipo() {
		return tipo;
	}
	public void setTipo(Integer tipo) {
		this.tipo = tipo;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public Long getIdOng() {
		return idOng;
	}
	public void setIdOng(Long idOng) {
		this.idOng = idOng;
	}
	public String getNombreOng() {
		return nombreOng;
	}
	public void setNombreOng(String nombreOng) {
		this.nombreOng = nombreOng;
	}
	public String getDirOrigen() {
		return dirOrigen;
	}
	public void setDirOrigen(String dirOrigen) {
		this.dirOrigen = dirOrigen;
	}
	public Boolean getRetirar() {
		return retirar;
	}
	public void setRetirar(Boolean retirar) {
		this.retirar = retirar;
	}
	public List<ArticuloDto> getArticulos() {
		return articulos;
	}
	public void setArticulos(List<ArticuloDto> articulos) {
		this.articulos = articulos;
	}	
	
}
