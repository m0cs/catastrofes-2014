package com.sica.dto;

public class PlanDto {

	private Long id;
	private String nombre;
	private String descripcion;
	private String pasos;
	private long orden;
	private int tipoPlan;
	

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getPasos() {
		return pasos;
	}
	public void setPasos(String pasos) {
		this.pasos = pasos;
	}
	public int getTipoPlan() {
		return tipoPlan;
	}
	public void setTipoPlan(int tipoPlan) {
		this.tipoPlan = tipoPlan;
	}
	public long getOrden() {
		return orden;
	}
	public void setOrden(long orden) {
		this.orden = orden;
	}	
}
