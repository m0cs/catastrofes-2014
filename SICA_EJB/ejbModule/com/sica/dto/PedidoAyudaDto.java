package com.sica.dto;

import java.util.List;

public class PedidoAyudaDto {

	private Long id;
	private String descripcion;
	private String fecha;
	private Boolean isHabilitado;
	private String latLong;
	//estado
	private String idEstadoUsuario;
	private String nombreEstadoUsuario;
	//usuario
	private String idUsuario;
	private String emailUsuario;
	//recursos
	List<Long> idRecursos;
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public Boolean getIsHabilitado() {
		return isHabilitado;
	}
	public void setIsHabilitado(Boolean isHabilitado) {
		this.isHabilitado = isHabilitado;
	}
	public String getLatLong() {
		return latLong;
	}
	public void setLatLong(String latLong) {
		this.latLong = latLong;
	}
	public String getIdEstadoUsuario() {
		return idEstadoUsuario;
	}
	public void setIdEstadoUsuario(String idEstadoUsuario) {
		this.idEstadoUsuario = idEstadoUsuario;
	}
	public String getNombreEstadoUsuario() {
		return nombreEstadoUsuario;
	}
	public void setNombreEstadoUsuario(String nombreEstadoUsuario) {
		this.nombreEstadoUsuario = nombreEstadoUsuario;
	}
	public String getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(String idUsuario) {
		this.idUsuario = idUsuario;
	}
	public String getEmailUsuario() {
		return emailUsuario;
	}
	public void setEmailUsuario(String emailUsuario) {
		this.emailUsuario = emailUsuario;
	}
	public List<Long> getIdRecursos() {
		return idRecursos;
	}
	public void setIdRecursos(List<Long> idRecursos) {
		this.idRecursos = idRecursos;
	}
	
	
	
}
