package com.sica.dto;

public class ServicioDto {

	private Long id;
	private String email;
	private String fecha;
	private String nombre;
	private Integer tipo;	
	private String telefono;
	private Long idOng;	
	private String nombreOng;
	private Integer cantidad;
	private String profesion;
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public Integer getTipo() {
		return tipo;
	}
	public void setTipo(Integer tipo) {
		this.tipo = tipo;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public Long getIdOng() {
		return idOng;
	}
	public void setIdOng(Long idOng) {
		this.idOng = idOng;
	}
	public String getNombreOng() {
		return nombreOng;
	}
	public void setNombreOng(String nombreOng) {
		this.nombreOng = nombreOng;
	}
	public Integer getCantidad() {
		return cantidad;
	}
	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}
	public String getProfesion() {
		return profesion;
	}
	public void setProfesion(String profesion) {
		this.profesion = profesion;
	}
	
	
}
