package com.sica.enums;

public enum EstadoCatastrofe {
	ACTIVA, FINALIZADA, CANCELADA
}
