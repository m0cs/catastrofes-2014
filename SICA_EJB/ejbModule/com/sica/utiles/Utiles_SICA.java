package com.sica.utiles;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import com.sica.dto.PlanRescatistaDto;
import com.sica.excepciones.NegocioException;


public class Utiles_SICA {


	/**
	 * Formato de fecha min. Dia, mes y año (dd-MM-yyyy)
	 */
	public static final DateFormat DATE_FORMAT_MIN = new SimpleDateFormat("dd-MM-yyyy");
	
	/**
	 * Formato de fecha full. Dia, mes, año, hora, minutos y segundos (dd-MM-yyyy HH:mm:ss)
	 */
	public static final DateFormat DATE_FORMAT_FULL = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");	
	
    public static boolean isNullOrEmpty(String str){
    	return (str == null || str.isEmpty());
    }
    
    public static boolean isNuevo(long id){
    	return ( id == 0 );
    }    
    
    public static boolean isNullOrCero(Long lng){
    	return (lng == null || lng.equals(0L));
    }    
    
    public static byte[] stringToBytea(String str ) throws NegocioException{
    		if (str != null){
    			byte[] b = str.getBytes();
//    			byte[] b = str.getBytes(Charset.forName("UTF-8"));
    			return b;
    		}
    		else return null;
    }
    
    public static String byteaToString(byte[] bytearray ){
    	if (bytearray != null){
    		return new String  ( bytearray );
    	}
    	else return "";
    }
    
    public static List<Object> convertirPlanesToObjects( List<PlanRescatistaDto> planes ){
    	List<Object> listaObject = new ArrayList<Object>();
    	if ( planes != null){
    		for (PlanRescatistaDto plan : planes){
    			listaObject.add(plan);
    		}
    	}
    	return listaObject;
    }
}
