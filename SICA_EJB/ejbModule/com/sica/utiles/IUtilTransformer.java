package com.sica.utiles;

import java.util.List;

public interface IUtilTransformer < E, Dto > {
	
    public E toEntity(Dto source);
    public void toEntity(Dto source, E target);
    public Dto toDto(E source);
    public void toDto(E source, Dto target);
    
    public List<E> toEntityList(List<Dto> listaDtos);
    public List<Dto> toDtoList(List<E> listaEntities);	
	
}
