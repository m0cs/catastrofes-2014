package com.sica.utiles;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.ws.rs.core.MultivaluedMap;

public class UtilesFiles {
	
	public static String SERVER_UPLOAD_LOCATION_FOLDER = "/home/juan/workspace_luna/catastrofes-2014/SICA_Web/WebContent/resources/uploads/";
	public static String APP_SERVER_UPLOAD_LOCATION_FOLDER = "/resources/uploads/";

	public static String getFileName(MultivaluedMap<String, String> header) {
		 
		String[] contentDisposition = header.getFirst("Content-Disposition").split(";");
 
		for (String filename : contentDisposition) {
			if ((filename.trim().startsWith("filename"))) {
 
				String[] name = filename.split("=");
 
				String finalFileName = name[1].trim().replaceAll("\"", "");
				return finalFileName;
			}
		}
		return "unknown";
	}	
	
	//save to somewhere
	public static void writeFile(byte[] content, String filename) throws IOException {
 
		File file = new File(filename);
 
		if (!file.exists()) {
			file.createNewFile();
		}
 
		FileOutputStream fop = new FileOutputStream(file);
 
		fop.write(content);
		fop.flush();
		fop.close();
 
	}		
	
}
