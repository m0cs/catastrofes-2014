package com.sica.facade;

import java.util.List;

import javax.ejb.Local;

import com.sica.dto.AdministradorDto;
import com.sica.dto.EventoCatastrofeDto;
import com.sica.dto.FuenteRssDto;
import com.sica.dto.ImagenDto;
import com.sica.dto.OngDto;
import com.sica.dto.PlanDto;
import com.sica.dto.RescatistaDto;
import com.sica.dto.TipoCatastrofeDto;
import com.sica.dto.UsuarioDto;
import com.sica.excepciones.NegocioException;

@Local
public interface IFacadeBackend {
	
	public UsuarioDto buscarPorEmailAndPassword( String email, String password ) throws NegocioException;
	
	/*
	 * Interfaces fuentes RSS
	 * */
    public long insertarFuenteRss( FuenteRssDto dto ) throws NegocioException;
    public void actualizarFuenteRss( FuenteRssDto dto ) throws NegocioException;
    public void eliminarFuenteRss( long id) throws NegocioException;
    public FuenteRssDto buscarPorIdFuenteRss( long id ) throws NegocioException;
    public List<FuenteRssDto>buscarTodosFuenteRss() throws NegocioException;
    
    
	/*
	 * Interfaces rescatista
	 * */
    public long insertarRescatista( RescatistaDto dto ) throws NegocioException;
    public void actualizarRescatista( RescatistaDto dto ) throws NegocioException;
    public void eliminarRescatista( long id) throws NegocioException;
    public RescatistaDto buscarPorIdRescatista( long id ) throws NegocioException;
    public List<RescatistaDto>buscarTodosRescatista() throws NegocioException;
    
	/*
	 * Interfaces administradores
	 * */
    public long insertarAdministrador( AdministradorDto dto ) throws NegocioException;
    public void actualizarAdministrador( AdministradorDto dto ) throws NegocioException;
    public void eliminarAdministrador( long id) throws NegocioException;
    public AdministradorDto buscarPorIdAdministrador( long id ) throws NegocioException;
    public List<AdministradorDto>buscarTodosAdministrador() throws NegocioException;
    
    
	/*
	 * Interfaces Ong
	 * */
    public long insertarOng( OngDto dto ) throws NegocioException;
    public void actualizarOng( OngDto dto ) throws NegocioException;
    public void eliminarOng( long id) throws NegocioException;
    public OngDto buscarPorIdOng( long id ) throws NegocioException;
    public List<OngDto>buscarTodasOng() throws NegocioException;
    
    
    /**
     * Planes
     * */
    public List<PlanDto> buscarTodosPlanesEmergencia()throws NegocioException;
    public List<PlanDto> buscarTodosPlanesRiesgo()throws NegocioException;
    public void eliminarPlanEmergencia(long id) throws NegocioException;
    public void eliminarPlanRiesgo(long id) throws NegocioException;
    public void insertarPlanEmergencia( PlanDto planDto ) throws NegocioException;
    public void insertarPlanRiesgo(PlanDto planDto) throws NegocioException;
    public void actualizarPlanEmergencia(PlanDto planDto) throws NegocioException;
    public void actualizarPlanRiesgo(PlanDto planDto) throws NegocioException;
    
    /**
     * Tipo catastrofe
     * */
    public long insertarTipoCatastrofe( TipoCatastrofeDto dto ) throws NegocioException;
    public void actualizarTipoCatastrofe( TipoCatastrofeDto dto ) throws NegocioException;
    public void eliminarTipoCatastrofe( long id) throws NegocioException;
    public TipoCatastrofeDto buscarPorIdTipoCatastrofe( long id ) throws NegocioException;
    public List<TipoCatastrofeDto>buscarTodasTipoCatastrofe() throws NegocioException;
    
    /**
     * Evento catastrofe
     * */
    public long insertarEventoCatastrofe( EventoCatastrofeDto dto ) throws NegocioException;
    public void actualizarEventoCatastrofe( EventoCatastrofeDto dto ) throws NegocioException;
    public void eliminarEventoCatastrofe( long id) throws NegocioException;
    public EventoCatastrofeDto buscarPorIdEventoCatastrofe( long id ) throws NegocioException;
    public List<EventoCatastrofeDto>buscarTodasEventoCatastrofe() throws NegocioException;

	public List<RescatistaDto> buscarTodosRescatistaSinPlanes() throws NegocioException;

	public ImagenDto buscarImagenPorId(long id) throws NegocioException;    
}
