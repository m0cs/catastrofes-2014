package com.sica.facade;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import com.sica.dto.AdministradorDto;
import com.sica.dto.EventoCatastrofeDto;
import com.sica.dto.FuenteRssDto;
import com.sica.dto.ImagenDto;
import com.sica.dto.OngDto;
import com.sica.dto.PlanDto;
import com.sica.dto.RescatistaDto;
import com.sica.dto.TipoCatastrofeDto;
import com.sica.dto.UsuarioDto;
import com.sica.ejb.IAdministradorManager;
import com.sica.ejb.IEventoCatastrofeManager;
import com.sica.ejb.IFuenteRssManager;
import com.sica.ejb.IImagenManager;
import com.sica.ejb.IOngManager;
import com.sica.ejb.IPersonaManager;
import com.sica.ejb.IPlanManager;
import com.sica.ejb.IRescatistaManager;
import com.sica.ejb.ITipoCatastrofeManager;
import com.sica.excepciones.NegocioException;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class FacadeBackend  implements IFacadeBackend{
	
	@EJB
	private IPersonaManager personaEJB;
	
	@EJB
	private IAdministradorManager<AdministradorDto> adminEJB;
	
	@EJB
	private IFuenteRssManager<FuenteRssDto> fuenteEJB;
	
	@EJB
	private IRescatistaManager<RescatistaDto> rescEJB;
	
	@EJB
	private IOngManager<OngDto> ongEJB;
	
	@EJB
	private IPlanManager planEJB;
	
	@EJB 
	private ITipoCatastrofeManager<TipoCatastrofeDto> tipoCataEJB;
	
	@EJB 
	private IEventoCatastrofeManager<EventoCatastrofeDto> catastrofeEJB;
	
	@EJB
	private IImagenManager imagenEJB;

	
	@Override
	public UsuarioDto buscarPorEmailAndPassword(String usuario, String password) throws NegocioException {
		
		Object persona = personaEJB.buscarPorEmailAndPassword(usuario, password);
		
		UsuarioDto usuarioDto = (UsuarioDto) persona;
		
		return usuarioDto;	
	}
	
	
	
	/************************************************************************************************************
	 * Metodos para Fuente RSS
	 ************************************************************************************************************ */
	
	@Override
    public long insertarFuenteRss( FuenteRssDto dto ) throws NegocioException{
    	return fuenteEJB.insertar(dto);
    }
    
	@Override
    public void actualizarFuenteRss( FuenteRssDto dto ) throws NegocioException{
    	fuenteEJB.actualizar(dto);
    }
    
	@Override
    public void eliminarFuenteRss( long id) throws NegocioException{
    	fuenteEJB.eliminar(id);
    }
    
	@Override
    public FuenteRssDto buscarPorIdFuenteRss( long id ) throws NegocioException{
    	return fuenteEJB.buscarPorId(id);
    }
    
	@Override
    public List<FuenteRssDto>buscarTodosFuenteRss() throws NegocioException{
    	return fuenteEJB.buscarTodos();
    }
	
	/************************************************************************************************************
	 * Metodos para Rescatistas
	 *************************************************************************************************************/

	@Override
    public long insertarRescatista( RescatistaDto dto ) throws NegocioException{
    	return rescEJB.insertar(dto);
    }
    
	@Override
    public void actualizarRescatista( RescatistaDto dto ) throws NegocioException{
		rescEJB.actualizar(dto);
    }
    
	@Override
    public void eliminarRescatista( long id) throws NegocioException{
		rescEJB.eliminar(id);
    }
    
	@Override
    public RescatistaDto buscarPorIdRescatista( long id ) throws NegocioException{
    	return rescEJB.buscarPorId(id);
    }
    
	@Override
    public List<RescatistaDto>buscarTodosRescatista() throws NegocioException{
    	return rescEJB.buscarTodos();
    }	
	
	
	@Override
    public List<RescatistaDto>buscarTodosRescatistaSinPlanes() throws NegocioException{
    	return rescEJB.buscarTodos();
    }		
	
	/************************************************************************************************************
	 * Metodos para Administradores
	 *************************************************************************************************************/

	@Override
    public long insertarAdministrador( AdministradorDto dto ) throws NegocioException{
    	return adminEJB.insertar(dto);
    }
    
	@Override
    public void actualizarAdministrador( AdministradorDto dto ) throws NegocioException{
		adminEJB.actualizar(dto);
    }
    
	@Override
    public void eliminarAdministrador( long id) throws NegocioException{
		adminEJB.eliminar(id);
    }
    
	@Override
    public AdministradorDto buscarPorIdAdministrador( long id ) throws NegocioException{
    	return adminEJB.buscarPorId(id);
    }
    
	@Override
    public List<AdministradorDto>buscarTodosAdministrador() throws NegocioException{
    	return adminEJB.buscarTodos();
    }	
	
	
	/************************************************************************************************************
	 * Metodos para Ong
	 *************************************************************************************************************/
	
    public long insertarOng( OngDto dto ) throws NegocioException{
    	return ongEJB.insertar(dto);
    }
    
    public void actualizarOng( OngDto dto ) throws NegocioException{
    	ongEJB.actualizar(dto);
    }
    
    public void eliminarOng( long id) throws NegocioException{
    	ongEJB.eliminar(id);
    }
    
    public OngDto buscarPorIdOng( long id ) throws NegocioException{
    	return ongEJB.buscarPorId(id);
    }
    
    public List<OngDto>buscarTodasOng() throws NegocioException{
    	return ongEJB.buscarTodos();
    }
    
    /**************************************************************************************************************
     * Metodos para planes
     * *************************************************************************************************************/
    
    @Override
    public List<PlanDto> buscarTodosPlanesEmergencia() throws NegocioException{
    	return planEJB.buscarTodosPlanesEmergencia();
    }
    
    @Override
    public List<PlanDto> buscarTodosPlanesRiesgo() throws NegocioException{
    	return planEJB.buscarTodosPlanesRiesgo();
    }
    
    @Override
    public void eliminarPlanEmergencia(long id) throws NegocioException{
    	planEJB.eliminarPlanEmergencia(id);
    }
    
    @Override
    public void eliminarPlanRiesgo(long id) throws NegocioException{
    	planEJB.eliminarPlanRiesgo(id);
    }

	@Override
	public void insertarPlanEmergencia(PlanDto planDto) throws NegocioException {
		planEJB.insertarPlanEmergencia(planDto);
		
	}

	@Override
	public void insertarPlanRiesgo(PlanDto planDto) throws NegocioException {
		planEJB.insertarPlanRiesgo(planDto);
		
	}

	@Override
	public void actualizarPlanEmergencia(PlanDto planDto) throws NegocioException {
		planEJB.actualizarPlanEmergencia(planDto);
		
	}

	@Override
	public void actualizarPlanRiesgo(PlanDto planDto) throws NegocioException {
		planEJB.actualizarPlanRiesgo(planDto);		
	}
	
	/**************************************************************************************************************
	 * Tipo Catastrofe
	 ************************************************************************************************************* */

	@Override
	public long insertarTipoCatastrofe(TipoCatastrofeDto dto) throws NegocioException {
		return tipoCataEJB.insertar(dto);
	}



	@Override
	public void actualizarTipoCatastrofe(TipoCatastrofeDto dto) throws NegocioException {
		tipoCataEJB.actualizar(dto);
		
	}



	@Override
	public void eliminarTipoCatastrofe(long id) throws NegocioException {
		tipoCataEJB.eliminar(id);
		
	}



	@Override
	public TipoCatastrofeDto buscarPorIdTipoCatastrofe(long id) throws NegocioException {
		return tipoCataEJB.buscarPorId(id);
	}



	@Override
	public List<TipoCatastrofeDto> buscarTodasTipoCatastrofe() throws NegocioException {
		return tipoCataEJB.buscarTodos();
	}


	/**************************************************************************************************************
	 * Evento Catastrofe
	 ************************************************************************************************************* */

	@Override
	public long insertarEventoCatastrofe(EventoCatastrofeDto dto) throws NegocioException {
		return catastrofeEJB.insertar(dto);
	}



	@Override
	public void actualizarEventoCatastrofe(EventoCatastrofeDto dto) throws NegocioException {
		catastrofeEJB.actualizar(dto);
		
	}


	@Deprecated
	@Override
	public void eliminarEventoCatastrofe(long id) throws NegocioException {
		// TODO Auto-generated method stub
		
	}



	@Override
	public EventoCatastrofeDto buscarPorIdEventoCatastrofe(long id) throws NegocioException {
		return catastrofeEJB.buscarPorId(id);
	}



	@Override
	public List<EventoCatastrofeDto> buscarTodasEventoCatastrofe() throws NegocioException {
		return catastrofeEJB.buscarTodos();
	}
	
	/***********************************************************************************************
	 * Imagenes
	 *************************************************************************************************/
	@Override
	public ImagenDto buscarImagenPorId(long id) throws NegocioException{
		return imagenEJB.buscarPorId(id);
	}

}
