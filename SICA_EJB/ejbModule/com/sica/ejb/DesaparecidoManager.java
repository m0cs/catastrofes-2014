package com.sica.ejb;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import com.sica.dao.IDesaparecidoDao;
import com.sica.dao.IPersonaDao;
import com.sica.dao.IUsuarioDao;
import com.sica.dto.DesaparecidoDto;
import com.sica.dto.assemblers.DesaparecidoAssembler;
import com.sica.entities.Desaparecido;
import com.sica.entities.Usuario;
import com.sica.excepciones.DaoException;
import com.sica.excepciones.NegocioException;
import com.sica.utiles.FiltroBusquedaDesaparecido;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class DesaparecidoManager implements IDesaparecidoManager<DesaparecidoDto> {

	@EJB
	private IDesaparecidoDao desaparecidoDao;
	
	@EJB
	private IUsuarioDao usuarioDao;
	
	@EJB
	private IPersonaDao personaDao;
	
	@Override
	public long insertar(DesaparecidoDto dto) throws NegocioException {
		try{
			Desaparecido desaparecido = DesaparecidoAssembler.getInstance().toEntity(dto);
    	
			if (dto.getEmailUsuario()!= null){
				Usuario usuario = (Usuario) personaDao.buscarUsuarioPorEmail(dto.getEmailUsuario());
				if (usuario == null){
					throw new NegocioException( "Usuario que reporta el desaparecido no existe.");
				}
				desaparecido.setUsuario( usuario );
			}
			
			desaparecidoDao.persist( desaparecido );
    	
    		return desaparecido.getId();
		}
		catch(DaoException e){
			throw new NegocioException( e.getMessage());
		}
	}

	@Override
	public void actualizar(DesaparecidoDto dto) throws NegocioException { 
		try{
			Desaparecido desaparecido = DesaparecidoAssembler.getInstance().toEntity(dto);
			
			if (dto.getEmailUsuario() != null){
				Usuario usuario = (Usuario) personaDao.buscarUsuarioPorEmail(dto.getEmailUsuario());
				if (usuario == null){
					throw new NegocioException( "Usuario que actualiza el desaparecido no existe.");
				}
			}			
    	
			desaparecidoDao.merge(desaparecido);
		}
		catch(DaoException e){
			throw new NegocioException( e.getMessage());
		} 
		
	}

	@Deprecated
	@Override
	public void eliminar(long id) throws NegocioException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public DesaparecidoDto buscarPorId(long id) throws NegocioException {
		try{
			Desaparecido des = desaparecidoDao.findByID( id );
			DesaparecidoDto desDto = DesaparecidoAssembler.getInstance().toDto( des );
    		
    		return desDto;
    		
		}
		catch(DaoException e){
			throw new NegocioException( e.getMessage());
		} 
	}

	@Override
	public List<DesaparecidoDto> buscarTodos() throws NegocioException {
    	try{
    		List<Desaparecido> listaDes = desaparecidoDao.findAll();
    		List<DesaparecidoDto> listaDesDto = DesaparecidoAssembler.getInstance().toDtoList(listaDes);
    		
    		return listaDesDto;
    	}
		catch( DaoException e ){
			throw new NegocioException( e.getMessage());
		} 
	}

	@Override 
	public List<DesaparecidoDto> buscarDesaparecidoPorUsuario(long idUsuario ) throws NegocioException{
		try{
			List<Desaparecido> listaDes = desaparecidoDao.buscarDesaparecidoPorUsuario(idUsuario);
			List<DesaparecidoDto> listaDesDto = DesaparecidoAssembler.getInstance().toDtoList( listaDes );
			
			return listaDesDto;
		}
		catch( DaoException e ){
			throw new NegocioException( e.getMessage());
		} 			
	}
	
	@Override
	public List<DesaparecidoDto> busquedaDesaparecido( FiltroBusquedaDesaparecido filtro) throws NegocioException {
		try{
			List<Desaparecido> listaDes = desaparecidoDao.busquedaDesaparecido(filtro);
			List<DesaparecidoDto> listaDesDto = DesaparecidoAssembler.getInstance().toDtoList(listaDes);
			
			return listaDesDto;
		}
		catch( DaoException e ){
			throw new NegocioException( e.getMessage());
		} 		
	}

}
