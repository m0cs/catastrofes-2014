package com.sica.ejb;

import javax.ejb.Local;

import com.sica.dto.RegistroUsuarioDto;
import com.sica.dto.UsuarioDto;
import com.sica.excepciones.NegocioException;


@Local
public interface IUsuarioManager<Dto>  extends IManager<Dto> {
	
	public UsuarioDto registrarUsuario( RegistroUsuarioDto registroDto) throws NegocioException;
}
