package com.sica.ejb;

import java.util.List;

import javax.ejb.Local;

import com.sica.excepciones.NegocioException;

@Local
public interface IManager<Dto> {

    public long insertar( Dto dto ) throws NegocioException;
    
    public void actualizar( Dto dto ) throws NegocioException;
    
    public void eliminar( long id) throws NegocioException;
    
    public Dto buscarPorId( long id ) throws NegocioException;
    
    public List<Dto>buscarTodos() throws NegocioException;
}
