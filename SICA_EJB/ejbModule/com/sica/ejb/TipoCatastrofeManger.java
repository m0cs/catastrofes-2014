package com.sica.ejb;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import com.sica.dao.ITipoCatastrofeDao;
import com.sica.dto.TipoCatastrofeDto;
import com.sica.dto.assemblers.TipoCatastrofeAssembler;
import com.sica.entities.TipoCatastrofe;
import com.sica.excepciones.DaoException;
import com.sica.excepciones.NegocioException;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class TipoCatastrofeManger implements ITipoCatastrofeManager<TipoCatastrofeDto> {

	@EJB
	ITipoCatastrofeDao tipoCataDao;
	
	@Override
	public long insertar(TipoCatastrofeDto dto) throws NegocioException {
		try{
			TipoCatastrofe tipoCata = TipoCatastrofeAssembler.getInstance().toEntity(dto);
    	
			tipoCataDao.persist(tipoCata);
    	
    		return tipoCata.getId();
		}
		catch(DaoException e){
			throw new NegocioException( e.getMessage());
		}
	}

	@Override
	public void actualizar(TipoCatastrofeDto dto) throws NegocioException {
		try{
			TipoCatastrofe tipoCata = TipoCatastrofeAssembler.getInstance().toEntity(dto);
    	
			tipoCataDao.merge(tipoCata);
		}
		catch(DaoException e){
			throw new NegocioException( e.getMessage());
		}  
	}

	@Override
	public void eliminar(long id) throws NegocioException {
		try{
			
			TipoCatastrofe tipoCata = tipoCataDao.findByID(id);
			tipoCataDao.remove(tipoCata);
		}
		catch(DaoException e){
			throw new NegocioException( e.getMessage());
		}   	
	}

	@Override
	public TipoCatastrofeDto buscarPorId(long id) throws NegocioException {
		try{
			TipoCatastrofe tipoCata = tipoCataDao.findByID( id );
			TipoCatastrofeDto tipoCataDto = TipoCatastrofeAssembler.getInstance().toDto(tipoCata);
    		
    		return tipoCataDto;
    		
		}
		catch(DaoException e){
			throw new NegocioException( e.getMessage());
		}  
	}

	@Override
	public List<TipoCatastrofeDto> buscarTodos() throws NegocioException {
    	try{
    		List<TipoCatastrofe> listaAdmin = tipoCataDao.findAll();
    		List<TipoCatastrofeDto> listaAdminDto = new ArrayList<TipoCatastrofeDto>();
    		
    		if (listaAdmin != null){
    			listaAdminDto = TipoCatastrofeAssembler.getInstance().toDtoList(listaAdmin);
    		}
    		return listaAdminDto;
    	}
		catch( DaoException e ){
			throw new NegocioException( e.getMessage());
		}   
	}
	

	@Override
	public TipoCatastrofeDto validarPorNombre( String nombre, long id ) throws NegocioException {
		try{
			TipoCatastrofe tipoCat = tipoCataDao.validarPorNombre( nombre , id );
			TipoCatastrofeDto tipoCatDto = null;
			if (tipoCat != null){
				tipoCatDto = TipoCatastrofeAssembler.getInstance().toDto( tipoCat );
			}
    		return tipoCatDto;
    		
		}
		catch(DaoException e){
			throw new NegocioException( e.getMessage());
		}  
	}		


}
