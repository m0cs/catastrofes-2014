package com.sica.ejb;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import com.sica.dao.IUsuarioDao;
import com.sica.dto.RegistroUsuarioDto;
import com.sica.dto.UsuarioDto;
import com.sica.dto.assemblers.UsuarioAssembler;
import com.sica.entities.Usuario;
import com.sica.excepciones.DaoException;
import com.sica.excepciones.NegocioException;
import com.sica.utiles.Utiles_JPA;

/**
 * Session Bean implementation class DemoService
 */
@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class UsuarioManager  implements IUsuarioManager<UsuarioDto> {

	@EJB
	private IUsuarioDao usuarioDao;
    
	/**
     * Default constructor. 
     */
    public UsuarioManager() {}
    
    @Override
    public UsuarioDto registrarUsuario( RegistroUsuarioDto registroDto) throws NegocioException{
    
	    	//aca se debe crear el usuario directamente ya que
	    	//el dto es distino al del usuario.
	    	UsuarioDto usuarioDto = new UsuarioDto();
	    	
	    	usuarioDto.setEmail( registroDto.getEmail() );
	    	usuarioDto.setPassword( registroDto.getPassword() );
	    	usuarioDto.setNick( registroDto.getNick() );
	    	usuarioDto.setTipo(Utiles_JPA.USUARIO);
	    	usuarioDto.setIsActivo(true);
	    	
	    	long id = insertar( usuarioDto );
	    	return buscarPorId(id);    	
    }
    
	
	@Override
	public long insertar(UsuarioDto dto) throws NegocioException {
		try{
			Usuario usuario = UsuarioAssembler.getInstance().toEntity(dto);
    	
			usuarioDao.persist(usuario);
    	
    		return usuario.getId();
		}
		catch(DaoException e){
			throw new NegocioException( e.getMessage());
		}
	}

	@Override
	public void actualizar(UsuarioDto dto) throws NegocioException {
		try{
			Usuario usuario = UsuarioAssembler.getInstance().toEntity(dto);
    	
			usuarioDao.merge(usuario);
		}
		catch(DaoException e){
			throw new NegocioException( e.getMessage());
		}  
	}

	@Override
	public void eliminar(long id) throws NegocioException {
		try{
			
			Usuario usuario = usuarioDao.findByID(id);
			usuarioDao.remove(usuario);
		}
		catch(DaoException e){
			throw new NegocioException( e.getMessage());
		}   	
	}

	@Override
	public UsuarioDto buscarPorId(long id) throws NegocioException {
		try{
			Usuario usuario = usuarioDao.findByID( id );
			UsuarioDto usuarioDto = UsuarioAssembler.getInstance().toDto(usuario);
    		
    		return usuarioDto;
    		
		}
		catch(DaoException e){
			throw new NegocioException( e.getMessage());
		}  
	}

	@Override
	public List<UsuarioDto> buscarTodos() throws NegocioException {
    	try{
    		List<Usuario> listaAdmin = usuarioDao.findAll();
    		List<UsuarioDto> listaAdminDto = UsuarioAssembler.getInstance().toDtoList(listaAdmin);
    		
    		return listaAdminDto;
    	}
		catch( DaoException e ){
			throw new NegocioException( e.getMessage());
		}   
	}
	
}
