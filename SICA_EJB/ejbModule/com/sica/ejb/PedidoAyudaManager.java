package com.sica.ejb;

import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import com.sica.dao.IEstadoUsuarioDao;
import com.sica.dao.IPedidoAyudaDao;
import com.sica.dao.IPersonaDao;
import com.sica.dao.IRecursoDao;
import com.sica.dto.PedidoAyudaDto;
import com.sica.dto.assemblers.PedidoAyudaAssembler;
import com.sica.entities.EstadoUsuario;
import com.sica.entities.PedidoAyuda;
import com.sica.entities.Persona;
import com.sica.entities.Usuario;
import com.sica.excepciones.DaoException;
import com.sica.excepciones.NegocioException;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class PedidoAyudaManager implements IPedidoAyudaManager<PedidoAyudaDto> {

	@EJB
	private IPedidoAyudaDao pedidoAyudaDao;
	
	@EJB
	private IEstadoUsuarioDao estadoUsuarioDao;
	
	@EJB
	private IPersonaDao personaDao;
	
	@EJB
	private IRecursoDao recursoDao;	
	
	
	@Override
	public long insertar(PedidoAyudaDto dto) throws NegocioException {
		try{
			PedidoAyuda pedido = PedidoAyudaAssembler.getInstance().toEntity(dto);
	    	
			if ( dto.getIdEstadoUsuario() != null ){
				EstadoUsuario estado = estadoUsuarioDao.findByID(Long.valueOf(dto.getIdEstadoUsuario()));
				pedido.setEstadoUsuario(estado);
			}
			
			if (dto.getEmailUsuario() != null ){
				Persona persona = personaDao.buscarUsuarioPorEmail(dto.getEmailUsuario());
				if ( persona == null ){
					throw new NegocioException( "Usuario que realiza la solicitud no se encontrada registrado.");
				}
				pedido.setUsuario((Usuario) persona);
			}
			pedido.setFecha(new Date());
			pedidoAyudaDao.persist(pedido);
    	
    		return pedido.getId();
		}
		catch(DaoException e){
			throw new NegocioException( e.getMessage());
		}
	}

	@Override
	public void actualizar(PedidoAyudaDto dto) throws NegocioException {
		try{
			
			PedidoAyuda pedido = PedidoAyudaAssembler.getInstance().toEntity(dto);
    	
			if ( dto.getIdEstadoUsuario() != null ){
				EstadoUsuario estado = estadoUsuarioDao.findByID(Long.valueOf(dto.getIdEstadoUsuario()));
				pedido.setEstadoUsuario(estado);
			}
			
			if (dto.getIdUsuario() != null ){
				Persona persona = personaDao.buscarUsuarioPorEmail(dto.getEmailUsuario());
				pedido.setUsuario((Usuario) persona);
			}
			
			pedidoAyudaDao.merge(pedido);
		}
		catch(DaoException e){
			throw new NegocioException( e.getMessage());
		}
		
	}

	@Override
	public void eliminar(long id) throws NegocioException {
		try{
			
			PedidoAyuda pedido = pedidoAyudaDao.findByID(id);
			pedidoAyudaDao.remove( pedido );
		}
		catch(DaoException e){
			throw new NegocioException( e.getMessage());
		} 
		
	}

	@Override
	public PedidoAyudaDto buscarPorId(long id) throws NegocioException {
		try{
			PedidoAyuda pedido = pedidoAyudaDao.findByID( id );
			PedidoAyudaDto pedidoDto = PedidoAyudaAssembler.getInstance().toDto( pedido );
    		
    		return pedidoDto;
		}
		catch(DaoException e){
			throw new NegocioException( e.getMessage());
		}  
	}

	@Override
	public List<PedidoAyudaDto> buscarTodos() throws NegocioException {
    	try{
    		List<PedidoAyuda> listaPedido = pedidoAyudaDao.findAll();
    		List<PedidoAyudaDto> listaPedidoDto = PedidoAyudaAssembler.getInstance().toDtoList( listaPedido );
    		
    		return listaPedidoDto;
    	}
		catch( DaoException e ){
			throw new NegocioException( e.getMessage());
		}  
	}
	
	
	@Override
	public List<PedidoAyudaDto> buscarPedidosAyudaPorUsuario( long idUsuario ) throws NegocioException {
    	try{
    		List<PedidoAyuda> listaPedido = pedidoAyudaDao.buscarPedidoAyudaPorUsuario(idUsuario);
    		List<PedidoAyudaDto> listaPedidoDto = PedidoAyudaAssembler.getInstance().toDtoList( listaPedido );
    		
    		return listaPedidoDto;
    	}
		catch( DaoException e ){
			throw new NegocioException( e.getMessage());
		}  
	}	
}
