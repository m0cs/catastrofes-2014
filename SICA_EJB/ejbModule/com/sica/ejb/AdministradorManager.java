package com.sica.ejb;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import com.sica.dao.IAdministradorDao;
import com.sica.dto.AdministradorDto;
import com.sica.dto.assemblers.AdministradorAssembler;
import com.sica.entities.Administrador;
import com.sica.excepciones.DaoException;
import com.sica.excepciones.NegocioException;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class AdministradorManager implements IAdministradorManager<AdministradorDto> {

	@EJB
	private IAdministradorDao administradorDao;

	@Override
	public long insertar(AdministradorDto dto) throws NegocioException {
		try{
			Administrador administrador = AdministradorAssembler.getInstance().toEntity(dto);
    	
			administradorDao.persist(administrador);
    	
    		return administrador.getId();
		}
		catch(DaoException e){
			throw new NegocioException( e.getMessage());
		}
	}

	@Override
	public void actualizar(AdministradorDto dto) throws NegocioException {
		try{
			Administrador administrador = AdministradorAssembler.getInstance().toEntity(dto);
    	
			administradorDao.merge(administrador);
		}
		catch(DaoException e){
			throw new NegocioException( e.getMessage());
		}  
	}

	@Override
	public void eliminar(long id) throws NegocioException {
		try{
			
			Administrador administrador = administradorDao.findByID(id);
			administradorDao.remove(administrador);
		}
		catch(DaoException e){
			throw new NegocioException( e.getMessage());
		}   	
	}

	@Override
	public AdministradorDto buscarPorId(long id) throws NegocioException {
		try{
			Administrador administrador = administradorDao.findByID( id );
			AdministradorDto administradorDto = AdministradorAssembler.getInstance().toDto(administrador);
    		
    		return administradorDto;
    		
		}
		catch(DaoException e){
			throw new NegocioException( e.getMessage());
		}  
	}

	@Override
	public List<AdministradorDto> buscarTodos() throws NegocioException {
    	try{
    		List<Administrador> listaAdmin = administradorDao.findAll();
    		List<AdministradorDto> listaAdminDto = AdministradorAssembler.getInstance().toDtoList(listaAdmin);
    		
    		return listaAdminDto;
    	}
		catch( DaoException e ){
			throw new NegocioException( e.getMessage());
		}   
	}
}
