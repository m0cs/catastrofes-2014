package com.sica.ejb;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import com.sica.dao.IPlanRescatistaDao;
import com.sica.dto.PlanRescatistaDto;
import com.sica.dto.assemblers.PlanRescatistaAssembler;
import com.sica.entities.PlanRescatista;
import com.sica.excepciones.DaoException;
import com.sica.excepciones.NegocioException;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class PlanRescatistaManager implements IPlanRescatistaManager {

	@EJB
	private IPlanRescatistaDao planDao;
	
	@Override
	public PlanRescatistaDto buscarPlanPorEmailRescatista( String email ) throws NegocioException {
		try{

			PlanRescatista plan = planDao.buscarPlanPorRescatista(email);
			PlanRescatistaDto planDto = PlanRescatistaAssembler.getInstance().toDto(plan);
			return planDto;
			
		}
		catch(DaoException e){
			throw new NegocioException( e.getMessage());
		}
	}
	
	@Override
	public List<PlanRescatistaDto>buscarPlanesCompletadosPorRescatista(String email ) throws NegocioException{
		try{

			List<PlanRescatista> planes = planDao.buscarPlanesCompletadosPorRescatista(email);
			List<PlanRescatistaDto> planDto = PlanRescatistaAssembler.getInstance().toDtoList(planes);
			return planDto;
			
		}
		catch(DaoException e){
			throw new NegocioException( e.getMessage());
		}		
	}
	
	@Override
	public void completarPlanRescatista( Long idPlan ) throws NegocioException{
		try{
				PlanRescatista planResc = planDao.findByID( idPlan );
				planResc.setIsCompletado( true );
				planDao.merge(planResc);
		}
		catch(DaoException e){
			throw new NegocioException( e.getMessage());
		}		
	}
	
	@Override
	public long insertar(PlanRescatistaDto dto) throws NegocioException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void actualizar(PlanRescatistaDto dto) throws NegocioException {
		try{
			PlanRescatista rescatista = PlanRescatistaAssembler.getInstance().toEntity(dto);
			planDao.persist(rescatista);
		}
		catch(DaoException e){
			throw new NegocioException( e.getMessage());
		}
	}

	@Deprecated
	@Override
	public void eliminar(long id) throws NegocioException {
		// TODO Auto-generated method stub

	}

	@Override
	public PlanRescatistaDto buscarPorId(long id) throws NegocioException {
		try{
			PlanRescatista planRescatista = planDao.findByID( id );
			PlanRescatistaDto planRescatistaDto = PlanRescatistaAssembler.getInstance().toDto( planRescatista );
			
			return planRescatistaDto;
		}
		catch(DaoException e){
			throw new NegocioException( e.getMessage());
		}
	}

	@Override
	public List<PlanRescatistaDto> buscarTodos() throws NegocioException {
		return null;
	}

}
