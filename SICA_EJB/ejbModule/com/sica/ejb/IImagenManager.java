package com.sica.ejb;

import javax.ejb.Local;

import com.sica.dto.ImagenDto;

@Local
public interface IImagenManager extends IManager<ImagenDto> {

}
