package com.sica.ejb;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import com.sica.dao.IOngDao;
import com.sica.dto.OngDto;
import com.sica.dto.assemblers.OngAssembler;
import com.sica.entities.Ong;
import com.sica.excepciones.DaoException;
import com.sica.excepciones.NegocioException;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class OngManager implements IOngManager<OngDto> {
	

	@EJB
	private IOngDao ongDao;

	@Override
	public long insertar(OngDto dto) throws NegocioException {
		try{
			Ong ong = OngAssembler.getInstance().toEntity(dto);
    	
			ongDao.persist(ong);
    	
    		return ong.getId();
		}
		catch(DaoException e){
			throw new NegocioException( e.getMessage());
		}
	}

	@Override
	public void actualizar(OngDto dto) throws NegocioException {
		try{
			Ong ong = OngAssembler.getInstance().toEntity(dto);
    	
			ongDao.merge(ong);
		}
		catch(DaoException e){
			throw new NegocioException( e.getMessage());
		} 
	}

	@Override
	public void eliminar(long id) throws NegocioException {
		try{
			
			Ong ong = ongDao.findByID(id);
			ongDao.remove(ong);
		}
		catch(DaoException e){
			throw new NegocioException( e.getMessage());
		} 
		
	}

	@Override
	public OngDto buscarPorId(long id) throws NegocioException {
		try{
			Ong ong = ongDao.findByID( id );
			OngDto ongDto = OngAssembler.getInstance().toDto(ong);
    		
    		return ongDto;
    		
		}
		catch(DaoException e){
			throw new NegocioException( e.getMessage());
		}  
	}
	
	@Override
	public OngDto buscarOngPorEmail( String email, long id ) throws NegocioException {
		try{
			Ong ong = ongDao.buscarOngPorEmail( email, id );
			OngDto ongDto = null;
			if (ong != null){
				ongDto = OngAssembler.getInstance().toDto(ong);
			}
    		return ongDto;
    		
		}
		catch(DaoException e){
			throw new NegocioException( e.getMessage());
		}  
	}	

	@Override
	public List<OngDto> buscarTodos() throws NegocioException {
    	try{
    		List<Ong> listaAdmin = ongDao.findAll();
    		List<OngDto> listaAdminDto = OngAssembler.getInstance().toDtoList(listaAdmin);
    		
    		return listaAdminDto;
    	}
		catch( DaoException e ){
			throw new NegocioException( e.getMessage());
		}   
	}

}
