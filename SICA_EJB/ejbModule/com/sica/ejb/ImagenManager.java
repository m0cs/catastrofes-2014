package com.sica.ejb;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import com.sica.dao.IImagenDao;
import com.sica.dto.ImagenDto;
import com.sica.dto.assemblers.ImagenAssembler;
import com.sica.entities.Imagen;
import com.sica.excepciones.DaoException;
import com.sica.excepciones.NegocioException;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class ImagenManager implements IImagenManager {
	
	@EJB
	IImagenDao imagenDao;
	
	@Override
	public long insertar(ImagenDto dto) throws NegocioException {
		try{
			Imagen imagen = ImagenAssembler.getInstance().toEntity(dto);
    	
			imagenDao.persist(imagen);
    	
    		return imagen.getId();
		}
		catch(DaoException e){
			throw new NegocioException( e.getMessage());
		}
	}

	@Override
	public void actualizar(ImagenDto dto) throws NegocioException {
		// TODO Auto-generated method stub

	}

	@Override
	public void eliminar(long id) throws NegocioException {
		// TODO Auto-generated method stub

	}

	@Override
	public ImagenDto buscarPorId(long id) throws NegocioException {
		try{
			Imagen imagen = imagenDao.findByID(id);
			ImagenDto imagenDto = ImagenAssembler.getInstance().toDto(imagen);
    	 
    		return imagenDto;
		}
		catch(DaoException e){
			throw new NegocioException( e.getMessage());
		}
	}

	@Override
	public List<ImagenDto> buscarTodos() throws NegocioException {
		// TODO Auto-generated method stub
		return null;
	}

}
