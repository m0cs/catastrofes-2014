package com.sica.ejb;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import com.sica.dao.IPlanEmergenciaDao;
import com.sica.dao.IPlanRiesgoDao;
import com.sica.dto.PlanDto;
import com.sica.dto.assemblers.PlanEmergenciaAssembler;
import com.sica.dto.assemblers.PlanRiesgoAssembler;
import com.sica.entities.PlanEmergencia;
import com.sica.entities.PlanRiesgo;
import com.sica.excepciones.DaoException;
import com.sica.excepciones.NegocioException;
import com.sica.utiles.Utiles_JPA;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class PlanManager implements IPlanManager {
	
	@EJB
	private IPlanRiesgoDao planRiesgoDao;
	
	@EJB
	private IPlanEmergenciaDao planEmergenciaDao;

	@Override
	public List<PlanDto> buscarTodosPlanesEmergencia() throws NegocioException{
		try {
			List<PlanEmergencia> planeEmergencia = planEmergenciaDao.findAll();
			List<PlanDto> listaPlanesDto = PlanEmergenciaAssembler.getInstance().toDtoList(planeEmergencia);
			return listaPlanesDto;
		} 
		catch (DaoException e) {
			throw new NegocioException( e.getMessage());
		}
	}
	
	@Override
	public List<PlanDto> buscarTodosPlanesRiesgo() throws NegocioException{
		try {
			List<PlanRiesgo> planesRiesgo = planRiesgoDao.findAll();
			List<PlanDto> listaPlanesDtos = PlanRiesgoAssembler.getInstance().toDtoList(planesRiesgo);
			return listaPlanesDtos;
			
		} 
		catch (DaoException e) {
			throw new NegocioException( e.getMessage());
		}
	}
	
	@Override
	public void eliminarPlanEmergencia(long id) throws NegocioException{
		try {
			PlanEmergencia planEmergencia = planEmergenciaDao.findByID(id);
			planEmergenciaDao.remove(planEmergencia);
		} 
		catch (DaoException e) {
			throw new NegocioException( e.getMessage());
		}
	}
	
	@Override
	public void eliminarPlanRiesgo(long id) throws NegocioException{
		try {
			
			PlanRiesgo planRiesgo = planRiesgoDao.findByID(id);
			planRiesgoDao.remove(planRiesgo);
		}
		catch (DaoException e) {
			throw new NegocioException( e.getMessage());
		}
	}

	@Override
	public void insertarPlanEmergencia(PlanDto planDto) throws NegocioException {
		try {
			PlanEmergencia planEmergencia = PlanEmergenciaAssembler.getInstance().toEntity(planDto);
			planEmergenciaDao.persist( planEmergencia);
		}
		catch (DaoException e) {
				throw new NegocioException( e.getMessage());
		}	
	}

	@Override
	public void insertarPlanRiesgo(PlanDto planDto) throws NegocioException {
		try {
			PlanRiesgo planRiesgo = PlanRiesgoAssembler.getInstance().toEntity(planDto);
			planRiesgoDao.persist( planRiesgo);
		}
		catch (DaoException e) {
				throw new NegocioException( e.getMessage());
		}	
		
	}

	@Override
	public void actualizarPlanEmergencia(PlanDto planDto) throws NegocioException {
		try {
			
			PlanEmergencia planEmergencia = PlanEmergenciaAssembler.getInstance().toEntity(planDto);
			planEmergenciaDao.merge(planEmergencia);
		}
		catch (DaoException e) {
			throw new NegocioException( e.getMessage());
		}
		
	}

	@Override
	public void actualizarPlanRiesgo(PlanDto planDto) throws NegocioException {
		try {
			
			PlanRiesgo planRiesgo = PlanRiesgoAssembler.getInstance().toEntity(planDto);
			planRiesgoDao.merge(planRiesgo);
		}
		catch (DaoException e) {
			throw new NegocioException( e.getMessage());
		}
		
	}

	@Override
	public PlanDto buscarPlanPasoEmergenciaPorId(long id) throws NegocioException {
		try{
			PlanEmergencia planEmergencia = planEmergenciaDao.findByID(id);
			return PlanEmergenciaAssembler.getInstance().toDto(planEmergencia);
		}
		catch (DaoException e) {
			throw new NegocioException( e.getMessage());
		}		
	}

	@Override
	public PlanDto buscarPlanPasoRiesgoPorId(long id) throws NegocioException {
		try {
			PlanRiesgo planRiesgo = planRiesgoDao.findByID(id);
			return PlanRiesgoAssembler.getInstance().toDto(planRiesgo);
		}
		catch (DaoException e) {
				throw new NegocioException( e.getMessage());
		}
	}
	
	@Override 
	public PlanDto validarUnicoOrdenPorPasoPlan(long orden, long idPaso, int tipo) throws NegocioException{
		try{
			PlanDto planDto = null;
			if (tipo == Utiles_JPA.PLAN_EMERGENCIA){
				PlanEmergencia planEme = planEmergenciaDao.validarOrdenPasoExistente( orden, idPaso );
				if (planEme != null)
					planDto = PlanEmergenciaAssembler.getInstance().toDto(planEme);
			}
			if (tipo == Utiles_JPA.PLAN_RIESGO){
				PlanRiesgo planRie = planRiesgoDao.validarOrdenPasoExistente( orden, idPaso );
				if (planRie != null)
					planDto = PlanRiesgoAssembler.getInstance().toDto(planRie);
			}

			return planDto;
		}
		catch (DaoException e) {
			throw new NegocioException( e.getMessage());
		}		
	}
}
