package com.sica.ejb;

import javax.ejb.Local;

import com.sica.dto.OngDto;
import com.sica.excepciones.NegocioException;

@Local
public interface IOngManager<Dto>  extends IManager<Dto>{
	
	public OngDto buscarOngPorEmail( String email , long id) throws NegocioException;
}
