package com.sica.ejb;

import java.util.List;

import javax.ejb.Local;

import com.sica.dto.PlanRescatistaDto;
import com.sica.excepciones.NegocioException;

@Local
public interface IPlanRescatistaManager extends IManager<PlanRescatistaDto> {

	public PlanRescatistaDto buscarPlanPorEmailRescatista(String email) throws NegocioException;

	public void completarPlanRescatista(Long idPlan)  throws NegocioException;

	public List<PlanRescatistaDto> buscarPlanesCompletadosPorRescatista(String email) throws NegocioException;

}
