package com.sica.ejb;

import javax.ejb.Local;

import com.sica.excepciones.NegocioException;

@Local
public interface IPersonaManager {
	public Object buscarPorEmailAndPassword( String usuario, String password ) throws NegocioException;
	public Object buscarUsuarioPorId (long id) throws NegocioException;
	Object buscarAdminPorEmail(String email, long id) throws NegocioException;
	Object buscarPorEmailAndPasswordAndTipo(String usuario, String password, int tipo) throws NegocioException;
}
