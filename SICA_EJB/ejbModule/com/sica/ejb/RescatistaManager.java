package com.sica.ejb;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import com.sica.dao.IRescatistaDao;
import com.sica.dto.RescatistaDto;
import com.sica.dto.assemblers.RescatistaAssembler;
import com.sica.entities.Rescatista;
import com.sica.excepciones.DaoException;
import com.sica.excepciones.NegocioException;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class RescatistaManager implements IRescatistaManager<RescatistaDto> {

	@EJB
	private IRescatistaDao rescatistaDao;
	
	public RescatistaManager() {}
	
	
	@Override
	public long insertar(RescatistaDto dto) throws NegocioException {
		try{
			Rescatista rescatista = RescatistaAssembler.getInstance().toEntity(dto);
    	
			rescatistaDao.persist(rescatista);
    	
    		return rescatista.getId();
		}
		catch(DaoException e){
			throw new NegocioException( e.getMessage());
		}
	}

	@Override
	public void actualizar(RescatistaDto dto) throws NegocioException {
		try{
			Rescatista rescatista = RescatistaAssembler.getInstance().toEntity(dto);
    	
			rescatistaDao.merge(rescatista);
		}
		catch(DaoException e){
			throw new NegocioException( e.getMessage());
		}  
	}

	@Override
	public void eliminar(long id) throws NegocioException {
		try{
			
			Rescatista rescatista = rescatistaDao.findByID(id);
			rescatistaDao.remove(rescatista);
		}
		catch(DaoException e){
			throw new NegocioException( e.getMessage());
		}   
		
	}

	@Override
	public RescatistaDto buscarPorId(long id) throws NegocioException {
		try{
			Rescatista rescatista = rescatistaDao.findByID( id );
			RescatistaDto rescatistaDto = RescatistaAssembler.getInstance().toDto(rescatista);
    		
    		return rescatistaDto;
    		
		}
		catch(DaoException e){
			throw new NegocioException( e.getMessage());
		}  
	}

	@Override
	public List<RescatistaDto> buscarTodos() throws NegocioException {
    	try{
    		List<Rescatista> listaFuenteRss = rescatistaDao.findAll();
    		List<RescatistaDto> listaFuenteRssDto = RescatistaAssembler.getInstance().toDtoList(listaFuenteRss);
    		
    		return listaFuenteRssDto;
    	}
		catch( DaoException e ){
			throw new NegocioException( e.getMessage());
		}   
	}

	@Override
	public void registrarRescatista(Rescatista rescatista)
			throws NegocioException {
		// TODO Auto-generated method stub
		
	}
}
