package com.sica.ejb;

import java.util.List;

import javax.ejb.Local;

import com.sica.dto.PlanDto;
import com.sica.excepciones.NegocioException;

@Local
public interface IPlanManager {
	
	public List<PlanDto> buscarTodosPlanesEmergencia() throws NegocioException;
	public List<PlanDto> buscarTodosPlanesRiesgo() throws NegocioException;
    public void eliminarPlanEmergencia(long id) throws NegocioException;
    public void eliminarPlanRiesgo(long id) throws NegocioException;
	public void insertarPlanEmergencia(PlanDto planDto) throws NegocioException;
	public void insertarPlanRiesgo(PlanDto planDto) throws NegocioException;
	public void actualizarPlanEmergencia(PlanDto planDto) throws NegocioException;
	public void actualizarPlanRiesgo(PlanDto planDto) throws NegocioException;
	public PlanDto buscarPlanPasoEmergenciaPorId(long id) throws NegocioException;
	public PlanDto buscarPlanPasoRiesgoPorId(long id) throws NegocioException;
	public PlanDto validarUnicoOrdenPorPasoPlan(long orden, long idPaso, int tipo) throws NegocioException;
}
