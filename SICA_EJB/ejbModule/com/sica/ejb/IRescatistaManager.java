package com.sica.ejb;

import javax.ejb.Local;

import com.sica.entities.Rescatista;
import com.sica.excepciones.NegocioException;

@Local
public interface IRescatistaManager<Dto> extends IManager<Dto> {
	public void registrarRescatista(Rescatista rescatista ) throws NegocioException;
}
