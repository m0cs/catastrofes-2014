package com.sica.ejb;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import com.sica.dao.IArticuloDao;
import com.sica.dao.IBienDao;
import com.sica.dao.IOngDao;
import com.sica.dto.ArticuloDto;
import com.sica.dto.BienDto;
import com.sica.dto.assemblers.ArticuloAssembler;
import com.sica.dto.assemblers.BienAssembler;
import com.sica.entities.Articulo;
import com.sica.entities.Bien;
import com.sica.entities.Ong;
import com.sica.excepciones.DaoException;
import com.sica.excepciones.NegocioException;
import com.sica.utiles.Utiles_JPA;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class BienManager implements IBienManager<BienDto> {

	@EJB
	private IBienDao bienDao;
	
	@EJB
	private IArticuloDao artDao;
	
	@EJB 
	private IOngDao ongDao;
	
	@Override 
	public long insertar(BienDto dto) throws NegocioException {
		try{
			Bien bien = BienAssembler.getInstance().toEntity(dto);
			bien.setFecha(new Date());
			bien.setTipo(Utiles_JPA.BIEN);
			
			if (!Utiles_JPA.isNullOrCero(dto.getIdOng())){
				Ong ong = ongDao.findByID(dto.getIdOng());
				bien.setOng(ong);
			}
			
			asociarArticulos(dto, bien);
			
			bienDao.persist(bien);
    	
    		return bien.getId();
		}
		catch(DaoException e){
			throw new NegocioException( e.getMessage());
		}
	}

	@Override
	public void actualizar(BienDto dto) throws NegocioException {
		try{
			Bien bien = BienAssembler.getInstance().toEntity(dto);
    	
			bienDao.merge( bien );
		}
		catch(DaoException e){
			throw new NegocioException( e.getMessage());
		}  
	}

	@Deprecated
	@Override
	public void eliminar(long id) throws NegocioException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public BienDto buscarPorId(long id) throws NegocioException {
		try{
			Bien bien = bienDao.findByID( id );
			BienDto bienDto = BienAssembler.getInstance().toDto(bien);
    		
    		return bienDto;
    		
		}
		catch(DaoException e){
			throw new NegocioException( e.getMessage());
		}  
	}

	@Override
	public List<BienDto> buscarTodos() throws NegocioException {
    	try{
    		List<Bien> listaBien = bienDao.findAll();
    		List<BienDto> listaBienDto = BienAssembler.getInstance().toDtoList(listaBien);
    		
    		return listaBienDto;
    	}
		catch( DaoException e ){
			throw new NegocioException( e.getMessage());
		}  
	}
	
	public void asociarArticulos( BienDto bienDto, Bien bien) throws NegocioException{
	
		// Cargo la lista de articulos para el donacion
		if (bien.getArticulos() == null) {
			bien.setArticulos(new ArrayList<Articulo>());
		} else {
			bien.getArticulos().clear();
		}

		if (bienDto.getArticulos() != null) {

			for (ArticuloDto artDTO : bienDto.getArticulos()) {

				Articulo articulo = ArticuloAssembler.getInstance().toEntity( artDTO);
				bien.getArticulos().add(articulo);
				articulo.setDonBien(bien);
			}
		}		
	}

}
