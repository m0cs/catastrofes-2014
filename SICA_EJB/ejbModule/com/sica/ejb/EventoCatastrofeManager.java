package com.sica.ejb;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import com.sica.dao.IEventoCatastrofeDao;
import com.sica.dao.IImagenDao;
import com.sica.dao.IOngDao;
import com.sica.dao.IPlanRescatistaDao;
import com.sica.dao.IRescatistaDao;
import com.sica.dao.ITipoCatastrofeDao;
import com.sica.dao.PlanDao;
import com.sica.dto.EventoCatastrofeDto;
import com.sica.dto.ImagenDto;
import com.sica.dto.OngDto;
import com.sica.dto.PlanRescatistaDto;
import com.sica.dto.assemblers.EventoCatastrofeAssembler;
import com.sica.entities.EventoCatastrofe;
import com.sica.entities.Imagen;
import com.sica.entities.Ong;
import com.sica.entities.PlanRescatista;
import com.sica.entities.TipoCatastrofe;
import com.sica.excepciones.DaoException;
import com.sica.excepciones.NegocioException;
import com.sica.utiles.Utiles_SICA;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class EventoCatastrofeManager implements IEventoCatastrofeManager<EventoCatastrofeDto> {

	@EJB
	private IEventoCatastrofeDao eventoCatastrofeDao;
	
	@EJB
	private IOngDao ongDao;
	
	@EJB
	private IPlanRescatistaDao planRescatistaDao;
	
	@EJB
	private IRescatistaDao rescatistaDao;
	
	@EJB
	private PlanDao planDao;
	
	@EJB
	private IImagenDao imgDao;
	
	@EJB
	private ITipoCatastrofeDao tipoDao;
	
	@Override
	public long insertar(EventoCatastrofeDto dto) throws NegocioException {
		try{
			EventoCatastrofe cata = EventoCatastrofeAssembler.getInstance().toEntity(dto);
    	
			if (!Utiles_SICA.isNullOrCero(dto.getIdTipo())){
				TipoCatastrofe tipo = tipoDao.findByID(Long.valueOf(dto.getIdTipo()));
				cata.setTipoCatastrofe(tipo);
			}
			
			asociarOngs(dto, cata);
			eventoCatastrofeDao.persist(cata);
			
			asociarPlanes(dto, cata);
			
			asociarImagenes(dto, cata);
			
			eventoCatastrofeDao.persist(cata);
			
    		return cata.getId();
		}
		catch(DaoException e){
			throw new NegocioException( e.getMessage());
		}
	}

	@Override
	public void actualizar(EventoCatastrofeDto dto) throws NegocioException {
		try{
			EventoCatastrofe cata = EventoCatastrofeAssembler.getInstance().toEntity(dto);
			if (!Utiles_SICA.isNullOrCero(dto.getIdTipo())){
				TipoCatastrofe tipo = tipoDao.findByID(Long.valueOf(dto.getIdTipo()));
				cata.setTipoCatastrofe(tipo);
			}			
			asociarOngs(dto, cata);
			asociarPlanes(dto, cata);
			asociarImagenes(dto, cata);
			eventoCatastrofeDao.merge(cata);
		}
		catch(DaoException e){
			throw new NegocioException( e.getMessage());
		} 

	}

	@Deprecated
	@Override
	public void eliminar(long id) throws NegocioException {
		// TODO Auto-generated method stub

	}

	@Override
	public EventoCatastrofeDto buscarPorId(long id) throws NegocioException {
		
		try{
			EventoCatastrofe cata = eventoCatastrofeDao.findByID( id );
			EventoCatastrofeDto cataDto = EventoCatastrofeAssembler.getInstance().toDto(cata);
    		
    		return cataDto;
    		
		}
		catch(DaoException e){
			throw new NegocioException( e.getMessage());
		}  
	}

	@Override
	public List<EventoCatastrofeDto> buscarTodos() throws NegocioException {
		try{
			List<EventoCatastrofe> listaEvento = new ArrayList<EventoCatastrofe>();
			listaEvento = eventoCatastrofeDao.findAll();
    		List<EventoCatastrofeDto> listaEventoDto = EventoCatastrofeAssembler.getInstance().toDtoList(listaEvento);
    		
    		return listaEventoDto;
		}
		catch(DaoException e){
			throw new NegocioException( e.getMessage());
		}
	}
	
	public void asociarImagenes(EventoCatastrofeDto catastrofeDTO, EventoCatastrofe catastrofe) throws NegocioException{
		try{
			if (catastrofe.getImagenes() == null){
				catastrofe.setImagenes(new ArrayList<Imagen>());
			}
			else{
				catastrofe.getImagenes().clear();
			}
			
			if (catastrofeDTO.getImagenes() != null){
				
                for(ImagenDto imgDTO : catastrofeDTO.getImagenes()){
                	
                	Imagen imgen = (Imagen) imgDao.findByID(imgDTO.getId());
                        
                	catastrofe.getImagenes().add(imgen);
                }				
			}
		}
		catch(DaoException e){
			throw new NegocioException( e.getMessage());
		}
	}
	
	 private void asociarPlanes(EventoCatastrofeDto catastrofeDTO, EventoCatastrofe catastrofe) throws  NegocioException
	 {
		try{ 
			 if (catastrofe.getPlanesRescatistas() == null){
				 catastrofe.setPlanesRescatistas(new ArrayList<PlanRescatista>());
			 }
			 else{
				 catastrofe.getPlanesRescatistas().clear();
			 }
			 
			 if (catastrofeDTO.getPlanesRescatista() != null){
				 for (PlanRescatistaDto plane : catastrofeDTO.getPlanesRescatista()){
					 
					 PlanRescatista planRescatista = null;
					 if (!Utiles_SICA.isNullOrCero(plane.getId())){
						 planRescatista = planRescatistaDao.findByID(plane.getId());
					 }
					 else{
						 planRescatista = new PlanRescatista();
						 planRescatista.setFecha(new Date());
						 planRescatista.setIsCompletado(false);
						 planRescatista.setEventoCatastrofe(catastrofe);
						 planRescatista.setRescatista( rescatistaDao.findByID(plane.getIdRescatista()) );
						 planRescatista.setPlanesEmergencia( planDao.buscarTodosPlanesEmergencia() );
						 planRescatista.setPlanesRiesgo( planDao.buscarTodosPlanesRiesgo() ); 
						 
						 planRescatistaDao.persist(planRescatista);
					 }
					 catastrofe.getPlanesRescatistas().add( planRescatista );
				 }
			 }
		}
		catch(DaoException e){
			throw new NegocioException( e.getMessage());
		}			 
	 }
	
	//logica particular
    private void asociarOngs(EventoCatastrofeDto catastrofeDTO, EventoCatastrofe catastrofe) throws  NegocioException
    {
    	try{
            // Cargo la lista de ong para catastrofe
            if(catastrofe.getOngs() == null){
            	catastrofe.setOngs(new ArrayList<Ong>());
            }
            else{
            	catastrofe.getOngs().clear();
            }
            
            if(catastrofeDTO.getOngs() != null){
            	
                    for(OngDto ongDTO : catastrofeDTO.getOngs()){
                    	
                    	Ong ong = (Ong) ongDao.findByID(ongDTO.getId());
                            
                    	catastrofe.getOngs().add(ong);
                    }
            }
		}
		catch(DaoException e){
			throw new NegocioException( e.getMessage());
		}
    }	

}
