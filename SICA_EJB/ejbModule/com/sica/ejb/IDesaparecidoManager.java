package com.sica.ejb;

import java.util.List;

import javax.ejb.Local;

import com.sica.dto.DesaparecidoDto;
import com.sica.excepciones.NegocioException;
import com.sica.utiles.FiltroBusquedaDesaparecido;

@Local
public interface IDesaparecidoManager<Dto> extends IManager<Dto>{
	public List<DesaparecidoDto> busquedaDesaparecido( FiltroBusquedaDesaparecido filtro ) throws NegocioException;

	public List<DesaparecidoDto> buscarDesaparecidoPorUsuario(long idUsuario) throws NegocioException;
}
