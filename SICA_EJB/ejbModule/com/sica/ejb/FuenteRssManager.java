package com.sica.ejb;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import com.sica.dao.IFuenteRssDao;
import com.sica.dto.FuenteRssDto;
import com.sica.dto.assemblers.FuenteRssAssembler;
import com.sica.entities.FuenteRss;
import com.sica.excepciones.DaoException;
import com.sica.excepciones.NegocioException;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class FuenteRssManager implements IFuenteRssManager<FuenteRssDto> {

	@EJB
	private IFuenteRssDao fuenteRssDao;
    /**
     * Default constructor. 
     */
    public FuenteRssManager() {}
	
    
    public long insertar(FuenteRssDto fuenteRssDto ) throws NegocioException {
		try{
    		FuenteRss fuenteRss = FuenteRssAssembler.getInstance().toEntity(fuenteRssDto);
    	
    		fuenteRssDao.persist(fuenteRss);
    	
    		return fuenteRss.getId();
		}
		catch(DaoException e){
			throw new NegocioException( e.getMessage());
		}
    }
    
    public void actualizar( FuenteRssDto fuenteRssDto ) throws NegocioException{
		try{
    		FuenteRss fuenteRss = FuenteRssAssembler.getInstance().toEntity(fuenteRssDto);
    	
    		fuenteRssDao.merge(fuenteRss);
		}
		catch(DaoException e){
			throw new NegocioException( e.getMessage());
		}    	
    }
    
    public void eliminar( long id) throws NegocioException{
		try{
			
			FuenteRss fuenteRss = fuenteRssDao.findByID(id);
    		fuenteRssDao.remove(fuenteRss);
		}
		catch(DaoException e){
			throw new NegocioException( e.getMessage());
		}       	
    }
    
    public FuenteRssDto buscarPorId( long id ) throws NegocioException{
		try{
    		FuenteRss fuenteRss = fuenteRssDao.findByID( id );
    		FuenteRssDto fuenteRssDto = FuenteRssAssembler.getInstance().toDto(fuenteRss);
    		
    		return fuenteRssDto;
    		
		}
		catch(DaoException e){
			throw new NegocioException( e.getMessage());
		}  
    	
    }
    
    public List<FuenteRssDto>buscarTodos() throws NegocioException{
    	
    	try{
    		List<FuenteRss> listaFuenteRss = fuenteRssDao.findAll();
    		List<FuenteRssDto> listaFuenteRssDto = FuenteRssAssembler.getInstance().toDtoList(listaFuenteRss);
    		
    		return listaFuenteRssDto;
    	}
		catch( DaoException e ){
			throw new NegocioException( e.getMessage());
		}      	
    }
    
    @Override
    public FuenteRssDto validarFuenteUnicaPorRuta(String ruta, long id) throws NegocioException{
    	try {
			FuenteRss fuenteRss = fuenteRssDao.validarFuenteUnicaPorRuta( ruta, id );
			FuenteRssDto fuenteRssDto = null;
			if (fuenteRss != null){
				fuenteRssDto = FuenteRssAssembler.getInstance().toDto(fuenteRss);
			}
    		return fuenteRssDto;			
		} 
    	catch (DaoException e) {
    		throw new NegocioException( e.getMessage());
		}
    }
}
