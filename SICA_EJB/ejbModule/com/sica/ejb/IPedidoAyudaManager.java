package com.sica.ejb;

import java.util.List;

import javax.ejb.Local;

import com.sica.dto.PedidoAyudaDto;
import com.sica.excepciones.NegocioException;

@Local
public interface IPedidoAyudaManager<Dto> extends IManager<Dto> {

	public List<PedidoAyudaDto> buscarPedidosAyudaPorUsuario(long idUsuario) throws NegocioException;

}
