package com.sica.ejb;

import javax.ejb.Local;

import com.sica.dto.FuenteRssDto;
import com.sica.excepciones.NegocioException;

@Local
public interface IFuenteRssManager<Dto> extends IManager<Dto> {

	FuenteRssDto validarFuenteUnicaPorRuta(String ruta, long id) throws NegocioException;

}
