package com.sica.ejb;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import com.sica.dao.IPersonaDao;
import com.sica.dto.assemblers.AdministradorAssembler;
import com.sica.dto.assemblers.RescatistaAssembler;
import com.sica.dto.assemblers.UsuarioAssembler;
import com.sica.entities.Administrador;
import com.sica.entities.Persona;
import com.sica.entities.Rescatista;
import com.sica.entities.Usuario;
import com.sica.excepciones.DaoException;
import com.sica.excepciones.NegocioException;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class PersonaManager implements IPersonaManager {

	@EJB
	private IPersonaDao personaDao;
    /**
     * Default constructor. 
     */
    public PersonaManager() {}	
	
	@Override
	public Object buscarPorEmailAndPassword( String usuario, String password ) throws NegocioException{
		try{
			Persona usu = personaDao.buscarPorEmailYPassword(usuario, password);
			Object object = null;
			
			if (usu == null){
				 throw new NegocioException("Usuario no encontrado.");
			}
			else{
				if (usu instanceof Usuario){
					object = UsuarioAssembler.getInstance().toDto((Usuario)usu);
				}
				else if (usu instanceof Rescatista){
					object = RescatistaAssembler.getInstance().toDto((Rescatista) usu);	
				}
				return object;
			}
		}
		catch (DaoException e) {
			throw new NegocioException("Error en en acceso a datos.");
		}
	}	
	
	
	@Override
	public Object buscarPorEmailAndPasswordAndTipo( String usuario, String password, int tipo) throws NegocioException{
		try{
			Persona usu = personaDao.buscarPorEmailYPasswordAndTipo(usuario, password, tipo);
			Object object = null;
			
			if (usu == null){
				 throw new NegocioException("Usuario no encontrado.");
			}
			else{
				if (usu instanceof Usuario){
					object = UsuarioAssembler.getInstance().toDto((Usuario)usu);
				}
				else if (usu instanceof Rescatista){
					object = RescatistaAssembler.getInstance().toDto((Rescatista) usu);	
				}
				else if (usu instanceof Administrador){
					object = AdministradorAssembler.getInstance().toDto((Administrador) usu);
				}
				return object;
			}
		}
		catch (DaoException e) {
			throw new NegocioException("Error en en acceso a datos.");
		}
	}	
	
	public Object buscarUsuarioPorId( long id) throws NegocioException{
		try{
			Persona usu = personaDao.findByID(id);
			Object object = null;
			
			if (usu == null){
				 throw new NegocioException("Usuario no encontrado.");
			}
			else{
				if (usu instanceof Usuario){
					object = UsuarioAssembler.getInstance().toDto((Usuario)usu);
				}
				else if (usu instanceof Rescatista){
					object = RescatistaAssembler.getInstance().toDto((Rescatista) usu);	
				}
				else if (usu instanceof Administrador){
					object = AdministradorAssembler.getInstance().toDto((Administrador) usu);
				}
				return object;
			}			
		}
		catch (DaoException e) {
			throw new NegocioException("Error en en acceso a datos.");
		}
	}
	
	
	@Override
	public Object buscarAdminPorEmail( String email, long id ) throws NegocioException {
		try{
			Object object = null;
			Persona usu = personaDao.buscarAdminPorEmail( email, id );
			
			if (usu instanceof Usuario){
				object = UsuarioAssembler.getInstance().toDto((Usuario)usu);
			}
			else if (usu instanceof Rescatista){
				object = RescatistaAssembler.getInstance().toDto((Rescatista) usu);	
			}
			else if (usu instanceof Administrador){
				object = AdministradorAssembler.getInstance().toDto((Administrador) usu);
			}
			return object;
    		
		}
		catch(DaoException e){
			throw new NegocioException( e.getMessage());
		}  
	}	
}
