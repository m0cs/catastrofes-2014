package com.sica.ejb;

import javax.ejb.Local;

import com.sica.dto.TipoCatastrofeDto;
import com.sica.excepciones.NegocioException;

@Local
public interface ITipoCatastrofeManager<Dto> extends IManager<Dto> {

	public TipoCatastrofeDto validarPorNombre(String nombre, long id) throws NegocioException;

	
}
