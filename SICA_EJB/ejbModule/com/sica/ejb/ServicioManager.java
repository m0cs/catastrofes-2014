package com.sica.ejb;

import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import com.sica.dao.IOngDao;
import com.sica.dao.IServicioDao;
import com.sica.dto.ServicioDto;
import com.sica.dto.assemblers.ServicioAssembler;
import com.sica.entities.Ong;
import com.sica.entities.Servicio;
import com.sica.excepciones.DaoException;
import com.sica.excepciones.NegocioException;
import com.sica.utiles.Utiles_JPA;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class ServicioManager implements IServicioManager<ServicioDto> {

	@EJB
	private IServicioDao servicioDao;
	
	@EJB
	private IOngDao ongDao;
	
	@Override
	public long insertar(ServicioDto dto) throws NegocioException {
		try{
			Servicio servicio = ServicioAssembler.getInstance().toEntity(dto);
    	
			servicio.setFecha(new Date());
			servicio.setTipo(Utiles_JPA.BIEN);
			
			if (!Utiles_JPA.isNullOrCero(dto.getIdOng())){
				Ong ong = ongDao.findByID(dto.getIdOng());
				servicio.setOng(ong);
			}			
			servicioDao.persist(servicio);
    	
    		return servicio.getId();
		}
		catch(DaoException e){
			throw new NegocioException( e.getMessage());
		}
	}

	@Override
	public void actualizar(ServicioDto dto) throws NegocioException {
		try{
			Servicio servicio = ServicioAssembler.getInstance().toEntity(dto);
    	
			servicioDao.merge( servicio  );
		}
		catch(DaoException e){
			throw new NegocioException( e.getMessage());
		}  
	}

	@Deprecated
	@Override
	public void eliminar(long id) throws NegocioException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public ServicioDto buscarPorId(long id) throws NegocioException {
		try{
			Servicio servicio = servicioDao.findByID( id );
			ServicioDto servicioDto = ServicioAssembler.getInstance().toDto(servicio);
    		
    		return servicioDto;
    		
		}
		catch(DaoException e){
			throw new NegocioException( e.getMessage());
		}  
	}

	@Override
	public List<ServicioDto> buscarTodos() throws NegocioException {
    	try{
    		List<Servicio> listaServicio = servicioDao.findAll();
    		List<ServicioDto> listaServicioDto = ServicioAssembler.getInstance().toDtoList(listaServicio);
    		
    		return listaServicioDto;
    	}
		catch( DaoException e ){
			throw new NegocioException( e.getMessage());
		}  
	}

}
