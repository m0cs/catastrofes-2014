package com.sica.resources;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.sica.dto.LoginDto;
import com.sica.dto.RegistroUsuarioDto;
import com.sica.dto.ServiceResponse;
import com.sica.dto.UsuarioDto;
import com.sica.ejb.IPersonaManager;
import com.sica.ejb.IUsuarioManager;
import com.sica.excepciones.NegocioException;
import com.sica.utiles.Utiles_SICA;

@Path("users")
public class PersonaResources {

	@EJB
	private IPersonaManager personaEJB;
	
	@EJB
	private IUsuarioManager<UsuarioDto> usuEJB;
	
	@GET
	@Produces("application/json")
	@Path("login/{email}/{password}/{tipo}")
	public String buscarUsuarioPorPasswordAndEmail( @PathParam("email") String email, @PathParam("password") String password, @PathParam("tipo") String tipo ){
		
		try{		
			Object user = personaEJB.buscarPorEmailAndPasswordAndTipo(email, password, Integer.valueOf(tipo));
			
			Gson gson = new Gson();
			return gson.toJson(user);
		}
		catch(NegocioException e){
			e.printStackTrace();
			ServiceResponse.getInstance().reset();
			ServiceResponse.getInstance().setCod(ServiceResponse.COD_ERROR);
			ServiceResponse.getInstance().setEstado(ServiceResponse.STATUS_ERROR);
			ServiceResponse.getInstance().setMensajes(e.getMessage());
			Gson gson = new Gson();
			return gson.toJson(ServiceResponse.getInstance());				
		}
	}
	
	@GET
	@Produces("application/json")
	@Path("buscar/{id}/")
	public String buscarUsuarioPorId( @PathParam("id") String id ){
		
		try{		
			Object user = personaEJB.buscarUsuarioPorId( Long.valueOf(id));
			
			Gson gson = new Gson();
			return gson.toJson(user);
		}
		catch(NegocioException e){
			e.printStackTrace();
			ServiceResponse.getInstance().reset();
			ServiceResponse.getInstance().setCod(ServiceResponse.COD_ERROR);
			ServiceResponse.getInstance().setEstado(ServiceResponse.STATUS_ERROR);
			ServiceResponse.getInstance().setMensajes(e.getMessage());
			Gson gson = new Gson();
			return gson.toJson(ServiceResponse.getInstance());			
		}
	}	
	
	@POST
	@Consumes("application/json")
	@Produces("application/json")
	@Path("editar/")
	public String editarUsuarioPorId( UsuarioDto usuario ){
		
		try{		
			usuEJB.actualizar(usuario);
			
			ServiceResponse.getInstance().reset();
			ServiceResponse.getInstance().setCod(ServiceResponse.STATUS_OK);
			ServiceResponse.getInstance().setEstado(ServiceResponse.STATUS_OK);
			ServiceResponse.getInstance().setMensajes("Solicitud realizada satisfactoriamente.");
			Gson gson = new Gson();
			return gson.toJson(ServiceResponse.getInstance());
		}
		catch(NegocioException e){
			e.printStackTrace();
			ServiceResponse.getInstance().reset();
			ServiceResponse.getInstance().setCod(ServiceResponse.COD_ERROR);
			ServiceResponse.getInstance().setEstado(ServiceResponse.STATUS_ERROR);
			ServiceResponse.getInstance().setMensajes(e.getMessage());
			Gson gson = new Gson();
			return gson.toJson(ServiceResponse.getInstance());			
		}
	}		
	
	@GET
	@Produces("application/json")
	@Consumes("text/plain")
	@Path("/validarPorEmail")
	public String buscarAdminPorEmail( @QueryParam("adminForm:admin_email") String email, @QueryParam("id") String id) {
		
		try{		
			long idAdmin = Utiles_SICA.isNullOrEmpty(id) ? 0 : Long.valueOf(id);
			Object objDto = personaEJB.buscarAdminPorEmail(email, idAdmin);

			boolean valid = false;
			if (objDto == null){
				valid = true;
			}
			JsonObject obj = new JsonObject();
			obj.addProperty("valid", valid);
			
			return obj.toString();
		}
		catch(NegocioException e){
			e.printStackTrace();
			ServiceResponse.getInstance().reset();
			ServiceResponse.getInstance().setCod(ServiceResponse.COD_ERROR);
			ServiceResponse.getInstance().setEstado(ServiceResponse.STATUS_ERROR);
			ServiceResponse.getInstance().setMensajes(e.getMessage());
			Gson gson = new Gson();
			return gson.toJson(ServiceResponse.getInstance());			
		}
	}		
	
	@GET
	@Produces("application/json")
	@Consumes("text/plain")
	@Path("/validarPorEmailResc") 
	public String buscarRescPorEmail( @QueryParam("rescatistaForm:rescatista_email") String email, @QueryParam("id") String id) {
		
		try{		
			long idAdmin = Utiles_SICA.isNullOrEmpty(id) ? 0 : Long.valueOf(id);
			Object objDto = personaEJB.buscarAdminPorEmail(email, idAdmin);

			boolean valid = false;
			if (objDto == null){
				valid = true;
			}
			JsonObject obj = new JsonObject();
			obj.addProperty("valid", valid);
			
			return obj.toString();
		}
		catch(NegocioException e){
			e.printStackTrace();
			ServiceResponse.getInstance().reset();
			ServiceResponse.getInstance().setCod(ServiceResponse.COD_ERROR);
			ServiceResponse.getInstance().setEstado(ServiceResponse.STATUS_ERROR);
			ServiceResponse.getInstance().setMensajes(e.getMessage());
			Gson gson = new Gson();
			return gson.toJson(ServiceResponse.getInstance());			
		}
	}		
	
	@POST
	@Produces("application/json")
	@Consumes("application/json")
	@Path("login/")
	public String loginUsuario( LoginDto loginDto ){
		
		try{		
			Object user = personaEJB.buscarPorEmailAndPasswordAndTipo(loginDto.getEmail(), loginDto.getPassword(), Integer.valueOf(loginDto.getTipo()));
			Gson gson = new Gson();
			return gson.toJson(user);
		}
		catch(NegocioException e){
			e.printStackTrace();
			ServiceResponse.getInstance().reset();
			ServiceResponse.getInstance().setCod(ServiceResponse.COD_ERROR);
			ServiceResponse.getInstance().setEstado(ServiceResponse.STATUS_ERROR);
			ServiceResponse.getInstance().setMensajes(e.getMessage());
			Gson gson = new Gson();
			return gson.toJson(ServiceResponse.getInstance());			
		}
	}	
	
	
	@POST
	@Produces("application/json")
	@Consumes("application/json")
	@Path("registro/")
	public String registoUsuario( RegistroUsuarioDto registroDto ){
		
		try {
			UsuarioDto usuario = usuEJB.registrarUsuario(registroDto);
			
			Gson gson = new Gson();
			return gson.toJson(usuario);
		} 
		catch (NegocioException e) {
			e.printStackTrace();
			ServiceResponse.getInstance().reset();
			ServiceResponse.getInstance().setCod(ServiceResponse.COD_ERROR);
			ServiceResponse.getInstance().setEstado(ServiceResponse.STATUS_ERROR);
			ServiceResponse.getInstance().setMensajes(e.getMessage());
			Gson gson = new Gson();
			return gson.toJson(ServiceResponse.getInstance());	
		}
	}
	
}