package com.sica.resources;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import com.google.gson.Gson;
import com.sica.dto.BienDto;
import com.sica.dto.DonacionDto;
import com.sica.dto.ServiceResponse;
import com.sica.dto.ServicioDto;
import com.sica.ejb.IBienManager;
import com.sica.ejb.IDonacionManager;
import com.sica.ejb.IServicioManager;
import com.sica.excepciones.NegocioException;

@Path("donaciones")
public class DonacionResources {
	
	@EJB
	private IServicioManager<ServicioDto> servicioEJB;
	
	@EJB
	private IBienManager<BienDto> bienEJB;
	
	@EJB 
	private IDonacionManager<DonacionDto> donacionEJB;

	@POST
	@Produces("application/json")
	@Consumes("application/json")
	@Path("nuevo/servicio/")
	public String crearDonacionServicio(ServicioDto servicioDto ){
		
		try{		
			long id = servicioEJB.insertar( servicioDto );
			ServiceResponse.getInstance().reset();
			ServiceResponse.getInstance().setCod(ServiceResponse.COD_OK);
			ServiceResponse.getInstance().setEstado(ServiceResponse.STATUS_OK);
			ServiceResponse.getInstance().setMensajes(ServiceResponse.MENSAJE);
			ServiceResponse.getInstance().setElemento(String.valueOf(id));
			Gson gson = new Gson();
			return gson.toJson(ServiceResponse.getInstance());	
		}
		catch(NegocioException e){
			e.printStackTrace();
			ServiceResponse.getInstance().reset();
			ServiceResponse.getInstance().setCod(ServiceResponse.COD_ERROR);
			ServiceResponse.getInstance().setEstado(ServiceResponse.STATUS_ERROR);
			ServiceResponse.getInstance().setMensajes(e.getMessage());
			Gson gson = new Gson();
			return gson.toJson(ServiceResponse.getInstance());			
		}
	}	
	
	
	@POST
	@Produces("application/json")
	@Consumes("application/json")
	@Path("nuevo/bien/")
	public String crearDonacionBien(BienDto bienDto ){
		
		try{		
			long id = bienEJB.insertar( bienDto );
			ServiceResponse.getInstance().reset();
			ServiceResponse.getInstance().setCod(ServiceResponse.COD_OK);
			ServiceResponse.getInstance().setEstado(ServiceResponse.STATUS_OK);
			ServiceResponse.getInstance().setMensajes(ServiceResponse.MENSAJE);
			ServiceResponse.getInstance().setElemento(String.valueOf(id));
			Gson gson = new Gson();
			return gson.toJson(ServiceResponse.getInstance());	
		}
		catch(NegocioException e){
			e.printStackTrace();
			ServiceResponse.getInstance().reset();
			ServiceResponse.getInstance().setCod(ServiceResponse.COD_ERROR);
			ServiceResponse.getInstance().setEstado(ServiceResponse.STATUS_ERROR);
			ServiceResponse.getInstance().setMensajes(e.getMessage());
			Gson gson = new Gson();
			return gson.toJson(ServiceResponse.getInstance());			
		}
	}
	
	/*
	@GET
	@Produces("application/json")
	@Path("buscarTodos/")
	public String buscarTodos(){
		
		try{		
			List<DonacionDto> donaciones = donacionEJB.buscarTodos();			
			Gson gson = new Gson();
			return gson.toJson(user);
		}
		catch(NegocioException e){
			e.printStackTrace();
			ServiceResponse.getInstance().reset();
			ServiceResponse.getInstance().setCod(ServiceResponse.COD_ERROR);
			ServiceResponse.getInstance().setEstado(ServiceResponse.STATUS_ERROR);
			ServiceResponse.getInstance().setMensajes(e.getMessage());
			Gson gson = new Gson();
			return gson.toJson(ServiceResponse.getInstance());			
		}
	}*/	
	
}
