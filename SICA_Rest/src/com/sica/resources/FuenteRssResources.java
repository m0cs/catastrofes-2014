package com.sica.resources;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.sica.dto.FuenteRssDto;
import com.sica.dto.ServiceResponse;
import com.sica.ejb.IFuenteRssManager;
import com.sica.excepciones.NegocioException;
import com.sica.utiles.Utiles_SICA;

@Path("fuentes")
public class FuenteRssResources {

	@EJB
	private IFuenteRssManager<FuenteRssDto> fuenteEJB;
	
	@GET
	@Produces("application/json")
	@Path("buscar/{id}")
	public String buscarPorId( @PathParam("id") String id ){
		
		try{		
			FuenteRssDto fuente = fuenteEJB.buscarPorId( Long.valueOf(id) );
			
			Gson gson = new Gson();
			
			return gson.toJson(fuente);
		}
		catch(NegocioException e){
			ServiceResponse.getInstance().reset();
			ServiceResponse.getInstance().setCod(ServiceResponse.COD_ERROR);
			ServiceResponse.getInstance().setEstado(ServiceResponse.STATUS_ERROR);
			ServiceResponse.getInstance().setMensajes(e.getMessage());
			Gson gson = new Gson();
			return gson.toJson(ServiceResponse.getInstance());			
		}
	}
	
	@GET
	@Produces("application/json")
	@Consumes("text/plain")
	@Path("/validarPorRuta")
	public String validarFuentePorRuta( @QueryParam("fuenteForm:fuente_ruta") String ruta, @QueryParam("id") String id) {
		
		try{		
			long idfuente = Utiles_SICA.isNullOrEmpty(id) ? 0 : Long.valueOf(id);
			FuenteRssDto fuenteDto = fuenteEJB.validarFuenteUnicaPorRuta(ruta, idfuente);

			boolean valid = false;
			if (fuenteDto == null){
				valid = true;
			}
			JsonObject obj = new JsonObject();
			obj.addProperty("valid", valid);
			
			return obj.toString();
		}
		catch(NegocioException e){
			e.printStackTrace();
			ServiceResponse.getInstance().reset();
			ServiceResponse.getInstance().setCod(ServiceResponse.COD_ERROR);
			ServiceResponse.getInstance().setEstado(ServiceResponse.STATUS_ERROR);
			ServiceResponse.getInstance().setMensajes(e.getMessage());
			Gson gson = new Gson();
			return gson.toJson(ServiceResponse.getInstance());	
		}
	}	
		
}
