package com.sica.resources;

import java.util.List;

import javax.ejb.EJB;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import com.google.gson.Gson;
import com.sica.dto.EventoCatastrofeDto;
import com.sica.dto.ServiceResponse;
import com.sica.ejb.IEventoCatastrofeManager;
import com.sica.excepciones.NegocioException;

@Path("eventosCatastrofes")
public class EventoCatastrofeResources {

	
	@EJB
	IEventoCatastrofeManager<EventoCatastrofeDto> eventoEJB;
	
	@GET
	@Produces("application/json")
	@Path("buscar/{id}/")
	public String buscarEventoCatastrofePorId( @PathParam("id") String id ){
		
		try{	
			EventoCatastrofeDto tipoDto = eventoEJB.buscarPorId(Long.valueOf(id));
			
			Gson gson = new Gson();
			return gson.toJson( tipoDto );
		}
		catch(NegocioException e){
			ServiceResponse.getInstance().reset();
			ServiceResponse.getInstance().setCod(ServiceResponse.COD_ERROR);
			ServiceResponse.getInstance().setEstado(ServiceResponse.STATUS_ERROR);
			ServiceResponse.getInstance().setMensajes(e.getMessage());
			Gson gson = new Gson();
			return gson.toJson(ServiceResponse.getInstance());	
		}
	}	
	
	
	@GET
	@Produces("application/json")
	@Path("buscarTodos/")
	public String buscarTodosEventoCatastrofe(){
		
		try{	
			List<EventoCatastrofeDto> listaCataDto = eventoEJB.buscarTodos();
			
			Gson gson = new Gson();
			return gson.toJson( listaCataDto );
		}
		catch(NegocioException e){
			ServiceResponse.getInstance().reset();
			ServiceResponse.getInstance().setCod(ServiceResponse.COD_ERROR);
			ServiceResponse.getInstance().setEstado(ServiceResponse.STATUS_ERROR);
			ServiceResponse.getInstance().setMensajes(e.getMessage());
			Gson gson = new Gson();
			return gson.toJson(ServiceResponse.getInstance());	
		}
	}		
	
	
	
	
}
