package com.sica.resources;

import java.util.List;

import javax.ejb.EJB;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import com.google.gson.Gson;
import com.sica.dto.EventoCatastrofeDto;
import com.sica.dto.ServiceResponse;
import com.sica.ejb.IEventoCatastrofeManager;
import com.sica.excepciones.NegocioException;

@Path("catastrofes")
public class EventoCastastrofeResources {

	@EJB
	IEventoCatastrofeManager<EventoCatastrofeDto> cataEJB;
	
	@GET
	@Produces("application/json")
	@Path("buscar/{id}/")
	public String buscarCatastrofePorId( @PathParam("id") String id ){
		
		try{		
			EventoCatastrofeDto evento = cataEJB.buscarPorId( Long.valueOf(id));
			
			Gson gson = new Gson();
			return gson.toJson(evento);
		}
		catch(NegocioException e){
			e.printStackTrace();
			ServiceResponse.getInstance().reset();
			ServiceResponse.getInstance().setCod(ServiceResponse.COD_ERROR);
			ServiceResponse.getInstance().setEstado(ServiceResponse.STATUS_ERROR);
			ServiceResponse.getInstance().setMensajes(e.getMessage());
			Gson gson = new Gson();
			return gson.toJson(ServiceResponse.getInstance());			
		}
	}
	
	@GET
	@Produces("application/json")
	@Path("buscarTodos/")
	public String buscarTodasCatastrofes( ){
		
		try{		
			List<EventoCatastrofeDto> eventos = cataEJB.buscarTodos();
			
			Gson gson = new Gson();
			if (eventos == null || eventos.isEmpty()){
				return gson.toJson("");
			}
			else{
				return gson.toJson(eventos);
			}
		}
		catch(NegocioException e){
			e.printStackTrace();
			ServiceResponse.getInstance().reset();
			ServiceResponse.getInstance().setCod(ServiceResponse.COD_ERROR);
			ServiceResponse.getInstance().setEstado(ServiceResponse.STATUS_ERROR);
			ServiceResponse.getInstance().setMensajes(e.getMessage());
			Gson gson = new Gson();
			return gson.toJson(ServiceResponse.getInstance());			
		}
	}	
}
