package com.sica.resources;

import java.util.List;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import com.google.gson.Gson;
import com.sica.dto.PedidoAyudaDto;
import com.sica.dto.ServiceResponse;
import com.sica.ejb.IPedidoAyudaManager;
import com.sica.excepciones.NegocioException;

@Path("pedidos")
public class PedidoAyudaResources {

	@EJB
	private IPedidoAyudaManager<PedidoAyudaDto> pedidosAyudaEJB;

	@POST
	@Produces("application/json")
	@Consumes("application/json")
	@Path("nuevo/")
	public String ingresarPedido( PedidoAyudaDto pedidoDto ) {
		
		try {
			long id = pedidosAyudaEJB.insertar(pedidoDto);
			
			ServiceResponse.getInstance().reset();
			ServiceResponse.getInstance().setCod(ServiceResponse.COD_OK);
			ServiceResponse.getInstance().setEstado(ServiceResponse.STATUS_OK);
			ServiceResponse.getInstance().setMensajes(ServiceResponse.MENSAJE);
			ServiceResponse.getInstance().setElemento(String.valueOf(id));
			Gson gson = new Gson();
			return gson.toJson(ServiceResponse.getInstance());	
		} 
		catch (NegocioException e) {
			e.printStackTrace();
			ServiceResponse.getInstance().reset();
			ServiceResponse.getInstance().setCod(ServiceResponse.COD_ERROR);
			ServiceResponse.getInstance().setEstado(ServiceResponse.STATUS_ERROR);
			ServiceResponse.getInstance().setMensajes(e.getMessage());
			Gson gson = new Gson();
			return gson.toJson(ServiceResponse.getInstance());	
		}
	}
	
	@GET
	@Produces("application/json")
	@Path("buscar/{id}/")
	public String buscarPedidoAyudaPorId( @PathParam("id") String id ){
		
		try{		
			PedidoAyudaDto pedidoAyuda = pedidosAyudaEJB.buscarPorId( Long.valueOf(id));
			
			Gson gson = new Gson();
			return gson.toJson(pedidoAyuda);
		}
		catch(NegocioException e){
			e.printStackTrace();
			ServiceResponse.getInstance().reset();
			ServiceResponse.getInstance().setCod(ServiceResponse.COD_ERROR);
			ServiceResponse.getInstance().setEstado(ServiceResponse.STATUS_ERROR);
			ServiceResponse.getInstance().setMensajes(e.getMessage());
			Gson gson = new Gson();
			return gson.toJson(ServiceResponse.getInstance());			
		}
	}	
	
	@GET
	@Produces("application/json")
	@Path("buscarPorUsuario/{id}/")
	public String buscarPedidoAyudaPorUsuario( @PathParam("id") String id ){
		
		try{		
			List<PedidoAyudaDto> pedidoAyudas = pedidosAyudaEJB.buscarPedidosAyudaPorUsuario(Long.valueOf(id));
			
			Gson gson = new Gson();
			if (pedidoAyudas == null || pedidoAyudas.isEmpty()){
				return gson.toJson("");
			}
			else{
				return gson.toJson(pedidoAyudas);
			}
		}
		catch(NegocioException e){
			e.printStackTrace();
			ServiceResponse.getInstance().reset();
			ServiceResponse.getInstance().setCod(ServiceResponse.COD_ERROR);
			ServiceResponse.getInstance().setEstado(ServiceResponse.STATUS_ERROR);
			ServiceResponse.getInstance().setMensajes(e.getMessage());
			Gson gson = new Gson();
			return gson.toJson(ServiceResponse.getInstance());			
		}
	}		
	
}
