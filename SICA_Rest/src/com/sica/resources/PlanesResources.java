package com.sica.resources;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.sica.dto.PlanDto;
import com.sica.dto.ServiceResponse;
import com.sica.ejb.IPlanManager;
import com.sica.excepciones.NegocioException;
import com.sica.utiles.Utiles_JPA;
import com.sica.utiles.Utiles_SICA;

@Path("planes")
public class PlanesResources {
	
	@EJB
	IPlanManager planManager;
	
	@GET
	@Produces("application/json")
	@Path("buscar/{id}/{tipo}/")
	public String buscarPlanPasoPorId( @PathParam("id") String id , @PathParam("tipo") String tipo ){
		
		try{	
			PlanDto planDto = null;
			if (Utiles_JPA.PLAN_EMERGENCIA == Integer.valueOf(tipo)){
				planDto = planManager.buscarPlanPasoEmergenciaPorId(Long.valueOf(id));
			}
			if (Utiles_JPA.PLAN_RIESGO == Integer.valueOf(tipo)){
				planDto = planManager.buscarPlanPasoRiesgoPorId(Long.valueOf(id));
			}
			
			Gson gson = new Gson();
			return gson.toJson(planDto);
		}
		catch(NegocioException e){
			ServiceResponse.getInstance().reset();
			ServiceResponse.getInstance().setCod(ServiceResponse.COD_ERROR);
			ServiceResponse.getInstance().setEstado(ServiceResponse.STATUS_ERROR);
			ServiceResponse.getInstance().setMensajes(e.getMessage());
			Gson gson = new Gson();
			return gson.toJson(ServiceResponse.getInstance());	
		}
	}	
	
	
	@GET
	@Produces("application/json")
	@Consumes("text/plain")
	@Path("/validarOrden")
	public String validarOrdenPlanPaso( @QueryParam("planForm:planes_orden") String orden, @QueryParam("id") String id, @QueryParam("tipo") String tipo ) {
		
		try{		
			long idPaso = Utiles_SICA.isNullOrEmpty(id) ? 0 : Long.valueOf(id);
			PlanDto planDto = planManager.validarUnicoOrdenPorPasoPlan(Long.valueOf(orden), idPaso, Integer.valueOf(tipo));

			boolean valid = false;
			if (planDto == null){
				valid = true;
			}
			JsonObject obj = new JsonObject();
			obj.addProperty("valid", valid);
			
			return obj.toString();
		}
		catch(NegocioException e){
			e.printStackTrace();
			ServiceResponse.getInstance().reset();
			ServiceResponse.getInstance().setCod(ServiceResponse.COD_ERROR);
			ServiceResponse.getInstance().setEstado(ServiceResponse.STATUS_ERROR);
			ServiceResponse.getInstance().setMensajes(e.getMessage());
			Gson gson = new Gson();
			return gson.toJson(ServiceResponse.getInstance());	
		}
	}	
}
