package com.sica.resources;

import java.util.List;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.sica.dto.OngDto;
import com.sica.dto.ServiceResponse;
import com.sica.ejb.IOngManager;
import com.sica.excepciones.NegocioException;
import com.sica.utiles.Utiles_SICA;

@Path("ongs")
public class OngResources {

	@EJB
	private IOngManager<OngDto> ongEJB;
	
	@GET
	@Produces("application/json")
	@Consumes("text/plain")
	@Path("/validarPorEmail")
	public String buscarOngPorEmail( @QueryParam("ongForm:ong_email") String email, @QueryParam("id") String id) {
		
		try{		
			long idOng = Utiles_SICA.isNullOrEmpty(id) ? 0 : Long.valueOf(id);
			OngDto ongDto = ongEJB.buscarOngPorEmail(email, idOng);

			boolean valid = false;
			if (ongDto == null){
				valid = true;
			}
			JsonObject obj = new JsonObject();
			obj.addProperty("valid", valid);
			
			return obj.toString();
		}
		catch(NegocioException e){
			ServiceResponse.getInstance().reset();
			ServiceResponse.getInstance().setCod(ServiceResponse.COD_ERROR);
			ServiceResponse.getInstance().setEstado(ServiceResponse.STATUS_ERROR);
			ServiceResponse.getInstance().setMensajes(e.getMessage());
			Gson gson = new Gson();
			return gson.toJson(ServiceResponse.getInstance());			
		}
	}	
	
	
	@GET
	@Produces("application/json")
	@Path("buscar/{id}")
	public String buscarPorId( @PathParam("id") String id ){
		
		try{		
			OngDto fuente = ongEJB.buscarPorId( Long.valueOf(id) );
			Gson gson = new Gson();
			return gson.toJson(fuente);
		}
		catch(NegocioException e){
			ServiceResponse.getInstance().reset();
			ServiceResponse.getInstance().setCod(ServiceResponse.COD_ERROR);
			ServiceResponse.getInstance().setEstado(ServiceResponse.STATUS_ERROR);
			ServiceResponse.getInstance().setMensajes(e.getMessage());
			Gson gson = new Gson();
			return gson.toJson(ServiceResponse.getInstance());		
		}
	}	
	
	@GET
	@Produces("application/json")
	@Path("buscarTodos/")
	public String buscarTodos(){
		
		try{		
			List<OngDto> ongs = ongEJB.buscarTodos();
			Gson gson = new Gson();
			return gson.toJson(ongs);
		}
		catch(NegocioException e){
			ServiceResponse.getInstance().reset();
			ServiceResponse.getInstance().setCod(ServiceResponse.COD_ERROR);
			ServiceResponse.getInstance().setEstado(ServiceResponse.STATUS_ERROR);
			ServiceResponse.getInstance().setMensajes(e.getMessage());
			Gson gson = new Gson();
			return gson.toJson(ServiceResponse.getInstance());		
		}
	}		
}
