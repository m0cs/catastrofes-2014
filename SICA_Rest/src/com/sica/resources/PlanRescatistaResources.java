package com.sica.resources;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import com.google.gson.Gson;
import com.sica.dto.PlanRescatistaDto;
import com.sica.dto.ServiceResponse;
import com.sica.ejb.IPlanRescatistaManager;
import com.sica.excepciones.NegocioException;
import com.sica.utiles.Utiles_SICA;

@Path("planes")
public class PlanRescatistaResources {
	
	@EJB
	private IPlanRescatistaManager planRescatistaEJB;
	
	@GET
	@Produces("application/json")
	@Path("buscar/{email}")
	public String buscarPlanesPorRescatista ( @PathParam("email") String email ){
		
		try{		
			PlanRescatistaDto plan = planRescatistaEJB.buscarPlanPorEmailRescatista( email );
			
			Gson gson = new Gson();
			ServiceResponse.getInstance().reset();
			ServiceResponse.getInstance().setCod(ServiceResponse.COD_OK);
			ServiceResponse.getInstance().setEstado(ServiceResponse.STATUS_OK);
			ServiceResponse.getInstance().setMensajes("Solicitud ejecutada correctamente.");			
			
			if (plan == null){
				ServiceResponse.getInstance().setObjeto("");
			}
			else{
				ServiceResponse.getInstance().setObjeto(plan);
			}
			return gson.toJson(ServiceResponse.getInstance());	
		}
		catch(NegocioException e){
			e.printStackTrace();
			ServiceResponse.getInstance().reset();
			ServiceResponse.getInstance().setCod(ServiceResponse.COD_ERROR);
			ServiceResponse.getInstance().setEstado(ServiceResponse.STATUS_ERROR);
			ServiceResponse.getInstance().setMensajes(e.getMessage());
			Gson gson = new Gson();
			return gson.toJson(ServiceResponse.getInstance());				
		}
	}
	
	@GET
	@Produces("application/json")
	@Path("buscarCompletados/{email}")
	public String buscarPlanesCompletadosPorRescatista ( @PathParam("email") String email ){
		
		try{		
			List<PlanRescatistaDto> planes = planRescatistaEJB.buscarPlanesCompletadosPorRescatista( email );
			
			Gson gson = new Gson();
			ServiceResponse.getInstance().reset();
			ServiceResponse.getInstance().setCod(ServiceResponse.COD_OK);
			ServiceResponse.getInstance().setEstado(ServiceResponse.STATUS_OK);
			ServiceResponse.getInstance().setMensajes("Solicitud ejecutada correctamente.");			
			
			if (planes == null){
				ServiceResponse.getInstance().setListaObjetos(new ArrayList<Object>());
			}
			else{
				ServiceResponse.getInstance().setListaObjetos( Utiles_SICA.convertirPlanesToObjects(planes) );
			}
			return gson.toJson(ServiceResponse.getInstance());	
		}
		catch(NegocioException e){
			e.printStackTrace();
			ServiceResponse.getInstance().reset();
			ServiceResponse.getInstance().setCod(ServiceResponse.COD_ERROR);
			ServiceResponse.getInstance().setEstado(ServiceResponse.STATUS_ERROR);
			ServiceResponse.getInstance().setMensajes(e.getMessage());
			Gson gson = new Gson();
			return gson.toJson(ServiceResponse.getInstance());				
		}
	}	
	
	@GET
	@Produces("application/json")
	@Path("completar/{id}")
	public String completarPlanRescatista ( @PathParam("id") String idPlan ) {
		try{
			
			planRescatistaEJB.completarPlanRescatista( Long.valueOf(idPlan) );
			
			ServiceResponse.getInstance().reset();
			ServiceResponse.getInstance().setCod(ServiceResponse.COD_OK);
			ServiceResponse.getInstance().setEstado(ServiceResponse.STATUS_OK);
			ServiceResponse.getInstance().setMensajes("Plan completado correctamente.");
			ServiceResponse.getInstance().setObjeto("");
			Gson gson = new Gson();
			return gson.toJson(ServiceResponse.getInstance());		
		}
		catch(NegocioException e){
			e.printStackTrace();
			ServiceResponse.getInstance().reset();
			ServiceResponse.getInstance().setCod(ServiceResponse.COD_ERROR);
			ServiceResponse.getInstance().setEstado(ServiceResponse.STATUS_ERROR);
			ServiceResponse.getInstance().setMensajes(e.getMessage());
			Gson gson = new Gson();
			return gson.toJson(ServiceResponse.getInstance());				
		}
	}
}
