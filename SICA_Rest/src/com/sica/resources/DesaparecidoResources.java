package com.sica.resources;

import java.util.List;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import com.google.gson.Gson;
import com.sica.dto.DesaparecidoDto;
import com.sica.dto.ServiceResponse;
import com.sica.ejb.IDesaparecidoManager;
import com.sica.excepciones.NegocioException;
import com.sica.utiles.FiltroBusquedaDesaparecido;

@Path("desaparecidos")
public class DesaparecidoResources {
	

	@EJB
	private IDesaparecidoManager<DesaparecidoDto> desapaEJB;

	
	@POST
	@Produces("application/json")
	@Consumes("application/json")
	@Path("buscar/")
	public String buscarDesaparecido ( FiltroBusquedaDesaparecido filtro ){
		
		
		try{		
			List<DesaparecidoDto> desaprecidos = desapaEJB.busquedaDesaparecido(filtro);
			Gson gson = new Gson();
			return gson.toJson(desaprecidos);
		}
		catch(NegocioException e){
			e.printStackTrace();
			ServiceResponse.getInstance().reset();
			ServiceResponse.getInstance().setCod(ServiceResponse.COD_ERROR);
			ServiceResponse.getInstance().setEstado(ServiceResponse.STATUS_ERROR);
			ServiceResponse.getInstance().setMensajes(e.getMessage());
			Gson gson = new Gson();
			return gson.toJson(ServiceResponse.getInstance());			
		}
	}
	
	@POST
	@Produces("application/json")
	@Consumes("application/json")
	@Path("reportar/")
	public String reportarDesaparecido( DesaparecidoDto desaparecidoDto ){
		
		try{		
			long id = desapaEJB.insertar(desaparecidoDto);
			ServiceResponse.getInstance().reset();
			ServiceResponse.getInstance().setCod(ServiceResponse.COD_OK);
			ServiceResponse.getInstance().setEstado(ServiceResponse.STATUS_OK);
			ServiceResponse.getInstance().setMensajes(ServiceResponse.MENSAJE);
			ServiceResponse.getInstance().setElemento(String.valueOf(id));
			Gson gson = new Gson();
			return gson.toJson(ServiceResponse.getInstance());	
		}
		catch(NegocioException e){
			e.printStackTrace();
			ServiceResponse.getInstance().reset();
			ServiceResponse.getInstance().setCod(ServiceResponse.COD_ERROR);
			ServiceResponse.getInstance().setEstado(ServiceResponse.STATUS_ERROR);
			ServiceResponse.getInstance().setMensajes(e.getMessage());
			Gson gson = new Gson();
			return gson.toJson(ServiceResponse.getInstance());			
		}
	}	
	
	
	@GET
	@Consumes("application/json")
	@Path("buscarPorUsuario/{id}/")
	public String buscarDesaparecidoPorUsuario (@PathParam("id")  String idUsuario ){

		try{		
			List<DesaparecidoDto> desaprecidos = desapaEJB.buscarDesaparecidoPorUsuario(Long.valueOf(idUsuario));
			
			Gson gson = new Gson();
			if (desaprecidos == null || desaprecidos.isEmpty()){
				return gson.toJson("");
			}
			else{
				return gson.toJson(desaprecidos);
			}
		}
		catch(NegocioException e){
			e.printStackTrace();
			ServiceResponse.getInstance().reset();
			ServiceResponse.getInstance().setCod(ServiceResponse.COD_ERROR);
			ServiceResponse.getInstance().setEstado(ServiceResponse.STATUS_ERROR);
			ServiceResponse.getInstance().setMensajes(e.getMessage());
			Gson gson = new Gson();
			return gson.toJson(ServiceResponse.getInstance());			
		}
	}

	
	@GET
	@Produces("application/json")
	@Path("buscarTodos/")
	public String buscarTodosDesaparecido(){

		try{		
			List<DesaparecidoDto> desaprecidos = desapaEJB.buscarTodos();
			
			Gson gson = new Gson();
			if (desaprecidos == null || desaprecidos.isEmpty()){
				return gson.toJson("");
			}
			else{
				return gson.toJson(desaprecidos);
			}
		}
		catch(NegocioException e){
			e.printStackTrace();
			ServiceResponse.getInstance().reset();
			ServiceResponse.getInstance().setCod(ServiceResponse.COD_ERROR);
			ServiceResponse.getInstance().setEstado(ServiceResponse.STATUS_ERROR);
			ServiceResponse.getInstance().setMensajes(e.getMessage());
			Gson gson = new Gson();
			return gson.toJson(ServiceResponse.getInstance());			
		}
	}	
	
}
