package com.sica.resources;

import java.io.InputStream;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MultivaluedMap;

import org.apache.commons.io.IOUtils;
import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;

import com.google.gson.Gson;
import com.sica.dto.ImagenDto;
import com.sica.dto.ServiceResponse;
import com.sica.ejb.IImagenManager;
import com.sica.utiles.UtilesFiles;


@Path("uploads")
public class FileUploadResources {
	
	@EJB
	private IImagenManager imagenEJB;
	
	@POST
	@Path("file/")
	@Consumes("multipart/form-data")
	public String uploadFile( MultipartFormDataInput  input ){
		
	    
		String fileName = "";
		String appSeverRoute = "";
		 
		Map<String, List<InputPart>> uploadForm = input.getFormDataMap();
		List<InputPart> inputParts = uploadForm.get("file");
	    
		
		for (InputPart inputPart : inputParts) {
			 
			 try {
				MultivaluedMap<String, String> header = inputPart.getHeaders();
				fileName = UtilesFiles.getFileName(header);
	 
				//convierte el archivo subido en inputstream
				InputStream inputStream = inputPart.getBody(InputStream.class,null);
	 
				byte [] bytes = IOUtils.toByteArray(inputStream);
	 
				//constuctor de la ruta de archivo
				appSeverRoute = UtilesFiles.APP_SERVER_UPLOAD_LOCATION_FOLDER + fileName;
				fileName = UtilesFiles.SERVER_UPLOAD_LOCATION_FOLDER + fileName;
				
				
				UtilesFiles.writeFile(bytes,fileName);
				
				ImagenDto imagenDto = new ImagenDto();
				imagenDto.setRuta(appSeverRoute);
				
				long id = imagenEJB.insertar(imagenDto);
				return String.valueOf(id);

			  } 
			 catch (Exception e) {
					e.printStackTrace();
					ServiceResponse.getInstance().reset();
					ServiceResponse.getInstance().setCod(ServiceResponse.COD_ERROR);
					ServiceResponse.getInstance().setEstado(ServiceResponse.STATUS_ERROR);
					ServiceResponse.getInstance().setMensajes(e.getMessage());
					Gson gson = new Gson();
					return gson.toJson(ServiceResponse.getInstance());
			  }
			}
		return null;		
	}
}
