package com.sica.resources;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.sica.dto.ServiceResponse;
import com.sica.dto.TipoCatastrofeDto;
import com.sica.ejb.ITipoCatastrofeManager;
import com.sica.excepciones.NegocioException;
import com.sica.utiles.Utiles_SICA;

@Path("tiposCatastrofes")
public class TipoCatastrofeResources {

	@EJB
	ITipoCatastrofeManager<TipoCatastrofeDto> tipoEJB;
	
	@GET
	@Produces("application/json")
	@Path("buscar/{id}/")
	public String buscarTipoCatastrofePorId( @PathParam("id") String id ){
		
		try{	
			TipoCatastrofeDto tipoDto = tipoEJB.buscarPorId(Long.valueOf(id));
			
			Gson gson = new Gson();
			return gson.toJson( tipoDto );
		}
		catch(NegocioException e){
			ServiceResponse.getInstance().reset();
			ServiceResponse.getInstance().setCod(ServiceResponse.COD_ERROR);
			ServiceResponse.getInstance().setEstado(ServiceResponse.STATUS_ERROR);
			ServiceResponse.getInstance().setMensajes(e.getMessage());
			Gson gson = new Gson();
			return gson.toJson(ServiceResponse.getInstance());	
		}
	}	
	

	@GET
	@Produces("application/json")
	@Consumes("text/plain")
	@Path("/validarPorNombre")
	public String buscarTipoCatastrofePorNombre( @QueryParam("tipoCatastrofeForm:tipoCatastrofe_nombre") String nombre, @QueryParam("id") String id) {
		
		try{		
			long idtipo = Utiles_SICA.isNullOrEmpty(id) ? 0 : Long.valueOf(id);
			TipoCatastrofeDto tipoDto = tipoEJB.validarPorNombre(nombre, idtipo);

			boolean valid = false;
			if (tipoDto == null){
				valid = true;
			}
			JsonObject obj = new JsonObject();
			obj.addProperty("valid", valid);
			
			return obj.toString();
		}
		catch(NegocioException e){
			ServiceResponse.getInstance().reset();
			ServiceResponse.getInstance().setCod(ServiceResponse.COD_ERROR);
			ServiceResponse.getInstance().setEstado(ServiceResponse.STATUS_ERROR);
			ServiceResponse.getInstance().setMensajes(e.getMessage());
			Gson gson = new Gson();
			return gson.toJson(ServiceResponse.getInstance());			
		}
	}		
}
