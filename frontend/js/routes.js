app.config(['$routeProvider', '$httpProvider', function ($routeProvider) {

    $routeProvider.when('/', {
        // controller: 'indexController',
        templateUrl: 'partials/home.html',
    })
    .when('/home_actions', {
    	// controller: 'indexController',
    	templateUrl: 'partials/home_actions.html',
    })
    .when('/profile', {
      controller: 'menuUserController',
      templateUrl: 'partials/profile.html',
    })
    .when('/profile_actions', {
      controller: 'menuUserController',
      templateUrl: 'partials/profile_actions.html',
    })
    .when('/editProfile', {
      controller: 'editProfileController',
      templateUrl: 'partials/editProfile.html',
    })
    .when('/misDesap', {
      controller: 'myRepController',
      templateUrl: 'partials/misDesaparecidos.html',
    })
    .when('/misPedidos', {
      controller: 'myHelpsController',
      templateUrl: 'partials/misPedidos.html',
    })
    .when('/catastrofe/:id', {
      controller: 'catastrofeController',
      templateUrl: 'partials/catastrofe.html',
    })
    .when('/desaparecidos', {
      controller: 'desaparecidoController',
      templateUrl: 'partials/desaparecidos.html',
    })
   .otherwise({ redirectTo: '/' });
}]);