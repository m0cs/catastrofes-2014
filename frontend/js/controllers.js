app.controller('indexController', ['$scope', 'WebSocket', 'ongService', '$timeout', 'mapIndexService', 'catastrofeService', 'leafletData', '$location', 'donateService', 'authService', 'rssService', function($scope, WebSocket, ongService, $timeout, mapIndexService, catastrofeService, leafletData, $location, donateService, authService, rssService) {
	
	$scope.markers = mapIndexService.getMarkers();
	$scope.paths = mapIndexService.getPaths();
	$scope.ongs = [];
	$scope.res = {};
	$scope.collapse = true;
	$scope.def = {};
	$scope.toggle = false;

	$scope.youtubeFirst = param['youtubeCNN'];
	$scope.youtubeSecond = param['youtubeBBC'];
	
	//rssService.consumeRss("http://www.youtube.com/rss/user/YourChannelNameHere/feed.rss")

	// Initialize home map
	console.info("Inicializando mapa...");
	angular.extend($scope, mapIndexService.getDefault());
	console.info("Mapa Inicializado");

	// Inicializar catástrofes en mapa
	console.info("Solicitando catástrofes...");
	mapIndexService.loadCatastrofes();

	// catastrofeService.getCatastrofes().then(function(response) {
 //      angular.forEach(angular.fromJson(response), function(value, key) {
 //        markers.push({
 //          title: value['nombre'],
 //          lat: parseFloat(value['lat']),
 //          lng: parseFloat(value['lon']),
 //          // message: "<span><a data-ng-click='ver("+value['id']+")'>"+value['nombre']+"</a></span>",
 //          message: value['nombre'],
 //          icon: {
 //              type: 'awesomeMarker',
 //              prefix: 'fa',
 //              icon: 'circle',
 //              markerColor: 'green'
 //            },
 //            catastrofe: {
 //            	id: value['id'],
 //            	nombre: value['nombre']
 //            }
 //        });                
 //      })
 //      console.info("Catástrofes obtenidas");
 //    })
    
	// Inicializar catástrofes en mapa
	//mapIndexService.loadCatastrofes();


	// Inicializar ongs
	console.info("Solicitando ongs...");
	ongService.loadONGs().then(function(response){
		//console.info(angular.fromJson(response));
		 $scope.ongs = angular.fromJson(response);
		//$scope.ongs = angular.fromJson(globalService.getONGs());
		// console.log($scope.ongs);
		console.info("ONGs obtenidas");
	});

	// leafletData.getMap('home').then(function (map) {
	// 	map.on('popupopen', function(e) {
	// 	  var marker = e.popup._source;
	// 	  console.log(marker);
	// 	  console.log(marker.options.catastrofe['id']);
	// 	});
	// });
	
	$scope.ver = function(id) {
		$location.path('/catastrofe/'+id);
	}
	

	$scope.$on('leafletDirectiveMarker.dblclick', function(e, args) {				
		// console.log(e);
		//console.log(args);
		markerName = args.leafletEvent.target.options.name; //has to be set above
		catastrofeId = args.leafletEvent.target.options.catastrofe['id'];
		catastrofeName = args.leafletEvent.target.options.catastrofe['nombre'];
		// $container = $(args.leafletEvent.target._popup._content).find('.leaflet-popup-  content'); 
		// $container.empty();
		$scope.ver(catastrofeId);
	}); 


	// Articles donation
	$scope.donateA = {
		articulos: [],
		selectONG: {},
		retirar: false,
		direccion: '',
		telefono: '',
		email: '',
		arts: [],
		idOng: '',
		nombre: ''
	}

	// Service donation
	$scope.donateS = {
		servicios: [],
		selectONG: {},
		telefono: '',
		email: '',
		srvs: [],
		idOng: '',
		nombre: '',
		profesion: '',
		cantidad: ''
	}

	// Modal functions

	$scope.toggle = function() {
		$scope.toggle = !$scope.toggle;
	}

	// Donate articulos action
	$scope.donateArticulos = function () {
		angular.forEach($scope.donateA.articulos, function(value, key) {
			$scope.donateA.arts.push({"nombre":value.nombre,"cantidad":value.cantidad});
		});

		$scope.donateA.email = authService.getUser().email;
		$scope.donateA.idOng = $scope.donateA.selectONG.id;
		
		console.log("datos articulos");
		console.log($scope.donateA);

		donateService.donateArtcls($scope.donateA).then(function() {
			$scope.donateA = angular.copy($scope.res);
		});
	}

	// Donate servicio action
	$scope.donateServices = function() {
		// angular.forEach($scope.donateS.servicios, function(value, key) {
		// 	$scope.donateS.srvs.push({"nombre":value.nombre, "profesion":value.profesion, "cantidad":value.horas});
		// });
		$scope.donateS.email = authService.getUser().email;
		$scope.donateS.idOng = $scope.donateS.selectONG.id;

		console.log("datos servicios");
		console.log($scope.donateS);

		donateService.donateServices($scope.donateS).then(function() {
			$scope.donateS = angular.copy($scope.res);
		});	
	}

	$scope.resetADonate = function() {
		$scope.donate = angular.copy($scope.res);
		$('#modalADonacion').modal('hide');
	}

	$scope.addArticulo = function() {
		$scope.donateA.articulos.push($scope.articulo);
		$scope.articulo = angular.copy($scope.def);
	}

	$scope.addServicio = function() {
		$scope.donateS.servicios.push($scope.servicio);
		$scope.servicio = angular.copy($scope.def);
	}

	$scope.removeItem = function removeItem(row) {
        index = $scope.donateA.articulos.indexOf(row);
        if (index !== -1) {
            $scope.donateA.articulos.splice(index, 1);
        }
    }

    $scope.removeItemServicio = function removeItem(row) {
        index = $scope.donateS.servicios.indexOf(row);
        if (index !== -1) {
            $scope.donateS.servicios.splice(index, 1);
        }
    }

    

    
    // Conf slider buscar desaparecido
  //   slidesInSlideshow = 4;
 	// slidesTimeIntervalInMs = 3000; 
  
	// $scope.slideshow = 1;
	// slideTimer =
	// $timeout(function interval() {
	//   $scope.slideshow = ($scope.slideshow % slidesInSlideshow) + 1;
	//   slideTimer = $timeout(interval, slidesTimeIntervalInMs);
	// }, slidesTimeIntervalInMs);

	// WebSocket.onopen(function() {
 //    	console.log('WebSocket conectado correctamente');
 //        WebSocket.send('Mensaje enviado de prueba')
	// });

	// WebSocket.onmessage(function(event) {
	// 	console.log('Recibido: ', event.data);
		// mapIndexService.addMarker(marker);
		// $scope.markers.push({
	 //        lat: -34.90529038012605,
	 //    	lng: -56.164984703063965,
	 //        message: 'hola mundo',
	 //        icon: {
	 //        	type: 'awesomeMarker',
	 //         	prefix: 'fa',
	 //         	icon: 'thumb-tack',
	 //         	markerColor: 'green'
  //       	 }
  //       });
    //     leafletData.getMap('home').then(function(map) {
    //     	L.circle([-34.90529038012605, -56.164984703063965], 4000, {
				//     color: 'black',
				//     fillColor: '#ff4c70',
				//     fillOpacity: 0.4
				// }).addTo(map);
    //     	});
        	// var dist = L.latLng(-34.90529038012605, -56.164984703063965).distanceTo(L.latLng(-34.90529038012605, -56.264984703063965));
        	// console.info(dist);
    // });	

	// WebSocket.onerror(function () {
 //        console.error('WebSocket error');
 //    });

	// WebSocket.onclose(function() {
	// 	console.info('WebSocket conexión cerrada');
	// })
}]);

app.controller('authController', ['Facebook', '$scope', 'authService', 'StateService', '$location', function(Facebook, $scope, authService, status, $location){
	$scope.logo = param['logo'];
	$scope.visible = true;
	$scope.def = {};
	$scope.loginData = {
		email: 'moco59@gmail.com',
		password: 'theMok1!'
	}

	$scope.regData = {
		nick: "",
		email: "",
		password: ""
	};

	$scope.perfilBtn = function() {		
		$location.path("/profile")
	};

	$scope.isLogeado = function () {
		return authService.islogged();
	};

	$scope.login = function () {
		$('#modalLogin').modal('hide');
		authService.login($scope.loginData.email, $scope.loginData.password);
		//$scope.loginData = angular.copy($scope.def);
	};

	$scope.loginFB = function () {
		$('#modalLogin').modal('hide');
		return authService.fbAuth();
	};

	$scope.register = function() {
		console.log("El nick: "+ $scope.reg.nick);
		console.log("El email: "+ $scope.reg.email);
		console.log("La pass: "+ $scope.reg.password);
		return authService.register($scope.reg.nick, $scope.reg.email, $scope.reg.password);
		$scope.reg = angular.copy($scope.def);
	};

	$scope.logout = function () {
		// $event.preventDefault()
		$('#sidebar-wrapper').removeClass('active');
		return authService.logout();
	};

	$scope.isAction = function() {
		return status.isAction();
	};

	// $scope.$on('Facebook:statusChange', function(ev, data) {
 //        console.log('Status: ', data);
 //        if (data.status == 'connected') {
 //          $scope.$apply(function() {
 //            authService.fbAuth();  
 //            $('#modalLogin').modal('hide');
 //          });
 //        }
 //      });

}]);

app.controller('rssController', ['$scope', 'rssService', function($scope, rssService){
	// rssService.consumeRss(param['rssIndex']).then(function(data) {
	// 	console.log(data);
	// 	$scope.rssContent = data;
	// });
}])

app.controller('actionController', ['$scope', '$location', 'GeoService', 'StateService', 'ayudaService', '$cookieStore', 'desaparecidoService', '$filter', function($scope, $location, geo, status, aService, $cookieStore, dService, $filter) {

	// Inits
	$scope.pos = '';
	$scope.lat = 'S/P';
	$scope.lon = 'S/P';
	$scope.def = {};
	$scope.filtrados = [];

	$scope.desaparecido = {
		apellido: '',
		detalles: '',
		edad: '',
		estatura: '',
		etnia: '',
		fecha: '',
		newFecha: '',
		nombre: '',
		ojos: '',
		piel: ''
	};

	$scope.persona = {
		nombre: '',
		apellido: '',
		email: ''
	};
	$scope.ready = false;

	$scope.resultado = [];

	$scope.buscar = {
		palabra: '',
		ojos: [],
		etnias: [],
		piel: []
	};

	// Default data
	$scope.actions = {
		ayudaTitle: 'Solicitar ayuda de rescatistas',
		reporteTitle: 'Reportar persona desaparecida',
		avisoTitle: 'Alertar autoridades',
		buscarTitle: 'Buscar persona en el registro del sistema',
	};

	$scope.datas = {
		ojos: [
			{id: 'Miel', label: 'Miel'},
			{id: 'Café', label: 'Café'},
			{id: 'Gris', label: 'Gris'},
			{id: 'Azul' ,label: 'Azul'},
			{id: 'Verde', label: 'Verde'}
		],
		piel: [
			{id: 'Albino', label: 'Albino'},
			{id: 'Blanco', label: 'Blanco'},
			{id: 'Trigueño', label: 'Trigueño'},
			{id: 'Negro' ,label: 'Negro'},
			{id: 'Moreno', label: 'Moreno'},
			{id: 'Amarillo', label: 'Amarillo'}
		],
		etnias: [
			{id: 'Caucásico', label: 'Caucásico'},
			{id: 'Mulato', label: 'Mulato'},
			{id: 'Mestizo', label: 'Mestizo'},
			{id: 'Afrodescendiente',label: 'Afrodescendiente'},
			{id: 'Indio', label: 'Indio'}
		]
	};


	// Multi select confs
	$scope.multiSettingOjosConf = {
	    smartButtonMaxItems: 2,
	    buttonClasses: 'btn btn-default btn-full'
	};
	$scope.ojosTranslate = {
		buttonDefaultText: 'Color',
		checkAll: 'Todos',
	    uncheckAll: 'Ninguno'
	};

	$scope.multiSettingEtniasConf = {
	    smartButtonMaxItems: 2,
	    buttonClasses: 'btn btn-default btn-full'
	};
	$scope.etniasTranslate = {
		buttonDefaultText: 'Etnia',
		checkAll: 'Todos',
	    uncheckAll: 'Ninguno'	
	};

	$scope.multiSettingPielConf = {
	    smartButtonMaxItems: 2,
	    buttonClasses: 'btn btn-default btn-full'
	};
	$scope.pielTranslate = {
		buttonDefaultText: 'Color',
		checkAll: 'Todos',
	    uncheckAll: 'Ninguno'
	};

	
	// Sliders confs
	$scope.sliderEdad = {
		min: 0,
		max: 100,
		step: 1,
		value: [0,100]
	};

	$scope.sliderAltura = {
		min: 0,
		max: 2.50,
		step: 0.01,
		value: [0,2.50]
	};

	
	// Service position confs
	geo().then(function (position) {
        $scope.pos = position;
        $scope.lat = $scope.pos['coords']['latitude'];
        $scope.lon = $scope.pos['coords']['longitude'];
        $scope.ready = true;

        angular.extend($scope, {
			defaults: {
				detectRetina: false,
				tileLayerOptions: {
			        attribution: 'SICA'
			    },
				scrollWheelZoom: false,
				doubleClickZoom: false,
				touchZoom: false
			},
			markers: {
                marker: {
                    lat: $scope.lat,
		        	lng: $scope.lon,
                    focus: true,
                    draggable: true,
                    icon: {
                    	type: 'awesomeMarker',
                    	prefix: 'fa',
                    	icon: 'thumb-tack',
                    	markerColor: 'green'
                    }
                }
            },
			center: {
		        lat: $scope.lat,
		        lng: $scope.lon,
		        zoom: 15
		    }
		});
	});

	
	// Actions btns
	///////////////

	// Pedido de ayuda
	$scope.ayudaBtn = function() {
		status.setAction(true);
		status.setAyuda(true);
		$scope.tmp = {
			nombre: $cookieStore.get("nick"),
			apellido: $cookieStore.get("nick"),
			email: $cookieStore.get("nick")
		}
		
		$scope.persona.nombre = $scope.tmp.nombre;
		$scope.persona.apellido = $scope.tmp.apellido;
		$scope.persona.email = $scope.tmp.email;
		
		$location.path("/home_actions");
	};

	$scope.ayudaActionBtn = function() {
		aService.pedidoAyuda($scomarker.lat, $scope.markers.marker.lng, $scope.persona);
	};
	
	// Reportar desaparecido
	$scope.reporteBtn = function() {
		status.setAction(true);
		status.setReporte(true);
		$location.path("/home_actions");
	};

	$scope.reporteActionBtn = function() {
		// console.log($scope.desaparecido);
		// console.log($filter('date')(new Date($scope.desaparecido.fecha), 'dd-MM-yyyy'));
		$scope.desaparecido.newFecha = $filter('date')(new Date($scope.desaparecido.fecha), 'dd-MM-yyyy');
		// console.log($scope.desaparecido);
		dService.reportar($scope.desaparecido).then(function() {
			$scope.desaparecido = angular.copy($scope.def);
			$location.path("/");
			status.setAction(false);
			status.setReporte(false);
		});
	};

	// Alerta de catástrofe
	$scope.avisoBtn = function() {
		status.setAction(true);
		status.setAviso(true);
		$location.path("/home_actions");
	};

	// Buscar desaparecido
	$scope.buscarBtn = function() {
		status.setAction(true);
		status.setBuscar(true);
		$location.path("/home_actions");
	};

	$scope.buscarActionBtn = function() {
		$scope.buscar.ets = [];
		$scope.buscar.ojs = [];
		$scope.buscar.pls = [];

		if ($scope.buscar.inicio != null)
			$scope.buscar.newInicio = $scope.desaparecido.newFecha = $filter('date')(new Date($scope.buscar.inicio), 'dd-MM-yyyy');
		if ($scope.buscar.fin != null)
			$scope.buscar.newFin = $scope.desaparecido.newFecha = $filter('date')(new Date($scope.buscar.fin), 'dd-MM-yyyy');

		angular.forEach($scope.buscar.etnias, function(value, key) {
		  $scope.buscar.ets.push(value.id);
		});
		angular.forEach($scope.buscar.ojos, function(value, key) {
		  $scope.buscar.ojs.push(value.id);
		});
		angular.forEach($scope.buscar.piel, function(value, key) {
		  $scope.buscar.pls.push(value.id);
		});
		console.log($scope.buscar);
		dService.setFiler($scope.buscar);

		// console.log(JSON.stringify($scope.buscar));
		// dService.buscar($scope.buscar).then(function(response) {
		// 	if (response.estado != "ERROR") {
		// 		$scope.filtrados = angular.fromJson(response);
		// 		console.info($scope.filtrados);
		// 		status.setResultado(true);
		// 		// $scope.buscar = angular.copy($scope.def);
		// 	};
		// });
		$location.path("/desaparecidos");
	};


	
	$scope.resetBuscar = function () {
		status.setResultado(false);
		$scope.filtrados = angular.copy($scope.def);
	};

		
	// Common canel btn
	$scope.cancelar = function() {
		$location.path("/");
		$scope.filtrados = angular.copy($scope.def);
		status.reset();
	};
	
	// Service boolean states

	$scope.isAction = function() {
		return status.isAction();
	};

	$scope.isAyuda = function() {
		return status.isAyuda();
	};

	$scope.isReporte = function() {
		return status.isReporte();
	};

	$scope.isAviso = function() {
		return status.isAviso();
	};

	$scope.isBuscar = function() {
		return status.isBuscar();
	};

	$scope.isResultado = function() {
		return status.isResultado();
	};

	$scope.isMenuUser = function() {
		return status.isMenuUser();
	};

}]);

app.controller('desaparecidoController', ['StateService', '$location', '$scope', 'desaparecidoService', function(status, $location, $scope, dService){
	$scope.filtrados = [];

	dService.buscar().then(function(response) {
		if (response.estado != "ERROR") {
			$scope.filtrados = angular.fromJson(response);
			console.info($scope.filtrados);
			// $scope.buscar = angular.copy($scope.def);
			$scope.$apply();
		};
	});

	$scope.cancelar = function() {
		$location.path("/");
		dService.resetFilter();
		status.reset();
	};

	$scope.resetBuscar = function () {
		$location.path("/home_actions");
		$scope.filtrados = [];
		status.setAction(true);
		status.setBuscar(true);
	};

}]);

app.controller('menuUserController', ['$scope', 'StateService', '$location', function($scope, status, $location){

	status.setAction(true);
	status.setMenuUser(true);

	$scope.isMenuUser = function() {
		return status.isMenuUser();
	};
	$scope.isEditProfile = function() {
		return status.isEditProfile();
	};
	$scope.isMyHelps = function() {
		return status.isMyHelps();
	};
	$scope.isMyRep = function() {
		return status.isMyRep();
	};

	$scope.btnEditProfile = function() {
		$location.path('/editProfile');
	};

	$scope.btnMyHelps = function() {
		status.setAction(true);
		$location.path('/misPedidos');
	};

	$scope.btnMyRep = function() {
		status.setAction(true);
		$location.path('/misDesap');
	};

	$scope.cancelar = function() {
		$location.path('/');
		status.reset();
	};
}]);

app.controller('editProfileController', ['$scope', 'StateService', '$location', 'authService', '$rootScope', '$filter', function($scope, status, $location, authService, $rootScope, $filter){
	
	status.setAction(true);
	// status.setEditProfile(true);

	$scope.logo = param['logo'];

	$scope.def = {};

		// id: data.id,
		// apellido: data.apellido,
		// email: data.email,
		// celular: data.celular,
		// isActivo: data.isActivo,
		// fechaNacimiento: data.fechaNacimiento,
		// nick: data.nick,
		// nombre: data.nombre,
		// password: data.password,
		// tipo: data.tipo,
		// sexo: data.sexo

	$scope.cancelar = function() {
		$location.path("/");
		status.reset();
		$scope.profile = angular.copy($scope.def);
	};

	$scope.id = authService.getUser().id;

	authService.getUserById($scope.id).then(function(request) {
		$rootScope.$apply(function() {
			$scope.profile = angular.fromJson(request);
			console.log(new Date($scope.profile.fechaNacimiento));
		});
	});

	$scope.btnSaveAction = function() {
		$scope.profile.newFechaNacimiento = $filter('date')(new Date($scope.profile.fechaNacimiento), 'dd-MM-yyyy');
		authService.editProfile($scope.profile).then(function() {
			$location.path('/');
		});
	}
}]);

app.controller('myHelpsController', ['$scope', 'StateService', '$location', function($scope, status, $location){

	status.setAction(true);
	// status.setMyHelps(true);

	$scope.data = {
		helps: {}
	};

	$scope.def = {};

	$scope.cancelar = function() {
		$location.path('/');
		status.reset();
	};

}]);

app.controller('myRepController', ['$scope', 'StateService', '$location', function($scope, status, $location){

	status.setAction(true);
	// status.setMyRep(true);


	$scope.data = {
		reps: {}
	};

	$scope.cancelar = function() {
		$location.path('/');
		status.reset();
	};

}]);

app.controller('catastrofeController', ['$scope', '$location', 'StateService', '$routeParams', 'catastrofeService', '$rootScope', 'blockUI', function($scope, $location, status, $routeParams, catastrofeService, $rootScope, blockUI){
		
	status.setAction(true);

	$scope.def = {};
	
	$scope.catastrofe = {
		descripcion: '',
		estado: '',
		fechaComienzo: '',
		fechaFin: '',
		fuentes: [],
		id: '',
		lat: '',
		lon: '',
		magnitud: '',
		nombre: ''
	};

	$scope.cancelar = function() {
		blockUI.start();
		$scope.catastrofe = angular.copy($scope.def);
		status.reset();
		// $('twitter-wjs').remove();
		$location.path('/')
		blockUI.stop();
	};

	loadTwitter = function() {
		!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");
	};

	$scope.carousel = {
		interval: 5000,
		slides: []
	}


	catastrofeService.getCatastrofeById($routeParams.id).then(function(request) {
		$rootScope.$apply(function() {
			$scope.catastrofe = angular.fromJson(request);
			loadTwitter();
		});
		angular.forEach($scope.catastrofe.imagenes, function(value, key){
			$scope.carousel.slides.push({
				image: value[key]
			});
		});
		twttr.widgets.load();
	});

}]);