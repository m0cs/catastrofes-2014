app.directive('myrss', function() {
	return {
		controller: 'rssController',
		restrict: 'A',
		//template: "<div class='info-item'><div class='header'><h3>{{ rss.items }}</h3></div><div class='content'>{{ rss }}</div></div>"
		template: "<div class='col-md-4 rss-wrapper'><div class='rss-header'><h3>HEADER PAPI</h3></div><div class='rss-content' >hola mundo, {{ name }}</div></div>"
	};
});

// Home map news bar
app.directive('rssnews', ['$http', 'rssService', function($http, rssService) {
	return {
		restrict: 'A',
		link: function($scope, $elem, attrs) {
			str = "";
			conf = {
				speed: 35, //pixels por segundo
			    direction: "left", //sentido del movimiento, izq o dcha
			    moving: false, //arranca en movimiento o quieto
			    startEmpty: true, //se crea vacío, o se actualiza contenido despues
			    duplicate: false, //si es muy corto, se duplica el contenido
			    rssurl: false, //solo para obtener los datos directamente
			    rssfrequency: 0, //tiempo de recargarse en minutos, 0 no actualiza
			    updatetype: "swap" //formas de actualizar reset o swap
			};

			//$http.jsonp('//ajax.googleapis.com/ajax/services/feed/load?v=1.0&num=50&callback=JSON_CALLBACK&q=' + encodeURIComponent(param['rssIndex']))
			//.success(function(data, status, headers, config) {
				
				//rssData = data.responseData.feed.entries;
			rssService.consumeRss(param['rssIndex']).then(function(data) {
				for (i = 0; i < data.length; i++) {
					str+="<li><a target='_blank' href='"+data[i.toString()]['link'].toString()+"'><i class='fa fa-circle'></i>"+"  "+data[i.toString()]['title'].toString() +"</a></li>";
				}

				$($elem).webTicker(conf);
				$($elem).webTicker('update',str,'swap');
			});
			// })
			// .error(function(data, status, headers, config) {
			// 	console.error('Error al consumir rss:', data);
			// });
		}
	};
}]);

// News div home
app.directive ('newsticker', ['$http', function($http) {
	return {
		restrict: 'A',
		link: function($scope, $elem, attrs) {
			$http.jsonp('//ajax.googleapis.com/ajax/services/feed/load?v=1.0&num=50&callback=JSON_CALLBACK&q=' + encodeURIComponent(param['rssCNN']))
			.success(function(data, status, headers, config) {
				$scope.rssNews = data.responseData.feed.entries;
				// $($elem).newsTicker({
				// 	row_height: 80,
				//     max_rows: 3,
				//     duration: 4000,
				// });
				$($elem).newsTicker({
					row_height: 80,
				    max_rows: 5,
				    duration: 4000,
				    // prevButton: $('#nt-example1-prev'),
				    // nextButton: $('#nt-example1-next')
				});
				$('#newsticker-infos').hover(function() {
				    $($elem).newsTicker('pause');
				}, function() {
				    $($elem).newsTicker('unpause');
				});

				str = "";
				for (i = 0; i < $scope.rssNews.length; i++) {
					//console.log($scope.rssNews);
					str="<h5 class='bold'>"+$scope.rssNews[i.toString()]['title']+".</h5>"+$scope.rssNews[i.toString()]['contentSnippet']+"<a target='_blank' href='"+$scope.rssNews[i.toString()]['link']+"'>Leer</a>";
					$($elem).newsTicker('add',str);
				}	
			})
			.error(function(data, status, headers, config) {
				console.error('Error al consumir rss:', data);
			});
		}
	}
}]);





app.directive('desaparecido', ['$scope', function($scope){
	return {
		// scope: {}, // {} = isolate, true = child, false/undefined = no change
		restrict: 'E',
		template: '<div class="col-md-6">'+
					'<div class="col-md-6 des-container text-center">'+
						'<div class="des-header">'+
							'<label title="Nombre y Apellido"><strong>{{desResult.nombre desResult.apellido}}</strong></label>'+
						'</div>'+
						'<div class="des-body">'+
							'<div class="col-md-7 item">'+
								'<div class="col-md-12">'+
									'<strong>{{desResult.edad}} años</strong> | <strong>{{desResult.etnia}}</strong> | <strong>Piel {{desResult.colorPiel}}</strong>'+
								'</div>'+
								'<div class="col-md-12">'+
									'<strong> Ojos {{desResult.colorOjos}}</strong> | <strong>{{desResult.estatura}} mts </strong>| <strong>{{desResult.fecha}}</strong>'+
								'</div>'+
								'<div class="col-md-12">'+
									'Reportada por: <strong>El bana nero</strong>'+
								'</div>'+
								'<div class="col-md-12">'+
									'<strong>Descripción:</strong> {{desResult.detalles}}'+
								'</div>'+
							'</div>'+
							// <div class='col-md-5'>
							// 	<div ng-switch='slideshow' ng-animate="'animate'">
							// 		 <div class='slider-content' ng-switch-when='1'>
							// 			<img width='128px' height='160px' class='img-thumbnail' data-ng-src='img/desResult.jpg'>
							// 		</div>	
							// 		<div class='slider-content' ng-switch-when='2'>
							// 			<img width='128px' height='160px' class='img-thumbnail' data-ng-src='img/desResult2.jpg'>
							// 		</div>	
							// 		<div class='slider-content' ng-switch-when='3'>
							// 			<img width='128px' height='160px' class='img-thumbnail' data-ng-src='img/desResult3.jpg'>
							// 		 </div>	
							// 		<div class='slider-content' ng-switch-when='4'>
							// 			<img class='img-thumbnail' data-ng-src='img/desResult4.jpg'>
							// 		'</div>'+	
							// 	'</div>'+
							// '</div>'+
						'</div>'+
					'</div>'+
				'</div>'
	};
}]);