app.service('authService', ['$http', '$location', 'md5', '$cookies', '$cookieStore', 'Facebook', 'growl', 'blockUI', '$rootScope', '$timeout', function($http, $location, md5, $cookies, $cookieStore, Facebook, growl, blockUI, $rootScope, $timeout){
  user = {
    email: '',
    nombre: '',
    apellido: '',
    id: '',
    nick: ''
  };
  def = {};
  return {
    editProfile: function(data) {
      return $.ajax({
        type: 'POST',
        // url: "http://192.168.0.110/SICA_Rest_resources/users/editar",
        url: param['ip']+param['editProf'],
        data: JSON.stringify({
          id: data.id,
          apellido: data.apellido,
          email: data.email,
          celular: data.celular,
          isActivo: data.isActivo,
          fechaNacimiento: data.newFechaNacimiento,
          nick: data.nick,
          nombre: data.nombre,
          password: data.password,
          tipo: data.tipo,
          sexo: data.sexo           
        }),
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        timeout: 8000,
        beforeSend: function() {
          blockUI.start();
        }
      })
      .done(function(data) {
        console.info("Usuario modificado con éxito");
        growl.addSuccessMessage("Usuario modificado con éxito");
        console.log(data);
      })
      .fail(function(){
        growl.addErrorMessage("No se pudo conectar con el servidor(editProfile).");
        console.error("No se pudo conectar con el servidor(editProfile).");
      })
      .always(function() {
        $rootScope.$apply(function()  {
          blockUI.stop();
        });
      });
    },
    getUserById: function(id) {
      return $.ajax({
        type: 'GET',
        //url: "http://192.168.0.110:8080/SICA_Rest/resources/users/buscar/"+id,
        url: param['ip']+param['getUsrById']+id,
        // url: 'data/usuario.json',
        timeout: 8000,
        beforeSend: function() {
          blockUI.start();
         }
      })
      .done(function(data) {
        console.info("Usuario obtenido con éxito");
        console.log(data);
      })
      .fail(function(){
        growl.addErrorMessage("Error al obtener usuario, no hay conexión.");
        console.error("No se pudo conectar con el servidor(ObtenerUsuarioById).");
      })
      .always(function() {
        $rootScope.$apply(function()  {
          blockUI.stop();
        });
      });
    },
    fbAuth: function() {
      Facebook.login(function(response) {
          console.log(response);
          if (response.status == 'connected') {
            $cookies.logueado = true;
            $location.path('/#home');
            Facebook.api('/me', function(response) {
              console.log(response);
            });
            return true;
          }
      });    
    },
    fbAuthLogout: function() {
        Facebook.logout(function() {
          $cookies.logueado = false;
          $location.path('/#home');
        });
    },
    islogged: function () {
      return $cookieStore.get('logueado') || false;
    },
    login: function(mail, pass) {
      $.ajax({
        type: 'POST',
        url: param['ip']+param['login'],
        // url: "http://192.168.0.110:8080/SICA_Rest/resources/users/login",
        // url: 'data/usuario.json',
        dataType: 'json',
        data: JSON.stringify({
            email: mail,
            password: pass,//md5.createHash(pass),
            tipo: '3'
          }),
          dataType: 'json',
          contentType: "application/json; charset=utf-8",
          timeout: 8000,
          beforeSend: function() {
            blockUI.start();
          }
      })
      .done(function(data) {
        console.info("Usuario logueado");
        console.log(data);

        user.email = data["email"];
        user.nombre = data["nombre"];
        user.apellido = data["apellido"];
        user.id = data["id"];
        user.nick = data["nick"];
        user.logueado = true;

        $cookieStore.put("user", user);
        $cookieStore.put("logueado", true);
        // $cookieStore.put("email", user.email);
        // $cookieStore.put("nombre", user.nombre);
        // $cookieStore.put("apellido", user.apellido);
        // $cookieStore.put("id", user.id);
        // $cookieStore.put("nick", user.nick);
        $location.path('/#home');
      })
      .fail(function(error) {
        console.error("Error al conectar con el servidor(login).");
        growl.addErrorMessage("Error al loguearse, no hay conexión.");
      })
      .always(function() {
        $rootScope.$apply(function()  {
          blockUI.stop();
        });
      });
    },
    register: function(nick, email, password) {
      $.ajax({
        type: 'POST',
        url: param['ip']+param['register'],
        // url: "http://192.168.0.110:8080/SICA_Rest/resources/users/registro",
        // url: 'data/usuario.json',
        data: JSON.stringify({
          nick: nick,
          email: email,
          password: password//md5.createHash(password),
        }),
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        timeout: 8000,
        beforeSend: function() {
            blockUI.start();
          }
      })
      .done(function(data) {
        console.log(data);    
        growl.addSuccessMessage("Registro realizado con éxito");

        user.email = data["email"];
        user.nombre = data["nombre"];
        user.apellido = data["apellido"];
        user.id = data["id"];
        user.nick = data["nick"];

        $cookieStore.put("user", user);
        $cookieStore.put("logueado", true);       

        $location.path('/#home');
      })
      .fail(function(error) {
        console.error(error);
        growl.addErrorMessage("Error al registrar usuario, no hay conexión.");
        console.error("No se pudo conectar con el servidor(registro).");
      })
      .always(function() {
        $rootScope.$apply(function()  {
          blockUI.stop();
        });
      });
    },
    logout: function() {
      blockUI.start();
      $cookieStore.remove("logueado");
      $cookieStore.remove("email");
      $cookieStore.remove("nombre");
      $cookieStore.remove("apellido");
      $cookieStore.remove("id");
      $cookieStore.remove("nick");
      $cookieStore.remove("user");
      
      $location.path('/#home');
      $timeout(function(){
        blockUI.stop();
      }, 6000);

       // Facebook.getLoginStatus(function(response) {
       //  if (response.status == 'connected') {
       //    Facebook.logout(function() {
       //      $cookies.logueado = false;
       //    });
       //  }
      // });
    },
    getUser: function() {
      return $cookieStore.get("user");
    }
  }
}]);

app.service('globalService', ['ongService', function(ongService){
  ongs = [];

  return {
    getONGs: function() {
      if (ongs.length < 1) {
        ongService.loadONGs().then(function(response){
          ongs = angular.fromJson(response);  
        });
      };
      return ongs;
    }
  }
}]);

app.service('mapIndexService',['catastrofeService', '$compile', function(catastrofeService, $compile) {
  markers = [];
  paths = [];
  
  lat = -32.990235559651055;
  lng = -56.14013671875;

  defaults = {
      detectRetina: true,
      tileLayerOptions: {
        attribution: 'SICA'
      },
      scrollWheelZoom: false,
      doubleClickZoom: false,
      touchZoom: false
  };

  center = {
    lat: lat,
    lng: lng,
    zoom: 7
  };

  return {
        getDefault: function () {
          return {
            defaults: defaults,
            center: center
          }
        },
        getMarkers: function () {
          return markers;
        },
        addMarker: function(marker) {
          markers.push(marker);
        },
        getPaths: function() {
          return paths;
        },
        addPath: function (path) {
          paths.push(path);
        },
        loadCatastrofes: function() {
          if (markers.length < 1) {
            catastrofeService.getCatastrofes().then(function(response) {
              angular.forEach(angular.fromJson(response), function(value, key) {
                markers.push({
                  title: value['nombre'],
                  lat: parseFloat(value['lat']),
                  lng: parseFloat(value['lon']),
                  message: value['nombre'],
                  icon: {
                      type: 'awesomeMarker',
                      prefix: 'fa',
                      icon: 'circle',
                      markerColor: 'green'
                    },
                    catastrofe: {
                     id: value['id'],
                     nombre: value['nombre']
                    }
                });
              });
            });
          }
        }
      };
}]);

app.service('ayudaService', ['$http', 'growl', 'blockUI', '$rootScope', function($http, growl, blockUI, $rootScope) {
  return {
    pedidoAyuda: function(lat, lng, persona) {
      $.ajax({
        type: 'POST',
        url: param['ip']+param['ayuda'],
        // url: "http://192.168.0.110:8080/SICA_Rest/resources/pedidos/nuevo",
        data: JSON.stringify({
          descripcion: '',
          latLong: lat +';'+lng,
          idEstadoUsuario: '4', 
          emailUsuario: persona.email
        }),
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        timeout: 8000,
        beforeSend: function() {
            blockUI.start();
          }
      })
      .done(function(data) {
        console.log(data);
        if (data.estado = "ERROR") {
          growl.addErrorMessage("Error al solicitar ayuda, no hay conexión.");
          console.error("Error en la solicitud(PedidoAyuda).");
        }
        else {
          growl.addSuccessMessage("Pedido realizado con éxito.");
          console.info("Pedido realizado con éxito.");
        }
      })
      .fail(function(error) {
        growl.addErrorMessage("No se pudo conectar con el servidor.");
        console.error("No se pudo conectar con el servidor(PedidoAyuda).");
      })
      .always(function() {
        $rootScope.$apply(function()  {
          blockUI.stop();
        });
      });
    }
  }
}]);

app.service('catastrofeService', ['$http', 'growl', 'blockUI', '$rootScope', '$location', '$timeout', function($http, growl, blockUI, $rootScope, $location, $timeout){
  return {
    getCatastrofes: function () {
      // return $http.get("data/catastrofes.json");
      return $.ajax({
        type: 'GET',
        // url: "http://192.168.0.110:8080/SICA_Rest/resources/catastrofes/buscarTodos",
        url: param['ip']+param['getCat'],
        // url: 'data/catastrofes.json',
        timeout: 8000,
        beforeSend: function() {
          blockUI.start();
         }
      })
      .done(function(data) {
        console.log("Catástrofes obtenidas con éxito");
        //console.log(data);
      })
      .fail(function(){
        growl.addErrorMessage("Error al obtener catástrofes, no hay conexión.");
        console.error("Error al obtener catástrofes, no se pudo conectar con el servidor");
      })
      .always(function() {
        $rootScope.$apply(function()  {
          blockUI.stop();
        });
      });
    },
    getCatastrofeById: function(id) {
      return $.ajax({
        type: 'GET',
        // url: "http://192.168.0.110:8080/SICA_Rest/resources/catastrofes/buscarTodos",
        url: param['ip']+param['getCatId']+id,
        // url: 'data/catastrofe.json',
        timeout: 8000,
        beforeSend: function() {
          blockUI.start();
         }
      })
      .done(function(data) {
        console.log("Catástrofe obtenida con éxito");
        console.log(data);
      })
      .fail(function(){
        growl.addErrorMessage("Error al obtener catástrofe, no hay conexión.");
        $location.path("/");
      })
      .always(function() {
        $rootScope.$apply(function()  {
            blockUI.stop();
        });
      });
    }
  };
}]);

app.service('ongService', ['growl', '$http', '$resource', 'blockUI', '$rootScope', function(growl, $http, $resource, blockUI, $rootScope) {
  return {
    loadONGs: function () {
      return $.ajax({
        type: 'GET',
        // url: "http://192.168.0.110:8080/SICA_Rest/resources/ongs/buscarTodos",
        url: param['ip']+param['getOngs'],
        // url: 'data/ongs.json',
        timeout: 8000,
        beforeSend: function() {
          blockUI.start();
        } 
      })
      .done(function(data) {
        console.info("ONGs obtenidas con éxito");
        // console.log(data);
      })
      .fail(function() {
        growl.addErrorMessage("Error al obtener ONGs, no hay conexión.");
        console.error("No se pudo conectar con el servidor(ObtenerONGs)");
      })
      .always(function() {
        $rootScope.$apply(function()  {
          blockUI.stop();
        });
      });
    },
  };
}]);

app.service('desaparecidoService', ['growl', '$http', '$resource', 'blockUI', '$rootScope', function(growl, $http, $resource, blockUI, $rootScope){
  def = {
    etnias: new Array(),
    ets: new Array(),
    fin: null,
    newFin: null,
    inicio: null,
    newInicio: null,
    ojos: new Array(),
    ojs: new Array(),
    palabra: "",
    piel: new Array(),
    pls: new Array(),
    nombre: "",
    apellido: ""
  };
  filter = angular.copy(def);

  return {
    getFiletr: function() {
      return filter;
    },
    resetFilter: function() {
      filter = angular.copy(def);
    },
    setFiler: function(data) {
      filter = angular.copy(data);
    },
    reportar: function(data) {
      return  $.ajax({
        type: 'POST',
        // url: "http://192.168.0.110:8080/SICA_Rest/resources/desaparecidos/reportar",
        url: param['ip']+param['reportar'],
        data: JSON.stringify({
          apellido: data.apellido,
          edad: data.edad,
          etnia: data.etnia.label,
          nombre: data.nombre,
          colorPiel: data.piel.label,
          colorOjos: data.ojos.label,
          estatura: data.estatura,
          fechaDesaparecido: data.newFecha,
          email: data.email,
          detalles: data.detalles
        }),
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        timeout: 8000,
        beforeSend: function() {
            blockUI.start();
          }
      })
      .done(function(data) {
        console.info("Reporte realizado con éxito");
        growl.addSuccessMessage("Reporte realizado con éxito.");
        // console.log(data);
      })
      .fail(function() {
        console.error("No se pudo conectar con el servidor(ReportarDesaparecido).");
        growl.addErrorMessage("Error al realizar reporte, no hay conexión.");
      })
      .always(function() {
        $rootScope.$apply(function (){
          blockUI.stop();
        });
      });
    },
    buscar: function() {
      return  $.ajax({
        type: 'POST',
        // url: "http://192.168.0.110:8080/SICA_Rest/resources/desaparecidos/buscar",
        url: param['ip']+param['buscar'],
        // url: 'data/desaparecidos.json',
        data: JSON.stringify({
          nombre: filter.nombre,
          apellido: filter.apellido,
          edad: filter.edad, //array
          etnias: filter.ets, //array
          colorPiel: filter.pls, //array
          colorOjos: filter.ojs, //array
          altura: filter.altura, //array
          fechaInicio: filter.newInicio,
          fechaFin: filter.newFin
        }),
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        timeout: 8000,
        beforeSend: function() {
            blockUI.start();
          }
      })
      .done(function(data) {
        console.info("Búsqueda ok");
        console.log(data);
      })
      .fail(function() {
        console.error("No se pudo conectar con el servidor(BuscarDesaparecido).");
        growl.addErrorMessage("Error al filtrar desaparecidos, no hay conexión.");
      })
      .always(function() {
        $rootScope.$apply(function (){
          blockUI.stop();
        });
      });
    }
  }
}]);

app.service('donateService', ['blockUI', 'growl', '$rootScope', function(blockUI, growl, $rootScope){
  return {
    donateArtcls: function(donacion) {
      return $.ajax({
        type: 'POST',
        // url: "http://192.168.0.110:8080/SICA_Rest/resources/donaciones/nuevo/bien",
        url: param['ip']+param['donateBien'],
        data: JSON.stringify({
          email: donacion.email,
          telefono: donacion.telefono,
          dirOrigen: donacion.direccion,
          retirar: donacion.retirar,
          articulos: donacion.arts,
          idOng: donacion.idOng,
          nombre: donacion.nombre
        }),
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        timeout: 8000,
        beforeSend: function() {
            blockUI.start();
          }
      })
      .done(function(data){
        console.info("Donación realizada con éxito(DonateArtcls).");
        console.log(data);
        $('#modalADonacion').modal('hide');
        growl.addSuccessMessage("Donación de bienes realizada con éxito.");
      })
      .fail(function() {
        growl.addErrorMessage("Error al realizar donación de bienes, no hay conexión.");
        console.error("No se pudo conectar con el servidor(DonateArticulos).");
      })
      .always(function() {
        $rootScope.$apply(function (){
          blockUI.stop();
        });
      });
    },
    donateServices: function(donacion) {
      return $.ajax({
        type: 'POST',
        //url: "http://192.168.0.110:8080/SICA_Rest/resources/donaciones/nuevo/servicio",
        url: param['ip']+param['donateServ'],
        data: JSON.stringify({
          email: donacion.email,
          telefono: donacion.telefono,
          //servicios: donacion.srvs,
          profesion: donacion.profesion,
          cantidad: donacion.cantidad,
          idOng: donacion.idOng,
          nombre: donacion.nombre
        }),
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        timeout: 8000,
        beforeSend: function() {
            blockUI.start();
          }
      })
      .done(function(data){
        console.info("Donacion de servicio realizada con éxito(donateServices).");
        console.log(data);
        $('#modalSDonacion').modal('hide');
        growl.addSuccessMessage("Donacion de servicio realizada con éxito.");
      })
      .fail(function() {
        growl.addErrorMessage("Error al realizar donación de servicios, no hay conexión.");
        console.error("No se pudo conectar con el servidor(DonateServicios).");
      })
      .always(function() {
        $rootScope.$apply(function (){
          blockUI.stop();
        });
      });
    }
  }
}]);

app.factory("GeoService", ['$q', '$window', '$rootScope', 'growl', function($q, $window, $rootScope, growl){
  return function(){
    var deferred = $q.defer();
    
    if(!$window.navigator) {
      deferred.reject("Geolocalizacion no soportada");
      // growl.addErrorMessage("No se pudo obtener la posición.");
      console.error("Geolocalizacion no soportada");
    } else {
      $window.navigator.geolocation.getCurrentPosition(function(position) {
        $rootScope.$apply(function(){
          deferred.resolve(position);
        });
      }, function() {
        // $rootScope.$apply(function(){
        //   growl.addErrorMessage("Ubicación rechazada.");
        // });
        console.error("Geolocalizacion rechazada");
      },
      {
        enableHighAccuracy: true,
        timeout: 5000,
      }
      );
    };
    return deferred.promise;
  };
}]);

app.factory('rssService', ['$http', '$q', function($http, $q){
    return {
        consumeRss : function(url) {
          var deferred = $q.defer();
          $http.jsonp('//ajax.googleapis.com/ajax/services/feed/load?v=1.0&num=50&callback=JSON_CALLBACK&q=' + encodeURIComponent(url))
            .success(function(data, status, headers, config) {
              //console.log(data.responseData.feed.entries);
              deferred.resolve(data.responseData.feed.entries);
              rssData = data.responseData.feed.entries;
              contentRss = [];
              for (i = 0; i < rssData.length; i++) {
                str ="<a target='_blank' href='"+rssData[i.toString()]['link'].toString()+"'><i class='fa fa-circle'></i>"+"  "+rssData[i.toString()]['title'].toString() +"</a>";                
                contentRss.push(str);
              };
              deferred.resolve(contentRss);
            })
            .error(function(data, status, headers, config) {
                console.log(status);
                deferred.reject(status);
            });
          return deferred.promise;   
        }
    };
}]);

app.service('StateService', function() {
  actions = {
    // Action Home
    action: false,
    
    // Home Actions
    ayuda: false,
    reporte: false,
    aviso: false,
    buscar: false,

    // Home Actions Result
    resultado: false,

    // MenuUser Home
    menuUser: false,

    // MenuUser Actions
    editProfile: false,
    myHelps: false,
    myRep: false
  };
  return {

    // Home Actions
    isAction: function() {
      return actions.action;
    },
    setAction: function(data) {
      actions.action = true;
    },
    isAyuda: function() {
      return actions.ayuda;
    },
    setAyuda: function(data) {
      actions.ayuda = data;
    },
    isReporte: function() {
      return actions.reporte;
    },
    setReporte: function(data) {
      actions.reporte = data;
    },
    isAviso: function() {
      return actions.aviso;
    },
    setAviso: function(data) {
      actions.aviso = data;
    },
    isBuscar: function() {
      return actions.buscar;
    },
    setBuscar: function(data) {
      actions.buscar = data;
    },
    isResultado: function() {
      return actions.resultado;
    },
    setResultado: function(data) {
      actions.resultado = data;
    },

    // Menu User Actions
    isMenuUser: function() {
      return actions.menuUser;
    },
    setMenuUser: function(data) {
      actions.menuUser = data;
    },
    isEditProfile: function() {
      return actions.editProfile;
    },
    setEditProfile: function(data) {
      actions.editProfile = data;
    },
    isMyHelps: function() {
      return actions.myHelps;
    },
    setMyHelps: function(data) {
      actions.myHelps = data;
    },
    isMyRep: function() {
      return actions.myRep;
    },
    setMyRep: function(data) {
      actions.myRep = data;
    },

    // Reset All
    reset: function() {
      actions.action = false;
      actions.ayuda = false;
      actions.reporte = false;
      actions.aviso = false;
      actions.buscar = false;
      actions.resultado = false;
      actions.menuUser = false;
      actions.editProfile = false;
      actions.myHelps = false;
      actions.myRep = false;
    }
  };
});