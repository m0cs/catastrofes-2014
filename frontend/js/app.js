var app = angular.module('app', ['ngRoute', 'ngResource', 
								'ngCookies', 'leaflet-directive', 
								'angular-md5', 'angular-growl',
								'angularjs-dropdown-multiselect',
								'ui.bootstrap', 'ui.bootstrap-slider',
								'angular-websocket', 'facebook',
								'blockUI', 'pretty-checkable',
								'twitter.timeline', 'td.easySocialShare',
								'feeds']);
								//'ngAnimate', 'snap']);


app.config(['growlProvider', 'WebSocketProvider', 'FacebookProvider', 'blockUIConfig', '$httpProvider', function(growlProvider, WebSocketProvider, FacebookProvider, blockUIConfig, $httpProvider) {
	// Enable Cors
	$httpProvider.defaults.useXDomain = true;
    delete $httpProvider.defaults.headers.common['X-Requested-With'];
    
	// Config growl
	growlProvider.globalTimeToLive(10000);
    growlProvider.onlyUniqueMessages(false);
    
    // Config WebSocket
	// WebSocketProvider
	// 	.prefix('')
	// 	.uri(param['websocket']);
	
	FacebookProvider.init('1509715655971070');
	
	blockUIConfig.autoBlock = true;
	blockUIConfig.delay = 0;
	blockUIConfig.template = "<div class='block-ui-overlay'></div><div class='block-ui-message-container' aria-live='assertive' aria-atomic='true'><div class='block-ui-message'><i class='fa fa-spinner fa-spin'></i></div></div>";

}]);


app.run(['$rootScope',function($rootScope) {
	$rootScope.$on('$viewContentLoaded',function(){
    	$("html, body").animate({ scrollTop: 0 }, 0);
    	$("html, body").animate({opacity:0.3}, { queue: false, duration: 0 });
    	$("html, body").animate({opacity:1}, { queue: true, duration: 600 });
	});
}]);