/*USUARIO*/

﻿insert into sica.persona (id, nombre, apellido, nick, email, password, sexo, celular, is_activo, tipo , fecha_nacimiento) values ( 1, 'Juan', 'Miraballes', 'jmg' , 'jmg@gmail.com', 'jmg' ,'M', '099336405', true, 3, current_date );
insert into sica.per_usuario (id) values (1);
insert into sica.persona (id, nombre, apellido, nick, email, password, sexo, celular, is_activo, tipo , fecha_nacimiento) values ( 3, 'Plablo', 'de Sosa', 'Moko' , 'moko@gmail.com', 'moko' ,'M', '099336405', true, 3, current_date );
insert into sica.per_usuario (id) values (3);

/*RESCATISTA*/
insert into sica.persona (id, nombre, apellido, nick, email, password, sexo, celular, is_activo, tipo , fecha_nacimiento) values ( 2, 'Fede', 'Rodriguez', 'fede' , 'fede@gmail.com', 'fede' ,'M', '099336405', true, 2, current_date );
insert into sica.per_rescatista (id) values (2);

/*ADMINISTRADOR*/
insert into sica.persona (id, nombre, apellido, nick, email, password, sexo, celular, is_activo, tipo , fecha_nacimiento) values ( 4, 'Turco', 'Basan', 'turco' , 'turco@gmail.com', 'turco' ,'M', '099336405', true, 1, current_date );
insert into sica.per_admin (id) values (4);

insert into sica.persona (id, nombre, apellido, nick, email, password, sexo, celular, is_activo, tipo , fecha_nacimiento) values ( 5, 'admin', 'admin', 'admin' , 'admin@gmail.com', 'admin' ,'M', '099336405', true, 1, current_date );
insert into sica.per_admin (id) values (4);

/*FUENTES RSS  */
insert into sica.fuente_rss (id, url, nombre ) values ( 1, 'http://www.alertacatastrofes.com/feed/' , 'Alerta Catastrofe');
insert into sica.fuente_rss (id, url, nombre) values ( 2, 'http://cnnespanol.cnn.com/feed' , 'CNN español' );
insert into sica.fuente_rss (id, url, nombre) values ( 3, 'http://www.bbc.co.uk/mundo/index.xml' , 'BBC Mundo');

/*FUENTES RSS  */
insert into sica.ong ( id, nombre, direccion, telefono, sitio_web, email ) values (1, 'Ong 1', 'Direccion1', '099336405', 'wwww.ong1.com', 'ong1@gmail.com' );
insert into sica.ong ( id, nombre, direccion, telefono, sitio_web, email ) values (2, 'Ong 2', 'Direccion2', '099336405', 'wwww.ong2.com', 'ong2@gmail.com' );
insert into sica.ong ( id, nombre, direccion, telefono, sitio_web, email ) values (3, 'Ong 3', 'Direccion3', '099336405', 'wwww.ong3.com', 'ong3@gmail.com' );

/*PLANES */
insert into sica.plan_emergencia (id, pasos, descripcion, orden) values (1,'Alerta', 'Se recibe notificación' , 1 );
insert into sica.plan_emergencia (id, pasos, descripcion, orden) values (2,'Búsqueda', 'Búsqueda de persona' , 2);
insert into sica.plan_emergencia (id, pasos, descripcion, orden) values (3,'Rescate', 'Rescate de persona'  , 3);
insert into sica.plan_emergencia (id, pasos, descripcion, orden) values (4,'Atención', 'Atención médica de persona' , 4);
insert into sica.plan_emergencia (id, pasos, descripcion, orden) values (5,'Traslado', 'Traslado de persona' , 5);

insert into sica.plan_riesgo (id, pasos, descripcion, orden) values (1,'Alerta', 'Se recibe notificación' , 1);
insert into sica.plan_riesgo (id, pasos, descripcion, orden) values (2,'Ánalis de situación', 'Ánalisis de situación de la catástrofe' , 2 );
insert into sica.plan_riesgo (id, pasos, descripcion, orden) values (3,'Aviso', 'Dar aviso a instituciones correspondientes' , 3);
insert into sica.plan_riesgo (id, pasos, descripcion, orden) values (4,'Preparación', 'Preparación de materiales para rescate' , 4);
insert into sica.plan_riesgo (id, pasos, descripcion, orden) values (5,'Reporte', 'Notificar reporte de situación' , 5);


/*TIPO CATASTROFES*/
insert into sica.tipo_catastrofe(id, nombre, estilo) values (1, 'Incendio', 'css estilo incendio');
insert into sica.tipo_catastrofe(id, nombre, estilo) values (2, 'Inundación', 'css estilo inundación');
insert into sica.tipo_catastrofe(id, nombre, estilo) values (3, 'Terremoto', 'css estilo terremoto');
insert into sica.tipo_catastrofe(id, nombre, estilo) values (4, 'Maremoto', 'css estilo maremoto');

/*ESTADO USUARIO*/
insert into sica.estado_usuario (id, condicion) values (1, 'BIEN');
insert into sica.estado_usuario (id, condicion) values (2, 'LEVE');
insert into sica.estado_usuario (id, condicion) values (3, 'MEDIO');
insert into sica.estado_usuario (id, condicion) values (4, 'GRAVE');

/*DESAPARECIDOS*/
insert into sica.desaparecido (id, nombre, apellido, edad, estado_desaparecido, etnia, id_usuario, color_piel, color_ojos, estatura, fecha_desaparecido, detalles)
values(1, 'Juan', 'Rodriguez', 19, 'DESAPARECIDO', '', 1, '', '', 1.76, '2014-12-03', 'Tiene tatuaje');

insert into sica.desaparecido (id, nombre, apellido, edad, estado_desaparecido, etnia, id_usuario, color_piel, color_ojos, estatura, fecha_desaparecido, detalles)
values(2, 'Jorge', 'Vazquez', 35, 'DESAPARECIDO', '', 1, '', '', 1.80, '2014-12-03', 'Lunar en la cara');

insert into sica.desaparecido (id, nombre, apellido, edad, estado_desaparecido, etnia, id_usuario, color_piel, color_ojos, estatura, fecha_desaparecido, detalles)
values(3, 'Andrea', 'Garcia', 25, 'DESAPARECIDO', '', 1, '', '', 1.60, '2014-12-03', 'Es rubia');
