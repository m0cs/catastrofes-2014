package com.sica.dao;

import java.util.List;

import javax.ejb.Local;

import com.sica.entities.PedidoAyuda;
import com.sica.excepciones.DaoException;

@Local
public interface IPedidoAyudaDao extends IDao<PedidoAyuda> {

	public List<PedidoAyuda> buscarPedidoAyudaPorUsuario(long idUsuario) throws DaoException;

}
