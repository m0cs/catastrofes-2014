package com.sica.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.Query;

import com.sica.entities.EventoCatastrofe;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class EventoCatastrofeDao extends Dao<EventoCatastrofe> implements IEventoCatastrofeDao {



	@SuppressWarnings("unchecked")
	@Override
	public List<EventoCatastrofe> findAll() {
		Query namedQuery = em.createNamedQuery("EventoCatastrofe.findAll");
		return (List<EventoCatastrofe>) namedQuery.getResultList();	
	}

}
