package com.sica.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import com.sica.entities.PlanEmergencia;
import com.sica.excepciones.DaoException;


@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class PlanEmergenciaDao extends Dao<PlanEmergencia> implements IPlanEmergenciaDao {


	@SuppressWarnings("unchecked")
	@Override
	public List<PlanEmergencia> findAll() throws DaoException {
		Query namedQuery = em.createNamedQuery("PlanEmergencia.findAll");
		return (List<PlanEmergencia>) namedQuery.getResultList();
	}

	public PlanEmergencia validarOrdenPasoExistente(long orden, long id) throws DaoException {
		try{
			StringBuilder sb = new StringBuilder();
			sb.append(" SELECT u FROM PlanEmergencia u WHERE u.orden=:orden ");
			
			if (id != 0){
				sb.append("and u.id <>:id");
			}	
			Query query = em.createQuery(sb.toString());
			query.setParameter("orden", orden);
			
			if (id != 0){
				query.setParameter("id", id);
			}
			PlanEmergencia plan = (PlanEmergencia) query.getSingleResult();
			
			return plan;
		}
		catch (NoResultException e) {
			 return null;
		}
        catch(Throwable e){
                throw new DaoException(e.getMessage());
        }		
	}	

}
