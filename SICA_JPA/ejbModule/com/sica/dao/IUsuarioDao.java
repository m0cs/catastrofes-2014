package com.sica.dao;

import javax.ejb.Local;

import com.sica.entities.Usuario;

@Local
public interface IUsuarioDao extends IDao<Usuario> {

}
