package com.sica.dao;

import javax.ejb.Local;

import com.sica.entities.TipoCatastrofe;
import com.sica.excepciones.DaoException;

@Local
public interface ITipoCatastrofeDao extends IDao<TipoCatastrofe> {

	public TipoCatastrofe validarPorNombre(String nombre, long id) throws DaoException;

}
