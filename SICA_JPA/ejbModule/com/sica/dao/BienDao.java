package com.sica.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.Query;

import com.sica.entities.Bien;
import com.sica.excepciones.DaoException;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class BienDao extends Dao<Bien> implements IBienDao {

	@SuppressWarnings("unchecked")
	@Override
	public List<Bien> findAll() throws DaoException {
		Query namedQuery = em.createNamedQuery("Servicio.findAll");
		return (List<Bien>) namedQuery.getResultList();	
	}

}
