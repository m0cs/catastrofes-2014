package com.sica.dao;

import javax.ejb.Local;

import com.sica.entities.PlanEmergencia;
import com.sica.excepciones.DaoException;


@Local
public interface IPlanEmergenciaDao extends IDao<PlanEmergencia> {

	public PlanEmergencia validarOrdenPasoExistente(long orden, long idPaso) throws DaoException;

}
