package com.sica.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import com.sica.entities.PlanRescatista;
import com.sica.excepciones.DaoException;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class PlanRescatistaDao extends Dao<PlanRescatista> implements IPlanRescatistaDao {

	@SuppressWarnings("unchecked")
	@Override
	public List<PlanRescatista> findAll() throws DaoException {
		Query namedQuery = em.createNamedQuery("PlanRescatista.findAll");
		return (List<PlanRescatista>) namedQuery.getResultList();
	}
	
	
	/**
	 * Busca el plan no completado del rescatista
	 * */
	@Override
	public PlanRescatista buscarPlanPorRescatista( String email ) throws DaoException{
		
		try{
			StringBuilder sb = new StringBuilder();
			sb.append(" SELECT u FROM PlanRescatista u WHERE u.rescatista.email=:email and u.isCompletado=:isCompletado");
			

			Query query = em.createQuery(sb.toString());
			query.setParameter("email", email);
			query.setParameter("isCompletado", false);
			
			PlanRescatista plan = (PlanRescatista) query.getSingleResult();
			
			return plan;
		}
		catch (NoResultException e) {
			 throw new DaoException( "No tiene planes pendientes.");
		}
        catch(Throwable e){
                throw new DaoException(e.getMessage());
        }	
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<PlanRescatista> buscarPlanesCompletadosPorRescatista (String email) throws DaoException{
		try {
			StringBuilder sb = new StringBuilder();
			sb.append(" SELECT u FROM PlanRescatista u WHERE u.rescatista.email=:email and u.isCompletado=:isCompletado");
			
			Query query = em.createQuery(sb.toString());
			query.setParameter("email", email);
			query.setParameter("isCompletado", true);	
			
			List<PlanRescatista> planes = (List<PlanRescatista>) query.getResultList();
			return planes;
			
		}
		catch (NoResultException e) {
			 throw new DaoException( "No tiene planes completados. ");
		}
       catch(Throwable e){
               throw new DaoException(e.getMessage());
       }	
	}
}
