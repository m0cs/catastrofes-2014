package com.sica.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.Query;

import com.sica.entities.Usuario;


@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class UsuarioDao extends Dao<Usuario> implements IUsuarioDao {

	@SuppressWarnings("unchecked")
	@Override
	public List<Usuario> findAll() {
		Query namedQuery = em.createNamedQuery("Usuario.findAll");
		return (List<Usuario>) namedQuery.getResultList();	
	}


}
