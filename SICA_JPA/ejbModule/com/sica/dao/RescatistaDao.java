package com.sica.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.Query;

import com.sica.entities.Rescatista;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class RescatistaDao extends Dao<Rescatista> implements IRescatistaDao {

	@SuppressWarnings("unchecked")
	@Override
	public List<Rescatista> findAll() {
		Query namedQuery = em.createNamedQuery("Rescatista.findAll");
		return (List<Rescatista>) namedQuery.getResultList();	
	}

}
