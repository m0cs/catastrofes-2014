package com.sica.dao;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.Query;
import javax.persistence.TemporalType;

import com.sica.entities.Desaparecido;
import com.sica.entities.Persona;
import com.sica.excepciones.DaoException;
import com.sica.utiles.FiltroBusquedaDesaparecido;
import com.sica.utiles.Utiles_JPA;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class DesaparecidoDao extends Dao<Desaparecido> implements IDesaparecidoDao {

	@EJB
	IPersonaDao personaDao;
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Desaparecido> findAll() {
		Query namedQuery = em.createNamedQuery("Desaparecido.findAll");
		return (List<Desaparecido>) namedQuery.getResultList();
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Desaparecido> buscarDesaparecidoPorUsuario( long idUsuario ) throws DaoException{
		try{
			StringBuilder sb = new StringBuilder();
			sb.append(" SELECT u FROM Desaparecido u WHERE u.usuario.id = :idUsuario");
			
			Query query = em.createQuery(sb.toString());
			
			query.setParameter("idUsuario", idUsuario);
			return (List<Desaparecido>) query.getResultList();
		}
		catch(Exception e){
			e.printStackTrace();
			throw new DaoException("Error realizando la busqueda desaparecidos.");
		}		
	}
	
	
	/**
	 * Filtro de busqueda de desaparecidos.
	 * */
	@SuppressWarnings("unchecked")
	public List<Desaparecido> busquedaDesaparecido( FiltroBusquedaDesaparecido filtro ) throws DaoException{
		
		try{
			StringBuilder sb = new StringBuilder();
			
			sb.append(" SELECT u FROM Desaparecido u ");
			
			if (filtro.containsFiels()){
				sb.append(" WHERE ");
				int index = 0;
				if (!Utiles_JPA.isNullOrEmpty(filtro.getNombre())){
					sb.append("u.nombre like '%"+ filtro.getNombre() + "%'");
					index++;
				}
				if (!Utiles_JPA.isNullOrEmpty(filtro.getApellido())){
					if (index > 0){
						sb.append(" and ");
					}
					sb.append(" u.apellido like '" + filtro.getApellido() + "%'");
					index++;
				}
				if (!Utiles_JPA.isNullOrEmpty(filtro.getEmailUsuario())){
					Persona usuario = personaDao.buscarUsuarioPorEmail(filtro.getEmailUsuario());
					if (index > 0){
						sb.append(" and ");
					}					
					sb.append(" u.usuario.id = " + usuario.getId() );
					index++;
				}
				if (!Utiles_JPA.isNullOrEmpty(filtro.getDetalles())){
					if (index > 0){
						sb.append(" and ");
					}
					sb.append(" u.detalles like '%" + filtro.getDetalles() + "%'");
					index++;
				}				
				if (!Utiles_JPA.isNullOrEmpty(filtro.getFechaInicio()) || !Utiles_JPA.isNullOrEmpty(filtro.getFechaFin())){
					
					if (!Utiles_JPA.isNullOrEmpty(filtro.getFechaInicio()) && !Utiles_JPA.isNullOrEmpty(filtro.getFechaFin())){
						if (index > 0){
							sb.append(" and ");
						}
						sb.append(" u.fechaDesaparecido >= :fechaInicio and u.fechaDesaparecido <= :fechaFin ");
						index++;
					}
					else{
						if (!Utiles_JPA.isNullOrEmpty(filtro.getFechaInicio())){
							if (index > 0){
								sb.append(" and ");
							}							
							sb.append(" u.fechaDesaparecido >= :fechaInicio ");
							index++;
						}						
						if (!Utiles_JPA.isNullOrEmpty(filtro.getFechaFin())){
							if (index > 0){
								sb.append(" and ");
							}
							sb.append(" u.fechaDesaparecido <= :fechaFin ");
							index++;
						}
					}
				}
				String conector = "";
				
				if (!Utiles_JPA.isListNullOrEmpty(filtro.getColorOjos())){
					if (index > 0){
						sb.append(" and ");
					}
					int i = 0;
					sb.append(" ( ");
					for (String color : filtro.getColorOjos()){
						if (i > 0){
							conector = " or ";
						}
						sb.append( conector + " u.colorOjos = '" + color + "'");
						i++;
					}
					sb.append(" ) ");
				}
				
				conector = "";
				if (!Utiles_JPA.isListNullOrEmpty(filtro.getColorPiel())){
					if (index > 0){
						sb.append(" and ");
					}
					int i = 0;
					sb.append(" ( ");
					for (String color : filtro.getColorPiel()){
						if (i > 0){
							conector = " or ";
						}
						sb.append( conector + " u.colorPiel = '" + color + "'");
						i++;
					}	
					sb.append(" ) ");
				}
				
				conector = "";
				if (!Utiles_JPA.isListNullOrEmpty(filtro.getEtnias())){
					if (index > 0){
						sb.append(" and ");
					}					
					int i = 0;
					sb.append(" ( ");
					for (String color : filtro.getEtnias()){
						if (i > 0){
							conector = " or ";
						}
						sb.append( conector + " u.etnia = '" + color + "'");
						i++;
					}	
					sb.append(" ) ");
				}	
				if (!Utiles_JPA.isListNullOrEmpty(filtro.getEdad())){
					if (index > 0){
						sb.append(" and ");
					}
					sb.append(" u.edad >= :edadInicio and u.edad <= :edadFin ");
					index++;
				}	
				if (!Utiles_JPA.isListNullOrEmpty(filtro.getAltura())){
					if (index > 0){
						sb.append(" and ");
					}
					sb.append(" u.estatura >= :alturaInicio and u.estatura <= :alturaFin ");
				}				
			}
			Query query = em.createQuery(sb.toString());
			
			if (!Utiles_JPA.isNullOrEmpty(filtro.getFechaInicio())){
				query.setParameter("fechaInicio", Utiles_JPA.DATE_FORMAT_MIN.parse(filtro.getFechaInicio()) , TemporalType.DATE);	
			}
			if (!Utiles_JPA.isNullOrEmpty(filtro.getFechaFin())){
				query.setParameter("fechaFin", Utiles_JPA.DATE_FORMAT_MIN.parse(filtro.getFechaFin()), TemporalType.DATE);
			}
			if (!Utiles_JPA.isListNullOrEmpty(filtro.getEdad())){
				query.setParameter("edadInicio", Integer.valueOf(filtro.getEdad().get(0)));
				query.setParameter("edadFin", Integer.valueOf(filtro.getEdad().get(1)));
			}	
			if (!Utiles_JPA.isListNullOrEmpty(filtro.getAltura())){
				query.setParameter("alturaInicio", Double.valueOf(filtro.getAltura().get(0)));
				query.setParameter("alturaFin", Double.valueOf(filtro.getAltura().get(1)));
			}				
			
			return (List<Desaparecido>) query.getResultList();
		}
		catch(Exception e){
			e.printStackTrace();
			throw new DaoException("Error realizando la busqueda desaparecidos.");
		}
	}
	
}
