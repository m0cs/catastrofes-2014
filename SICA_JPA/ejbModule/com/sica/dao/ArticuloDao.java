package com.sica.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.Query;

import com.sica.entities.Articulo;
import com.sica.excepciones.DaoException;


@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class ArticuloDao extends Dao<Articulo> implements IArticuloDao {

	@SuppressWarnings("unchecked")
	@Override
	public List<Articulo> findAll() throws DaoException {
		Query namedQuery = em.createNamedQuery("Articulo.findAll");
		return (List<Articulo>) namedQuery.getResultList();	
	}

}
