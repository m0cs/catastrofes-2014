package com.sica.dao;

import java.util.List;

import javax.ejb.Local;

import com.sica.entities.Desaparecido;
import com.sica.excepciones.DaoException;
import com.sica.utiles.FiltroBusquedaDesaparecido;

@Local
public interface IDesaparecidoDao extends IDao<Desaparecido> {

	public List<Desaparecido> busquedaDesaparecido( FiltroBusquedaDesaparecido filtro) throws DaoException;

	public List<Desaparecido> buscarDesaparecidoPorUsuario(long idUsuario) throws DaoException;
}
