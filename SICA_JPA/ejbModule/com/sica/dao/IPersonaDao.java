package com.sica.dao;

import javax.ejb.Local;

import com.sica.entities.Persona;
import com.sica.excepciones.DaoException;

@Local
public interface IPersonaDao extends IDao<Persona> {

	public Persona buscarPorEmailYPassword( String email, String password ) throws DaoException;

	public Persona buscarAdminPorEmail(String email, long id) throws DaoException;

	public Persona buscarPorEmailYPasswordAndTipo(String email, String password, int tipo) throws DaoException;

	public Persona buscarUsuarioPorEmail(String email) throws DaoException;
}
