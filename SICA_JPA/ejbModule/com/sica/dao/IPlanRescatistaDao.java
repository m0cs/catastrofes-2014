package com.sica.dao;

import java.util.List;

import javax.ejb.Local;

import com.sica.entities.PlanRescatista;
import com.sica.excepciones.DaoException;

@Local
public interface IPlanRescatistaDao extends IDao<PlanRescatista> {

	public PlanRescatista buscarPlanPorRescatista(String email) throws DaoException;

	public List<PlanRescatista> buscarPlanesCompletadosPorRescatista(String email) throws DaoException;

}
