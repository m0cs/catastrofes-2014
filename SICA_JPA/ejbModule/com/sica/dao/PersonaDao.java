package com.sica.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import com.sica.entities.Persona;
import com.sica.excepciones.DaoException;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class PersonaDao extends Dao<Persona> implements IPersonaDao {

	@SuppressWarnings("unchecked")
	@Override
	public List<Persona> findAll() {
		Query namedQuery = em.createNamedQuery("Persona.findAll");
		return (List<Persona>) namedQuery.getResultList();	
	}

	/**
	 * Busca una Persona por su email y su password..
	 * @throws DaoException 
	 * */
	public Persona buscarPorEmailYPassword( String email, String password ) throws DaoException{
		
		try{
			Query query = em.createNamedQuery("Persona.buscarPorEmailYPassword");
				
			query.setParameter("email", email)
				 .setParameter("pass", password);
			
			Persona persona = (Persona) query.getSingleResult();
			
			return persona;
		}
		catch (NoResultException e) {
			 return null;
		}
        catch(Throwable e){
                throw new DaoException(e.getMessage());
        }
	}
	
	
	/**
	 * Busca una Persona por su email y su password..
	 * @throws DaoException 
	 * */
	@Override
	public Persona buscarPorEmailYPasswordAndTipo( String email, String password, int tipo) throws DaoException{
		
		try{
			Query query = em.createNamedQuery("Persona.buscarPorEmailYPasswordAndTipo");
				
			query.setParameter("email", email)
				 .setParameter("pass", password)
				 .setParameter("tipo", tipo);
			
			Persona persona = (Persona) query.getSingleResult();
			
			return persona;
		}
		catch (NoResultException e) {
			 return null;
		}
        catch(Throwable e){
                throw new DaoException(e.getMessage());
        }
	}	
	

	@Override
	public Persona buscarAdminPorEmail(String email, long id) throws DaoException {
		try{
			StringBuilder sb = new StringBuilder();
			sb.append(" SELECT u FROM Persona u WHERE u.email=:email ");
			
			if (id != 0){
				sb.append("and u.id <>:id");
			}	
			Query query = em.createQuery(sb.toString());
			query.setParameter("email", email);
			
			if (id != 0){
				query.setParameter("id", id);
			}
			Persona usu = (Persona) query.getSingleResult();
			
			return usu;
		}
		catch (NoResultException e) {
			 return null;
		}
        catch(Throwable e){
                throw new DaoException(e.getMessage());
        }	
	}	
	
	@Override
	public Persona buscarUsuarioPorEmail(String email) throws DaoException {
		try{
			StringBuilder sb = new StringBuilder();
			sb.append(" SELECT u FROM Persona u WHERE u.email=:email ");
			
			Query query = em.createQuery(sb.toString());
			query.setParameter("email", email);
			
			Persona usu = (Persona) query.getSingleResult();
			
			return usu;
		}
		catch (NoResultException e) {
			 return null;
		}
        catch(Throwable e){
                throw new DaoException(e.getMessage());
        }	
	}	

}
