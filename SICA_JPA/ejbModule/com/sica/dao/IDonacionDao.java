package com.sica.dao;

import javax.ejb.Local;

import com.sica.entities.Donacion;

@Local
public interface IDonacionDao extends IDao<Donacion> {

}
