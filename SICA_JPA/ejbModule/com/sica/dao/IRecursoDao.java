package com.sica.dao;

import javax.ejb.Local;

import com.sica.entities.Recurso;

@Local
public interface IRecursoDao extends IDao<Recurso> {

}
