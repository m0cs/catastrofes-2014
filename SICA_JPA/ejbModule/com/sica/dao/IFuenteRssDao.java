package com.sica.dao;

import javax.ejb.Local;

import com.sica.entities.FuenteRss;
import com.sica.excepciones.DaoException;

@Local
public interface IFuenteRssDao extends IDao<FuenteRss> {

	FuenteRss validarFuenteUnicaPorRuta(String ruta, long id) throws DaoException;

}
