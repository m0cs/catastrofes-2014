package com.sica.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.Query;

import com.sica.entities.Servicio;
import com.sica.excepciones.DaoException;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class ServicioDao extends Dao<Servicio> implements IServicioDao {

	@SuppressWarnings("unchecked")
	@Override
	public List<Servicio> findAll() throws DaoException {
		Query namedQuery = em.createNamedQuery("Servicio.findAll");
		return (List<Servicio>) namedQuery.getResultList();	
	}


}
