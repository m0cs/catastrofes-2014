package com.sica.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import com.sica.entities.FuenteRss;
import com.sica.excepciones.DaoException;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class FuenteRssDao extends Dao<FuenteRss> implements IFuenteRssDao {


	@SuppressWarnings("unchecked")
	@Override
	public List<FuenteRss> findAll() {
		Query namedQuery = em.createNamedQuery("FuenteRss.findAll");
		return (List<FuenteRss>) namedQuery.getResultList();	
	}
	
	@Override
	public FuenteRss validarFuenteUnicaPorRuta(String ruta, long id) throws DaoException {
		try{
			StringBuilder sb = new StringBuilder();
			sb.append(" SELECT u FROM FuenteRss u WHERE u.url=:url");
			
			if (id != 0){
				sb.append(" and u.id <>:id ");
			}	
			Query query = em.createQuery(sb.toString());
			query.setParameter("url", ruta);
			
			if (id != 0){
				query.setParameter("id", id);
			}
			FuenteRss fuente = (FuenteRss) query.getSingleResult();
			
			return fuente;
		}
		catch (NoResultException e) {
			 return null;
		}
        catch(Throwable e){
                throw new DaoException(e.getMessage());
        }		
	}	
}
