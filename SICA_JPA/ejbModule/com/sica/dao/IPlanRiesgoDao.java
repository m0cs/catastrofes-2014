package com.sica.dao;

import javax.ejb.Local;

import com.sica.entities.PlanRiesgo;
import com.sica.excepciones.DaoException;

@Local
public interface IPlanRiesgoDao extends IDao<PlanRiesgo> {

	PlanRiesgo validarOrdenPasoExistente(long orden, long idPaso) throws DaoException;

}
