package com.sica.dao;

import javax.ejb.Local;

import com.sica.entities.Ong;
import com.sica.excepciones.DaoException;

@Local
public interface IOngDao extends IDao<Ong> {

	public Ong buscarOngPorEmail(String email, long id) throws DaoException;
}
