package com.sica.dao;

import javax.ejb.Local;

import com.sica.entities.Articulo;

@Local
public interface IArticuloDao extends IDao<Articulo> {

}
