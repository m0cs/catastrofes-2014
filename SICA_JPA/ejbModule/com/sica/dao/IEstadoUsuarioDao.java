package com.sica.dao;

import javax.ejb.Local;

import com.sica.entities.EstadoUsuario;
import com.sica.excepciones.DaoException;

@Local
public interface IEstadoUsuarioDao extends IDao<EstadoUsuario> {

	public EstadoUsuario buscarEstadoPorNombre(String nombre) throws DaoException;

}
