package com.sica.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.Query;

import com.sica.entities.Administrador;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class AdministradorDao extends Dao<Administrador> implements IAdministradorDao {

	@SuppressWarnings("unchecked")
	@Override
	public List<Administrador> findAll() {
		Query namedQuery = em.createNamedQuery("Administrador.findAll");
		return (List<Administrador>) namedQuery.getResultList();	
	}

}
