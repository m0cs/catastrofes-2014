package com.sica.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import com.sica.entities.Ong;
import com.sica.excepciones.DaoException;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class OngDao extends Dao<Ong> implements IOngDao {

	@SuppressWarnings("unchecked")
	@Override
	public List<Ong> findAll() {
		Query namedQuery = em.createNamedQuery("Ong.findAll");
		return (List<Ong>) namedQuery.getResultList();	
	}
	
	public Ong buscarOngPorEmail(String email, long id) throws DaoException {
		try{
			StringBuilder sb = new StringBuilder();
			sb.append(" SELECT u FROM Ong u WHERE u.email=:email ");
			
			if (id != 0){
				sb.append("and u.id <>:id");
			}	
			Query query = em.createQuery(sb.toString());
			query.setParameter("email", email);
			
			if (id != 0){
				query.setParameter("id", id);
			}
			Ong ong = (Ong) query.getSingleResult();
			
			return ong;
		}
		catch (NoResultException e) {
			 return null;
		}
        catch(Throwable e){
                throw new DaoException(e.getMessage());
        }		
	}

}
