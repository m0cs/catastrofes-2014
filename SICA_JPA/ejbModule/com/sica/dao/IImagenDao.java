package com.sica.dao;

import javax.ejb.Local;

import com.sica.entities.Imagen;

@Local
public interface IImagenDao extends IDao<Imagen> {

}
