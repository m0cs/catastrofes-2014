package com.sica.dao;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import com.sica.entities.PlanEmergencia;
import com.sica.entities.PlanRiesgo;
import com.sica.excepciones.DaoException;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class PlanDao {
	
	@EJB
	private IPlanRiesgoDao planRiesgoDao;
	
	@EJB
	private IPlanEmergenciaDao planEmergenciaDao;
	


	public List<PlanEmergencia> buscarTodosPlanesEmergencia() throws DaoException{
		try {
			return planEmergenciaDao.findAll();
		} 
		catch (DaoException e) {
			throw new DaoException(e.getMessage());
		}
	}
	
	public List<PlanRiesgo> buscarTodosPlanesRiesgo() throws DaoException{
		try {
			return planRiesgoDao.findAll();
		}
		catch (DaoException e) {
			throw new DaoException(e.getMessage());
		}
	}
	
//	public void eliminarPlanEmergencia(long id){
//		try {
//			
//			planEmergenciaDao.remove(entity);
//		} 
//		catch (DaoException e) {
//			throw new DaoException( e.getMessage());
//		}
//	}
//	
//	public void eliminarPlanRiesgo(long id){
//		try {
//			
//			planRiesgoDao.remove(entity);;
//		}
//		catch (DaoException e) {
//			throw new DaoException( e.getMessage());
//		}
//	}
	

}
