package com.sica.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.Query;

import com.sica.entities.Donacion;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class DonacionDao extends Dao<Donacion> implements IDonacionDao {

	@SuppressWarnings("unchecked")
	@Override
	public List<Donacion> findAll() {
		Query namedQuery = em.createNamedQuery("Donacion.findAll");
		return (List<Donacion>) namedQuery.getResultList();	
	}

}
