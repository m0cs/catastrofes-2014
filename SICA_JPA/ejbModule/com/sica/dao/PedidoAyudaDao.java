package com.sica.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.Query;

import com.sica.entities.PedidoAyuda;
import com.sica.excepciones.DaoException;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class PedidoAyudaDao extends Dao<PedidoAyuda> implements IPedidoAyudaDao {

	@SuppressWarnings("unchecked")
	@Override
	public List<PedidoAyuda> findAll() {
		Query namedQuery = em.createNamedQuery("PedidoAyuda.findAll");
		return (List<PedidoAyuda>) namedQuery.getResultList();	
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<PedidoAyuda> buscarPedidoAyudaPorUsuario( long idUsuario ) throws DaoException{
		try{
			StringBuilder sb = new StringBuilder();
			sb.append(" SELECT u FROM PedidoAyuda u WHERE u.usuario.id = :idUsuario");
			
			Query query = em.createQuery(sb.toString());
			
			query.setParameter("idUsuario", idUsuario);
			return (List<PedidoAyuda>) query.getResultList();
		}
		catch(Exception e){
			e.printStackTrace();
			throw new DaoException("Error realizando la busqueda desaparecidos.");
		}		
	}	

}
