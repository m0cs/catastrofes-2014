package com.sica.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.Query;

import com.sica.entities.Recurso;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class RecursoDao extends Dao<Recurso> implements IRecursoDao {


	@SuppressWarnings("unchecked")
	@Override
	public List<Recurso> findAll() {
		Query namedQuery = em.createNamedQuery("Recurso.findAll");
		return (List<Recurso>) namedQuery.getResultList();	
	}

}
