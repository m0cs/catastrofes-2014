package com.sica.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import com.sica.entities.TipoCatastrofe;
import com.sica.excepciones.DaoException;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class TipoCatastrofeDao extends Dao<TipoCatastrofe> implements ITipoCatastrofeDao {

	@SuppressWarnings("unchecked")
	@Override
	public List<TipoCatastrofe> findAll() {
		Query namedQuery = em.createNamedQuery("TipoCatastrofe.findAll");
		return (List<TipoCatastrofe>) namedQuery.getResultList();	
	}
	
	
	public TipoCatastrofe validarPorNombre(String nombre, long id) throws DaoException {
		try{
			StringBuilder sb = new StringBuilder();
			sb.append(" SELECT u FROM TipoCatastrofe u WHERE u.nombre=:nombre ");
			
			if (id != 0){
				sb.append("and u.id <>:id");
			}	
			Query query = em.createQuery(sb.toString());
			query.setParameter("nombre", nombre);
			
			if (id != 0){
				query.setParameter("id", id);
			}
			TipoCatastrofe tipo = (TipoCatastrofe) query.getSingleResult();
			
			return tipo;
		}
		catch (NoResultException e) {
			 return null;
		}
        catch(Throwable e){
                throw new DaoException(e.getMessage());
        }		
	}	
	

}
