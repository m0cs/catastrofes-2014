package com.sica.dao;

import java.util.List;

import com.sica.excepciones.DaoException;

public interface IDao <E> {

	void flush() throws DaoException;

	void persist(E entity) throws DaoException ;

	void merge(E entity) throws DaoException;

	void remove(E entity) throws DaoException;

	E findByID(long id) throws DaoException;

	List<E> findAll() throws DaoException;

	public void rollBack() throws DaoException;

	public Boolean contains(E entity) throws DaoException;
		
}	
