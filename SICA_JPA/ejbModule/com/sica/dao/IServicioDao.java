package com.sica.dao;

import javax.ejb.Local;

import com.sica.entities.Servicio;

@Local
public interface IServicioDao extends IDao<Servicio> {

}
