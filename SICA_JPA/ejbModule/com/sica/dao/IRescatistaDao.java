package com.sica.dao;

import javax.ejb.Local;

import com.sica.entities.Rescatista;

@Local
public interface IRescatistaDao extends IDao<Rescatista>{

}
