package com.sica.dao;

import javax.ejb.Local;

import com.sica.entities.PlanRescatista;

@Local
public interface IPlanDao extends IDao<PlanRescatista> {

}
