package com.sica.dao;

import javax.ejb.Local;

import com.sica.entities.Administrador;

@Local
public interface IAdministradorDao extends IDao<Administrador> {

}
