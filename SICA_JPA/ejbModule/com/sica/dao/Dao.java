package com.sica.dao;

import java.lang.reflect.ParameterizedType;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.sica.excepciones.DaoException;
import com.sica.utiles.Utiles_JPA;


public abstract class Dao<E> implements IDao<E> {

//	private static Logger logger = Logger.getLogger(Dao.class);
	
	protected Class<E> entityClass;

	@PersistenceContext(unitName=Utiles_JPA.UNIT_PERSISTENCE_NAME)
	protected EntityManager em;
 
	@Override
	public void flush() {
		em.flush();
	}

	@SuppressWarnings("unchecked")
	public Dao() {
		ParameterizedType genericSuperclass = (ParameterizedType) getClass().getGenericSuperclass();
		this.entityClass = (Class<E>) genericSuperclass.getActualTypeArguments()[0];		
	}

	@Override
	public void persist(E entity) throws DaoException {
		
        try{
                em.persist(entity);
        }
        catch(Throwable e){
        	throw new DaoException(e.getMessage());
        }
           		
	}

	@Override
	public void merge(E entity) throws DaoException {
		try{
			em.merge(entity);
		}
		catch(Throwable e){
			throw new DaoException(e.getMessage());
		}
	}

	@Override
	public void remove(E entity) throws DaoException {
		try{
			em.remove(entity);
		}
		catch(Throwable e){
			throw new DaoException(e.getMessage());
		}
	}

	@Override
	public E findByID(long id) throws DaoException {
		try{
			return em.find(entityClass, id);
		}
		catch(Throwable e){
			throw new DaoException(e.getMessage());
		}
	}

	@Override
	public void rollBack() throws DaoException{
		try{
			em.getTransaction().rollback();
		}
		catch(Throwable e){
			throw new DaoException(e.getMessage());
		}
	}
	
	@Override
	public Boolean contains(E entity) throws DaoException{
		try{
			return em.contains(entity);
		}
		catch(Throwable e){
			throw new DaoException(e.getMessage());
		}
	}
}
