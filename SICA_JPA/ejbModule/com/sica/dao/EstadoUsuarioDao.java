package com.sica.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import com.sica.entities.EstadoUsuario;
import com.sica.excepciones.DaoException;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class EstadoUsuarioDao extends Dao<EstadoUsuario> implements IEstadoUsuarioDao {

	@SuppressWarnings("unchecked")
	@Override
	public List<EstadoUsuario> findAll() {
		Query namedQuery = em.createNamedQuery("EstadoUsuario.findAll");
		return (List<EstadoUsuario>) namedQuery.getResultList();	
	}

	@Override
	public EstadoUsuario buscarEstadoPorNombre(String nombre) throws DaoException {
		try{
			StringBuilder sb = new StringBuilder();
			sb.append(" SELECT u FROM EstadoUsuario u WHERE u.condicion=:nombre ");
			
			Query query = em.createQuery(sb.toString());
			query.setParameter("nombre", nombre);
			
			EstadoUsuario estado = (EstadoUsuario) query.getSingleResult();
			
			return estado;
		}
		catch (NoResultException e) {
			 return null;
		}
        catch(Throwable e){
                throw new DaoException(e.getMessage());
        }	
	}		
}
