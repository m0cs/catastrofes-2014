package com.sica.dao;

import javax.ejb.Local;

import com.sica.entities.Bien;

@Local
public interface IBienDao extends IDao<Bien> {

}
