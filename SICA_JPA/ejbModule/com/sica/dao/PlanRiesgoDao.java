package com.sica.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import com.sica.entities.PlanRiesgo;
import com.sica.excepciones.DaoException;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class PlanRiesgoDao extends Dao<PlanRiesgo> implements IPlanRiesgoDao {


	@SuppressWarnings("unchecked")
	@Override
	public List<PlanRiesgo> findAll() throws DaoException {
		Query namedQuery = em.createNamedQuery("PlanRiesgo.findAll");
		return (List<PlanRiesgo>) namedQuery.getResultList();
	}
	
	public PlanRiesgo validarOrdenPasoExistente(long orden, long id) throws DaoException {
		try{
			StringBuilder sb = new StringBuilder();
			sb.append(" SELECT u FROM PlanRiesgo u WHERE u.orden=:orden ");
			
			if (id != 0){
				sb.append("and u.id <>:id");
			}	
			Query query = em.createQuery(sb.toString());
			query.setParameter("orden", orden);
			
			if (id != 0){
				query.setParameter("id", id);
			}
			PlanRiesgo plan = (PlanRiesgo) query.getSingleResult();
			
			return plan;
		}
		catch (NoResultException e) {
			 return null;
		}
        catch(Throwable e){
                throw new DaoException(e.getMessage());
        }		
	}	

}
