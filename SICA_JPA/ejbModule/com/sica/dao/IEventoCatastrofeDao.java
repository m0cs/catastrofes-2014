package com.sica.dao;

import javax.ejb.Local;

import com.sica.entities.EventoCatastrofe;

@Local
public interface IEventoCatastrofeDao extends IDao<EventoCatastrofe> {

}
