package com.sica.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the desaparecido database table.
 * 
 */
@Entity
@NamedQuery(name="Desaparecido.findAll", query="SELECT d FROM Desaparecido d")
public class Desaparecido implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="sic_sec_des")
    @SequenceGenerator(name="sic_sec_des", sequenceName = "sec_des", allocationSize=1)
	private Long id;

	private String apellido;

	private Integer edad;

	@Column(name="estado_desaparecido")
	private String estadoDesaparecido;

	private String etnia;

	private String nombre;
	
	@Column(name="color_piel")
	private String colorPiel;
	
	@Column(name="color_ojos")
	private String colorOjos;
	
	private double estatura;
	
	private String detalles;
	
	@Temporal(TemporalType.DATE)
	@Column(name="fecha_desaparecido")
	private Date fechaDesaparecido;
	
	@OneToMany
	@JoinTable(
		name="desaparecido_imagen"
		, joinColumns={
			@JoinColumn(name="id_desaparecido", referencedColumnName="id")
			}
		, inverseJoinColumns={
			@JoinColumn(name="id_imagen", referencedColumnName="id")
			}
		)
	private List<Imagen> imagens;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_usuario")
	private Usuario usuario;

	public Desaparecido() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getApellido() {
		return this.apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public Integer getEdad() {
		return this.edad;
	}

	public void setEdad(Integer edad) {
		this.edad = edad;
	}

	public String getEstadoDesaparecido() {
		return this.estadoDesaparecido;
	}

	public void setEstadoDesaparecido(String estadoDesaparecido) {
		this.estadoDesaparecido = estadoDesaparecido;
	}

	public String getEtnia() {
		return this.etnia;
	}

	public void setEtnia(String etnia) {
		this.etnia = etnia;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public List<Imagen> getImagens() {
		return this.imagens;
	}

	public void setImagens(List<Imagen> imagens) {
		this.imagens = imagens;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public String getColorPiel() {
		return colorPiel;
	}

	public void setColorPiel(String colorPiel) {
		this.colorPiel = colorPiel;
	}

	public String getColorOjos() {
		return colorOjos;
	}

	public void setColorOjos(String colorOjos) {
		this.colorOjos = colorOjos;
	}

	public double getEstatura() {
		return estatura;
	}

	public void setEstatura(double estatura) {
		this.estatura = estatura;
	}

	public Date getFechaDesaparecido() {
		return fechaDesaparecido;
	}

	public void setFechaDesaparecido(Date fechaDesaparecido) {
		this.fechaDesaparecido = fechaDesaparecido;
	}

	public String getDetalles() {
		return detalles;
	}

	public void setDetalles(String detalles) {
		this.detalles = detalles;
	}
	
	

}