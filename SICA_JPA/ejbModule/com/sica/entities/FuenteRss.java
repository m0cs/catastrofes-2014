package com.sica.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


/**
 * The persistent class for the fuente_rss database table.
 * 
 */
@Entity
@Table(name="fuente_rss")
@NamedQuery(name="FuenteRss.findAll", query="SELECT f FROM FuenteRss f")
public class FuenteRss implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="sic_sec_frs")
    @SequenceGenerator(name="sic_sec_frs", sequenceName = "sec_frs", allocationSize=1)
	private Long id;

	private String url;
	
	private String nombre;


	public FuenteRss() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUrl() {
		return this.url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
}