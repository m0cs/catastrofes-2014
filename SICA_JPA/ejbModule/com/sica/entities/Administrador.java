package com.sica.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the per_admin database table.
 * 
 */
@Entity
@Table(name="per_admin")

@NamedQuery(name="Administrador.findAll", query="SELECT a FROM Administrador a")

public class Administrador extends Persona implements Serializable {
	private static final long serialVersionUID = 1L;

	public Administrador() {}

}