package com.sica.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;


/**
 * The persistent class for the per_rescatista database table.
 * 
 */
@Entity
@Table(name="per_rescatista")

@NamedQuery(name="Rescatista.findAll", query="SELECT r FROM Rescatista r")
public class Rescatista extends Persona implements Serializable {
	private static final long serialVersionUID = 1L;

	private String descripcion;

	@Column(name="is_disponible")
	private Boolean isDisponible;
	
	@OneToMany(mappedBy="rescatista", fetch=FetchType.EAGER)
	private List<PlanRescatista> planesRescatistas;

	public Rescatista() {
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Boolean getIsDisponible() {
		return this.isDisponible;
	}

	public void setIsDisponible(Boolean isDisponible) {
		this.isDisponible = isDisponible;
	}

	public List<PlanRescatista> getPlanesRescatistas() {
		return planesRescatistas;
	}

	public void setPlanesRescatistas(List<PlanRescatista> planesRescatistas) {
		this.planesRescatistas = planesRescatistas;
	}

}