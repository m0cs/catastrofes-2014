package com.sica.entities;

import java.io.Serializable;

import javax.persistence.*;


/**
 * The persistent class for the voluntario database table.
 * 
 */
@Entity
@NamedQuery(name="Voluntario.findAll", query="SELECT v FROM Voluntario v")
public class Voluntario implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="sic_sec_vol")
    @SequenceGenerator(name="sic_sec_vol", sequenceName = "sec_vol", allocationSize=1)
	private Long id;

	private String especialidad;

	private Integer hora;

	private String nombre;


	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_bien_servicio", referencedColumnName="id")
	private Servicio donServicio;

	public Voluntario() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEspecialidad() {
		return this.especialidad;
	}

	public void setEspecialidad(String especialidad) {
		this.especialidad = especialidad;
	}

	public Integer getHora() {
		return this.hora;
	}

	public void setHora(Integer hora) {
		this.hora = hora;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Servicio getDonServicio() {
		return this.donServicio;
	}

	public void setDonServicio(Servicio donServicio) {
		this.donServicio = donServicio;
	}

}