package com.sica.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


/**
 * The persistent class for the plan_emergencia database table.
 * 
 */
@Entity
@Table(name="plan_emergencia")
@NamedQuery(name="PlanEmergencia.findAll", query="SELECT p FROM PlanEmergencia p order by p.orden asc")

public class PlanEmergencia implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="sic_sec_pem")
    @SequenceGenerator(name="sic_sec_pem", sequenceName = "sec_pem", allocationSize=1)
	private Long id;

	private long orden;
	
	private String descripcion;

	private String pasos;

	public PlanEmergencia() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getPasos() {
		return this.pasos;
	}

	public void setPasos(String pasos) {
		this.pasos = pasos;
	}

	public long getOrden() {
		return orden;
	}

	public void setOrden(long orden) {
		this.orden = orden;
	}

}