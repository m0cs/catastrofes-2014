package com.sica.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;


/**
 * The persistent class for the per_usuario database table.
 * 
 */
@Entity
@Table(name="per_usuario")

@NamedQueries({
	@NamedQuery(name="Usuario.findAll", 
			query="SELECT u FROM Usuario u"),
	
	@NamedQuery(name="Usuario.buscarPorEmailYPassword", 
		query="SELECT u FROM Usuario u WHERE u.email=:email AND u.password=:pass")
})

public class Usuario extends Persona implements Serializable {
	private static final long serialVersionUID = 1L;

	public Usuario() {}
	
	//bi-directional many-to-one association to PedidoAyuda
	@OneToMany(mappedBy="usuario", fetch=FetchType.LAZY)
	private List<PedidoAyuda> pedidoAyudas;
	
	//bi-directional many-to-one association to AvisoCatastrofe
	@OneToMany(mappedBy="usuario", fetch=FetchType.LAZY)
	private List<AvisoCatastrofe> avisoCatastrofes;

	//bi-directional many-to-one association to Desaparecido
	@OneToMany(mappedBy="usuario", fetch=FetchType.LAZY)
	private List<Desaparecido> desaparecidos;	
	

	public List<AvisoCatastrofe> getAvisoCatastrofes() {
		return this.avisoCatastrofes;
	}

	public void setAvisoCatastrofes(List<AvisoCatastrofe> avisoCatastrofes) {
		this.avisoCatastrofes = avisoCatastrofes;
	}

	public AvisoCatastrofe addAvisoCatastrofe(AvisoCatastrofe avisoCatastrofe) {
		getAvisoCatastrofes().add(avisoCatastrofe);
		avisoCatastrofe.setUsuario(this);

		return avisoCatastrofe;
	}

	public AvisoCatastrofe removeAvisoCatastrofe(AvisoCatastrofe avisoCatastrofe) {
		getAvisoCatastrofes().remove(avisoCatastrofe);
		avisoCatastrofe.setUsuario(null);

		return avisoCatastrofe;
	}

	public List<Desaparecido> getDesaparecidos() {
		return this.desaparecidos;
	}

	public void setDesaparecidos(List<Desaparecido> desaparecidos) {
		this.desaparecidos = desaparecidos;
	}

	public Desaparecido addDesaparecido(Desaparecido desaparecido) {
		getDesaparecidos().add(desaparecido);
		desaparecido.setUsuario(this);

		return desaparecido;
	}

	public Desaparecido removeDesaparecido(Desaparecido desaparecido) {
		getDesaparecidos().remove(desaparecido);
		desaparecido.setUsuario(null);

		return desaparecido;
	}	
	
	public List<PedidoAyuda> getPedidoAyudas() {
		return this.pedidoAyudas;
	}

	public void setPedidoAyudas(List<PedidoAyuda> pedidoAyudas) {
		this.pedidoAyudas = pedidoAyudas;
	}

	public PedidoAyuda addPedidoAyuda(PedidoAyuda pedidoAyuda) {
		getPedidoAyudas().add(pedidoAyuda);
		pedidoAyuda.setUsuario(this);

		return pedidoAyuda;
	}

	public PedidoAyuda removePedidoAyuda(PedidoAyuda pedidoAyuda) {
		getPedidoAyudas().remove(pedidoAyuda);
		pedidoAyuda.setUsuario(null);

		return pedidoAyuda;
	}	
	
}