package com.sica.entities;

import java.io.Serializable;

import javax.persistence.*;

import java.util.List;


/**
 * The persistent class for the ong database table.
 * 
 */
@Entity
@NamedQueries({
	@NamedQuery(name="Ong.findAll", 
				query="SELECT o FROM Ong o"),
	
	@NamedQuery(name="Ong.buscarPorEmail", 
		query="SELECT u FROM Ong u WHERE u.email=:email")
})
public class Ong implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="sic_sec_ong")
    @SequenceGenerator(name="sic_sec_ong", sequenceName = "sec_ong", allocationSize=1)
	private Long id;

	private String direccion;

	private String email;

	private String nombre;

	@Column(name="sitio_web")
	private String sitioWeb;

	private String telefono;

	//bi-directional many-to-one association to Donacion
	@OneToMany(mappedBy="ong")
	private List<Donacion> donacions;

	//bi-directional many-to-many association to EventoCatastrofe
	@ManyToMany(mappedBy="ongs")
	private List<EventoCatastrofe> eventoCatastrofes;

	public Ong() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDireccion() {
		return this.direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getSitioWeb() {
		return this.sitioWeb;
	}

	public void setSitioWeb(String sitioWeb) {
		this.sitioWeb = sitioWeb;
	}

	public String getTelefono() {
		return this.telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public List<Donacion> getDonacions() {
		return this.donacions;
	}

	public void setDonacions(List<Donacion> donacions) {
		this.donacions = donacions;
	}

	public Donacion addDonacion(Donacion donacion) {
		getDonacions().add(donacion);
		donacion.setOng(this);

		return donacion;
	}

	public Donacion removeDonacion(Donacion donacion) {
		getDonacions().remove(donacion);
		donacion.setOng(null);

		return donacion;
	}

	public List<EventoCatastrofe> getEventoCatastrofes() {
		return this.eventoCatastrofes;
	}

	public void setEventoCatastrofes(List<EventoCatastrofe> eventoCatastrofes) {
		this.eventoCatastrofes = eventoCatastrofes;
	}

}