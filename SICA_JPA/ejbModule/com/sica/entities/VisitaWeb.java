package com.sica.entities;

import java.io.Serializable;

import javax.persistence.*;

import java.util.Date;


/**
 * The persistent class for the visita_web database table.
 * 
 */
@Entity
@Table(name="visita_web")
@NamedQuery(name="VisitaWeb.findAll", query="SELECT v FROM VisitaWeb v")
public class VisitaWeb implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="sic_sec_vwe")
    @SequenceGenerator(name="sic_sec_vwe", sequenceName = "sec_vwe", allocationSize=1)
	private Long id;

	@Temporal(TemporalType.DATE)
	private Date fecha;

	@Column(name="id_sesion")
	private String idSesion;

	public VisitaWeb() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getFecha() {
		return this.fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public String getIdSesion() {
		return this.idSesion;
	}

	public void setIdSesion(String idSesion) {
		this.idSesion = idSesion;
	}

}