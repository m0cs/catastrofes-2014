package com.sica.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the aviso_catastrofe database table.
 * 
 */
@Entity
@Table(name="aviso_catastrofe")
@NamedQuery(name="AvisoCatastrofe.findAll", query="SELECT a FROM AvisoCatastrofe a")
public class AvisoCatastrofe implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;

	private String descripcion;

	private String ubicacion;


	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_tipo_catastrofe")
	private TipoCatastrofe tipoCatastrofe;


	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_usuario")
	private Usuario usuario;

	public AvisoCatastrofe() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getUbicacion() {
		return this.ubicacion;
	}

	public void setUbicacion(String ubicacion) {
		this.ubicacion = ubicacion;
	}

	public TipoCatastrofe getTipoCatastrofe() {
		return this.tipoCatastrofe;
	}

	public void setTipoCatastrofe(TipoCatastrofe tipoCatastrofe) {
		this.tipoCatastrofe = tipoCatastrofe;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

}