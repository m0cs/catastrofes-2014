package com.sica.entities;

import java.io.Serializable;

import javax.persistence.*;

import java.util.Date;
import java.util.List;


/**
 * The persistent class for the evento_catastrofe database table.
 * 
 */
@Entity
@Table(name="evento_catastrofe")
@NamedQuery(name="EventoCatastrofe.findAll", query="SELECT e FROM EventoCatastrofe e order by e.id asc")
public class EventoCatastrofe implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="sic_sica_eca")
    @SequenceGenerator(name="sic_sica_eca", sequenceName = "sica_eca", allocationSize=1)
	private Long id;

	private String descripcion;

	private Integer distancia;//radio

	private String estado;

	private String magnitud;

	private String nombre;

	private String ubicacion;
	
	private String area;
	
	@Column(name="fuente_twitter")
	private String fuenteTwitter;
	
	@Temporal(TemporalType.DATE)
	@Column(name="fecha_comienzo")
	private Date fechaComienzo;
	
	@Temporal(TemporalType.DATE)
	@Column(name="fecha_fin")
	private Date fechaFin;	
	

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_tipo_catastrofe")
	private TipoCatastrofe tipoCatastrofe;	
	
    @OneToMany(mappedBy="eventoCatastrofe")
	private List<PlanRescatista> planesRescatistas;

	//bi-directional many-to-many association to Ong
	@ManyToMany
	@JoinTable(
		name="evento_catastrofe_ong"
		, joinColumns={
			@JoinColumn(name="id_evento_catastrofe")
			}
		, inverseJoinColumns={
			@JoinColumn(name="id_ong")
			}
		)
	private List<Ong> ongs;

	@OneToMany
	@JoinTable(
		name="evento_catastrofe_rss"
		, joinColumns={
			@JoinColumn(name="id_evento_catastrofe", referencedColumnName="id")
			}
		, inverseJoinColumns={
			@JoinColumn(name="id_fuente_rss", referencedColumnName="id")
			}
		)
	private List<FuenteRss> fuenteRsses;

	
	@OneToMany
	@JoinTable(
		name="evento_catastrofe_img"
		, joinColumns={
			@JoinColumn(name="id_evento_catastrofe", referencedColumnName="id")
			}
		, inverseJoinColumns={
			@JoinColumn(name="id_imagen", referencedColumnName="id")
			}
		)
	private List<Imagen> imagenes;	

	public EventoCatastrofe() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Integer getDistancia() {
		return this.distancia;
	}

	public void setDistancia(Integer distancia) {
		this.distancia = distancia;
	}

	public String getEstado() {
		return this.estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getMagnitud() {
		return this.magnitud;
	}

	public void setMagnitud(String magnitud) {
		this.magnitud = magnitud;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getUbicacion() {
		return this.ubicacion;
	}

	public void setUbicacion(String ubicacion) {
		this.ubicacion = ubicacion;
	}

	public List<Ong> getOngs() {
		return this.ongs;
	}

	public void setOngs(List<Ong> ongs) {
		this.ongs = ongs;
	}

	public TipoCatastrofe getTipoCatastrofe() {
		return this.tipoCatastrofe;
	}

	public void setTipoCatastrofe(TipoCatastrofe tipoCatastrofe) {
		this.tipoCatastrofe = tipoCatastrofe;
	}

	public List<FuenteRss> getFuenteRsses() {
		return this.fuenteRsses;
	}

	public void setFuenteRsses(List<FuenteRss> fuenteRsses) {
		this.fuenteRsses = fuenteRsses;
	}

	public Date getFechaComienzo() {
		return fechaComienzo;
	}

	public void setFechaComienzo(Date fechaComienzo) {
		this.fechaComienzo = fechaComienzo;
	}

	public Date getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}

	public List<PlanRescatista> getPlanesRescatistas() {
		return planesRescatistas;
	}

	public void setPlanesRescatistas(List<PlanRescatista> planesRescatistas) {
		this.planesRescatistas = planesRescatistas;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getFuenteTwitter() {
		return fuenteTwitter;
	}

	public void setFuenteTwitter(String fuenteTwitter) {
		this.fuenteTwitter = fuenteTwitter;
	}

	public List<Imagen> getImagenes() {
		return imagenes;
	}

	public void setImagenes(List<Imagen> imagenes) {
		this.imagenes = imagenes;
	}
	
	

}