package com.sica.entities;

import java.io.Serializable;

import javax.persistence.*;

import java.util.Date;
import java.util.List;


/**
 * The persistent class for the pedido_ayuda database table.
 * 
 */
@Entity
@Table(name="pedido_ayuda")
@NamedQuery(name="PedidoAyuda.findAll", query="SELECT p FROM PedidoAyuda p")
public class PedidoAyuda implements Serializable {
	private static final long serialVersionUID = 1L;

	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="sic_sec_pay")
    @SequenceGenerator(name="sic_sec_pay", sequenceName = "sec_pay", allocationSize=1)
	private Long id;

	private String descripcion;

	@Temporal(TemporalType.DATE)
	private Date fecha;

	@Column(name="is_habilitado")
	private Boolean isHabilitado;

	@Column(name="lat_long")
	private String latLong;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_estado_usuario")
	private EstadoUsuario estadoUsuario;

	//bi-directional many-to-one association to Recurso
	@OneToMany(mappedBy="pedidoAyuda")
	private List<Recurso> recursos;

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="id_usuario")
	private Usuario usuario;

	public PedidoAyuda() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Date getFecha() {
		return this.fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public Boolean getIsHabilitado() {
		return this.isHabilitado;
	}

	public void setIsHabilitado(Boolean isHabilitado) {
		this.isHabilitado = isHabilitado;
	}

	public String getLatLong() {
		return this.latLong;
	}

	public void setLatLong(String latLong) {
		this.latLong = latLong;
	}

	public EstadoUsuario getEstadoUsuario() {
		return this.estadoUsuario;
	}

	public void setEstadoUsuario(EstadoUsuario estadoUsuario) {
		this.estadoUsuario = estadoUsuario;
	}

	public List<Recurso> getRecursos() {
		return this.recursos;
	}

	public void setRecursos(List<Recurso> recursos) {
		this.recursos = recursos;
	}

	public Recurso addRecurso(Recurso recurso) {
		getRecursos().add(recurso);
		recurso.setPedidoAyuda(this);

		return recurso;
	}

	public Recurso removeRecurso(Recurso recurso) {
		getRecursos().remove(recurso);
		recurso.setPedidoAyuda(null);

		return recurso;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

}