package com.sica.entities;

import java.io.Serializable;

import javax.persistence.*;


/**
 * The persistent class for the articulo database table.
 * 
 */
@Entity
@NamedQuery(name="Articulo.findAll", query="SELECT a FROM Articulo a")
public class Articulo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="sic_sec_art")
    @SequenceGenerator(name="sic_sec_art", sequenceName = "sec_art", allocationSize=1)
	private Long id;

	private String nombre;
	
	private Integer cantidad;

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="id_don_bien")
	private Bien donBien;

	public Articulo() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Bien getDonBien() {
		return this.donBien;
	}

	public void setDonBien(Bien donBien) {
		this.donBien = donBien;
	}

	public Integer getCantidad() {
		return cantidad;
	}

	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}
	
	

}