package com.sica.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the don_servicio database table.
 * 
 */
@Entity
@Table(name="don_servicio")

@NamedQuery(name="Servicio.findAll", query="SELECT s FROM Servicio s")

public class Servicio extends Donacion implements Serializable {
	private static final long serialVersionUID = 1L;

	private Integer cantidad;

	private String profesion;
	
	public Servicio() {
	}

	public Integer getCantidad() {
		return this.cantidad;
	}

	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}

	public String getProfesion() {
		return profesion;
	}

	public void setProfesion(String profesion) {
		this.profesion = profesion;
	}
	
	

}