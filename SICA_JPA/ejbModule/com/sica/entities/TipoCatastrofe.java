package com.sica.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


/**
 * The persistent class for the tipo_catastrofe database table.
 * 
 */
@Entity
@Table(name="tipo_catastrofe")
@NamedQuery(name="TipoCatastrofe.findAll", query="SELECT t FROM TipoCatastrofe t")
public class TipoCatastrofe implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="sic_sec_tca")
    @SequenceGenerator(name="sic_sec_tca", sequenceName = "sec_tca", allocationSize=1)
	private Long id;

	private byte[] estilo;

	private String nombre;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_logo_fk")
	private Imagen imagen;

	public TipoCatastrofe() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public byte[] getEstilo() {
		return this.estilo;
	}

	public void setEstilo(byte[] estilo) {
		this.estilo = estilo;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Imagen getImagen() {
		return this.imagen;
	}

	public void setImagen(Imagen imagen) {
		this.imagen = imagen;
	}

}