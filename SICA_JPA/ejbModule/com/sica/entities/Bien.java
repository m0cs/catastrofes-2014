package com.sica.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;


/**
 * The persistent class for the don_bien database table.
 * 
 */
@Entity
@Table(name="don_bien")

@NamedQuery(name="Bien.findAll", query="SELECT b FROM Bien b")

public class Bien extends Donacion implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="dir_origen")
	private String dirOrigen;

	private Boolean retirar;
	
	@OneToMany(mappedBy="donBien", fetch=FetchType.LAZY, cascade=CascadeType.PERSIST)
	private List<Articulo> articulos;

	public Bien() {
	}

	public String getDirOrigen() {
		return this.dirOrigen;
	}

	public void setDirOrigen(String dirOrigen) {
		this.dirOrigen = dirOrigen;
	}

	public Boolean getRetirar() {
		return this.retirar;
	}

	public void setRetirar(Boolean retirar) {
		this.retirar = retirar;
	}

	public List<Articulo> getArticulos() {
		return articulos;
	}

	public void setArticulos(List<Articulo> articulos) {
		this.articulos = articulos;
	}

	

}