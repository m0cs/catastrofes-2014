package com.sica.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the plan_rescatista database table.
 * 
 */
@Entity
@Table(name="plan_rescatista")
@NamedQuery(name="PlanRescatista.findAll", query="SELECT p FROM PlanRescatista p")
public class PlanRescatista implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="sic_sec_pre")
    @SequenceGenerator(name="sic_sec_pre", sequenceName = "sec_pre", allocationSize=1)
	private Long id;

	@Temporal(TemporalType.DATE)
	private Date fecha;

	@Column(name="is_completado")
	private Boolean isCompletado;

	private Integer radio;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="id_rescatista")
	private Rescatista rescatista;
	
	@OneToMany
	@JoinTable(
		name="plan_rescatista_eme"
		, joinColumns={
			@JoinColumn(name="id_plan_rescatista", referencedColumnName="id")
			}
		, inverseJoinColumns={
			@JoinColumn(name="id_plan_emergencia", referencedColumnName="id")
			}
		)
	private List<PlanEmergencia> planesEmergencia;	
	
	@OneToMany
	@JoinTable(
		name="plan_rescatista_rie"
		, joinColumns={
			@JoinColumn(name="id_plan_rescatista", referencedColumnName="id")
			}
		, inverseJoinColumns={
			@JoinColumn(name="id_plan_riesgo", referencedColumnName="id")
			}
		)
	private List<PlanRiesgo> planesRiesgo;		
	
	@ManyToOne
	@JoinColumn(name="id_evento_catastrofe")	
	private EventoCatastrofe eventoCatastrofe;

	public PlanRescatista() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getFecha() {
		return this.fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public Boolean getIsCompletado() {
		return this.isCompletado;
	}

	public void setIsCompletado(Boolean isCompletado) {
		this.isCompletado = isCompletado;
	}

	public Integer getRadio() {
		return this.radio;
	}

	public void setRadio(Integer radio) {
		this.radio = radio;
	}

	public Rescatista getRescatista() {
		return rescatista;
	}

	public void setRescatista(Rescatista rescatista) {
		this.rescatista = rescatista;
	}

	public EventoCatastrofe getEventoCatastrofe() {
		return eventoCatastrofe;
	}

	public void setEventoCatastrofe(EventoCatastrofe eventoCatastrofe) {
		this.eventoCatastrofe = eventoCatastrofe;
	}

	public List<PlanEmergencia> getPlanesEmergencia() {
		return planesEmergencia;
	}

	public void setPlanesEmergencia(List<PlanEmergencia> planesEmergencia) {
		this.planesEmergencia = planesEmergencia;
	}

	public List<PlanRiesgo> getPlanesRiesgo() {
		return planesRiesgo;
	}

	public void setPlanesRiesgo(List<PlanRiesgo> planesRiesgo) {
		this.planesRiesgo = planesRiesgo;
	}
	
}