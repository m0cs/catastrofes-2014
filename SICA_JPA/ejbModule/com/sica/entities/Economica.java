package com.sica.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the don_economica database table.
 * 
 */
@Entity
@Table(name="don_economica")

@NamedQuery(name="Economica.findAll", query="SELECT e FROM Economica e")

public class Economica extends Donacion implements Serializable {
	private static final long serialVersionUID = 1L;

	private String moneda;

	private Integer monto;

	public Economica() {
	}

	public String getMoneda() {
		return this.moneda;
	}

	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}

	public Integer getMonto() {
		return this.monto;
	}

	public void setMonto(Integer monto) {
		this.monto = monto;
	}

}