package com.sica.entities;

import java.io.Serializable;

import javax.persistence.*;


/**
 * The persistent class for the recurso database table.
 * 
 */
@Entity
@NamedQuery(name="Recurso.findAll", query="SELECT r FROM Recurso r")
public class Recurso implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="sic_sec_rec")
    @SequenceGenerator(name="sic_sec_rec", sequenceName = "sec_rec", allocationSize=1)
	private Long id;

	private Integer cantidad;

	private String nombre;

	private String unidad;

	//bi-directional many-to-one association to PedidoAyuda
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_pedido_ayuda")
	private PedidoAyuda pedidoAyuda;

	public Recurso() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getCantidad() {
		return this.cantidad;
	}

	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}

	public String getUnidad() {
		return this.unidad;
	}

	public void setUnidad(String unidad) {
		this.unidad = unidad;
	}

	public PedidoAyuda getPedidoAyuda() {
		return this.pedidoAyuda;
	}

	public void setPedidoAyuda(PedidoAyuda pedidoAyuda) {
		this.pedidoAyuda = pedidoAyuda;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	

}