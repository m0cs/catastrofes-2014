package com.sica.utiles;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

public class Utiles_JPA {

	public static final String UNIT_PERSISTENCE_NAME="SICA";
	
	public static final int ADMINISTRADOR = 1;
	public static final int RESCATISTA = 2;
	public static final int USUARIO = 3;
	
	public static final int SERVICIO = 1;
	public static final int BIEN = 2;
	public static final int ECONOMICA = 3;
	
	
	public static final int PLAN_RIESGO = 1;
	public static final int PLAN_EMERGENCIA = 2;
	
    public static boolean isNullOrEmpty(String str){
    	return (str == null || str.isEmpty());
    }
    
    public static boolean isListNullOrEmpty(List<String> str){
    	return (str == null || str.isEmpty());
    }  
    
    public static boolean isListNullOrEmptyDouble(List<Double> str){
    	return (str == null || str.isEmpty());
    }      
    
    public static boolean isNullOrCero(Long lng){
    	return (lng == null || lng.equals(0L));
    }      
    
	/**
	 * Formato de fecha min. Dia, mes y año (dd/MM/yyyy)
	 */
	public static final DateFormat DATE_FORMAT_MIN_INVERSE = new SimpleDateFormat("yyyy-MM-dd");
	
	/**
	 * Formato de fecha full. Dia, mes, año, hora, minutos y segundos (dd/MM/yyyy HH:mm:ss)
	 */
	public static final DateFormat DATE_FORMAT_FULL_INVERSE = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");	
	
	/**
	 * Formato de fecha min. Dia, mes y año (dd/MM/yyyy)
	 */
	public static final DateFormat DATE_FORMAT_MIN = new SimpleDateFormat("dd-MM-yyyy");
	
	/**
	 * Formato de fecha full. Dia, mes, año, hora, minutos y segundos (dd/MM/yyyy HH:mm:ss)
	 */
	public static final DateFormat DATE_FORMAT_FULL = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");		
	
}
