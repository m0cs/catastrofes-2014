package com.sica.utiles;

import java.util.List;

public class FiltroBusquedaDesaparecido {

	private String apellido;
	private String nombre;
	
	private List<String> etnias;
	private List<String> colorPiel;
	private List<String> colorOjos;
	
	private List<String> edad;
	private List<String> altura;
	
	private String fechaInicio;
	private String fechaFin;
	private String emailUsuario;
	private String detalles;
	
	
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public List<String> getEtnias() {
		return etnias;
	}
	public void setEtnias(List<String> etnias) {
		this.etnias = etnias;
	}
	public List<String> getColorPiel() {
		return colorPiel;
	}
	public void setColorPiel(List<String> colorPiel) {
		this.colorPiel = colorPiel;
	}
	public List<String> getColorOjos() {
		return colorOjos;
	}
	public void setColorOjos(List<String> colorOjos) {
		this.colorOjos = colorOjos;
	}
	public String getEmailUsuario() {
		return emailUsuario;
	}
	public void setEmailUsuario(String emailUsuario) {
		this.emailUsuario = emailUsuario;
	}
	public List<String> getEdad() {
		return edad;
	}
	public void setEdad(List<String> edad) {
		this.edad = edad;
	}
	public List<String> getAltura() {
		return altura;
	}
	public void setAltura(List<String> altura) {
		this.altura = altura;
	}
	public String getFechaInicio() {
		return fechaInicio;
	}
	public void setFechaInicio(String fechaInicio) {
		this.fechaInicio = fechaInicio;
	}
	public String getFechaFin() {
		return fechaFin;
	}
	public void setFechaFin(String fechaFin) {
		this.fechaFin = fechaFin;
	}	
	public String getDetalles() {
		return detalles;
	}
	public void setDetalles(String detalles) {
		this.detalles = detalles;
	}
	public boolean containsFiels(){
		if ( !Utiles_JPA.isNullOrEmpty(nombre) || 
				!Utiles_JPA.isNullOrEmpty(apellido) || 
					!Utiles_JPA.isNullOrEmpty(fechaInicio) || 
						!Utiles_JPA.isNullOrEmpty(fechaFin) || 
							!Utiles_JPA.isNullOrEmpty(emailUsuario) ||
								!Utiles_JPA.isListNullOrEmpty(altura) ||
									!Utiles_JPA.isListNullOrEmpty(colorOjos) ||
										!Utiles_JPA.isListNullOrEmpty(colorPiel) ||
											!Utiles_JPA.isListNullOrEmpty(edad) ||
												!Utiles_JPA.isListNullOrEmpty(etnias)){
			return true;
		}
		else return false;
	}
}
